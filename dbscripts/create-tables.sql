create extension postgis;

CREATE SEQUENCE global_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1
  CYCLE;

CREATE SEQUENCE order_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1
  CYCLE;

CREATE TABLE customer
(
    customer_id numeric NOT NULL DEFAULT nextval('global_seq'::regclass),
    customer_name TEXT,
    customer_email TEXT,
    customer_phone TEXT,
    customer_country TEXT,
    customer_timezone TEXT, --Indonesia/Jakarta,Indonesia/Jayapura
    customer_timeabbreviation TEXT, --WIB,WITA,WIT
    customer_gmtoffset NUMERIC, -- +7,0,-7
    customer_company TEXT,
    customer_logo TEXT,
    customer_creationdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    customer_lastupdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    customer_createby TEXT,
    customer_lastupdateby TEXT,
    customer_id_referenced NUMERIC
);
ALTER TABLE customer
  ADD CONSTRAINT customer_pkey PRIMARY KEY(customer_id);
COMMENT ON COLUMN customer.customer_id_referenced IS 'Customer ini direferensikan oleh siapa';

CREATE TABLE customer_members
(
    customer_id NUMERIC,
    custmember_customer_id NUMERIC,
    custmember_role_id NUMERIC
);
CREATE TABLE customer_map_config
(
  customer_id numeric,
  cmc_iconheading boolean NOT NULL DEFAULT false
)
WITH (
  OIDS=FALSE
);
CREATE TABLE incoming_ais
(
    received_on TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL,
    zona_id INTEGER,
    mmsi INTEGER,
    arp_id INTEGER,
    messagetype INTEGER,
    slottimeout INTEGER,
    iais_timestamp INTEGER,
    dimensiontobow NUMERIC,
    navigationalstatus INTEGER,
    trueheading NUMERIC,
    positionaccuracy INTEGER,
    repeatindicator INTEGER,
    longitude NUMERIC,
    courseoverground NUMERIC,
    maneuverindicator INTEGER,
    navstatusdesc TEXT,
    zonename TEXT,
    dimensiontostern NUMERIC,
    destination TEXT,
    dimensiontostarboard NUMERIC,
    spare INTEGER,
    syncstate INTEGER,
    timestampreceiver NUMERIC(20),
    submessage TEXT,
    dimensiontoport NUMERIC,
    draught NUMERIC,
    rateofturn NUMERIC,
    speedoverground NUMERIC,
    latitude NUMERIC,
    raimflag NUMERIC,
    etamonth INTEGER DEFAULT 0 NOT NULL,
    etaday INTEGER DEFAULT 0 NOT NULL,
    etahour INTEGER DEFAULT 0 NOT NULL,
    etaminute INTEGER DEFAULT 0 NOT NULL
)WITH (
  OIDS=TRUE
);

CREATE TABLE orders
(
    order_id numeric NOT NULL DEFAULT nextval('order_seq'::regclass),
    order_date TIMESTAMP WITH TIME ZONE,
    order_po_number TEXT,
    order_creationdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    order_lastupdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    order_createby TEXT,
    order_lastupdateby TEXT,
    customer_id INTEGER NOT NULL
);
CREATE TABLE order_details
(
    orddtl_id numeric NOT NULL DEFAULT nextval('order_seq'::regclass),
    product_id NUMERIC NOT NULL,
    orddtl_quantity INTEGER,
    orddtl_unitprice NUMERIC,
    orddtl_currency TEXT DEFAULT 'RP'::text NOT NULL,
    customer_id NUMERIC NOT NULL,
    orddtl_creationdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    orddtl_lastupdate TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT now(),
    orddtl_createby TEXT,
    orddtl_discount NUMERIC,
    orddtl_lastupdateby TEXT,
    order_id NUMERIC NOT NULL,
    orddtl_contract_startdate TIMESTAMP WITH TIME ZONE,
    orddtl_contract_enddate TIMESTAMP WITH TIME ZONE
);
CREATE TABLE order_details_status
(
    orddtl_id NUMERIC,
    orddtl_status TEXT,
    orddtl_status_date TIMESTAMP WITH TIME ZONE
);
COMMENT ON TABLE order_details_status
  IS 'Contoh status : 
- Pemesanan
- Persiapan engine ais satelit
- Aplikasi siap
- Konfirmasi user aplikasi siap
- Menunggu payment
- User konfirm payment
- Validasi Payment
- Payment OK/Not OK
- Open akses user
- Close akses user
- User batal pemesanan (jika belum terjadi pembayaran)';

CREATE TABLE ais_datasource
(
  ad_id numeric NOT NULL DEFAULT nextval('global_seq'::regclass),
  ad_url text,
  ad_vendorname text,
  ad_urlusername text,
  ad_urlpassword text
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE ais_datasource
  IS 'Ais receiver read url ini. Hapus jika ingin deactivate.';
ALTER TABLE ais_datasource
  ADD CONSTRAINT ais_datasource_pkey PRIMARY KEY(ad_id);


CREATE TABLE order_ships
(
   orddtl_id numeric NOT NULL,
    ship_mmsi INTEGER,
    ship_imo INTEGER,
    ship_callsign TEXT,
    ship_name TEXT
)
WITH (
  OIDS=TRUE
);
CREATE TABLE order_bill
(
  order_id numeric NOT NULL,
  ordbill_id numeric NOT NULL DEFAULT nextval('order_seq'::regclass),
  ordbill_amount numeric,
  ordbill_invoice_date timestamp with time zone,
  ordbill_currency text, -- RP,USD,etc
  ordbill_discount numeric
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN order_bill.ordbill_currency IS 'RP,USD,etc';

ALTER TABLE order_bill
  ADD CONSTRAINT order_bill_pkey PRIMARY KEY(ordbill_id);

CREATE TABLE payment
(
  pay_id numeric NOT NULL DEFAULT nextval('order_seq'::regclass),
  ordbill_id numeric NOT NULL,
  pay_amount numeric NOT NULL,
  pay_currency text, -- RP,USD,etc
  pay_method text, -- DEBET,CreditCard,etc
  pay_date timestamp with time zone,
  pay_creationdate timestamp with time zone NOT NULL DEFAULT now(),
  pay_lastupdate timestamp with time zone NOT NULL DEFAULT now(),
  pay_createby text,
  pay_lastupdateby text,
  CONSTRAINT payment_pk PRIMARY KEY (pay_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE payment
  OWNER TO postgres;
COMMENT ON COLUMN payment.pay_currency IS 'RP,USD,etc';
COMMENT ON COLUMN payment.pay_method IS 'DEBET,CreditCard,etc';

ALTER TABLE payment
  ADD CONSTRAINT payment_pk PRIMARY KEY(pay_id);


CREATE TABLE product
(
    product_id numeric NOT NULL DEFAULT nextval('global_seq'::regclass),
    product_name TEXT,
    product_creationdate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    product_lastupdate TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
    product_createby TEXT,
    product_lastupdateby TEXT,
    product_description TEXT,
    pp_id numeric,
    pp_price numeric,
   CONSTRAINT product_pkey PRIMARY KEY (product_id)
);

CREATE TABLE product_price
(
  pp_id numeric NOT NULL DEFAULT nextval('global_seq'::regclass),
  pp_price numeric,
  product_id numeric,
  pp_currency text, -- RP,USD,etc
  pp_creationdate timestamp with time zone  NOT NULL DEFAULT now(),
  pp_lastupdate timestamp with time zone  NOT NULL DEFAULT now(),
  pp_createby text,
  pp_lastupdateby text,
  CONSTRAINT pp_pkey PRIMARY KEY (pp_id)
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN product_price.pp_currency IS 'RP,USD,etc';


CREATE TABLE ship_info
(
  received_on timestamp with time zone NOT NULL DEFAULT now(),
  mmsi integer,
  etaminute integer,
  repeatindicator integer,
  etamonth integer,
  dte integer,
  spare integer,
  dimensiontostern numeric,
  positionfixtype integer,
  messagetype integer,
  draught integer,
  vesselname text,
  destination text,
  etaday integer,
  shiptypedesc text,
  dimensiontoport numeric,
  timestampreceiver numeric(20,0),
  imonumber integer,
  dimensiontobow numeric,
  etahour integer,
  aisversion integer,
  callsign text,
  dimensiontostarboard numeric,
  shiptype integer,
  grt integer,
  enginehp integer,
  enginediesel boolean,
  si_creationdate timestamp with time zone  NOT NULL DEFAULT now(),
  si_lastupdate timestamp with time zone  NOT NULL DEFAULT now(),
  si_createby text,
  si_lastupdateby text,
  si_wide numeric,
  si_length numeric
)
WITH (
  OIDS=TRUE,
  autovacuum_enabled=true
);


CREATE INDEX ship_info_mmsi_idx
  ON ship_info
  USING btree
  (mmsi);

CREATE INDEX ship_info_received_on
  ON ship_info
  USING btree
  (received_on);


CREATE INDEX customer_name_idx ON customer (customer_name);
CREATE INDEX incoming_ais_received_on_desc_idx ON incoming_ais (received_on, customer_id);
CREATE INDEX inaisreconmmsi_idx ON incoming_ais (mmsi, received_on, customer_id);

CREATE TABLE ship_type (
    id_shiptype integer NOT NULL,
    description text NOT NULL
)
WITH (autovacuum_enabled=true);

INSERT INTO ship_type VALUES (0, 'Not available (default)');
INSERT INTO ship_type VALUES (1, 'Reserved for future use');
INSERT INTO ship_type VALUES (2, 'Reserved for future use');
INSERT INTO ship_type VALUES (3, 'Reserved for future use');
INSERT INTO ship_type VALUES (4, 'Reserved for future use');
INSERT INTO ship_type VALUES (5, 'Reserved for future use');
INSERT INTO ship_type VALUES (6, 'Reserved for future use');
INSERT INTO ship_type VALUES (7, 'Reserved for future use');
INSERT INTO ship_type VALUES (8, 'Reserved for future use');
INSERT INTO ship_type VALUES (9, 'Reserved for future use');
INSERT INTO ship_type VALUES (10, 'Reserved for future use');
INSERT INTO ship_type VALUES (11, 'Reserved for future use');
INSERT INTO ship_type VALUES (12, 'Reserved for future use');
INSERT INTO ship_type VALUES (13, 'Reserved for future use');
INSERT INTO ship_type VALUES (14, 'Reserved for future use');
INSERT INTO ship_type VALUES (15, 'Reserved for future use');
INSERT INTO ship_type VALUES (16, 'Reserved for future use');
INSERT INTO ship_type VALUES (17, 'Reserved for future use');
INSERT INTO ship_type VALUES (18, 'Reserved for future use');
INSERT INTO ship_type VALUES (19, 'Reserved for future use');
INSERT INTO ship_type VALUES (20, 'Wing in ground (WIG), all ships of this type');
INSERT INTO ship_type VALUES (21, 'Wing in ground (WIG), Hazardous category A');
INSERT INTO ship_type VALUES (22, 'Wing in ground (WIG), Hazardous category B');
INSERT INTO ship_type VALUES (23, 'Wing in ground (WIG), Hazardous category C');
INSERT INTO ship_type VALUES (24, 'Wing in ground (WIG), Hazardous category D');
INSERT INTO ship_type VALUES (25, 'Wing in ground (WIG), Reserved for future use');
INSERT INTO ship_type VALUES (26, 'Wing in ground (WIG), Reserved for future use');
INSERT INTO ship_type VALUES (27, 'Wing in ground (WIG), Reserved for future use');
INSERT INTO ship_type VALUES (28, 'Wing in ground (WIG), Reserved for future use');
INSERT INTO ship_type VALUES (29, 'Wing in ground (WIG), Reserved for future use');
INSERT INTO ship_type VALUES (30, 'Fishing');
INSERT INTO ship_type VALUES (31, 'Towing');
INSERT INTO ship_type VALUES (32, 'Towing: length exceeds 200m or breadth exceeds 25m');
INSERT INTO ship_type VALUES (33, 'Dredging or underwater ops');
INSERT INTO ship_type VALUES (34, 'Diving ops');
INSERT INTO ship_type VALUES (35, 'Military ops');
INSERT INTO ship_type VALUES (36, 'Sailing');
INSERT INTO ship_type VALUES (37, 'Pleasure Craft');
INSERT INTO ship_type VALUES (38, 'Reserved');
INSERT INTO ship_type VALUES (39, 'Reserved');
INSERT INTO ship_type VALUES (40, 'High speed craft (HSC), all ships of this type');
INSERT INTO ship_type VALUES (41, 'High speed craft (HSC), Hazardous category A');
INSERT INTO ship_type VALUES (42, 'High speed craft (HSC), Hazardous category B');
INSERT INTO ship_type VALUES (43, 'High speed craft (HSC), Hazardous category C');
INSERT INTO ship_type VALUES (44, 'High speed craft (HSC), Hazardous category D');
INSERT INTO ship_type VALUES (45, 'High speed craft (HSC), Reserved for future use');
INSERT INTO ship_type VALUES (46, 'High speed craft (HSC), Reserved for future use');
INSERT INTO ship_type VALUES (47, 'High speed craft (HSC), Reserved for future use');
INSERT INTO ship_type VALUES (48, 'High speed craft (HSC), Reserved for future use');
INSERT INTO ship_type VALUES (49, 'High speed craft (HSC), No additional information');
INSERT INTO ship_type VALUES (50, 'Pilot Vessel');
INSERT INTO ship_type VALUES (51, 'Search and Rescue vessel');
INSERT INTO ship_type VALUES (52, 'Tug');
INSERT INTO ship_type VALUES (53, 'Port Tender');
INSERT INTO ship_type VALUES (54, 'Anti-pollution equipment');
INSERT INTO ship_type VALUES (55, 'Law Enforcement');
INSERT INTO ship_type VALUES (56, 'Spare - Local Vessel');
INSERT INTO ship_type VALUES (57, 'Spare - Local Vessel');
INSERT INTO ship_type VALUES (58, 'Medical Transport');
INSERT INTO ship_type VALUES (59, 'Ship according to RR Resolution No. 18');
INSERT INTO ship_type VALUES (60, 'Passenger, all ships of this type');
INSERT INTO ship_type VALUES (61, 'Passenger, Hazardous category A');
INSERT INTO ship_type VALUES (62, 'Passenger, Hazardous category B');
INSERT INTO ship_type VALUES (63, 'Passenger, Hazardous category C');
INSERT INTO ship_type VALUES (64, 'Passenger, Hazardous category D');
INSERT INTO ship_type VALUES (65, 'Passenger, Reserved for future use');
INSERT INTO ship_type VALUES (66, 'Passenger, Reserved for future use');
INSERT INTO ship_type VALUES (67, 'Passenger, Reserved for future use');
INSERT INTO ship_type VALUES (68, 'Passenger, Reserved for future use');
INSERT INTO ship_type VALUES (69, 'Passenger, No additional information');
INSERT INTO ship_type VALUES (70, 'Cargo, all ships of this type');
INSERT INTO ship_type VALUES (71, 'Cargo, Hazardous category A');
INSERT INTO ship_type VALUES (72, 'Cargo, Hazardous category B');
INSERT INTO ship_type VALUES (73, 'Cargo, Hazardous category C');
INSERT INTO ship_type VALUES (74, 'Cargo, Hazardous category D');
INSERT INTO ship_type VALUES (75, 'Cargo, Reserved for future use');
INSERT INTO ship_type VALUES (76, 'Cargo, Reserved for future use');
INSERT INTO ship_type VALUES (77, 'Cargo, Reserved for future use');
INSERT INTO ship_type VALUES (78, 'Cargo, Reserved for future use');
INSERT INTO ship_type VALUES (79, 'Cargo, No additional information');
INSERT INTO ship_type VALUES (80, 'Tanker, all ships of this type');
INSERT INTO ship_type VALUES (81, 'Tanker, Hazardous category A');
INSERT INTO ship_type VALUES (82, 'Tanker, Hazardous category B');
INSERT INTO ship_type VALUES (83, 'Tanker, Hazardous category C');
INSERT INTO ship_type VALUES (84, 'Tanker, Hazardous category D');
INSERT INTO ship_type VALUES (85, 'Tanker, Reserved for future use');
INSERT INTO ship_type VALUES (86, 'Tanker, Reserved for future use');
INSERT INTO ship_type VALUES (87, 'Tanker, Reserved for future use');
INSERT INTO ship_type VALUES (88, 'Tanker, Reserved for future use');
INSERT INTO ship_type VALUES (89, 'Tanker, No additional information');
INSERT INTO ship_type VALUES (90, 'Other Type, all ships of this type');
INSERT INTO ship_type VALUES (91, 'Other Type, Hazardous category A');
INSERT INTO ship_type VALUES (92, 'Other Type, Hazardous category B');
INSERT INTO ship_type VALUES (93, 'Other Type, Hazardous category C');
INSERT INTO ship_type VALUES (94, 'Other Type, Hazardous category D');
INSERT INTO ship_type VALUES (95, 'Other Type, Reserved for future use');
INSERT INTO ship_type VALUES (96, 'Other Type, Reserved for future use');
INSERT INTO ship_type VALUES (97, 'Other Type, Reserved for future use');
INSERT INTO ship_type VALUES (98, 'Other Type, Reserved for future use');
INSERT INTO ship_type VALUES (99, 'Other Type, no additional information');

CREATE TABLE navigation_status (
    id_navstatus integer NOT NULL,
    description text
)
WITH (autovacuum_enabled=true);


INSERT INTO navigation_status VALUES (0, 'Under way using engine');
INSERT INTO navigation_status VALUES (1, 'At anchor');
INSERT INTO navigation_status VALUES (2, 'Not under command');
INSERT INTO navigation_status VALUES (3, 'Restricted manoeuverability');
INSERT INTO navigation_status VALUES (4, 'Constrained by her draught');
INSERT INTO navigation_status VALUES (5, 'Moored');
INSERT INTO navigation_status VALUES (6, 'Aground');
INSERT INTO navigation_status VALUES (7, 'Engaged in Fishing');
INSERT INTO navigation_status VALUES (8, 'Under way sailing');
INSERT INTO navigation_status VALUES (9, 'Reserved for future amendment of Navigational Status for HSC');
INSERT INTO navigation_status VALUES (10, 'Reserved for future amendment of Navigational Status for WIG');
INSERT INTO navigation_status VALUES (11, 'Reserved for future use');
INSERT INTO navigation_status VALUES (12, 'Reserved for future use');
INSERT INTO navigation_status VALUES (13, 'Reserved for future use');
INSERT INTO navigation_status VALUES (14, 'Reserved for future use');
INSERT INTO navigation_status VALUES (15, 'Not defined (default)');

CREATE TABLE ship_mislocation
(
  mmsi integer,
  received_on timestamp without time zone,
  vesselname text
)
WITH (
  OIDS=TRUE
);

CREATE INDEX shipmisloc_idx
  ON ship_mislocation
  USING btree
  (mmsi, received_on);

CREATE TABLE konfigurasi
(
  konfig_key text NOT NULL,
  konfig_value text,
  konfig_keyasmandatory boolean,
  CONSTRAINT pk_konfigurasi PRIMARY KEY (konfig_key)
)
WITH (
  OIDS=FALSE
);

CREATE UNIQUE INDEX konfigurasi_pk
  ON konfigurasi
  USING btree
  (konfig_key COLLATE pg_catalog."default");


CREATE SCHEMA moresecure;


SET search_path = moresecure, pg_catalog;
CREATE SEQUENCE moresecure.seq_userrole
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;


SET default_tablespace = '';

SET default_with_oids = true;

CREATE TABLE elements (
    element_id integer DEFAULT nextval('seq_userrole'::regclass) NOT NULL,
    element_type text
);



CREATE TABLE permissions (
    role_id integer NOT NULL,
    element_id integer NOT NULL
);


CREATE TABLE roles (
    role_id integer DEFAULT nextval('seq_userrole'::regclass) NOT NULL,
    role_name text,
    role_description text
);


CREATE TABLE users (
    user_id integer DEFAULT nextval('seq_userrole'::regclass) NOT NULL,
    user_name text,
    last_login timestamp with time zone,
    password bytea,
    last_logout timestamp with time zone
);

CREATE TABLE usersinroles (
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE ONLY elements
    ADD CONSTRAINT elements_el_id PRIMARY KEY (element_id);


ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pk PRIMARY KEY (role_id, element_id);


ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_role_id_pk PRIMARY KEY (role_id);


ALTER TABLE ONLY users
    ADD CONSTRAINT users_user_id_pk PRIMARY KEY (user_id);


ALTER TABLE ONLY usersinroles
    ADD CONSTRAINT usersinroles_uir_pk PRIMARY KEY (user_id, role_id);


