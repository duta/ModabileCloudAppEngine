INSERT INTO product VALUES (2, 'Six months', '2016-06-09 16:01:02.847567+07', '2016-06-09 16:01:02.847567+07', NULL, NULL, 'Produk dengan masa berlaku 6 bulan', NULL, 1000000);
INSERT INTO product VALUES (3, 'One year', '2016-06-09 16:01:02.847567+07', '2016-06-09 16:01:02.847567+07', NULL, NULL, 'Produk dengan masa berlaku 1 tahun', NULL, 2000000);
INSERT INTO product VALUES (1, 'Trial', '2016-06-09 16:01:02.847567+07', '2016-06-09 16:01:02.847567+07', NULL, NULL, 'Trial product', 1, 0);

alter table product add column product_period int;