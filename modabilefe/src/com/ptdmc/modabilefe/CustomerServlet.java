package com.ptdmc.modabilefe;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.CustomerMembers;

@SuppressWarnings("serial")
public class CustomerServlet extends MasterServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		List<Customer> customers = null;
		try {
			String email = this.user.getEmail();
			customers = CallFactory.getInstance(req).getFactory().GetCustomer()
					.GetAllCustomerAndProduct();
			for (Customer customer : customers) {
				List<CustomerMembers> cms = CallFactory.getInstance(req).getFactory().GetCustomerMember().GetAllByCustomerID(customer.getCustomer_id());
				StringBuilder sb = new StringBuilder(customer.getCustomer_email());
				for (CustomerMembers customerMembers : cms) {
					sb.append("<br>&gt;"+CallFactory.getInstance(req).getFactory().GetUsers().GetUserByUserID(Integer.parseInt(customerMembers.getCustmember_customer_id())).getUser_name());
				}
				customer.setCustomer_email(sb.toString());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		req.setAttribute("useremail", this.user.getEmail());
		req.setAttribute("customers", customers);

		RequestDispatcher jsp = req
				.getRequestDispatcher("./views/customer/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
