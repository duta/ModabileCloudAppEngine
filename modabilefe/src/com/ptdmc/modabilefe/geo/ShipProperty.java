package com.ptdmc.modabilefe.geo;

import java.sql.Timestamp;

public class ShipProperty {
    /**
	 * @return the mMSI
	 */
	public int getMMSI() {
		return MMSI;
	}
	/**
	 * @param mMSI the mMSI to set
	 */
	public void setMMSI(int mMSI) {
		MMSI = mMSI;
	}
	/**
	 * @return the timeStampReceiver
	 */
	public double getTimeStampReceiver() {
		return TimeStampReceiver;
	}
	/**
	 * @param timeStampReceiver the timeStampReceiver to set
	 */
	public void setTimeStampReceiver(double timeStampReceiver) {
		TimeStampReceiver = timeStampReceiver;
	}
	/**
	 * @return the hDG
	 */
	public double getHDG() {
		return HDG;
	}
	/**
	 * @param hDG the hDG to set
	 */
	public void setHDG(double hDG) {
		HDG = hDG;
	}
	/**
	 * @return the sOG
	 */
	public double getSOG() {
		return SOG;
	}
	/**
	 * @param sOG the sOG to set
	 */
	public void setSOG(double sOG) {
		SOG = sOG;
	}
	/**
	 * @return the navStatus
	 */
	public int getNavStatus() {
		return NavStatus;
	}
	/**
	 * @param navStatus the navStatus to set
	 */
	public void setNavStatus(int navStatus) {
		NavStatus = navStatus;
	}
	/**
	 * @return the navStatusDescription
	 */
	public String getNavStatusDescription() {
		return NavStatusDescription;
	}
	/**
	 * @param navStatusDescription the navStatusDescription to set
	 */
	public void setNavStatusDescription(String navStatusDescription) {
		NavStatusDescription = navStatusDescription;
	}
	/**
	 * @return the rOT
	 */
	public String getROT() {
		return ROT;
	}
	/**
	 * @param rOT the rOT to set
	 */
	public void setROT(String rOT) {
		ROT = rOT;
	}
	/**
	 * @return the cOG
	 */
	public double getCOG() {
		return COG;
	}
	/**
	 * @param cOG the cOG to set
	 */
	public void setCOG(double cOG) {
		COG = cOG;
	}
	/**
	 * @return the shipName
	 */
	public String getShipName() {
		return ShipName;
	}
	/**
	 * @param shipName the shipName to set
	 */
	public void setShipName(String shipName) {
		ShipName = shipName;
	}
	/**
	 * @return the shipType
	 */
	public int getShipType() {
		return ShipType;
	}
	/**
	 * @param shipType the shipType to set
	 */
	public void setShipType(int shipType) {
		ShipType = shipType;
	}
	/**
	 * @return the shipTypeDescription
	 */
	public String getShipTypeDescription() {
		return ShipTypeDescription;
	}
	/**
	 * @param shipTypeDescription the shipTypeDescription to set
	 */
	public void setShipTypeDescription(String shipTypeDescription) {
		ShipTypeDescription = shipTypeDescription;
	}
	/**
	 * @return the callSign
	 */
	public String getCallSign() {
		return CallSign;
	}
	/**
	 * @param callSign the callSign to set
	 */
	public void setCallSign(String callSign) {
		CallSign = callSign;
	}
	/**
	 * @return the iMONumber
	 */
	public int getIMONumber() {
		return IMONumber;
	}
	/**
	 * @param iMONumber the iMONumber to set
	 */
	public void setIMONumber(int iMONumber) {
		IMONumber = iMONumber;
	}
	/**
	 * @return the toPort
	 */
	public double getToPort() {
		return ToPort;
	}
	/**
	 * @param toPort the toPort to set
	 */
	public void setToPort(double toPort) {
		ToPort = toPort;
	}
	/**
	 * @return the toStarBoard
	 */
	public double getToStarBoard() {
		return ToStarBoard;
	}
	/**
	 * @param toStarBoard the toStarBoard to set
	 */
	public void setToStarBoard(double toStarBoard) {
		ToStarBoard = toStarBoard;
	}
	/**
	 * @return the toBow
	 */
	public double getToBow() {
		return ToBow;
	}
	/**
	 * @param toBow the toBow to set
	 */
	public void setToBow(double toBow) {
		ToBow = toBow;
	}
	/**
	 * @return the toStern
	 */
	public double getToStern() {
		return ToStern;
	}
	/**
	 * @param toStern the toStern to set
	 */
	public void setToStern(double toStern) {
		ToStern = toStern;
	}
	/**
	 * @return the eTAMonth
	 */
	public double getETAMonth() {
		return ETAMonth;
	}
	/**
	 * @param eTAMonth the eTAMonth to set
	 */
	public void setETAMonth(double eTAMonth) {
		ETAMonth = eTAMonth;
	}
	/**
	 * @return the eTADay
	 */
	public double getETADay() {
		return ETADay;
	}
	/**
	 * @param eTADay the eTADay to set
	 */
	public void setETADay(double eTADay) {
		ETADay = eTADay;
	}
	/**
	 * @return the eTAHour
	 */
	public double getETAHour() {
		return ETAHour;
	}
	/**
	 * @param eTAHour the eTAHour to set
	 */
	public void setETAHour(double eTAHour) {
		ETAHour = eTAHour;
	}
	/**
	 * @return the eTAMinute
	 */
	public double getETAMinute() {
		return ETAMinute;
	}
	/**
	 * @param eTAMinute the eTAMinute to set
	 */
	public void setETAMinute(double eTAMinute) {
		ETAMinute = eTAMinute;
	}
	/**
	 * @return the draught
	 */
	public double getDraught() {
		return Draught;
	}
	/**
	 * @param draught the draught to set
	 */
	public void setDraught(double draught) {
		Draught = draught;
	}
	/**
	 * @return the destination
	 */
	public String getDestination() {
		return Destination;
	}
	/**
	 * @param destination the destination to set
	 */
	public void setDestination(String destination) {
		Destination = destination;
	}
	/**
	 * @return the grt
	 */
	public double getGrt() {
		return Grt;
	}
	/**
	 * @param grt the grt to set
	 */
	public void setGrt(double grt) {
		Grt = grt;
	}
	/**
	 * @return the zoneID
	 */
	public int getZoneID() {
		return ZoneID;
	}
	/**
	 * @param zoneID the zoneID to set
	 */
	public void setZoneID(int zoneID) {
		ZoneID = zoneID;
	}
	/**
	 * @return the zoneName
	 */
	public String getZoneName() {
		return ZoneName;
	}
	/**
	 * @param zoneName the zoneName to set
	 */
	public void setZoneName(String zoneName) {
		ZoneName = zoneName;
	}
	/**
	 * @return the stationID
	 */
	public int getStationID() {
		return StationID;
	}
	/**
	 * @param stationID the stationID to set
	 */
	public void setStationID(int stationID) {
		StationID = stationID;
	}
	/**
	 * @return the stationName
	 */
	public String getStationName() {
		return StationName;
	}
	/**
	 * @param stationName the stationName to set
	 */
	public void setStationName(String stationName) {
		StationName = stationName;
	}
	/**
	 * @return the dangerDist
	 */
	public int getDangerDist() {
		return DangerDist;
	}
	/**
	 * @param dangerDist the dangerDist to set
	 */
	public void setDangerDist(int dangerDist) {
		DangerDist = dangerDist;
	}
	/**
	 * @return the dangerHead
	 */
	public Boolean getDangerHead() {
		return DangerHead;
	}
	/**
	 * @param dangerHead the dangerHead to set
	 */
	public void setDangerHead(Boolean dangerHead) {
		DangerHead = dangerHead;
	}
	
	private int MMSI;
	private double  TimeStampReceiver;
    private double HDG;
    private double SOG;
    private int NavStatus;
    private String NavStatusDescription;
    private String ROT;
    private double COG;
    //type 5
    private String ShipName;
    private int ShipType;
    private String ShipTypeDescription;
    private String CallSign;
    private int IMONumber;
    private double ToPort;
    private double ToStarBoard;
    private double ToBow;
    private double ToStern;
    private double ETAMonth;
    private double ETADay;
    private double ETAHour;
    private double ETAMinute;
    private double Draught;
    private String Destination;
    private double Grt;

    //zonasi
    private int ZoneID;
    private String ZoneName;

    //ais receiver position
    private int StationID;
    private String StationName;

    //danger score
    private int DangerDist;
    private Boolean DangerHead;

    //fleetmon
    public String getFmBattery() {
		return FmBattery;
	}
	public void setFmBattery(String fmBattery) {
		FmBattery = fmBattery;
	}
	public double getFmWavePeriod() {
		return FmWavePeriod;
	}
	public void setFmWavePeriod(double fmWavePeriod) {
		FmWavePeriod = fmWavePeriod;
	}
	public double getFmTempWater() {
		return FmTempWater;
	}
	public void setFmTempWater(double fmTempWater) {
		FmTempWater = fmTempWater;
	}
	public double getFmIceConcentration() {
		return FmIceConcentration;
	}
	public void setFmIceConcentration(double fmIceConcentration) {
		FmIceConcentration = fmIceConcentration;
	}
	public Timestamp getFmFirstCreated() {
		return FmFirstCreated;
	}
	public void setFmFirstCreated(Timestamp fmFirstCreated) {
		FmFirstCreated = fmFirstCreated;
	}
	public double getFmAirPressure() {
		return FmAirPressure;
	}
	public void setFmAirPressure(double fmAirPressure) {
		FmAirPressure = fmAirPressure;
	}
	public double getFmCurrentSpeed() {
		return FmCurrentSpeed;
	}
	public void setFmCurrentSpeed(double fmCurrentSpeed) {
		FmCurrentSpeed = fmCurrentSpeed;
	}
	public int getFmGpsDeviceId() {
		return FmGpsDeviceId;
	}
	public void setFmGpsDeviceId(int fmGpsDeviceId) {
		FmGpsDeviceId = fmGpsDeviceId;
	}
	public int getFmWindDir() {
		return FmWindDir;
	}
	public void setFmWindDir(int fmWindDir) {
		FmWindDir = fmWindDir;
	}
	public double getFmWindSpeed() {
		return FmWindSpeed;
	}
	public void setFmWindSpeed(double fmWindSpeed) {
		FmWindSpeed = fmWindSpeed;
	}
	public int getFmWaveDir() {
		return FmWaveDir;
	}
	public void setFmWaveDir(int fmWaveDir) {
		FmWaveDir = fmWaveDir;
	}
	public double getFmTemperatureAir() {
		return FmTemperatureAir;
	}
	public void setFmTemperatureAir(double fmTemperatureAir) {
		FmTemperatureAir = fmTemperatureAir;
	}
	public String getFmName() {
		return FmName;
	}
	public void setFmName(String fmName) {
		FmName = fmName;
	}
	public int getFmCurrentDir() {
		return FmCurrentDir;
	}
	public void setFmCurrentDir(int fmCurrentDir) {
		FmCurrentDir = fmCurrentDir;
	}
	public double getFmWindSurfaceGust() {
		return FmWindSurfaceGust;
	}
	public void setFmWindSurfaceGust(double fmWindSurfaceGust) {
		FmWindSurfaceGust = fmWindSurfaceGust;
	}
	public boolean isFmAis() {
		return FmAis;
	}
	public void setFmAis(boolean fmAis) {
		FmAis = fmAis;
	}
	public int getFmTotalCloudCover() {
		return FmTotalCloudCover;
	}
	public void setFmTotalCloudCover(int fmTotalCloudCover) {
		FmTotalCloudCover = fmTotalCloudCover;
	}
	public int getFmRelativeHumidity() {
		return FmRelativeHumidity;
	}
	public void setFmRelativeHumidity(int fmRelativeHumidity) {
		FmRelativeHumidity = fmRelativeHumidity;
	}
	public double getFmWaveHeight() {
		return FmWaveHeight;
	}
	public void setFmWaveHeight(double fmWaveHeight) {
		FmWaveHeight = fmWaveHeight;
	}
	public String getFmEsn() {
		return FmEsn;
	}
	public void setFmEsn(String fmEsn) {
		FmEsn = fmEsn;
	}
	public String getFmLocation() {
		return FmLocation;
	}
	public void setFmLocation(String fmLocation) {
		FmLocation = fmLocation;
	}

	private String FmBattery;
	private double FmWavePeriod;
	private double FmTempWater;
	private double FmIceConcentration;
	private Timestamp FmFirstCreated;
	private double FmAirPressure;
	private double FmCurrentSpeed;
	private int FmGpsDeviceId;
	private int FmWindDir;
	private double FmWindSpeed;
	private int FmWaveDir;
	private double FmTemperatureAir;
	private String FmName;
	private int FmCurrentDir;
	private double FmWindSurfaceGust;
	private boolean FmAis;
	private int FmTotalCloudCover;
	private int FmRelativeHumidity;
	private double FmWaveHeight;
	private String FmEsn;
	private String FmLocation;
}
