package com.ptdmc.modabilefe.geo;

import java.util.HashMap;
import java.util.Map;

import org.geojson.Feature;
import org.geojson.LngLatAlt;
import org.geojson.Point;

import com.ptdmc.modabilefe.util.Convert;

@SuppressWarnings("serial")
public class ShipFeature extends Feature {
	
	public ShipFeature(double lon, double lat, ShipProperty property) {
		Point point = new Point();
		LngLatAlt lonlat = new LngLatAlt();
		lonlat.setLongitude(lon);
		lonlat.setLatitude(lat);
		point.setCoordinates(lonlat);
		
		this.setGeometry(point);
		this.setProperties(CreateProperties(property));
	}

	private Map<String, Object> CreateProperties(ShipProperty property){
		Map<String, Object> maps = new HashMap<String, Object>();		
		try {
			maps = Convert.BeanToMap(property);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return maps;
	}
	
	
	
}
