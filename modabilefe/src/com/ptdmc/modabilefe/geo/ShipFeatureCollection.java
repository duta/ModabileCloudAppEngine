package com.ptdmc.modabilefe.geo;

import java.util.List;

import org.geojson.Feature;
import org.geojson.FeatureCollection;

@SuppressWarnings("serial")
public class ShipFeatureCollection extends FeatureCollection {
	
	private String type = "FeatureCollection";
	
	public String getType() {
		return this.type;
	}
	
	public void setType(String val) {
		this.type = val;
	}
	
	public ShipFeatureCollection(List<Feature> shipFeatures) {		
		this.setFeatures(shipFeatures);		
	}
	
	

	
}
