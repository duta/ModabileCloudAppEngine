package com.ptdmc.modabilefe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.*;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.geojson.Feature;
import org.geojson.Point;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.ShipInfo;
import com.ptdmc.modabilefe.geo.ShipFeatureCollection;

@SuppressWarnings("serial")
public class VesselServlet extends HttpServlet {

	@Override
	protected void doOptions(HttpServletRequest req, HttpServletResponse res) {
		res.addHeader("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept");
		res.addHeader("Access-Control-Allow-Origin", "*");
	}

	private String checkToken(String token) {
		try {
			URL url = new URL(
					"https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="
							+ token);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setRequestProperty("X-Custom-Header", "xxx");
			connection.setRequestProperty("Content-Type", "application/json");

			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				// OK
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(url.openStream()));
				StringBuffer res = new StringBuffer();
				String line;
				while ((line = reader.readLine()) != null) {
					res.append(line);
				}
				reader.close();

				JSONObject jsonObj = new JSONObject(res.toString());
				// check aud to verify that this token is made for this site
				// (use azp to check which app made the token)
				if ("820145933541-gvlb64b91uqtogs1jr7dluajqhj79gru.apps.googleusercontent.com"
						.equals(jsonObj.getString("aud")))
					return jsonObj.getString("email");
			} else {
				// Server returned HTTP error code.
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		String methodName = req.getParameter("op");
		res.addHeader("Access-Control-Allow-Origin", "*");
		if (methodName.toLowerCase().equals("getall")) {
			try {
				GetAll(req, res);
			} catch (NumberFormatException | SQLException | IOException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (methodName.toLowerCase().equals("getcustomerdata")) {
			String json;
			try {
				String email = checkToken(req.getParameter("token"));
				if (email == null)
					return;
				int userid = CallFactory.getInstance(req).getFactory()
						.GetUsers().GetUserIDByUserName(email);
				Customer customer = CallFactory.getInstance(req).getFactory()
						.GetCustomer().GetByEmail(email);
				if (customer == null) {
					customer = CallFactory.getInstance(req).getFactory()
							.GetCustomerMember().getCustomerByUserID(userid);
				}
				res.setContentType("application/json");
				res.setCharacterEncoding("UTF-8");
				String company = customer.getCustomer_company();
				if (company == null)
					res.getWriter().write("{\"company\":null}");
				else
					res.getWriter().write(
							"{\"company\":'" + customer.getCustomer_company()
									+ "'}");
				res.flushBuffer();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (methodName.toLowerCase().equals("getlabelvalue")) {
			// BasePage.CreateFactoryLogPos().GetShipInfo().GetLabelValueOfMMSIShipName(search);

			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();
			String json;
			try {
				json = ow
						.writeValueAsString(CallFactory
								.getInstance(req)
								.getFactory()
								.GetShipInfo()
								.GetLabelValueOfMMSIShipName(
										req.getParameter("search")));
				res.setContentType("application/json");
				res.setCharacterEncoding("UTF-8");
				res.getWriter().write(json);
				res.flushBuffer();
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (methodName.toLowerCase().equals("getkml")) {
			try {
				GetKML(req, res);
			} catch (NumberFormatException | IOException | ParseException
					| SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				try {
					res.sendError(500, errors.toString());
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
		}
		if (methodName.toLowerCase().equals("getxlsx")) {
			try {
				GetXLSX(req, res);
			} catch (NumberFormatException | IOException | ParseException
					| SQLException | WriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				StringWriter errors = new StringWriter();
				e.printStackTrace(new PrintWriter(errors));
				try {
					res.sendError(500, errors.toString());
				} catch (IOException ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
				}
			}
		}
		if (methodName.toLowerCase().equals("getimage")) {
			GetImage(req, res);
		}
	}

	private void GetImage(HttpServletRequest req, HttpServletResponse res) {
		try {
		GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
	      .initialRetryDelayMillis(10)
	      .retryMaxAttempts(10)
	      .totalRetryPeriodMillis(15000)
	      .build());
	    GcsInputChannel readChannel = gcsService.openPrefetchingReadChannel(new GcsFilename("modabile.appspot.com", "vessel_image_"+req.getParameter("mmsi")+".png"), 0, 2 * 1024 * 1024);
			res.setContentType("image/png");
			copy(Channels.newInputStream(readChannel), res.getOutputStream());
			return;
		} catch (Exception e) {
			try {
				res.sendError(404, e.getMessage());
				return;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	  private void copy(InputStream input, OutputStream output) throws IOException {
		    try {
		      byte[] buffer = new byte[2 * 1024 * 1024];
		      int bytesRead = input.read(buffer);
		      while (bytesRead != -1) {
		        output.write(buffer, 0, bytesRead);
		        bytesRead = input.read(buffer);
		      }
		    } finally {
		      input.close();
		      output.close();
		    }
		  }

	private void GetXLSX(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ParseException, NumberFormatException,
			SQLException, RowsExceededException, WriteException {
		Customer customer = null;
		try {
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			int userid = -1;
			String useremail = "";
			if (user == null) {
				useremail = checkToken(req.getParameter("token"));
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			} else {
				useremail = user.getEmail();
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			}
			if (userid == -1) {
				return;
			}
			customer = CallFactory.getInstance(req).getFactory()
					.GetCustomer().GetByEmail(useremail);
			if (customer == null) {
				customer = CallFactory.getInstance(req).getFactory()
						.GetCustomerMember().getCustomerByUserID(userid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
		DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmm");
		DateFormat formatter3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		String fileName = "Recorded_" + formatter.format(new Date()) + ".xls";
		res.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		res.setBufferSize(0);
		res.addHeader("Content-Disposition", "attachment; filename=" + fileName);
		Date start = (Date) formatter.parse(req.getParameter("dateStart"));
		Date end = (Date) formatter.parse(req.getParameter("dateEnd"));
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
		Timestamp timeStampStart = new Timestamp(cal.getTime().getTime());
		Date realstart = cal.getTime();
		cal.setTime(end);
		cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
		Timestamp timeStampEnd = new Timestamp(cal.getTime().getTime());
		Enumeration<String> params = req.getParameterNames();

		// Set up
		WritableWorkbook wb = Workbook.createWorkbook(res.getOutputStream());
		WritableSheet sheet = wb.createSheet(fileName, 0);
		// sheet.getSettings().setDefaultRowHeight(12);
		// Create header
		WritableCellFormat cf = new WritableCellFormat();
		cf.setFont(new WritableFont(WritableFont.ARIAL, 14));
		Label label = new Label(0, 0, "Vessel Name");
		label.setCellFormat(cf);
		sheet.addCell(label);
		label = new Label(1, 0, "MMSI");
		label.setCellFormat(cf);
		sheet.addCell(label);
		label = new Label(2, 0, "Speed");
		label.setCellFormat(cf);
		sheet.addCell(label);
		label = new Label(3, 0, "Timestamp");
		label.setCellFormat(cf);
		sheet.addCell(label);
		label = new Label(4, 0, "Longitude");
		label.setCellFormat(cf);
		sheet.addCell(label);
		label = new Label(5, 0, "Latitude");
		label.setCellFormat(cf);
		sheet.addCell(label);
		// fill the rest
		int rowCounter = 1;
		while (params.hasMoreElements()) // for each ship in request
		{
			String item = params.nextElement();
			if (!item.startsWith("mmsi"))
				continue;
			ShipFeatureCollection result;
			start = realstart;
			timeStampStart = new Timestamp(realstart.getTime());
			while (timeStampStart.before(timeStampEnd)) {
				cal.setTime(timeStampStart);
				try {
					cal.add(Calendar.SECOND, Integer.parseInt(req
							.getParameter("IntervalSecond")));
				} catch (Exception e) {
				}
				try {
					cal.add(Calendar.MINUTE, Integer.parseInt(req
							.getParameter("IntervalMinute")));
				} catch (Exception e) {
				}
				try {
					cal.add(Calendar.HOUR,
							Integer.parseInt(req.getParameter("IntervalHour")));
				} catch (Exception e) {
				}
				Timestamp timeStampDateNext = new Timestamp(cal.getTime()
						.getTime());
				if (timeStampDateNext.after(timeStampEnd))
					timeStampDateNext = timeStampEnd;
				result = CallFactory
						.getInstance(req)
						.getFactory()
						.GetIncomingAis()
						.RecordedPositionByMMSIAndDate(
								Integer.parseInt(req.getParameter(item)),
								timeStampStart, timeStampDateNext);
				for (Feature ship : result.getFeatures()) {
					DateFormat formatterd = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
			        DecimalFormat df2 = new DecimalFormat("#");
					cal.setTime(formatterd.parse(df2.format(ship.getProperty("TimeStampReceiver"))));
					cal.add(Calendar.SECOND, customer.getCustomer_gmtoffset());
					label = new Label(0, rowCounter, ship.getProperty(
							"ShipName").toString());
					sheet.addCell(label);
					label = new Label(1, rowCounter, ship.getProperty("MMSI")
							.toString());
					sheet.addCell(label);
					label = new Label(2, rowCounter, ship.getProperty("SOG")
							.toString());
					sheet.addCell(label);
					String tet = formatterd.format(cal.getTime());
					label = new Label(3, rowCounter,
							formatter3.format(formatter2.parse(tet.substring(0,
									12))));
					sheet.addCell(label);
					jxl.write.Number n = new jxl.write.Number(4, rowCounter,
							((org.geojson.Point) ship.getGeometry())
									.getCoordinates().getLongitude());
					sheet.addCell(n);
					n = new jxl.write.Number(5, rowCounter,
							((org.geojson.Point) ship.getGeometry())
									.getCoordinates().getLatitude());
					sheet.addCell(n);
					rowCounter++;
				}
				timeStampStart = timeStampDateNext;
			}
		}

		for (int x = 0; x < 7; x++) {
			CellView cell = sheet.getColumnView(x);
			cell.setAutosize(true);
			sheet.setColumnView(x, cell);
		}

		// Write the output
		wb.write();
		wb.close();
	}

	private String[] colourList = { "7F000000", "7F00FF00", "7F0000FF",
			"7FFF0000", "7FFEFF01",

			"7FFEA6FF", "7F66DBFF", "7F016400", "7F670001", "7F3A0095",
			"7FB57D00", "7FF600FF", "7FE8EEFF", "7F004D77", "7F92FB90",
			"7FFF7600", "7F00FFD5", "7F7E93FF", "7F6C826A", "7F9D02FF" };

	private void GetKML(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ParseException, SQLException {
		Customer customer = null;
		try {
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			int userid = -1;
			String useremail = "";
			if (user == null) {
				useremail = checkToken(req.getParameter("token"));
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			} else {
				useremail = user.getEmail();
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			}
			if (userid == -1) {
				return;
			}
			customer = CallFactory.getInstance(req).getFactory()
					.GetCustomer().GetByEmail(useremail);
			if (customer == null) {
				customer = CallFactory.getInstance(req).getFactory()
						.GetCustomerMember().getCustomerByUserID(userid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		res.setContentType("application/vnd.google-earth.kml+xml");
		res.setBufferSize(0);
		java.util.Date DateNow = new java.util.Date();
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy-HH-mm")
				.format(DateNow);
		res.addHeader("Content-Disposition",
				"attachment; filename=RecordedKML_" + timeStamp + ".kml");
		res.getWriter()
				.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><kml xmlns=\"http://www.opengis.net/kml/2.2\" xmlns:gx=\"http://www.google.com/kml/ext/2.2\"><Document>");
		List<String> freeColour = new ArrayList<String>(
				Arrays.asList(colourList));
		Random r = new Random();
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
		DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmm");
		DateFormat formatter3 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date start = (Date) formatter.parse(req.getParameter("dateStart"));
		Date end = (Date) formatter.parse(req.getParameter("dateEnd"));
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
		Timestamp timeStampStart = new Timestamp(cal.getTime().getTime());
		Date realstart = cal.getTime();
		cal.setTime(end);
		cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
		Timestamp timeStampEnd = new Timestamp(cal.getTime().getTime());
		Enumeration<String> params = req.getParameterNames();
		DecimalFormat df = new DecimalFormat("#.###");
		while (params.hasMoreElements()) // for each ship in request
		{
			String item = params.nextElement();
			if (!item.startsWith("mmsi"))
				continue;
			ShipFeatureCollection result;
			StringBuilder sb = new StringBuilder();
			int resultfeat = 0, mmsi = 0;
			String name = "";
			timeStampStart = new Timestamp(realstart.getTime());
			while (timeStampStart.before(timeStampEnd)) {
				cal.setTime(timeStampStart);
				try {
					cal.add(Calendar.SECOND, Integer.parseInt(req
							.getParameter("IntervalSecond")));
				} catch (Exception e) {
				}
				try {
					cal.add(Calendar.MINUTE, Integer.parseInt(req
							.getParameter("IntervalMinute")));
				} catch (Exception e) {
				}
				try {
					cal.add(Calendar.HOUR,
							Integer.parseInt(req.getParameter("IntervalHour")));
				} catch (Exception e) {
				}
				Timestamp timeStampDateNext = new Timestamp(cal.getTime()
						.getTime());
				if (timeStampDateNext.after(timeStampEnd))
					timeStampDateNext = timeStampEnd;
				result = CallFactory
						.getInstance(req)
						.getFactory()
						.GetIncomingAis()
						.RecordedPositionByMMSIAndDate(
								Integer.parseInt(req.getParameter(item)),
								timeStampStart, timeStampDateNext);
				for (Feature ship : result.getFeatures()) {
					DateFormat formatterd = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
			        DecimalFormat df2 = new DecimalFormat("#");
					cal.setTime(formatterd.parse(df2.format(ship.getProperty("TimeStampReceiver"))));
					cal.add(Calendar.SECOND, customer.getCustomer_gmtoffset());
					res.getWriter().write("<Placemark>");
					res.getWriter()
							.write("<name>" + ship.getProperty("ShipName")
									+ "</name>");
					String tet = formatterd.format(cal.getTime());
					res.getWriter().write(
							"<description><table><tr><td>MMSI</td><td>: "
									+ ship.getProperty("MMSI")
									+ "</td></tr><tr><td>Speed</td><td>: "
									+ ship.getProperty("SOG")
									+ " k"
									+ "</td></tr><tr><td>Date</td><td>: "
									+ formatter3.format(formatter2.parse(tet
											.substring(0, 12)))
									+ "</td></tr><tr><td>Longitude</td><td>: "
									+ df.format(((org.geojson.Point) ship
											.getGeometry()).getCoordinates()
											.getLongitude())
									+ "</td></tr><tr><td>Latitude</td><td>: "
									+ df.format(((org.geojson.Point) ship
											.getGeometry()).getCoordinates()
											.getLatitude())
									+ "</td></tr></table></description>");
					res.getWriter().write(
							"<Point><coordinates>"
									+ ((org.geojson.Point) ship.getGeometry())
											.getCoordinates().getLongitude()
									+ ","
									+ ((org.geojson.Point) ship.getGeometry())
											.getCoordinates().getLatitude()
									+ ",0</coordinates></Point>");
					res.getWriter().write("</Placemark>");
					sb.append(((org.geojson.Point) ship.getGeometry())
							.getCoordinates().getLongitude()
							+ ","
							+ ((org.geojson.Point) ship.getGeometry())
									.getCoordinates().getLatitude() + ",0 ");
					name = ship.getProperty("ShipName");
					mmsi = ship.getProperty("MMSI");
				}
				resultfeat += result.getFeatures().size();
				timeStampStart = timeStampDateNext;
			}
			if (resultfeat == 0)
				continue;
			res.getWriter().write("<Style id=\"style" + item + "\">");
			res.getWriter().write("<LineStyle>");
			String colour = freeColour.get(r.nextInt(freeColour.size()));
			freeColour.remove(colour);
			res.getWriter().write("<color>" + colour + "</color>");
			res.getWriter().write("<width>4</width>");
			res.getWriter().write("<gx:labelVisibility>1</gx:labelVisibility>");
			res.getWriter().write("</LineStyle>");
			res.getWriter().write("</Style>");
			res.getWriter().write("<Placemark>");
			res.getWriter().write("<name>" + name + "</name>");
			res.getWriter().write("<styleUrl>#style" + item + "</styleUrl>");
			res.getWriter().write(
					"<description><table><tr><td>MMSI</td><td>: " + mmsi
							+ "</td></tr></table></description>");
			res.getWriter().write("<LineString>");
			res.getWriter().write(
					"<coordinates>" + sb.toString() + "</coordinates>");
			res.getWriter().write("</LineString>");
			res.getWriter().write("</Placemark>");
		}
		res.getWriter().write("</Document>");
		res.getWriter().write("</kml>");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.addHeader("Access-Control-Allow-Origin", "*");
		Customer customer = null;
		try {
			UserService userService = UserServiceFactory.getUserService();
			User user = userService.getCurrentUser();
			int userid = -1;
			String useremail = "";
			if (user == null) {
				useremail = checkToken(req.getParameter("token"));
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			} else {
				useremail = user.getEmail();
				userid = CallFactory.getInstance(req).getFactory().GetUsers()
						.GetUserIDByUserName(useremail);
			}
			if (userid == -1) {
				return;
			}
			customer = CallFactory.getInstance(req).getFactory()
					.GetCustomer().GetByEmail(useremail);
			if (customer == null) {
				customer = CallFactory.getInstance(req).getFactory()
						.GetCustomerMember().getCustomerByUserID(userid);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (req.getParameter("op").toLowerCase().equals("getrecorded")) {
			// BasePage.CreateFactoryLogPos().GetAISLog().RecordedPositionByMMSIAndDate(mmsi,
			// dateFrom, dateTo);
			ObjectWriter ow = new ObjectMapper().writer()
					.withDefaultPrettyPrinter();

			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date date;
			Date date2;
			try {
				date = (Date) formatter.parse(req.getParameter("from"));
				date2 = (Date) formatter.parse(req.getParameter("to"));
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
				Timestamp t1 = new Timestamp(cal.getTime().getTime());
				cal.setTime(date2);
				cal.add(Calendar.SECOND, -customer.getCustomer_gmtoffset());
				Timestamp t2 = new Timestamp(cal.getTime().getTime());
				ShipFeatureCollection shf = CallFactory
						.getInstance(req)
						.getFactory()
						.GetIncomingAis()
						.RecordedPositionByMMSIAndDate(
								Integer.parseInt(req.getParameter("mmsi")),
								t1,
								t2);
				for (int j = 0; j < shf.getFeatures().size(); j++) {
					DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
			        DecimalFormat df = new DecimalFormat("#");
					cal.setTime(formatter2.parse(df.format(shf.getFeatures().get(j).getProperty("TimeStampReceiver"))));
					cal.add(Calendar.SECOND, customer.getCustomer_gmtoffset());
					shf.getFeatures().get(j).setProperty("TimeStampReceiver", Double.parseDouble(formatter2.format(cal.getTime())));
				}
				String json = ow.writeValueAsString(shf);
				res.setContentType("application/json");
				res.setCharacterEncoding("UTF-8");
				res.getWriter().write(json);
			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void GetAll(HttpServletRequest req, HttpServletResponse res)
			throws NumberFormatException, SQLException, IOException, ParseException {
		List<ShipInfo> sil = new ArrayList<ShipInfo>();
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		int userid = -1;
		String useremail = "";
		if (user == null) {
			useremail = checkToken(req.getParameter("token"));
			userid = CallFactory.getInstance(req).getFactory().GetUsers()
					.GetUserIDByUserName(useremail);
		} else {
			useremail = user.getEmail();
			userid = CallFactory.getInstance(req).getFactory().GetUsers()
					.GetUserIDByUserName(useremail);
		}
		if (userid == -1) {
			return;
		}
		Customer customer = CallFactory.getInstance(req).getFactory()
				.GetCustomer().GetByEmail(useremail);
		if (customer == null) {
			customer = CallFactory.getInstance(req).getFactory()
					.GetCustomerMember().getCustomerByUserID(userid);
		}

		java.util.ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> userRoles = new java.util.ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();

		userRoles = CallFactory.getInstance(req).getFactory().GetUsers()
				.GetRolesByUserID(userid);
		List<Integer> mmsis = null;

		for (com.ptdmc.modabilefe.entity.leyfi.Roles role : userRoles) {
			if (role.role_name.equals("superadmin (ADMIN)"))
				mmsis = CallFactory.getInstance(req).getFactory().GetShipInfo()
						.getAll();
		}
		if (mmsis == null || mmsis.size() == 0)
			mmsis = CallFactory
					.getInstance(req)
					.getFactory()
					.GetCustomer()
					.getActiveMMSI(
							CallFactory.getInstance(req).getFactory()
									.GetCustomer().GetByEmail(useremail));

		try {
			sil = CallFactory.getInstance(req).getFactory().GetShipInfo()
					.GetBySomeMMSI(mmsis);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// end of dummy list

		// now we got our ships
		List<double[][]> line = new ArrayList<double[][]>();
		List<ShipFeatureCollection> all = new ArrayList<ShipFeatureCollection>();
		ShipFeatureCollection shipFeatures;
		double[][] trace;
		
		Calendar cal = Calendar.getInstance();
		DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
        DecimalFormat df = new DecimalFormat("#");
        Point point;
		
		for (int i = 0; i < mmsis.size(); i++) {
			shipFeatures = CallFactory.getInstance(req).getFactory()
					.GetIncomingAis()
					.RecordedPositionByMMSITopX(mmsis.get(i), 10);

			all.add(shipFeatures);
			trace = new double[shipFeatures.getFeatures().size()][];
			for (int j = 0; j < shipFeatures.getFeatures().size(); j++) {
				point = (Point) shipFeatures.getFeatures().get(j)
						.getGeometry();
				trace[j] = new double[2];
				trace[j][0] = point.getCoordinates().getLongitude();
				trace[j][1] = point.getCoordinates().getLatitude();
				
				cal.setTime(formatter2.parse(df.format(shipFeatures.getFeatures().get(j).getProperty("TimeStampReceiver"))));
				cal.add(Calendar.SECOND, customer.getCustomer_gmtoffset());
				shipFeatures.getFeatures().get(j).setProperty("TimeStampReceiver", Double.parseDouble(formatter2.format(cal.getTime())));
			}
			line.add(trace);
		}
		Object[] result = new Object[2];
		result[0] = all;
		result[1] = line;

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(result);
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write(json);
	}

}
