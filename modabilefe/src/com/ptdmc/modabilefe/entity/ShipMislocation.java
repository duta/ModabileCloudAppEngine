package com.ptdmc.modabilefe.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ShipMislocation {  
  @Column(name="received_on")
  private java.sql.Timestamp received_on;
  @Column(name="mmsi")
  private Long mmsi;
  @Column(name="vesselname")
  private String vesselname;

  public java.sql.Timestamp getReceived_on() {
    return received_on;
  }

  public void setReceived_on(java.sql.Timestamp received_on) {
    this.received_on = received_on;
  }

  public Long getMmsi() {
    return mmsi;
  }

  public void setMmsi(Long mmsi) {
    this.mmsi = mmsi;
  }

  public String getVesselname() {
    return vesselname;
  }

  public void setVesselname(String vesselname) {
    this.vesselname = vesselname;
  }
}

