package com.ptdmc.modabilefe.entity;

import com.ptdmc.modabilefe.FunctionM;

public class ProductPrice {
  private int pp_id;
  private int pp_price;
  private int product_id;
  private String pp_currency;
  private java.sql.Timestamp pp_creationdate;
  private java.sql.Timestamp pp_lastupdate;
  private String pp_createby;
  private String pp_lastupdateby;
  
public int getPp_id() {
	return pp_id;
}
public void setPp_id(int pp_id) {
	this.pp_id = pp_id;
}
public int getPp_price() {
	return pp_price;
}
public void setPp_price(int pp_price) {
	this.pp_price = pp_price;
}
public int getProduct_id() {
	return product_id;
}
public void setProduct_id(int product_id) {
	this.product_id = product_id;
}
public String getPp_currency() {
	return pp_currency;
}
public void setPp_currency(String pp_currency) {
	this.pp_currency = pp_currency;
}
public java.sql.Timestamp getPp_creationdate() {
	return pp_creationdate;
}
public void setPp_creationdate(java.sql.Timestamp pp_creationdate) {
	this.pp_creationdate = pp_creationdate;
}
public java.sql.Timestamp getPp_lastupdate() {
	return pp_lastupdate;
}
public void setPp_lastupdate(java.sql.Timestamp pp_lastupdate) {
	this.pp_lastupdate = pp_lastupdate;
}
public String getPp_createby() {
	return pp_createby;
}
public void setPp_createby(String pp_createby) {
	this.pp_createby = pp_createby;
}
public String getPp_lastupdateby() {
	return pp_lastupdateby;
}
public void setPp_lastupdateby(String pp_lastupdateby) {
	this.pp_lastupdateby = pp_lastupdateby;
}
public String getFormatted_pp_price() {
	return FunctionM.getFormatRp(this.pp_price);
}

}
