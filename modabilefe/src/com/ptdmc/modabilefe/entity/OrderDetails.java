package com.ptdmc.modabilefe.entity;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class OrderDetails {
  private Long orddtl_id;
  private Long product_id;
  private String product_name;
  private Long orddtl_quantity;
  private Long orddtl_unitprice;
  private String orddtl_currency;
  private Long customer_id;
  private String customer_company;
  private java.sql.Timestamp orddtl_creationdate;
  private java.sql.Timestamp orddtl_lastupdate;
  private String orddtl_createby;
  private Long orddtl_discount;
  private String orddtl_lastupdateby;
  private Long order_id;
  private java.sql.Timestamp orddtl_contract_startdate;
  private java.sql.Timestamp orddtl_contract_enddate;
  private String shipList = "";

  public Long getOrddtl_id() {
    return orddtl_id;
  }

  public void setOrddtl_id(Long orddtl_id) {
    this.orddtl_id = orddtl_id;
  }

  public Long getProduct_id() {
    return product_id;
  }

  public void setProduct_id(Long product_id) {
    this.product_id = product_id;
  }

  public Long getOrddtl_quantity() {
    return orddtl_quantity;
  }

  public void setOrddtl_quantity(Long orddtl_quantity) {
    this.orddtl_quantity = orddtl_quantity;
  }

  public Long getOrddtl_unitprice() {
    return orddtl_unitprice;
  }

  public void setOrddtl_unitprice(Long orddtl_unitprice) {
    this.orddtl_unitprice = orddtl_unitprice;
  }

  public String getOrddtl_currency() {
    return orddtl_currency;
  }

  public void setOrddtl_currency(String orddtl_currency) {
    this.orddtl_currency = orddtl_currency;
  }

  public Long getCustomer_id() {
    return customer_id;
  }

  public void setCustomer_id(Long customer_id) {
    this.customer_id = customer_id;
  }

  public Timestamp getOrddtl_creationdate() {
    return orddtl_creationdate;
  }

  public void setOrddtl_creationdate(Timestamp orddtl_creationdate) {
    this.orddtl_creationdate = orddtl_creationdate;
  }

  public Timestamp getOrddtl_lastupdate() {
    return orddtl_lastupdate;
  }

  public void setOrddtl_lastupdate(Timestamp orddtl_lastupdate) {
    this.orddtl_lastupdate = orddtl_lastupdate;
  }

  public String getOrddtl_createby() {
    return orddtl_createby;
  }

  public void setOrddtl_createby(String orddtl_createby) {
    this.orddtl_createby = orddtl_createby;
  }

  public Long getOrddtl_discount() {
    return orddtl_discount;
  }

  public void setOrddtl_discount(Long orddtl_discount) {
    this.orddtl_discount = orddtl_discount;
  }

  public String getOrddtl_lastupdateby() {
    return orddtl_lastupdateby;
  }

  public void setOrddtl_lastupdateby(String orddtl_lastupdateby) {
    this.orddtl_lastupdateby = orddtl_lastupdateby;
  }

  public Long getOrder_id() {
    return order_id;
  }

  public void setOrder_id(Long order_id) {
    this.order_id = order_id;
  }

  public Timestamp getOrddtl_contract_startdate() {
    return orddtl_contract_startdate;
  }
  
  public String getFormatted_startdate() {
  	SimpleDateFormat f = new SimpleDateFormat("dd MMMM yyyy");
    return f.format(this.orddtl_contract_startdate);
  }

  public void setOrddtl_contract_startdate(Timestamp orddtl_contract_startdate) {
    this.orddtl_contract_startdate = orddtl_contract_startdate;
  }

  public Timestamp getOrddtl_contract_enddate() {
    return orddtl_contract_enddate;
  }
  
  public String getFormatted_enddate() {
	SimpleDateFormat f = new SimpleDateFormat("dd MMMM yyyy");
    return f.format(this.orddtl_contract_enddate);
  }

  public void setOrddtl_contract_enddate(Timestamp orddtl_contract_enddate) {
    this.orddtl_contract_enddate = orddtl_contract_enddate;
  }

public String getProduct_name() {
	return product_name;
}

public void setProduct_name(String product_name) {
	this.product_name = product_name;
}

public String getCustomer_company() {
	return customer_company;
}

public void setCustomer_company(String customer_company) {
	this.customer_company = customer_company;
}

public String getShipList() {
	return shipList;
}

public void setShipList(String shipList) {
	this.shipList = shipList;
}
}
