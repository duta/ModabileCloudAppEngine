package com.ptdmc.modabilefe.entity;


public class IncomingAis {
  private java.sql.Timestamp received_on;
  private Long zona_id;
  private Long mmsi;
  private Long arp_id;
  private Long messagetype;
  private Long slottimeout;
  private Long iais_timestamp;
  private String dimensiontobow;
  private Long navigationalstatus;
  private Double trueheading;
  private Long positionaccuracy;
  private Long repeatindicator;
  private String longitude;
  private String courseoverground;
  private Long maneuverindicator;
  private String navstatusdesc;
  private String zonename;
  private String dimensiontostern;
  private String destination;
  private String dimensiontostarboard;
  private Long spare;
  private Long syncstate;
  private String timestampreceiver;
  private String submessage;
  private String dimensiontoport;
  private String draught;
  private String rateofturn;
  private String speedoverground;
  private String latitude;
  private String raimflag;
  private Long etamonth;
  private Long etaday;
  private Long etahour;
  private Long etaminute;
  private Long customer_id;

  public java.sql.Timestamp getReceived_on() {
    return received_on;
  }

  public void setReceived_on(java.sql.Timestamp received_on) {
    this.received_on = received_on;
  }

  public Long getZona_id() {
    return zona_id;
  }

  public void setZona_id(Long zona_id) {
    this.zona_id = zona_id;
  }

  public Long getMmsi() {
    return mmsi;
  }

  public void setMmsi(Long mmsi) {
    this.mmsi = mmsi;
  }

  public Long getArp_id() {
    return arp_id;
  }

  public void setArp_id(Long arp_id) {
    this.arp_id = arp_id;
  }

  public Long getMessagetype() {
    return messagetype;
  }

  public void setMessagetype(Long messagetype) {
    this.messagetype = messagetype;
  }

  public Long getSlottimeout() {
    return slottimeout;
  }

  public void setSlottimeout(Long slottimeout) {
    this.slottimeout = slottimeout;
  }

  public Long getIais_timestamp() {
    return iais_timestamp;
  }

  public void setIais_timestamp(Long iais_timestamp) {
    this.iais_timestamp = iais_timestamp;
  }

  public String getDimensiontobow() {
    return dimensiontobow;
  }

  public void setDimensiontobow(String dimensiontobow) {
    this.dimensiontobow = dimensiontobow;
  }

  public Long getNavigationalstatus() {
    return navigationalstatus;
  }

  public void setNavigationalstatus(Long navigationalstatus) {
    this.navigationalstatus = navigationalstatus;
  }

  public Double getTrueheading() {
    return trueheading;
  }

  public void setTrueheading(Double trueheading) {
    this.trueheading = trueheading;
  }

  public Long getPositionaccuracy() {
    return positionaccuracy;
  }

  public void setPositionaccuracy(Long positionaccuracy) {
    this.positionaccuracy = positionaccuracy;
  }

  public Long getRepeatindicator() {
    return repeatindicator;
  }

  public void setRepeatindicator(Long repeatindicator) {
    this.repeatindicator = repeatindicator;
  }

  public String getLongitude() {
    return longitude;
  }

  public void setLongitude(String longitude) {
    this.longitude = longitude;
  }

  public String getCourseoverground() {
    return courseoverground;
  }

  public void setCourseoverground(String courseoverground) {
    this.courseoverground = courseoverground;
  }

  public Long getManeuverindicator() {
    return maneuverindicator;
  }

  public void setManeuverindicator(Long maneuverindicator) {
    this.maneuverindicator = maneuverindicator;
  }

  public String getNavstatusdesc() {
    return navstatusdesc;
  }

  public void setNavstatusdesc(String navstatusdesc) {
    this.navstatusdesc = navstatusdesc;
  }

  public String getZonename() {
    return zonename;
  }

  public void setZonename(String zonename) {
    this.zonename = zonename;
  }

  public String getDimensiontostern() {
    return dimensiontostern;
  }

  public void setDimensiontostern(String dimensiontostern) {
    this.dimensiontostern = dimensiontostern;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getDimensiontostarboard() {
    return dimensiontostarboard;
  }

  public void setDimensiontostarboard(String dimensiontostarboard) {
    this.dimensiontostarboard = dimensiontostarboard;
  }

  public Long getSpare() {
    return spare;
  }

  public void setSpare(Long spare) {
    this.spare = spare;
  }

  public Long getSyncstate() {
    return syncstate;
  }

  public void setSyncstate(Long syncstate) {
    this.syncstate = syncstate;
  }

  public String getTimestampreceiver() {
    return timestampreceiver;
  }

  public void setTimestampreceiver(String timestampreceiver) {
    this.timestampreceiver = timestampreceiver;
  }

  public String getSubmessage() {
    return submessage;
  }

  public void setSubmessage(String submessage) {
    this.submessage = submessage;
  }

  public String getDimensiontoport() {
    return dimensiontoport;
  }

  public void setDimensiontoport(String dimensiontoport) {
    this.dimensiontoport = dimensiontoport;
  }

  public String getDraught() {
    return draught;
  }

  public void setDraught(String draught) {
    this.draught = draught;
  }

  public String getRateofturn() {
    return rateofturn;
  }

  public void setRateofturn(String rateofturn) {
    this.rateofturn = rateofturn;
  }

  public String getSpeedoverground() {
    return speedoverground;
  }

  public void setSpeedoverground(String speedoverground) {
    this.speedoverground = speedoverground;
  }

  public String getLatitude() {
    return latitude;
  }

  public void setLatitude(String latitude) {
    this.latitude = latitude;
  }

  public String getRaimflag() {
    return raimflag;
  }

  public void setRaimflag(String raimflag) {
    this.raimflag = raimflag;
  }

  public Long getEtamonth() {
    return etamonth;
  }

  public void setEtamonth(Long etamonth) {
    this.etamonth = etamonth;
  }

  public Long getEtaday() {
    return etaday;
  }

  public void setEtaday(Long etaday) {
    this.etaday = etaday;
  }

  public Long getEtahour() {
    return etahour;
  }

  public void setEtahour(Long etahour) {
    this.etahour = etahour;
  }

  public Long getEtaminute() {
    return etaminute;
  }

  public void setEtaminute(Long etaminute) {
    this.etaminute = etaminute;
  }

  public Long getCustomer_id() {
    return customer_id;
  }

  public void setCustomer_id(Long customer_id) {
    this.customer_id = customer_id;
  }
}
