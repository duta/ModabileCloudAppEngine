package com.ptdmc.modabilefe.entity;


import com.ptdmc.modabilefe.FunctionM;

import javax.persistence.Id;

public class Product {
  @Id
  private int product_id;
  private String product_name;
  private java.sql.Timestamp product_creationdate;
  private java.sql.Timestamp product_lastupdate;
  private int product_period;
  private String product_createby;
  private String product_lastupdateby;
  private String product_description;
  private int pp_id;
  private int pp_price;

  public int getProduct_id() {
    return product_id;
  }

  public void setProduct_id(int product_id) {
    this.product_id = product_id;
  }

  public String getProduct_name() {
    return product_name;
  }

  public void setProduct_name(String product_name) {
    this.product_name = product_name;
  }

  public java.sql.Timestamp getProduct_creationdate() {
    return product_creationdate;
  }

  public void setProduct_creationdate(java.sql.Timestamp product_creationdate) {
    this.product_creationdate = product_creationdate;
  }

  public java.sql.Timestamp getProduct_lastupdate() {
    return product_lastupdate;
  }

  public void setProduct_lastupdate(java.sql.Timestamp product_lastupdate) {
    this.product_lastupdate = product_lastupdate;
  }

  public String getProduct_createby() {
    return product_createby;
  }

  public void setProduct_createby(String product_createby) {
    this.product_createby = product_createby;
  }

  public String getProduct_lastupdateby() {
    return product_lastupdateby;
  }

  public void setProduct_lastupdateby(String product_lastupdateby) {
    this.product_lastupdateby = product_lastupdateby;
  }

  public String getProduct_description() {
    return product_description;
  }

  public void setProduct_description(String product_description) {
    this.product_description = product_description;
  }

public int getPp_id() {
	return pp_id;
}

public void setPp_id(int pp_id) {
	this.pp_id = pp_id;
}

public int getPp_price() {
	return pp_price;
}

public void setPp_price(int pp_price) {
	this.pp_price = pp_price;
}

public String getFormatted_pp_price() {
	return FunctionM.getFormatRp(this.pp_price);
}

public int getProduct_period() {
	return product_period;
}

public void setProduct_period(int product_period) {
	this.product_period = product_period;
}
}
