package com.ptdmc.modabilefe.entity;

public class Konfigurasi {
  private String konfig_key;
  private String konfig_value;
  private String konfig_keyasmandatory;

  public String getKonfig_key() {
    return konfig_key;
  }

  public void setKonfig_key(String konfig_key) {
    this.konfig_key = konfig_key;
  }

  public String getKonfig_value() {
    return konfig_value;
  }

  public void setKonfig_value(String konfig_value) {
    this.konfig_value = konfig_value;
  }

  public String getKonfig_keyasmandatory() {
    return konfig_keyasmandatory;
  }

  public void setKonfig_keyasmandatory(String konfig_keyasmandatory) {
    this.konfig_keyasmandatory = konfig_keyasmandatory;
  }
}
