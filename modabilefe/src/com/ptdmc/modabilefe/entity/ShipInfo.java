package com.ptdmc.modabilefe.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class ShipInfo {  
  @Column(name="received_on")
  private java.sql.Timestamp received_on;
  @Column(name="mmsi")
  private Long mmsi;
  @Column(name="etaminute")
  private Long etaminute;
  @Column(name="repeatindicator")
  private Long repeatindicator;
  @Column(name="etamonth")
  private Long etamonth;
  @Column(name="dte")
  private Long dte;
  @Column(name="spare")
  private Long spare;
  @Column(name="dimensiontostern")
  private String dimensiontostern;
  @Column(name="positionfixtype")
  private Long positionfixtype;
  @Column(name="messagetype")
  private Long messagetype;
  @Column(name="draught")
  private Long draught;
  @Column(name="vesselname")
  private String vesselname;
  @Column(name="destination")
  private String destination;
  @Column(name="etaday")
  private Long etaday;
  @Column(name="shiptypedesc")
  private String shiptypedesc;
  @Column(name="dimensiontoport")
  private String dimensiontoport;
  @Column(name="timestampreceiver")
  private String timestampreceiver;
  @Column(name="imonumber")
  private Long imonumber;
  @Column(name="dimensiontobow")
  private String dimensiontobow;
  @Column(name="etahour")
  private Long etahour;
  @Column(name="aisversion")
  private Long aisversion;
  @Column(name="callsign")
  private String callsign;
  @Column(name="dimensiontostarboard")
  private String dimensiontostarboard;
  @Column(name="shiptype")
  private Long shiptype;
  @Column(name="grt")
  private Long grt;
  @Column(name="enginehp")
  private Long enginehp;
  @Column(name="dte")
  private String enginediesel;
  @Column(name="si_creationdate")
  private java.sql.Timestamp si_creationdate;
  @Column(name="si_lastupdate")
  private java.sql.Timestamp si_lastupdate;
  @Column(name="si_createby")
  private String si_createby;
  @Column(name="si_lastupdateby")
  private String si_lastupdateby;
  @Column(name="si_latitude")
  private String si_latitude;
  @Column(name="si_longitude")
  private String si_longitude;
  @Column(name="si_position_date")
  private java.sql.Timestamp si_position_date;
  @Column(name="si_position_date")
  private String si_wide;
  @Column(name="si_length")
  private String si_length;

  public java.sql.Timestamp getReceived_on() {
    return received_on;
  }

  public void setReceived_on(java.sql.Timestamp received_on) {
    this.received_on = received_on;
  }

  public Long getMmsi() {
    return mmsi;
  }

  public void setMmsi(Long mmsi) {
    this.mmsi = mmsi;
  }

  public Long getEtaminute() {
    return etaminute;
  }

  public void setEtaminute(Long etaminute) {
    this.etaminute = etaminute;
  }

  public Long getRepeatindicator() {
    return repeatindicator;
  }

  public void setRepeatindicator(Long repeatindicator) {
    this.repeatindicator = repeatindicator;
  }

  public Long getEtamonth() {
    return etamonth;
  }

  public void setEtamonth(Long etamonth) {
    this.etamonth = etamonth;
  }

  public Long getDte() {
    return dte;
  }

  public void setDte(Long dte) {
    this.dte = dte;
  }

  public Long getSpare() {
    return spare;
  }

  public void setSpare(Long spare) {
    this.spare = spare;
  }

  public String getDimensiontostern() {
    return dimensiontostern;
  }

  public void setDimensiontostern(String dimensiontostern) {
    this.dimensiontostern = dimensiontostern;
  }

  public Long getPositionfixtype() {
    return positionfixtype;
  }

  public void setPositionfixtype(Long positionfixtype) {
    this.positionfixtype = positionfixtype;
  }

  public Long getMessagetype() {
    return messagetype;
  }

  public void setMessagetype(Long messagetype) {
    this.messagetype = messagetype;
  }

  public Long getDraught() {
    return draught;
  }

  public void setDraught(Long draught) {
    this.draught = draught;
  }

  public String getVesselname() {
    return vesselname;
  }

  public void setVesselname(String vesselname) {
    this.vesselname = vesselname;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public Long getEtaday() {
    return etaday;
  }

  public void setEtaday(Long etaday) {
    this.etaday = etaday;
  }

  public String getShiptypedesc() {
    return shiptypedesc;
  }

  public void setShiptypedesc(String shiptypedesc) {
    this.shiptypedesc = shiptypedesc;
  }

  public String getDimensiontoport() {
    return dimensiontoport;
  }

  public void setDimensiontoport(String dimensiontoport) {
    this.dimensiontoport = dimensiontoport;
  }

  public String getTimestampreceiver() {
    return timestampreceiver;
  }

  public void setTimestampreceiver(String timestampreceiver) {
    this.timestampreceiver = timestampreceiver;
  }

  public Long getImonumber() {
    return imonumber;
  }

  public void setImonumber(Long imonumber) {
    this.imonumber = imonumber;
  }

  public String getDimensiontobow() {
    return dimensiontobow;
  }

  public void setDimensiontobow(String dimensiontobow) {
    this.dimensiontobow = dimensiontobow;
  }

  public Long getEtahour() {
    return etahour;
  }

  public void setEtahour(Long etahour) {
    this.etahour = etahour;
  }

  public Long getAisversion() {
    return aisversion;
  }

  public void setAisversion(Long aisversion) {
    this.aisversion = aisversion;
  }

  public String getCallsign() {
    return callsign;
  }

  public void setCallsign(String callsign) {
    this.callsign = callsign;
  }

  public String getDimensiontostarboard() {
    return dimensiontostarboard;
  }

  public void setDimensiontostarboard(String dimensiontostarboard) {
    this.dimensiontostarboard = dimensiontostarboard;
  }

  public Long getShiptype() {
    return shiptype;
  }

  public void setShiptype(Long shiptype) {
    this.shiptype = shiptype;
  }

  public Long getGrt() {
    return grt;
  }

  public void setGrt(Long grt) {
    this.grt = grt;
  }

  public Long getEnginehp() {
    return enginehp;
  }

  public void setEnginehp(Long enginehp) {
    this.enginehp = enginehp;
  }

  public String getEnginediesel() {
    return enginediesel;
  }

  public void setEnginediesel(String enginediesel) {
    this.enginediesel = enginediesel;
  }

  public java.sql.Timestamp getSi_creationdate() {
    return si_creationdate;
  }

  public void setSi_creationdate(java.sql.Timestamp si_creationdate) {
    this.si_creationdate = si_creationdate;
  }

  public java.sql.Timestamp getSi_lastupdate() {
    return si_lastupdate;
  }

  public void setSi_lastupdate(java.sql.Timestamp si_lastupdate) {
    this.si_lastupdate = si_lastupdate;
  }

  public String getSi_createby() {
    return si_createby;
  }

  public void setSi_createby(String si_createby) {
    this.si_createby = si_createby;
  }

  public String getSi_lastupdateby() {
    return si_lastupdateby;
  }

  public void setSi_lastupdateby(String si_lastupdateby) {
    this.si_lastupdateby = si_lastupdateby;
  }

  public String getSi_latitude() {
    return si_latitude;
  }

  public void setSi_latitude(String si_latitude) {
    this.si_latitude = si_latitude;
  }

  public String getSi_longitude() {
    return si_longitude;
  }

  public void setSi_longitude(String si_longitude) {
    this.si_longitude = si_longitude;
  }

  public java.sql.Timestamp getSi_position_date() {
    return si_position_date;
  }

  public void setSi_position_date(java.sql.Timestamp si_position_date) {
    this.si_position_date = si_position_date;
  }

  public String getSi_wide() {
    return si_wide;
  }

  public void setSi_wide(String si_wide) {
    this.si_wide = si_wide;
  }

  public String getSi_length() {
    return si_length;
  }

  public void setSi_length(String si_length) {
    this.si_length = si_length;
  }
}

