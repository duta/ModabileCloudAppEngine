package com.ptdmc.modabilefe.entity;

import java.sql.Timestamp;

public class OrderDetailsStatus {
  private Long orddtl_id;
  private String orddtl_status;
  private java.sql.Timestamp orddtl_status_date;

  public Long getOrddtl_id() {
    return orddtl_id;
  }

  public void setOrddtl_id(Long orddtl_id) {
    this.orddtl_id = orddtl_id;
  }

  public String getOrddtl_status() {
    return orddtl_status;
  }

  public void setOrddtl_status(String orddtl_status) {
    this.orddtl_status = orddtl_status;
  }

  public Timestamp getOrddtl_status_date() {
    return orddtl_status_date;
  }

  public void setOrddtl_status_date(Timestamp orddtl_status_date) {
    this.orddtl_status_date = orddtl_status_date;
  }
}

