package com.ptdmc.modabilefe.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ShipsTracksNow {
	@Id
	private Long Id;
	private String jsonTrack;
	
	public Long getId(){
		return Id;
	}
	
	public String getJsonTrack(){
		return jsonTrack;		
	}
	
	public void setJsonTrack(String jsonTrack){
		this.jsonTrack = jsonTrack;
	}
	
}
