package com.ptdmc.modabilefe.entity;


public class CustomerMembers {
  private String customer_id;
  private String custmember_customer_id;
  private String custmember_role_id;

  public String getCustomer_id() {
    return customer_id;
  }

  public void setCustomer_id(String customer_id) {
    this.customer_id = customer_id;
  }

  public String getCustmember_customer_id() {
    return custmember_customer_id;
  }

  public void setCustmember_customer_id(String custmember_customer_id) {
    this.custmember_customer_id = custmember_customer_id;
  }

  public String getCustmember_role_id() {
    return custmember_role_id;
  }

  public void setCustmember_role_id(String custmember_role_id) {
    this.custmember_role_id = custmember_role_id;
  }
}
