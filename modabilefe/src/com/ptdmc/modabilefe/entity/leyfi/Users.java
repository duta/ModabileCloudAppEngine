package com.ptdmc.modabilefe.entity.leyfi;

import java.util.Date;

public class Users {

	public Integer user_id;

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public String user_name;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public byte[] password;
	public Date last_login;
	public Date last_logout;
}
