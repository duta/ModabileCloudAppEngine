package com.ptdmc.modabilefe.entity.leyfi;

public class Elements {

	public Integer element_id;

	public Integer getElement_id() {
		return element_id;
	}

	public void setElement_id(Integer element_id) {
		this.element_id = element_id;
	}

	public String element_type;

	public String getElement_type() {
		return element_type;
	}

	public void setElement_type(String element_type) {
		this.element_type = element_type;
	}
}
