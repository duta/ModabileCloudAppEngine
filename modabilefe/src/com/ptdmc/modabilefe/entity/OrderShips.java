package com.ptdmc.modabilefe.entity;

public class OrderShips {
	  private Long orddtl_id;
	  private Long ship_mmsi;
	  private Long ship_imo;
	  private String ship_name;
	  private String ship_callsign;

	  public Long getOrder_id() {
	    return orddtl_id;
	  }

	  public void setOrder_id(Long order_id) {
	    this.orddtl_id = order_id;
	  }

	  public Long getShip_mmsi() {
	    return ship_mmsi;
	  }

	  public void setShip_mmsi(Long ship_mmsi) {
	    this.ship_mmsi = ship_mmsi;
	  }

	  public Long getShip_imo() {
	    return ship_imo;
	  }

	  public void setShip_imo(Long ship_imo) {
	    this.ship_imo = ship_imo;
	  }

	  public String getShip_name() {
	    return ship_name;
	  }

	  public void setShip_name(String ship_name) {
	    this.ship_name = ship_name;
	  }

	  public String getShip_callsign() {
	    return ship_callsign;
	  }

	  public void setShip_callsign(String ship_callsign) {
	    this.ship_callsign = ship_callsign;
	  }
	}
