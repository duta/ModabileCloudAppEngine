package com.ptdmc.modabilefe.entity;

public class Customer {
	  private int customer_id;
	  private String customer_name;
	  private String customer_email;
	  private String customer_phone;
	  private String customer_country;
	  private String customer_company;
	  private String customer_logo;
	  private String customer_timezone;
	  private String customer_timeabbreviation;
	  private int customer_gmtoffset;
	  private java.sql.Timestamp customer_creationdate;
	  private java.sql.Timestamp customer_lastupdate;
	  private String customer_createby;
	  private String customer_lastupdateby;
	  private String customer_id_referenced;
	  private String productName;
	  private int orderDetailId;

	  public int getCustomer_id() {
	    return customer_id;
	  }

	  public void setCustomer_id(int customer_id) {
	    this.customer_id = customer_id;
	  }

	  public String getCustomer_name() {
	    return customer_name;
	  }

	  public void setCustomer_name(String customer_name) {
	    this.customer_name = customer_name;
	  }

	  public String getCustomer_email() {
	    return customer_email;
	  }

	  public void setCustomer_email(String customer_email) {
	    this.customer_email = customer_email;
	  }

	  public String getCustomer_phone() {
	    return customer_phone;
	  }

	  public void setCustomer_phone(String customer_phone) {
	    this.customer_phone = customer_phone;
	  }

	  public String getCustomer_country() {
	    return customer_country;
	  }

	  public void setCustomer_country(String customer_country) {
	    this.customer_country = customer_country;
	  }

	  public String getCustomer_company() {
	    return customer_company;
	  }

	  public void setCustomer_company(String customer_company) {
	    this.customer_company = customer_company;
	  }

	  public java.sql.Timestamp getCustomer_creationdate() {
	    return customer_creationdate;
	  }

	  public void setCustomer_creationdate(java.sql.Timestamp customer_creationdate) {
	    this.customer_creationdate = customer_creationdate;
	  }

	  public java.sql.Timestamp getCustomer_lastupdate() {
	    return customer_lastupdate;
	  }

	  public void setCustomer_lastupdate(java.sql.Timestamp customer_lastupdate) {
	    this.customer_lastupdate = customer_lastupdate;
	  }

	  public String getCustomer_createby() {
	    return customer_createby;
	  }

	  public void setCustomer_createby(String customer_createby) {
	    this.customer_createby = customer_createby;
	  }

	  public String getCustomer_lastupdateby() {
	    return customer_lastupdateby;
	  }

	  public void setCustomer_lastupdateby(String customer_lastupdateby) {
	    this.customer_lastupdateby = customer_lastupdateby;
	  }

	  public String getCustomer_id_referenced() {
	    return customer_id_referenced;
	  }

	  public void setCustomer_id_referenced(String customer_id_referenced) {
	    this.customer_id_referenced = customer_id_referenced;
	  }

	public String getCustomer_timezone() {
		return customer_timezone;
	}

	public void setCustomer_timezone(String customer_timezone) {
		this.customer_timezone = customer_timezone;
	}

	public String getCustomer_timeabbreviation() {
		return customer_timeabbreviation;
	}

	public void setCustomer_timeabbreviation(String customer_timeabbreviation) {
		this.customer_timeabbreviation = customer_timeabbreviation;
	}

	public int getCustomer_gmtoffset() {
		return customer_gmtoffset;
	}

	public void setCustomer_gmtoffset(int customer_gmtoffset) {
		this.customer_gmtoffset = customer_gmtoffset;
	}

	public String getCustomer_logo() {
		return customer_logo;
	}

	public void setCustomer_logo(String customer_logo) {
		this.customer_logo = customer_logo;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the orderDetailId
	 */
	public int getOrderDetailId() {
		return orderDetailId;
	}

	/**
	 * @param orderDetailId the orderDetailId to set
	 */
	public void setOrderDetailId(int orderDetailId) {
		this.orderDetailId = orderDetailId;
	}

}
