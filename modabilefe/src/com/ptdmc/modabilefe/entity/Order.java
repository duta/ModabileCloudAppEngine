package com.ptdmc.modabilefe.entity;

import java.sql.Timestamp;

public class Order {
  private Long order_id;
  private java.sql.Timestamp order_date;
  private String order_po_number;
  private java.sql.Timestamp order_creationdate;
  private java.sql.Timestamp order_lastupdate;
  private String order_createby;
  private String order_lastupdateby;
  private Long customer_id;

  public long getOrder_id() {
    return order_id;
  }

  public void setOrder_id(Long order_id) {
    this.order_id = order_id;
  }

  public Timestamp getOrder_date() {
    return order_date;
  }

  public void setOrder_date(Timestamp order_date) {
    this.order_date = order_date;
  }

  public String getOrder_po_number() {
    return order_po_number;
  }

  public void setOrder_po_number(String order_po_number) {
    this.order_po_number = order_po_number;
  }

  public Timestamp getOrder_creationdate() {
    return order_creationdate;
  }

  public void setOrder_creationdate(Timestamp order_creationdate) {
    this.order_creationdate = order_creationdate;
  }

  public Timestamp getOrder_lastupdate() {
    return order_lastupdate;
  }

  public void setOrder_lastupdate(Timestamp order_lastupdate) {
    this.order_lastupdate = order_lastupdate;
  }

  public String getOrder_createby() {
    return order_createby;
  }

  public void setOrder_createby(String order_createby) {
    this.order_createby = order_createby;
  }

  public String getOrder_lastupdateby() {
    return order_lastupdateby;
  }

  public void setOrder_lastupdateby(String order_lastupdateby) {
    this.order_lastupdateby = order_lastupdateby;
  }

  public Long getCustomer_id() {
    return customer_id;
  }

  public void setCustomer_id(Long customer_id) {
    this.customer_id = customer_id;
  }
}
