package com.ptdmc.modabilefe.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class AisDatasource {
  @Column(name="ad_id")
  private int ad_id;
  @Column(name="ad_url")
  private String ad_url;
  @Column(name="ad_vendorname")
  private String ad_vendorname;
  @Column(name="ad_urlusername")
  private String ad_urlusername;
  @Column(name="ad_urlpassword")
  private String ad_urlpassword;

  public int getAd_id() {
    return ad_id;
  }

  public void setAd_id(int ad_id) {
    this.ad_id = ad_id;
  }

  public String getAd_url() {
    return ad_url;
  }

  public void setAd_url(String ad_url) {
    this.ad_url = ad_url;
  }

  public String getAd_vendorname() {
    return ad_vendorname;
  }

  public void setAd_vendorname(String ad_vendorname) {
    this.ad_vendorname = ad_vendorname;
  }

  public String getAd_urlusername() {
    return ad_urlusername;
  }

  public void setAd_urlusername(String ad_urlusername) {
    this.ad_urlusername = ad_urlusername;
  }

  public String getAd_urlpassword() {
    return ad_urlpassword;
  }

  public void setAd_urlpassword(String ad_urlpassword) {
    this.ad_urlpassword = ad_urlpassword;
  }
}
