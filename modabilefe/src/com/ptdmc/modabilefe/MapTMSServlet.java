package com.ptdmc.modabilefe;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class MapTMSServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){
		RequestDispatcher jsp = req.getRequestDispatcher("./views/map/MobTMS.jsp");
		try {
			req.setAttribute("company", customer.getCustomer_company());
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
}
