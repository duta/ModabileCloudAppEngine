package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.data.leyfi.Roles;
import com.ptdmc.modabilefe.data.leyfi.Users;
import com.ptdmc.modabilefe.util.Mail;

public class SuperadminServlet extends MasterServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3132590381505543447L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		try {
			Factory factory = CallFactory.getInstance(req).getFactory();
			req.setAttribute("osList", factory.GetUsers().GetAllAdmin());
			req.getRequestDispatcher("/views/superadmin/index.jsp").forward(
					req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException {
		Factory factory = CallFactory.getInstance(req).getFactory();
		String path = ((HttpServletRequest) req).getRequestURI().toString();
		try {
			Roles roles = factory.GetRoles();
			Users users = factory.GetUsers();
			if (path.equals("/superadmin/add")) {
				// save user
				users.Upsert(null, req.getParameter("email"), new byte[0]);
				users.InsertUsersInRoles(
						roles.GetIDByName("superadmin (ADMIN)"),
						users.GetUserIDByUserName(req.getParameter("email")));
				// send email
				String[] s = PropertiesReader.getInstance().getProperties().getProperty("admin-notified").split(",");
				ArrayList<String> recipients = new ArrayList<String>();
				for (int i = 0; i < s.length; i++) {
					recipients.add(s[i]);
				}
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("creator", this.user.getEmail());
				parameters.put("created", req.getParameter("email"));
				parameters.put("page", "Daftar Superadmin");
				Mail.SendMail("noreply@modabile.appspotmail.com", recipients, "User baru Modabile App Engine", "resource/MailTemplate/NewUser.html", parameters);
			}
			if (path.startsWith("/superadmin/edit")) {
				// save user
				users.Upsert(Integer.parseInt(path.split("/")[3]), req.getParameter("email"), new byte[0]);
			}
			if (path.startsWith("/superadmin/delete")) {
				// save user
				users.Delete(Integer.parseInt(path.split("/")[3]));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			try {
				res.sendError(500, errors.toString());
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
	}

}
