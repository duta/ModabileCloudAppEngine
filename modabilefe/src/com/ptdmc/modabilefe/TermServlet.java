package com.ptdmc.modabilefe;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class TermServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {	
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		HttpSession dataSession = req.getSession();
		
		boolean local = "localhost".equals(req.getServerName());
		String loginUrl = userService.createLoginURL("/");
		String logoutUrl = userService.createLogoutURL("/");
		
		req.setAttribute("user", user);
		req.setAttribute("loginUrl", loginUrl);
		req.setAttribute("logoutUrl", logoutUrl);
		res.setContentType("text/html");
		RequestDispatcher jsp = null;
		if (req.getRequestURI().toLowerCase().equals("/terms/index")) {
			jsp = req.getRequestDispatcher("/views/terms/terms.jsp");
		} else if (req.getRequestURI().toLowerCase().equals("/terms/privacy")) {
			jsp = req.getRequestDispatcher("/views/terms/privacypolicy.jsp");
		} 

		try {
			if (jsp != null) {
				jsp.forward(req, res);
			}
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
