package com.ptdmc.modabilefe;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class FunctionM {

	public static String getFormatRp(double harga){
		DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
		DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(harga);
	}
	
	public static int getTimestamp(String DataTime){
		System.out.println(DataTime);
        String time = DataTime.substring(Math.max(DataTime.length() -5, 0));
        String plusOrMinus = DataTime.substring(0, Math.min(DataTime.length(), 1));
        
        String[] units = time.split(":"); //will break the string up into an array
        int hours = Integer.parseInt(units[0]); //first element
        int minutes = Integer.parseInt(units[1]); //second element
        int duration = (60 * 60 *  hours) + (minutes * 60); //add up our values
        
        int rslt = 0;
        if(!plusOrMinus.isEmpty() && plusOrMinus.equals("-")) {
            rslt = duration * -1;
        }else{
        	rslt = duration;
        }
        
        return rslt;
	}
}
