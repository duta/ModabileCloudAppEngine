package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {
	private Properties prop;

	private static PropertiesReader instance = null;

	private PropertiesReader() {
		// Exists only to defeat instantiation.
	}

	public static PropertiesReader getInstance() {
		if(instance == null) {	    	

			instance = new PropertiesReader();
		}
		return instance;
	}

	public Properties getProperties(){		
		InputStream is= null;
		try {
			is =  this.getClass().getClassLoader().getResourceAsStream("config.properties");
			prop = new Properties();
			prop.load(is);

		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return prop;
	}

}
