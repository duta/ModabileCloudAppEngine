package com.ptdmc.modabilefe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.leyfi.Elements;
import com.ptdmc.modabilefe.entity.leyfi.Roles;

@SuppressWarnings("serial")
public class MasterServlet extends javax.servlet.http.HttpServlet {
	public User user;
	public Customer customer;
	public ArrayList<Roles> roles;
	public ArrayList<Elements> elements;

	@Override
	public void service(ServletRequest req, ServletResponse res)
			throws ServletException, IOException {
		String path = ((HttpServletRequest) req).getRequestURI().toString();
		UserService userService = UserServiceFactory.getUserService();
		user = userService.getCurrentUser();

		String loginUrl = userService.createLoginURL("/");
		String logoutUrl = userService.createLogoutURL("/");

		req.setAttribute("user", user);
		req.setAttribute("loginUrl", loginUrl);
		req.setAttribute("logoutUrl", logoutUrl);
		req.setAttribute("nameserver", req.getServerName());
		if (user != null){
			req.setAttribute("useremail", user.getEmail());
		}
		else {
			req.setAttribute("useremail", "not login yet");	
		}
		res.setContentType("text/html");
		
		try {
			Factory f = CallFactory.getInstance((HttpServletRequest) req).getFactory();
			if (user != null) {
				Integer user_id = f.GetUsers().GetUserIDByUserName(user.getEmail());
				customer = f.GetCustomer().GetByEmail(user.getEmail());
				if (customer == null){
					customer = f.GetCustomerMember().getCustomerByUserID(user_id);
				}
				
				// checking user ini apakah sesuai role-nya
				roles = f.GetUsers().GetRolesByUserID(user_id);
				//roles.addAll(f.GetCustomerMember().GetRolesByUserID(user_id));
				elements = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements>();
				for (com.ptdmc.modabilefe.entity.leyfi.Roles role : roles) {
					elements.addAll(f.GetRoles().GetElementsPermittedByRoleID(role.role_id));
					if (role.role_name.equals("superadmin (ADMIN)")){
						customer = new Customer();
						customer.setCustomer_id(-1);
						customer.setCustomer_name("Superadmin");
					}
				}
			} else {
				elements = f.GetRoles().GetElementsPermittedByRoleID(CallFactory.getInstance((HttpServletRequest) req).getFactory().GetRoles().GetIDByName("Guest"));
			}

			boolean Pass = false;

			for (com.ptdmc.modabilefe.entity.leyfi.Elements element : elements) {
				if (Pattern.compile(element.element_type.toLowerCase().trim()).matcher(path.toLowerCase().trim()).find())
					Pass = true;
			}
			if (!Pass) {
				RequestDispatcher jsp = req
						.getRequestDispatcher("/views/403.jsp");
				req.setAttribute("path", path);
				jsp.forward(req, res);
			} else {
				super.service(req, res);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
			req.setAttribute("ex", ex.getMessage());
			jsp.forward(req, res);
		}
	}
}
