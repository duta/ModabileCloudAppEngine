package com.ptdmc.modabilefe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.data.CustomerMemberRepo;
import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.data.leyfi.Roles;
import com.ptdmc.modabilefe.data.leyfi.Users;
import com.ptdmc.modabilefe.entity.CustomerMembers;
import com.ptdmc.modabilefe.util.Mail;

public class CustomerMemberServlet extends MasterServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3132590381505543447L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		try {
			Factory factory = CallFactory.getInstance(req).getFactory();
			int cid = -1;
			boolean isadmin = false;
			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
				if (role.role_name.equals("superadmin (ADMIN)")){
					isadmin = true;
				}
			}
			try{
			if (isadmin)
				cid = Integer.parseInt(((HttpServletRequest) req).getRequestURI().toString().split("/")[2]);
			else
				cid = this.customer.getCustomer_id();
			} catch(Exception ex){
				
			}			
			List<CustomerMembers> list = factory.GetCustomerMember().GetAllByCustomerID(cid);
			Map<com.ptdmc.modabilefe.entity.leyfi.Users, com.ptdmc.modabilefe.entity.leyfi.Roles> osList = new HashMap<com.ptdmc.modabilefe.entity.leyfi.Users, com.ptdmc.modabilefe.entity.leyfi.Roles>();
			for (CustomerMembers customerMembers : list) {
				osList.put(factory.GetUsers().GetUserByUserID(Integer.parseInt(customerMembers.getCustmember_customer_id())), factory.GetRoles().GetRoleByID(Integer.parseInt(customerMembers.getCustmember_role_id())));
			}
			req.setAttribute("osList", osList);
			req.getRequestDispatcher("/views/customermember/index.jsp").forward(req, res);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException {
		Factory factory = CallFactory.getInstance(req).getFactory();
		String path = ((HttpServletRequest) req).getRequestURI().toString();
		try {
			int cid = -1;
			boolean isadmin = false;
			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
				if (role.role_name.equals("superadmin (ADMIN)")){
					isadmin = true;
				}
			}
			try{
			if (isadmin)
				cid = Integer.parseInt(((HttpServletRequest) req).getRequestURI().toString().split("/")[2]);
			else
				cid = this.customer.getCustomer_id();
			} catch(Exception ex){
				
			}	
			Roles roles = factory.GetRoles();
			Users users = factory.GetUsers();
			if (path.equals("/customermember/add")) {
				// save user
				CustomerMemberRepo cmr = factory.GetCustomerMember();
				users.Upsert(null, req.getParameter("email"), new byte[0]);
				users.InsertUsersInRoles(
						roles.GetIDByName(req.getParameter("role")),
						users.GetUserIDByUserName(req.getParameter("email")));
				cmr.InsertCustomerRole(
						roles.GetIDByName(req.getParameter("role")), cid, 
						users.GetUserIDByUserName(req.getParameter("email")));
				// send email
				String[] s = PropertiesReader.getInstance().getProperties().getProperty("admin-notified").split(",");
				ArrayList<String> recipients = new ArrayList<String>();
				for (int i = 0; i < s.length; i++) {
					recipients.add(s[i]);
				}
				HashMap<String, String> parameters = new HashMap<String, String>();
				parameters.put("creator", this.user.getEmail());
				parameters.put("created", req.getParameter("email"));
				parameters.put("page", "Customer Member");
				Mail.SendMail("noreply@modabile.appspotmail.com", recipients, "User baru Modabile App Engine", "resource/MailTemplate/NewUser.html", parameters);
			}
			if (path.startsWith("/customermember/edit")) {
				// save user
				CustomerMemberRepo cmr = factory.GetCustomerMember();
				users.Upsert(Integer.parseInt(path.split("/")[3]), req.getParameter("email"), new byte[0]);
				ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> userRoles = CallFactory.getInstance(req)
						.getFactory().GetUsers().GetRolesByUserID(Integer.parseInt(path.split("/")[3]));
				for (com.ptdmc.modabilefe.entity.leyfi.Roles role : userRoles) {
					CallFactory.getInstance(req).getFactory().GetUsers()
							.DeleteUsersInRoles(role.role_id, Integer.parseInt(path.split("/")[3]));
				}
				users.InsertUsersInRoles(
						roles.GetIDByName(req.getParameter("role")),
						Integer.parseInt(path.split("/")[3]));
				factory.GetCustomerMember().Update(cid, Integer.parseInt(path.split("/")[3]), roles.GetIDByName(req.getParameter("role")));
			}
			if (path.startsWith("/customermember/delete")) {
				// save user
				users.Delete(Integer.parseInt(path.split("/")[3]));
				factory.GetCustomerMember().Delete(Integer.parseInt(path.split("/")[3]));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
