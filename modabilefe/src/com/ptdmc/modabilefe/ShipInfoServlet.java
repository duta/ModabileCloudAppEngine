package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.entity.ShipInfo;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

import java.nio.channels.Channels;

public class ShipInfoServlet extends MasterServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -541778179512310877L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		String path = req.getRequestURI();
		String[] next = path.split("/");
		if (next.length <= 2) {
			ShowPage(req, res);
		}
	}
	
	  /**
	   * Transfer the data from the inputStream to the outputStream. Then close both streams.
	   */
	  private void copy(InputStream input, OutputStream output) throws IOException {
	    try {
	      byte[] buffer = new byte[2 * 1024 * 1024];
	      int bytesRead = input.read(buffer);
	      while (bytesRead != -1) {
	        output.write(buffer, 0, bytesRead);
	        bytesRead = input.read(buffer);
	      }
	    } finally {
	      input.close();
	      output.close();
	    }
	  }
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		String path = req.getRequestURI();
		String[] next = path.split("/");
		if (next[2].equals("add")) {
			insertKapal(req, res);
		}
		if (next[2].equals("edit")) {
			updateKapal(next[3], req, res);
		}
		if (next[2].equals("delete")) {
			deleteKapal(next[3], req);
		}
		if (next[2].equals("image")) {
			GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
		      .initialRetryDelayMillis(10)
		      .retryMaxAttempts(10)
		      .totalRetryPeriodMillis(15000)
		      .build());
			GcsFileOptions instance = GcsFileOptions.getDefaultInstance();
			  GcsFilename fileName = new GcsFilename("modabile.appspot.com", "vessel_image_"+next[3]+".png");
			  GcsOutputChannel outputChannel;
			  try {
				outputChannel = gcsService.createOrReplace(fileName, instance);
			    ServletFileUpload upload = new ServletFileUpload();
			    FileItemIterator iter = upload.getItemIterator(req);
			    FileItemStream imageItem = iter.next();
			    InputStream imgStream = imageItem.openStream();
			  copy(imgStream, Channels.newOutputStream(outputChannel));
			  res.sendRedirect("/shipInfo/");
				} catch (IOException | FileUploadException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

		}
	}
	
	private void updateKapal(String mmsi, HttpServletRequest req,
			HttpServletResponse res) {
		Factory factory = CallFactory.getInstance(req).getFactory();
		try{
			ShipInfo si = new ShipInfo();
			si.setVesselname(req.getParameter("Name"));
			si.setMmsi(Long.parseLong(req.getParameter("MMSI")));
			si.setImonumber(Long.parseLong(req.getParameter("IMO")));
			si.setCallsign(req.getParameter("CallSign"));
			si.setDimensiontobow(req.getParameter("Bow"));
			si.setDimensiontostern(req.getParameter("Stern"));
			si.setDimensiontoport(req.getParameter("Port"));
			si.setDimensiontostarboard(req.getParameter("StarBoard"));
			si.setGrt(Long.parseLong(req.getParameter("GRT")));
			si.setEnginehp(Long.parseLong(req.getParameter("EHP")));
			si.setEnginediesel(req.getParameter("ED"));
			si.setDte("true".equals(req.getParameter("ED"))?1L:0L);
			si.setShiptype(Long.parseLong(req.getParameter("ShipType")));
			si.setShiptypedesc(req.getParameter("ShipTypeDesc"));
			factory.GetShipInfo().update(si, user.getEmail(), Integer.parseInt(mmsi));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void deleteKapal(String mmsi, HttpServletRequest req) {
		CallFactory.getInstance(req).getFactory().GetShipInfo().delete(Integer.parseInt(mmsi));
		
	}

	private void insertKapal(HttpServletRequest req, HttpServletResponse res) {
		Factory factory = CallFactory.getInstance(req).getFactory();
		try{
			ShipInfo si = new ShipInfo();
			si.setVesselname(req.getParameter("Name"));
			si.setMmsi(Long.parseLong(req.getParameter("MMSI")));
			si.setImonumber(Long.parseLong(req.getParameter("IMO")));
			si.setCallsign(req.getParameter("CallSign"));
			si.setDimensiontobow(req.getParameter("Bow"));
			si.setDimensiontostern(req.getParameter("Stern"));
			si.setDimensiontoport(req.getParameter("Port"));
			si.setDimensiontostarboard(req.getParameter("StarBoard"));
			si.setGrt(Long.parseLong(req.getParameter("GRT")));
			si.setEnginehp(Long.parseLong(req.getParameter("EHP")));
			si.setEnginediesel(req.getParameter("ED"));
			si.setDte("true".equals(req.getParameter("ED"))?1L:0L);
			si.setShiptype(Long.parseLong(req.getParameter("ShipType")));
			si.setShiptypedesc(req.getParameter("ShipTypeDesc"));
			factory.GetShipInfo().insert(si, user.getEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void ShowPage(HttpServletRequest req, HttpServletResponse res) {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/shipinfo/index.jsp");
		try {
			req.setAttribute("osList", CallFactory.getInstance(req).getFactory().GetShipInfo().getAllComplete());
			jsp.forward(req, res);
		} catch (ServletException | IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
