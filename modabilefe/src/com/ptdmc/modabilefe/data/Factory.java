package com.ptdmc.modabilefe.data;

import java.util.Properties;
import com.ptdmc.modabilefe.data.leyfi.*;

/**
 * @author anang.bakti
 */
public class Factory {
	
	private DBProvider dbProvider;
	private static Factory instance = null;
	   	
	public static Factory getInstance(Boolean isLocal, Properties prop) {
	      if(instance == null) {	    	  
	         instance = new Factory(isLocal,prop);
	      }
	      return instance;
	   }
	
	public Factory(Boolean isLocal, Properties prop){
		if (isLocal){			
			CallDBProvider.getLocal(prop);
			this.dbProvider = CallDBProvider.dbProvider;
		}else{
			CallDBProvider.getServer(prop);
			this.dbProvider = CallDBProvider.dbProvider;
		}		
	}

	public CustomerRepo GetCustomer(){
		return new CustomerRepo(dbProvider);		
	}
	
	public ProductRepo GetProduct(){
		return new ProductRepo(dbProvider);		
	}
	
	public IncomingAisRepo GetIncomingAis() {
		return new IncomingAisRepo(dbProvider);
	}
	
	public ShipInfoRepo GetShipInfo() {
		return new ShipInfoRepo(dbProvider);
	}
	
	// TODO: Factory Order
	public OrdersRepo GetOrders(){
		return new OrdersRepo(dbProvider);		
	}
	
	public OrderDetailsRepo GetOrderDetails(){
		return new OrderDetailsRepo(dbProvider);		
	}
	
	public OrderDetailsStatusRepo GetOrderDetailsStatus(){
		return new OrderDetailsStatusRepo(dbProvider);		
	}

	public ProductPriceRepo GetProductPrice(){
		return new ProductPriceRepo(dbProvider);		
	}

	public OrderShipsRepo GetOrderShips(){
		return new OrderShipsRepo(dbProvider);		
	}
	
	public OrderSeqRepo GetOrderSeq(){
		return new OrderSeqRepo(dbProvider);		
	}
	
	public KonfigurasiRepo GetKonfigurasi(){
		return new KonfigurasiRepo(dbProvider);
	}
	
	public AisDatasourceRepo GetAisDataSource(){
		return new AisDatasourceRepo(dbProvider);
	}

	public CustomerMemberRepo GetCustomerMember() {
		return new CustomerMemberRepo(dbProvider);
	}

	public ShipMislocationRepo GetShipMislocation() {
		return new ShipMislocationRepo(dbProvider);
	}

	public Users GetUsers() {
		return new Users(dbProvider);
	}

	public Roles GetRoles() {
		return new Roles(dbProvider);
	}

	public Elements GetElements() {
		return new Elements(dbProvider);
	}
	
}
