package com.ptdmc.modabilefe.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ptdmc.modabilefe.data.ResultSetMapper.ResultSetMapperException;
import com.ptdmc.modabilefe.entity.AisDatasource;

public class AisDatasourceRepo {
	DBProvider dbProvider;

	public AisDatasourceRepo(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}

	public AisDatasource GetById(int id) throws SQLException {
		AisDatasource ads = new AisDatasource();
		String qString = "select * from ais_datasource where ad_id=? ";
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(qString);
			pst.setObject(1, id);
			
			rs = pst.executeQuery();
			while(rs.next()){
				ads.setAd_id(rs.getInt("ad_id"));
				ads.setAd_url(rs.getString("ad_url"));
				ads.setAd_urlusername(rs.getString("ad_urlusername"));
				ads.setAd_urlpassword(rs.getString("ad_urlpassword"));
				ads.setAd_vendorname(rs.getString("ad_vendorname"));
			}

		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return ads;
	}
	
	public List<AisDatasource> GetAll() throws SQLException {
		List<AisDatasource> result = new ArrayList<AisDatasource>();
		String qString = "select * from ais_datasource ";
		Connection conn = null;
		ResultSet rs = null;
		java.sql.Statement st = null;
		try {
			conn = dbProvider.OpenConnection();
		
			conn.setAutoCommit(false);
			st = conn.createStatement();			

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery(qString);	
			while(rs.next()){
				AisDatasource ads = new AisDatasource();
				ads.setAd_id(rs.getInt("ad_id"));
				ads.setAd_url(rs.getString("ad_url"));
				ads.setAd_urlusername(rs.getString("ad_urlusername"));
				ads.setAd_urlpassword(rs.getString("ad_urlpassword"));
				ads.setAd_vendorname(rs.getString("ad_vendorname"));
				result.add(ads);
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public int Insert(AisDatasource ads) throws SQLException {
		int result = 0;
		StringBuilder sb = new StringBuilder();
		sb.append(" insert into ais_datasource (ad_url, ad_vendorname, ad_urlusername, ad_urlpassword,ad_id) ");
		sb.append(" values (?,?,?,?,?) ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, ads.getAd_url());
			pst.setObject(2, ads.getAd_vendorname());
			pst.setObject(3, ads.getAd_urlusername());
			pst.setObject(4, ads.getAd_urlpassword());
			pst.setInt(5, GetNextID());
			result = pst.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		if (pst != null) {
			pst.close();
		}
		if (conn != null) {
			conn.close();
		}
	}
		return result;
	}
	public Integer GetNextID() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  (max(ad_id)+1) ad_id ");
		stringBuilder.append(" from  ais_datasource ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("ad_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}

	public int UpdateById(AisDatasource ads) throws SQLException {
		int result = 0;
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ais_datasource ");
		sb.append("   SET ad_url=?, ad_vendorname=?, ad_urlusername=?, ad_urlpassword=? ");
		sb.append(" WHERE ad_id=? ");
		
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, ads.getAd_url());
			pst.setObject(2, ads.getAd_vendorname());
			pst.setObject(3, ads.getAd_urlusername());
			pst.setObject(4, ads.getAd_urlpassword());
			pst.setObject(5, ads.getAd_id());
			result = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}

	public void Delete(AisDatasource p) throws SQLException {
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("delete from ais_datasource where ad_id = ? ");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getAd_id()));
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}
