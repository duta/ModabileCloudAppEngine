package com.ptdmc.modabilefe.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBProvider {
	private String connString;
	private String user;
	private String password;
	
	
	
	public DBProvider(String connString, String user, String password){		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //Don't forget to load the driver 
		this.connString = connString;
		this.user = user;
		this.password = password;
	}
	
	public Connection OpenConnection(){
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(this.connString, this.user, this.password);			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}           
		return conn;
	}
	
	
}