package com.ptdmc.modabilefe.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderSeqRepo {
	
	DBProvider dbProvider;
	
	public OrderSeqRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public int getLastValue() throws SQLException {
		int result = -1;
		StringBuilder sb = new StringBuilder();
        sb.append("SELECT last_value FROM order_seq");
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			
			ResultSet returnSql = st.executeQuery();
			if (returnSql.next()) {
		        result = returnSql.getInt("last_value");
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
}
