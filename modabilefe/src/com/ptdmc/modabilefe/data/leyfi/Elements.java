package com.ptdmc.modabilefe.data.leyfi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ptdmc.modabilefe.data.DBProvider;

public class Elements {
	DBProvider dbProvider;

	public Elements(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}

	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> GetAll() throws SQLException {
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements>();
		stringBuilder.append(" select ");
		stringBuilder.append(" \telement_id,element_type  ");
		stringBuilder.append(" from moresecure.elements  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Elements e = new com.ptdmc.modabilefe.entity.leyfi.Elements();
				e.element_id = (Integer) (rs.getInt("element_id"));
				e.element_type = rs.getString("element_type");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}

	public int Upsert(Integer element_id, String element_type)
			throws SQLException {
		StringBuilder sb = new StringBuilder();
		if (element_id != null) {
			sb.append("update moresecure.elements set element_type=? ");
			sb.append("    where element_id = ?");
		} else {
			sb.append("insert into moresecure.elements (element_type,element_id) ");
			sb.append(" values (?,?) ");
		}

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setString(1, element_type);
			if (element_id != null) {
				st.setInt(2, element_id);
			} else {
				st.setInt(2, GetNextID());
			}
			res = st.executeUpdate();
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		return res;
	}
	public Integer GetNextID() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  (max(element_id)+1) element_id ");
		stringBuilder.append(" from  moresecure.elements ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("element_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}

	public int Delete(int element_id) throws SQLException {
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("delete from moresecure.elements where element_id = ? ");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, element_id);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
}
