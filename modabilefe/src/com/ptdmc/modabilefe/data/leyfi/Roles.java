package com.ptdmc.modabilefe.data.leyfi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ptdmc.modabilefe.data.DBProvider;

public class Roles {

	DBProvider dbProvider;

	public Roles(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> GetAll() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();
		stringBuilder.append(" select ");
		stringBuilder.append(" \trole_id,role_name,role_description  ");
		stringBuilder.append(" from moresecure.roles  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Roles e = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				e.role_id = (Integer) (rs.getInt("role_id"));
				e.role_name = rs.getString("role_name");
				e.role_description = rs.getString("role_description");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public com.ptdmc.modabilefe.entity.leyfi.Roles GetRoleByID(int user_id) throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select ");
		stringBuilder.append(" role_id,role_name");
		stringBuilder.append(" from moresecure.roles  ");
		stringBuilder.append(" where role_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			if (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Roles e = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				e.role_id = (Integer) (rs.getInt("role_id"));
				e.role_name = rs.getString("role_name");
				return e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return null;
	}
	public Integer GetIDByName(String role_name) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  role_id ");
		stringBuilder.append(" from  moresecure.roles ");
		stringBuilder.append(" where upper(role_name) = upper(?)  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setString(1, role_name);
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("role_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return -1;
	}
	public int Upsert(Integer role_id, String role_name, String role_description) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (role_id != null)
		{
			stringBuilder.append("update moresecure.roles set role_name= ?, role_description= ? ");
			stringBuilder.append("    where role_id = ?");
		}
		else
		{
			stringBuilder.append("insert into moresecure.roles (role_name, role_description, role_id) ");
			stringBuilder.append(" values (?, ?, ?) ");
		}

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, role_name);
			st.setString(2, role_description);
			if (role_id != null) {
				st.setInt(3, role_id);
			} else {
				st.setInt(3, GetNextID());
			}
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public Integer GetNextID() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  (max(role_id)+1) role_id ");
		stringBuilder.append(" from  moresecure.roles ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("role_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}
	public int Delete(int role_id) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from moresecure.permissions where role_id = ? ");
			pst.setObject(1, role_id);
			res = pst.executeUpdate();
			pst.close();
			pst = conn.prepareStatement("delete from moresecure.usersinroles where role_id = ? ");
			pst.setObject(1, role_id);
			res = pst.executeUpdate();
			pst.close();
			pst = conn.prepareStatement("delete from moresecure.roles where role_id = ? ");
			pst.setObject(1, role_id);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Permissions> GetPermissions() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Permissions> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Permissions>();
		stringBuilder.append(this.QueryGetPermissions());
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Permissions p = new com.ptdmc.modabilefe.entity.leyfi.Permissions();
				com.ptdmc.modabilefe.entity.leyfi.Elements e = new com.ptdmc.modabilefe.entity.leyfi.Elements();
				e.element_id = (Integer) (rs.getInt("element_id"));
				e.element_type = rs.getString("element_type");
				p.elements = e;
				com.ptdmc.modabilefe.entity.leyfi.Roles r = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				r.role_id = (Integer) (rs.getInt("role_id"));
				r.role_name = rs.getString("role_name");
				r.role_description = rs.getString("role_description");
				p.roles = r;
				list.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	private String QueryGetPermissions()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  ");
		stringBuilder.append("     r.*, e.element_id,e.element_type ");
		stringBuilder.append(" from moresecure.roles r left join moresecure.permissions p on r.role_id = p.role_id ");
		stringBuilder.append("             left join moresecure.elements e on p.element_id = e.element_id ");
		return stringBuilder.toString();
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> GetElementsPermittedByRoleID(int role_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements>();
		stringBuilder.append(" select   ");
		stringBuilder.append("     e.element_id,e.element_type  ");
		stringBuilder.append(" from moresecure.roles r join moresecure.permissions p on r.role_id = p.role_id  ");
		stringBuilder.append("   join moresecure.elements e on p.element_id = e.element_id  ");
		stringBuilder.append(" where r.role_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, role_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Elements e = new com.ptdmc.modabilefe.entity.leyfi.Elements();
				e.element_id = (Integer) (rs.getInt("element_id"));
				e.element_type = rs.getString("element_type");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public int InsertPermission(int role_id, int element_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("insert into moresecure.permissions (role_id, element_id) ");
		stringBuilder.append(" values (?, ?) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, role_id);
			st.setInt(2, element_id);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public boolean ThisRoleHavePermissionFor(int role_id, int element_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select count(*) jml ");
		stringBuilder.append(" from  moresecure.permissions ");
		stringBuilder.append(" where role_id = ? and element_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, role_id);
			pst.setInt(2, element_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Elements e = new com.ptdmc.modabilefe.entity.leyfi.Elements();
				return rs.getInt("jml") != 0;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return false;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> AvailableElementsForRoleID(int role_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Elements>();
		stringBuilder.append(" select element_id, element_type  ");
		stringBuilder.append(" from moresecure.elements ");
		stringBuilder.append(" where element_id not in ");
		stringBuilder.append(" ( ");
		stringBuilder.append(" select   ");
		stringBuilder.append(" \te.element_id ");
		stringBuilder.append(" from moresecure.roles r join moresecure.permissions p on r.role_id = p.role_id  ");
		stringBuilder.append("   join moresecure.elements e on p.element_id = e.element_id  ");
		stringBuilder.append(" where r.role_id = ? ");
		stringBuilder.append(" ) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, role_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Elements e = new com.ptdmc.modabilefe.entity.leyfi.Elements();
				e.element_id = (Integer) (rs.getInt("element_id"));
				e.element_type = rs.getString("element_type");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public int DeletePermission(int role_id, int element_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("delete from  moresecure.permissions ");
		stringBuilder.append(" where role_id = ? and element_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setObject(1, role_id);
			pst.setObject(2, element_id);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> UsersHaveThisRole(int role_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users>();
		stringBuilder.append(" select uir.user_id, u.user_name ");
		stringBuilder.append(" from moresecure.roles r join moresecure.usersinroles uir on r.role_id = uir.role_id ");
		stringBuilder.append("                         join moresecure.users u on uir.user_id = u.user_id ");
		stringBuilder.append(" where r.role_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Users e = new com.ptdmc.modabilefe.entity.leyfi.Users();
				e.user_id = (Integer) (rs.getInt("user_id"));
				e.user_name = rs.getString("user_name");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
}
