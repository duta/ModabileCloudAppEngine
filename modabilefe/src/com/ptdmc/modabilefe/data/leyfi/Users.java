package com.ptdmc.modabilefe.data.leyfi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ptdmc.modabilefe.data.DBProvider;

public class Users {

	DBProvider dbProvider;
	private SimpleDateFormat parserSDF;

	public Users(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
		parserSDF=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> GetAllAdmin() throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users>();
		stringBuilder.append(" select ");
		stringBuilder.append(" u.user_id,user_name, to_char(last_login, 'dd-mm-yyyy HH24:mi:ss') last_login, to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss') last_logout ");
		stringBuilder.append(" from moresecure.users u ");
		stringBuilder.append(" join moresecure.usersinroles uir on u.user_id=uir.user_id and uir.role_id = (select role_id from moresecure.roles where role_name = 'superadmin (ADMIN)') ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			String query = stringBuilder.toString();
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Users e = new com.ptdmc.modabilefe.entity.leyfi.Users();
				e.user_id = (Integer) (rs.getInt("user_id"));
				e.user_name = rs.getString("user_name");
				if (rs.getString("last_login") != null) 
					e.last_login = parserSDF.parse(rs.getString("last_login"));
				if (rs.getString("last_logout") != null)
					e.last_logout = parserSDF.parse(rs.getString("last_logout"));
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> GetAll() throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Users>();
		stringBuilder.append(" select ");
		stringBuilder.append(" \tuser_id,user_name, to_char(last_login, 'dd-mm-yyyy HH24:mi:ss') last_login, to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss') last_logout ");
		stringBuilder.append(" from moresecure.users  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Users e = new com.ptdmc.modabilefe.entity.leyfi.Users();
				e.user_id = (Integer) (rs.getInt("user_id"));
				e.user_name = rs.getString("user_name");
				if (rs.getString("last_login") != null) 
					e.last_login = parserSDF.parse(rs.getString("last_login"));
				if (rs.getString("last_logout") != null)
					e.last_logout = parserSDF.parse(rs.getString("last_logout"));
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public int Delete(int user_id) throws SQLException
	{

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from moresecure.usersinroles where user_id = ? ");
			pst.setObject(1, user_id);
			res = pst.executeUpdate();
			pst.close();
			pst = conn.prepareStatement("delete from moresecure.users where user_id = ? ");
			pst.setObject(1, user_id);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public int InsertUsersInRoles(int role_id, int user_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("insert into moresecure.usersinroles (role_id, user_id) ");
		stringBuilder.append(" values (?, ?) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, role_id);
			st.setInt(2, user_id);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public int DeleteUsersInRoles(int role_id, int user_id) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from moresecure.usersinroles where role_id = ? and user_id = ? ");
			pst.setObject(1, role_id);
			pst.setObject(2, user_id);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public int Upsert(Integer user_id, String user_name, byte[] password) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		if (user_id != null)
		{
			stringBuilder.append("update moresecure.users set user_name= ?, password = ? ");
			stringBuilder.append("    where user_id = ?");
		}
		else
		{
			stringBuilder.append("insert into moresecure.users (user_name, password, user_id) ");
			stringBuilder.append(" values (?, ?, ? ) ");
		}

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, user_name);
			st.setBytes(2, password);
			if (user_id != null) {
				st.setInt(3, user_id);
			} else {
				st.setInt(3, GetNextID());
			}
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> GetRolesByUserID(int user_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select ");
		stringBuilder.append("     r.* ");
		stringBuilder.append(" from moresecure.users u  join moresecure.usersinroles uir on u.user_id = uir.user_id ");
		stringBuilder.append("      join moresecure.roles r on uir.role_id = r.role_id ");
		stringBuilder.append(" where u.user_id = ?  ");
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Roles e = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				e.role_id = (Integer) (rs.getInt("role_id"));
				e.role_name = rs.getString("role_name");
				e.role_description = rs.getString("role_description");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> AvailableRolesForUserID(int user_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  ");
		stringBuilder.append("   r.*\t ");
		stringBuilder.append(" from moresecure.roles r ");
		stringBuilder.append(" where  ");
		stringBuilder.append("   r.role_id not in ( ");
		stringBuilder.append(" \tselect ");
		stringBuilder.append(" \t\tr.role_id ");
		stringBuilder.append(" \tfrom moresecure.users u join moresecure.usersinroles uir on u.user_id = uir.user_id ");
		stringBuilder.append("          join moresecure.roles r on uir.role_id = r.role_id ");
		stringBuilder.append("     where u.user_id = ? ");
		stringBuilder.append(" ) ");
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Roles e = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				e.role_id = (Integer) (rs.getInt("role_id"));
				e.role_name = rs.getString("role_name");
				e.role_description = rs.getString("role_description");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public boolean LastLoginExpired(int user_id, int max_hour_expired) throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  to_char(last_login, 'dd-mm-yyyy HH24:mi:ss')  last_login ");
		stringBuilder.append(" from  moresecure.users ");
		stringBuilder.append(" where user_id = ?  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				if (rs.getString("last_login") != null)
					return parserSDF.parse(rs.getString("last_login")).getTime() - new Date().getTime() > ((double) max_hour_expired * 60 * 60 * 1000);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return true;
	}
	public boolean LastLogoutNull(int user_id) throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss')  last_logout ");
		stringBuilder.append(" from  moresecure.users ");
		stringBuilder.append(" where user_id = ?  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			while (rs.next()) {
				if (rs.getString("last_logout") != null)
					return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return true;
	}
	public int SetUserLogOut(int user_id) throws SQLException
	{
		return this.SetUserLog(user_id, false);
	}
	public int SetUserLogIn(int user_id) throws SQLException
	{
		return this.SetUserLog(user_id, true);
	}
	private int SetUserLog(int user_id, boolean isLogin) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("update moresecure.users set ");
		if (isLogin)
		{
			stringBuilder.append(" last_login = now(), last_logout = NULL ");
		}
		else
		{
			stringBuilder.append(" last_logout= now() ");
		}

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, user_id);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public Integer GetNextID() throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  (max(user_id)+1) user_id ");
		stringBuilder.append(" from  moresecure.users ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("user_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}
	public Integer GetUserIDByUserName(String user_name) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select  user_id ");
		stringBuilder.append(" from  moresecure.users ");
		stringBuilder.append(" where upper(user_name) = upper(?)  ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setString(1, user_name);
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("user_id");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return -1;
	}
	public com.ptdmc.modabilefe.entity.leyfi.Users GetUserByUserID(int user_id) throws Exception
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select ");
		stringBuilder.append(" \tuser_id,user_name, to_char(last_login, 'dd-mm-yyyy HH24:mi:ss') last_login, to_char(last_logout, 'dd-mm-yyyy HH24:mi:ss') last_logout, password ");
		stringBuilder.append(" from moresecure.users  ");
		stringBuilder.append(" where user_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			if (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Users e = new com.ptdmc.modabilefe.entity.leyfi.Users();
				e.user_id = (Integer) (rs.getInt("user_id"));
				e.user_name = rs.getString("user_name");
				if (rs.getString("last_login") != null) 
					e.last_login = parserSDF.parse(rs.getString("last_login"));
				if (rs.getString("last_logout") != null)
					e.last_logout = parserSDF.parse(rs.getString("last_logout"));
				return e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return null;
	}
}
