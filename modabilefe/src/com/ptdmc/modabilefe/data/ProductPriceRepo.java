package com.ptdmc.modabilefe.data;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.ptdmc.modabilefe.entity.Product;
import com.ptdmc.modabilefe.entity.ProductPrice;

public class ProductPriceRepo {
	DBProvider dbProvider;
	
	public ProductPriceRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public List<ProductPrice> GetWhere(Product p) throws SQLException, ParseException
	{
		List<ProductPrice> result = new ArrayList<ProductPrice>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from product_price where product_id = ? order by pp_creationdate desc");
	        
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
	        pst.setObject(1, Integer.valueOf(p.getProduct_id()));
			rs = pst.executeQuery();
			while(rs.next()){
				ProductPrice pp = new ProductPrice();
				pp.setPp_price(rs.getInt("pp_price"));
				pp.setPp_currency(rs.getString("pp_currency"));
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
				Date parsedDate = (Date) dateFormat.parse(rs.getString("pp_creationdate"));
				Timestamp waktu = new Timestamp(parsedDate.getTime());
				pp.setPp_creationdate(waktu);
				pp.setPp_createby(rs.getString("pp_createby"));
				result.add(pp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}
	
	public void Insert(ProductPrice p) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("insert into product_price(pp_price,product_id,pp_currency,pp_createby,pp_lastupdateby) "
	        		+ "values(?,?,?,?,?)");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getPp_price()));
			pst.setObject(2, Integer.valueOf(p.getProduct_id()));
			pst.setObject(3, p.getPp_currency().toString());
			pst.setObject(4, p.getPp_createby().toString());
			pst.setObject(5, p.getPp_lastupdateby().toString());
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}