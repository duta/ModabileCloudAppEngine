package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.CustomerMembers;

public class CustomerMemberRepo {
	DBProvider dbProvider;
	
	public CustomerMemberRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> GetRolesByUserID(int UserID) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select ");
		stringBuilder.append("     r.* ");
		stringBuilder.append(" from customer_members cm ");
		stringBuilder.append("      join moresecure.roles r on cm.custmember_role_id = r.role_id ");
		stringBuilder.append(" where cm.custmember_customer_id = ?  ");
		ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> list = new ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, UserID);
			rs = pst.executeQuery();
			while (rs.next()) {
				com.ptdmc.modabilefe.entity.leyfi.Roles e = new com.ptdmc.modabilefe.entity.leyfi.Roles();
				e.role_id = (Integer) (rs.getInt("role_id"));
				e.role_name = rs.getString("role_name");
				e.role_description = rs.getString("role_description");
				list.add(e);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return list;
	}
	public int InsertCustomerRole(int role_id, int customer_id, int user_id) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("insert into customer_members (custmember_customer_id, customer_id, custmember_role_id) ");
		stringBuilder.append(" values (?, ?, ?) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, user_id);
			st.setInt(2, customer_id);
			st.setInt(3, role_id);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}

	public Customer getCustomerByUserID(Integer user_id) throws SQLException {

		List<Customer> result = new ArrayList<Customer>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		
		Customer c = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" select ");
			stringBuilder.append("     r.* ");
			stringBuilder.append(" from customer_members cm ");
			stringBuilder.append("      join customer r on cm.customer_id = r.customer_id ");
			stringBuilder.append(" where cm.custmember_customer_id = ?  ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, user_id);
			rs = pst.executeQuery();
			
			while(rs.next()){
				c = new Customer();
				c.setCustomer_id(rs.getInt("customer_id"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setCustomer_email(rs.getString("customer_email"));
				c.setCustomer_phone(rs.getString("customer_phone"));
				c.setCustomer_country(rs.getString("customer_country"));
				c.setCustomer_company(rs.getString("customer_company"));				
			}
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return c;
	}

	public List<CustomerMembers> GetAllByCustomerID(int cid) throws SQLException {

		List<CustomerMembers> result = new ArrayList<CustomerMembers>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		
		CustomerMembers c = null;
		try {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(" select * ");
			stringBuilder.append(" from customer_members cm ");
			stringBuilder.append(" where cm.customer_id = ?  ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			pst.setInt(1, cid);
			rs = pst.executeQuery();
			
			while(rs.next()){
				c = new CustomerMembers();
				c.setCustomer_id(((Integer)rs.getInt("customer_id")).toString());
				c.setCustmember_customer_id(((Integer)rs.getInt("custmember_customer_id")).toString());	
				c.setCustmember_role_id(((Integer)rs.getInt("custmember_role_id")).toString());	
				result.add(c);
			}
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}

	public void Delete(int user_id) throws SQLException {
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from customer_members where custmember_customer_id = ? ");
			pst.setObject(1, user_id);
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

	public void Update(Integer customer_id, Integer user_id, int role_id) throws SQLException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("update customer_members set custmember_role_id = ? ");
		stringBuilder.append(" where custmember_customer_id = ? and customer_id = ? ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
				try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, role_id);
			st.setInt(2, user_id);
			st.setInt(3, customer_id);
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}

}
