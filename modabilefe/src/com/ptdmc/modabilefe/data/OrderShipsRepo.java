package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.OrderShips;
import com.ptdmc.modabilefe.entity.ShipInfo;

public class OrderShipsRepo {
	DBProvider dbProvider;
	
	public OrderShipsRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public void insert(OrderShips orderShips)throws SQLException {
		// TODO: Insert Order Ships
		StringBuilder sb = new StringBuilder();
        sb.append("insert into order_ships(orddtl_id,ship_mmsi,ship_name,ship_callsign) ");
        sb.append(" values (?,?,?,?)");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
		try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setObject(1, orderShips.getOrder_id());
			st.setLong(2, orderShips.getShip_mmsi());
			st.setString(3, orderShips.getShip_name());
			st.setString(4, orderShips.getShip_callsign());
			
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
    }
	
	public HashMap<OrderShips, ShipInfo> getDetailLastLocTime(OrderShips od)throws SQLException  {
		HashMap<OrderShips, ShipInfo> result = new HashMap<OrderShips, ShipInfo>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(" SELECT os.*, "); 
			sb.append("        si.si_position_date, ");
			sb.append("        si.si_latitude, ");
			sb.append("        si.si_longitude ");
			sb.append(" FROM order_ships os LEFT JOIN  ship_info si on os.ship_mmsi = si.mmsi ");
			sb.append(" WHERE orddtl_id = ? ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, od.getOrder_id());
			rs = pst.executeQuery();
			while(rs.next()){
				OrderShips ods = new OrderShips();
				ods.setShip_mmsi(rs.getLong("ship_mmsi"));
				ods.setShip_name(rs.getString("ship_name"));
				ods.setShip_imo(rs.getLong("ship_imo"));
				ods.setShip_callsign(rs.getString("ship_callsign"));
				
				ShipInfo si = new ShipInfo();
				si.setSi_longitude(rs.getString("si_longitude"));
				si.setSi_latitude(rs.getString("si_latitude"));
				si.setSi_position_date(rs.getTimestamp("si_position_date"));
				
				result.put(ods, si);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;		
	}
	
	
	public List<OrderShips> getWhere(OrderShips od)throws SQLException  {
		List<OrderShips> result = new ArrayList<OrderShips>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT * FROM order_ships WHERE orddtl_id =  ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, od.getOrder_id());
			rs = pst.executeQuery();
			while(rs.next()){
				OrderShips ods = new OrderShips();
				ods.setShip_mmsi(rs.getLong("ship_mmsi"));
				ods.setShip_name(rs.getString("ship_name"));
				result.add(ods);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;
	}
	
	public HashMap<OrderShips, ShipInfo> getLastOrder(Customer c)throws SQLException  {
		HashMap<OrderShips, ShipInfo> result = new HashMap<OrderShips, ShipInfo>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT max(orddtl_id) as id FROM order_details WHERE customer_id = ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, Integer.valueOf(c.getCustomer_id()));
			rs = pst.executeQuery();
			if(rs.next()){
				OrderShips os = new OrderShips();
				os.setOrder_id(Long.valueOf(rs.getInt("id")));
				rs.close();
				result = getDetailLastLocTime(os);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;
	}
	
	public List<OrderShips> getLastOrder2(Customer c)throws SQLException  {
		List<OrderShips> result = new ArrayList<OrderShips>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT max(orddtl_id) as id FROM order_details WHERE customer_id = ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, Integer.valueOf(c.getCustomer_id()));
			rs = pst.executeQuery();
			if(rs.next()){
				OrderShips os = new OrderShips();
				os.setOrder_id(Long.valueOf(rs.getInt("id")));
				rs.close();
				result = getWhere(os);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;
	}

	public void removeByOrder(long parseLong) throws SQLException {

		StringBuilder sb = new StringBuilder();
        sb.append("delete from order_ships where orddtl_id=?");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
		try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setObject(1, parseLong);
			
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}
