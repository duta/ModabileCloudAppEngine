package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;

import com.ptdmc.modabilefe.entity.Customer;

public class CustomerRepo {
	DBProvider dbProvider;
	
	public CustomerRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	
	/*public ShipInfo GetByMMSI(int mmsi){
        ShipInfo result = new ShipInfo();
        StringBuilder sb = new StringBuilder();
        sb.append("select received_on, mmsi from ship_info where mmsi =  :mmsi");
        Connection conn = DBProvider.getSql2o().open();
        List<ShipInfo> ships = conn.createQuery(sb.toString())
                .addParameter("mmsi",mmsi)
                .executeAndFetch(ShipInfo.class);
        if (ships.size() > 0) {
            result = ships.get(0);
        }
        return result;
    }*/
	
	public List<Customer> GetAll() throws SQLException
	{
		List<Customer> result = new ArrayList<Customer>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from customer");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());			
			rs = pst.executeQuery();
			while(rs.next()){
				Customer c = new Customer();
				c.setCustomer_id(rs.getInt("customer_id"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setCustomer_email(rs.getString("customer_email"));
				c.setCustomer_phone(rs.getString("customer_phone"));
				c.setCustomer_country(rs.getString("customer_country"));
				c.setCustomer_timezone(rs.getString("customer_timezone"));
				c.setCustomer_timeabbreviation(rs.getString("customer_timeabbreviation"));
				c.setCustomer_gmtoffset(rs.getInt("customer_gmtoffset"));
				c.setCustomer_company(rs.getString("customer_company"));
				c.setCustomer_logo(rs.getString("customer_logo"));
				result.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}

	public List<Customer> GetAllCustomerAndProduct() throws SQLException
	{
		List<Customer> result = new ArrayList<Customer>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append(" select customer.*, product.product_name, o.orddtl_id");
	        sb.append(" from customer");
	        sb.append(" left join order_details o on customer.customer_id = o.customer_id");
	        sb.append(" 	and o.orddtl_creationdate = (select max(orddtl_creationdate) from order_details o2 where o2.customer_id = customer.customer_id)");
	        sb.append(" left join product on o.product_id = product.product_id");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());			
			rs = pst.executeQuery();
			while(rs.next()){
				Customer c = new Customer();
				c.setCustomer_id(rs.getInt("customer_id"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setCustomer_email(rs.getString("customer_email"));
				c.setCustomer_phone(rs.getString("customer_phone"));
				c.setCustomer_country(rs.getString("customer_country"));
				c.setCustomer_timezone(rs.getString("customer_timezone"));
				c.setCustomer_timeabbreviation(rs.getString("customer_timeabbreviation"));
				c.setCustomer_gmtoffset(rs.getInt("customer_gmtoffset"));
				c.setCustomer_company(rs.getString("customer_company"));
				c.setCustomer_logo(rs.getString("customer_logo"));
				c.setProductName(rs.getString("product_name"));
				c.setOrderDetailId(rs.getInt("orddtl_id"));
				result.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}
	
	public int checkRegisterUser(String email) throws SQLException {
		int result = 0;
		
		StringBuilder sb = new StringBuilder();
        sb.append("SELECT count(*) FROM customer WHERE lower(customer.customer_email) = lower(?) ");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setString(1, email);
			
			ResultSet returnSql = st.executeQuery();
			if (returnSql.next()) {
		        result = returnSql.getInt(1);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public Customer GetByEmail(String email) throws SQLException
	{
		List<Customer> result = new ArrayList<Customer>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		
		Customer c = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from customer where customer_email = ? ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
			pst.setString(1, email);
			rs = pst.executeQuery();
			
			while(rs.next()){
				c = new Customer();
				c.setCustomer_id(rs.getInt("customer_id"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setCustomer_email(rs.getString("customer_email"));
				c.setCustomer_phone(rs.getString("customer_phone"));
				c.setCustomer_country(rs.getString("customer_country"));
				c.setCustomer_timezone(rs.getString("customer_timezone"));
				c.setCustomer_timeabbreviation(rs.getString("customer_timeabbreviation"));
				c.setCustomer_gmtoffset(rs.getInt("customer_gmtoffset"));
				c.setCustomer_company(rs.getString("customer_company"));
				//c.setCustomer_logo(rs.getString("customer_logo"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return c;
	}
	
	public Customer GetByID(Integer id) throws SQLException
	{
		List<Customer> result = new ArrayList<Customer>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		
		Customer c = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from customer where customer_id = ? ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
			pst.setInt(1, id);
			rs = pst.executeQuery();
			
			while(rs.next()){
				c = new Customer();
				c.setCustomer_id(rs.getInt("customer_id"));
				c.setCustomer_name(rs.getString("customer_name"));
				c.setCustomer_email(rs.getString("customer_email"));
				c.setCustomer_phone(rs.getString("customer_phone"));
				c.setCustomer_country(rs.getString("customer_country"));
				c.setCustomer_timezone(rs.getString("customer_timezone"));
				c.setCustomer_timeabbreviation(rs.getString("customer_timeabbreviation"));
				c.setCustomer_gmtoffset(rs.getInt("customer_gmtoffset"));
				c.setCustomer_company(rs.getString("customer_company"));
				//c.setCustomer_logo(rs.getString("customer_logo"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return c;
	}
	
	public List<String> getListPerusahaan() throws SQLException {
		List<String> mmsis = new ArrayList<String>();
		
		StringBuilder sb = new StringBuilder();
        sb.append(" select distinct customer_company from customer where customer_company is not null ");

        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			
			ResultSet returnSql = st.executeQuery();
			while (returnSql.next()) {
				mmsis.add(returnSql.getString(1));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return mmsis;
	}
	
	public List<Integer> getActiveMMSI(Customer c) throws SQLException {
		List<Integer> mmsis = new ArrayList<Integer>();
		
		StringBuilder sb = new StringBuilder();
        sb.append(" SELECT ship_mmsi");
        sb.append(" FROM order_ships os  JOIN order_details od on od.orddtl_id = os.orddtl_id");
        sb.append(" WHERE od.customer_id = ? and now() between orddtl_contract_startdate and orddtl_contract_enddate");

        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setInt(1, c.getCustomer_id());
			
			ResultSet returnSql = st.executeQuery();
			while (returnSql.next()) {
				mmsis.add(returnSql.getInt(1));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return mmsis;
	}
	
	public boolean hadAPacket(Customer c) throws SQLException {
		boolean mmsis = false;
		
		StringBuilder sb = new StringBuilder();
        sb.append(" SELECT order_id");
        sb.append(" FROM orders");
        sb.append(" WHERE customer_id = ?");

        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setInt(1, c.getCustomer_id());
			
			ResultSet returnSql = st.executeQuery();
			if (returnSql.next()) {
				return true;
		    } else {
		    	return false;
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return mmsis;
	}
	
	public int getIdCustomer(String email) throws SQLException {
		int result = 0;
		
		StringBuilder sb = new StringBuilder();
        sb.append("SELECT customer_id FROM customer WHERE customer.customer_email = ? ");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setString(1, email);
			
			ResultSet returnSql = st.executeQuery();
			if (returnSql.next()) {
		        result = returnSql.getInt(1);
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public void insert(Customer customer)throws SQLException  {
		// TODO: Insert Details Status
		StringBuilder sb = new StringBuilder();
        sb.append("insert into customer(customer_email, customer_company, customer_name) ");
        sb.append(" values (?, ?, ?)");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setString(1, customer.getCustomer_email());
			st.setString(2, customer.getCustomer_company());
			st.setString(3, customer.getCustomer_name());
			st.executeUpdate();
		} catch (SQLException e) {
			// propagate the error
			throw e;
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
    }


	public void Update(Customer customer)throws SQLException {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
        sb.append("UPDATE customer ");
        sb.append(" SET customer_name=?, customer_email=?, customer_phone=?, customer_country=?,  ");
        sb.append(" customer_company=?,customer_timezone=?, customer_timeabbreviation=?, customer_gmtoffset = ? ");
        sb.append(" WHERE customer_id = ? ");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setString(1, customer.getCustomer_name());
			st.setString(2, customer.getCustomer_email());
			st.setString(3, customer.getCustomer_phone());
			st.setString(4, customer.getCustomer_country());
			st.setString(5, customer.getCustomer_company());
			st.setString(6, customer.getCustomer_timezone());
			st.setString(7, customer.getCustomer_timeabbreviation());
			st.setInt(8, customer.getCustomer_gmtoffset());
			st.setInt(9, customer.getCustomer_id());
			
			
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}


	public void Delete(int parseInt) throws SQLException {
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from public.customer where customer_id = ? ");
			pst.setObject(1, parseInt);
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}
