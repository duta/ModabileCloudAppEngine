package com.ptdmc.modabilefe.data;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Connection;

import org.geojson.Feature;

import com.ptdmc.modabilefe.geo.ShipFeatureCollection;
import com.ptdmc.modabilefe.geo.ShipProperty;
import com.ptdmc.modabilefe.util.Convert;

public class IncomingAisRepo {
	DBProvider dbProvider;

	public IncomingAisRepo(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}

	public ShipFeatureCollection RecordedAllPositionByMMSIAndDate(int mmsi,
			Timestamp from, Timestamp to) throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append(" select si.mmsi, si.vesselname ship_name, logis.timestampreceiver,  ");
		sb.append("          trueheading,speedoverground,si.dimensiontobow,si.dimensiontostern,si.dimensiontoport,si.dimensiontostarboard,navigationalstatus,navstatusdesc, ");
		sb.append("          rateofturn,courseoverground, latitude, longitude,         ");
		sb.append("      si.shiptypedesc ship_type, logis.received_on, si.callsign, si.grt, logis.destination, si.draught,        ");
		sb.append("      logis.etamonth, logis.etaday, logis.etahour, logis.etaminute  ");
		sb.append("   from ship_info si join ( ");
		sb.append("             select mmsi,  ");
		sb.append("                             timestampreceiver, ");
		sb.append("                             trueheading, ");
		sb.append("                             speedoverground, ");
		sb.append("                             navigationalstatus, ");
		sb.append("                             navstatusdesc, ");
		sb.append("                             rateofturn, ");
		sb.append("                             courseoverground, ");
		sb.append("                             latitude, ");
		sb.append("                             longitude, ");
		sb.append("                             received_on, ");
		sb.append("                             destination, ");
		sb.append("                             etamonth, ");
		sb.append("                             etaday, ");
		sb.append("                             etahour, ");
		sb.append("                             etaminute, ");
		sb.append("                             DENSE_RANK() OVER (PARTITION BY mmsi ORDER BY received_on) row_number  ");
		sb.append("             from incoming_ais  ");
		sb.append("             where received_on between ? and ?  ");
		sb.append("         order by mmsi, received_on asc ) logis  ");
		sb.append("        on si.mmsi = logis.mmsi ");
		sb.append(" and  si.mmsi = ?   ");
		sb.append("        order by ship_name ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		List<Feature> shipFeatures = new ArrayList<Feature>();

		try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);

			st = conn.prepareStatement(sb.toString());
			st.setObject(1, from);
			st.setObject(2, to);
			st.setObject(3, mmsi);

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();

			while (rs.next()) {
				// a row was returned				
				shipFeatures.add(BindRsToShipFeature(rs));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		ShipFeatureCollection result = new ShipFeatureCollection(shipFeatures);
		return result;
	}

	public ShipFeatureCollection RecordedPositionByMMSIAndDate(int mmsi,
			Timestamp from, Timestamp to) throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append(" select si.mmsi, si.vesselname ship_name, logis.timestampreceiver,  ");
		sb.append("          trueheading,speedoverground,si.dimensiontobow,si.dimensiontostern,si.dimensiontoport,si.dimensiontostarboard,navigationalstatus,navstatusdesc, ");
		sb.append("          rateofturn,courseoverground, latitude, longitude,         ");
		sb.append("      si.shiptypedesc ship_type, logis.received_on, si.callsign, si.grt, logis.destination, si.draught,        ");
		sb.append("      logis.etamonth, logis.etaday, logis.etahour, logis.etaminute  ");
		sb.append("   from ship_info si join ( ");
		sb.append("             select mmsi,  ");
		sb.append("                             timestampreceiver, ");
		sb.append("                             trueheading, ");
		sb.append("                             speedoverground, ");
		sb.append("                             navigationalstatus, ");
		sb.append("                             navstatusdesc, ");
		sb.append("                             rateofturn, ");
		sb.append("                             courseoverground, ");
		sb.append("                             latitude, ");
		sb.append("                             longitude, ");
		sb.append("                             received_on, ");
		sb.append("                             destination, ");
		sb.append("                             etamonth, ");
		sb.append("                             etaday, ");
		sb.append("                             etahour, ");
		sb.append("                             etaminute, ");
		sb.append("                             DENSE_RANK() OVER (PARTITION BY mmsi ORDER BY received_on) row_number  ");
		sb.append("             from incoming_ais  ");
		sb.append("             where received_on between ? and ?  ");
		sb.append("         order by mmsi, received_on asc ) logis  ");
		sb.append("        on si.mmsi = logis.mmsi ");
        sb.append("        where row_number = 1 ");
        if (mmsi != -1) sb.append(" and  si.mmsi = ?   ");
		sb.append("        order by ship_name ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		List<Feature> shipFeatures = new ArrayList<Feature>();

		try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			String s = sb.toString();
			st = conn.prepareStatement(sb.toString());
			st.setTimestamp(1, from);
			st.setTimestamp(2, to);
			if (mmsi != -1) st.setInt(3, mmsi);

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();

			while (rs.next()) {
				// a row was returned				
				shipFeatures.add(BindRsToShipFeature(rs));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		ShipFeatureCollection result = new ShipFeatureCollection(shipFeatures);
		return result;
	}

	public ShipFeatureCollection RecordedPositionByMMSITopX(int mmsi, int topx) throws SQLException {
		StringBuilder sb = new StringBuilder();
		sb.append(" select logis.row_number,si.mmsi, si.vesselname ship_name, logis.timestampreceiver,  ");
        sb.append("          trueheading,speedoverground,si.dimensiontobow,si.dimensiontostern,si.dimensiontoport,si.dimensiontostarboard,navigationalstatus,navstatusdesc, ");
        sb.append("          rateofturn,courseoverground, latitude, longitude,         ");
        sb.append("      si.shiptypedesc ship_type, logis.received_on, si.callsign, si.grt, logis.destination, si.draught,      ");
        sb.append("      logis.etamonth, logis.etaday, logis.etahour, logis.etaminute  ");
        //fleetmon
        sb.append("                    , logis.fmairpressure, logis.fmais, logis.fmbattery, logis.fmcurrentdir, logis.fmcurrentspeed, logis.fmesn, logis.fmfirstcreated  ");
        sb.append("                    , logis.fmgpsdeviceid, logis.fmiceconcentration, logis.fmlocation, logis.fmname, logis.fmrelativehumidity  ");
        sb.append("                    , logis.fmtemperatureair, logis.fmtempwater, logis.fmtotalcloudcover, logis.fmwavedir, logis.fmwaveheight, logis.fmwaveperiod ");
        sb.append("                    , logis.fmwinddir, logis.fmwindspeed, logis.fmwindsurfacegust ");
        //end fleetmon
        sb.append("   from ship_info si join ( ");
        sb.append("             select mmsi,  ");
        sb.append("                 timestampreceiver, ");
        sb.append("                 trueheading, ");
        sb.append("                 speedoverground, ");
        sb.append("                 navigationalstatus, ");
        sb.append("                 navstatusdesc, ");
        sb.append("                 rateofturn, ");
        sb.append("                 courseoverground, ");
        sb.append("                 latitude, ");
        sb.append("                 longitude, ");
        sb.append("                 received_on, ");
        sb.append("                             destination, ");
        sb.append("                             etamonth, ");
        sb.append("                             etaday, ");
        sb.append("                             etahour, ");
        sb.append("                             etaminute, ");
        sb.append("                 ROW_NUMBER() OVER (ORDER BY received_on desc) row_number  ");
        //fleetmon
        sb.append("                    , fmairpressure, fmais, fmbattery, fmcurrentdir, fmcurrentspeed, fmesn, fmfirstcreated  ");
        sb.append("                    , fmgpsdeviceid, fmiceconcentration, fmlocation, fmname, fmrelativehumidity  ");
        sb.append("                    , fmtemperatureair, fmtempwater, fmtotalcloudcover, fmwavedir, fmwaveheight, fmwaveperiod ");
        sb.append("                    , fmwinddir, fmwindspeed, fmwindsurfacegust ");
        //end fleetmon
        sb.append("             from incoming_ais ia  ");
        sb.append("        where ia.mmsi = ? ");
        sb.append("                    AND received_on NOT IN (SELECT received_on "); // show all except on ship_mislocation
        sb.append("                                          FROM ship_mislocation ");
        sb.append("                                         WHERE mmsi = ?) ");
        sb.append("         order by  received_on desc ) logis  ");
        sb.append("        on logis.row_number <= ? and logis.mmsi=si.mmsi ");
        sb.append("        where si.mmsi = ? ");
        sb.append("        order by logis.row_number desc ");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		List<Feature> shipFeatures = new ArrayList<Feature>();
        
		conn = dbProvider.OpenConnection();
		// make sure autocommit is off
		try {
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setObject(1, mmsi);
			st.setObject(2, mmsi);
			st.setObject(3, topx);
			st.setObject(4, mmsi);

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();
			
			while (rs.next()) {
				// a row was returned				
				shipFeatures.add(BindRsToShipFeature(rs));
			}			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	
		ShipFeatureCollection result = new ShipFeatureCollection(shipFeatures);
		return result;
	}
	
	private Feature BindRsToShipFeature(ResultSet rs) throws SQLException {
		ShipProperty shipProperty = new ShipProperty();
		shipProperty.setMMSI(rs.getInt("mmsi"));
		shipProperty.setShipTypeDescription(rs.getString("ship_type"));
		shipProperty.setShipName(rs.getString("ship_name"));
		shipProperty.setTimeStampReceiver(rs.getDouble("timestampreceiver"));
		shipProperty.setHDG(rs.getDouble("trueheading"));
		shipProperty.setSOG(rs.getDouble("speedoverground"));
		shipProperty.setToBow(rs.getDouble("dimensiontobow"));
		shipProperty.setToStern(rs.getDouble("dimensiontostern"));
		shipProperty.setToPort(rs.getDouble("dimensiontoport"));
		shipProperty.setToStarBoard(rs.getDouble("dimensiontostarboard"));
		shipProperty.setNavStatus(rs.getInt("navigationalstatus"));
		shipProperty.setNavStatusDescription(rs.getString("navstatusdesc"));
		shipProperty.setROT(rs.getString("rateofturn"));
		shipProperty.setCOG(rs.getDouble("courseoverground"));
		
		shipProperty.setCallSign(rs.getString("callsign"));
		shipProperty.setGrt(rs.getDouble("grt"));
		shipProperty.setDestination(rs.getString("destination"));
		shipProperty.setDraught(rs.getDouble("draught"));
		
		shipProperty.setETAMonth(rs.getDouble("etamonth"));
		shipProperty.setETADay(rs.getDouble("etaday")); 
		shipProperty.setETAHour(rs.getDouble("etahour"));
		shipProperty.setETAMinute(rs.getDouble("etaminute"));
		
		try { //fleetmon (need try catch since ResultSet can't see if it's got certain keys)
            shipProperty.setFmAirPressure(rs.getDouble("fmairpressure"));
            shipProperty.setFmAis(rs.getBoolean("fmais"));
            shipProperty.setFmBattery(rs.getString("fmbattery"));
            shipProperty.setFmCurrentDir(rs.getInt("fmcurrentdir"));
            shipProperty.setFmCurrentSpeed(rs.getDouble("fmcurrentspeed"));
            shipProperty.setFmEsn(rs.getString("fmesn"));
            shipProperty.setFmFirstCreated(rs.getTimestamp("fmfirstcreated"));
            shipProperty.setFmGpsDeviceId(rs.getInt("fmgpsdeviceid"));
            shipProperty.setFmIceConcentration(rs.getDouble("fmiceconcentration"));
            shipProperty.setFmLocation(rs.getString("fmlocation"));
            shipProperty.setFmName(rs.getString("fmname"));
            shipProperty.setFmRelativeHumidity(rs.getInt("fmrelativehumidity"));
            shipProperty.setFmTemperatureAir(rs.getDouble("fmtemperatureair"));
            shipProperty.setFmTempWater(rs.getDouble("fmtempwater"));
            shipProperty.setFmTotalCloudCover(rs.getInt("fmtotalcloudcover"));
            shipProperty.setFmWaveDir(rs.getInt("fmwavedir"));
            shipProperty.setFmWaveHeight(rs.getDouble("fmwaveheight"));
            shipProperty.setFmWavePeriod(rs.getDouble("fmwaveperiod"));
            shipProperty.setFmWindDir(rs.getInt("fmwinddir"));
            shipProperty.setFmWindSpeed(rs.getDouble("fmwindspeed"));
            shipProperty.setFmWindSurfaceGust(rs.getDouble("fmwindsurfacegust"));			
		} catch (Exception ex) { }
		
		Feature feature= new Feature();				
		feature.setGeometry(Convert.CreatePoint(rs.getDouble("longitude"), rs.getDouble("latitude")));
		feature.setProperties(Convert.CreateProperties(shipProperty));
		
		return feature;
	}

}
