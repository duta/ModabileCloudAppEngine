package com.ptdmc.modabilefe.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ptdmc.modabilefe.entity.Konfigurasi;;

public class KonfigurasiRepo {
	DBProvider dbProvider;
	
	public KonfigurasiRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public String GetValueByKey(String key) throws SQLException{		
		String result = "";
		String qString = "select konfig_value from konfigurasi where konfig_key=?";
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
		try {
			conn = dbProvider.OpenConnection();
		
			conn.setAutoCommit(false);
			st = conn.prepareStatement(qString);			
			st.setObject(1, key);						
			rs = st.executeQuery();
			while(rs.next()){
				result = rs.getString("konfig_value");
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public List<Konfigurasi> GetAll() throws SQLException {
		List<Konfigurasi> result = new ArrayList<Konfigurasi>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from konfigurasi");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());			
			rs = pst.executeQuery();
			while(rs.next()){
				Konfigurasi k = new Konfigurasi();
				k.setKonfig_key(rs.getString("konfig_key"));
				k.setKonfig_value(rs.getString("konfig_value"));
				k.setKonfig_keyasmandatory(rs.getString("konfig_keyasmandatory"));
				result.add(k);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;
	}
	
	public int UpdateValueByKey(String konfigKey, String konfigValue) throws SQLException{
		int result = 0;
		StringBuilder sb = new StringBuilder();
        sb.append("UPDATE konfigurasi ");
        sb.append(" SET konfig_value=?  ");
        sb.append(" WHERE lower(konfig_key) = ? ");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setString(1, konfigValue.toLowerCase().trim());
			st.setString(2, konfigKey.toLowerCase().trim());
			
			result = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public int Insert(String key, String value, boolean mandatory) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("insert into konfigurasi (konfig_key, konfig_value, konfig_keyasmandatory) ");
		stringBuilder.append(" values (?, ?, ? ) ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, key);
			st.setString(2, value);
			st.setBoolean(3, mandatory);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	
	public int Update(String oldkey, String key, String value, boolean mandatory) throws SQLException
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("UPDATE konfigurasi SET konfig_key=?, konfig_value=?, konfig_keyasmandatory=? ");
 		stringBuilder.append(" WHERE konfig_key=? ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, key);
			st.setString(2, value);
			st.setBoolean(3, mandatory);
			st.setString(4, oldkey);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
	public int Delete(String key) throws SQLException
	{

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		Integer res = null;
		try {
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement("delete from konfigurasi where konfig_key = ? ");
			pst.setString(1, key);
			res = pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}
}
