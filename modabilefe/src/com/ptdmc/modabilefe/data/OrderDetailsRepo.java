package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import com.ptdmc.modabilefe.entity.OrderDetails;

public class OrderDetailsRepo {
	DBProvider dbProvider;
	
	public OrderDetailsRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public int insert(OrderDetails orderDetail)throws SQLException  {
		StringBuilder sb = new StringBuilder();
        sb.append("insert into order_details ");
        sb.append(" (");
        sb.append("product_id,orddtl_quantity,orddtl_unitprice,orddtl_currency,customer_id,");
        sb.append("orddtl_createby,orddtl_discount,orddtl_lastupdateby,");
        sb.append("order_id,orddtl_id,orddtl_contract_startdate,orddtl_contract_enddate,");
        sb.append("orddtl_creationdate,orddtl_lastupdate");
        sb.append(")");
        sb.append(" values (?,?,?,?,?,?,?,?,?,?,?,?,now(),now())");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
        	int orddtl_id = getNextVal();
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			//conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			st.setLong(1, orderDetail.getProduct_id());
			st.setLong(2, orderDetail.getOrddtl_quantity());
			st.setLong(3, orderDetail.getOrddtl_unitprice());
			st.setString(4, orderDetail.getOrddtl_currency());
			st.setLong(5, orderDetail.getCustomer_id());
			st.setString(6, orderDetail.getOrddtl_createby());
			st.setLong(7, orderDetail.getOrddtl_discount());
			st.setString(8, orderDetail.getOrddtl_lastupdateby());
			st.setObject(9, orderDetail.getOrder_id());
			st.setObject(10, orddtl_id);
			st.setObject(11, orderDetail.getOrddtl_contract_startdate());
			st.setObject(12, orderDetail.getOrddtl_contract_enddate());
			
			
			st.executeUpdate();
			return orddtl_id;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return -1;
    }

	public void update(OrderDetails orderDetail)throws SQLException  {
		StringBuilder sb = new StringBuilder();
        sb.append(" UPDATE order_details SET");
        sb.append("     orddtl_quantity  = ?,");
        sb.append("     orddtl_unitprice = ?,");
        sb.append("     orddtl_currency  = ?,");
        sb.append("     orddtl_discount  = ?,");
        sb.append("     orddtl_contract_startdate = ?,");
        sb.append("     orddtl_contract_enddate   = ?,");
        sb.append("     orddtl_lastupdate  = now()");
        sb.append(" WHERE orddtl_id = ?");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setLong(1, orderDetail.getOrddtl_quantity());
			st.setLong(2, orderDetail.getOrddtl_unitprice());
			st.setString(3, orderDetail.getOrddtl_currency());
			st.setLong(4, orderDetail.getOrddtl_discount());
			st.setTimestamp(5, orderDetail.getOrddtl_contract_startdate());
			st.setTimestamp(6, orderDetail.getOrddtl_contract_enddate());
			st.setLong(7, orderDetail.getOrddtl_id());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
    }
	
	private int getNextVal() throws SQLException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select nextval('order_seq') nval ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("nval");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}
	
	public List<OrderDetails> getAll()throws SQLException  {
		List<OrderDetails> result = new ArrayList<OrderDetails>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT od.orddtl_id, p.product_name, "
	        		+ "(CASE WHEN (customer_company = '') IS FALSE THEN customer_company "
	        		+ " WHEN (customer_name = '') IS FALSE THEN customer_name "
	        		+ "ELSE customer_email END) customer_comp, od.orddtl_contract_startdate, od.orddtl_contract_enddate from order_details od "
	        		+ "INNER JOIN product p ON p.product_id = od.product_id "
	        		+ "INNER JOIN customer c ON c.customer_id = od.customer_id ");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				OrderDetails od = new OrderDetails();
				od.setOrddtl_id(rs.getLong("orddtl_id"));
				od.setProduct_name(rs.getString("product_name"));
				od.setCustomer_company(rs.getString("customer_comp"));
				od.setOrddtl_contract_startdate(rs.getTimestamp("orddtl_contract_startdate"));
				od.setOrddtl_contract_enddate(rs.getTimestamp("orddtl_contract_enddate"));
				result.add(od);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}
	
	public List<OrderDetails> getByCustomerId(int customerId)throws SQLException  {
		List<OrderDetails> result = new ArrayList<OrderDetails>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT od.orddtl_id, od.orddtl_unitprice, p.product_name, od.orddtl_contract_startdate, od.orddtl_contract_enddate from product p "
	        		+ "INNER JOIN order_details od ON p.product_id = od.product_id "
	        		+ "WHERE od.customer_id = ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, customerId);
			rs = pst.executeQuery();
			while(rs.next()){
				OrderDetails ods = new OrderDetails();
				ods.setOrddtl_id(rs.getLong("orddtl_id"));
				ods.setOrddtl_unitprice(rs.getLong("orddtl_unitprice"));
				ods.setProduct_name(rs.getString("product_name"));
				ods.setOrddtl_contract_startdate(rs.getTimestamp("orddtl_contract_startdate"));
				ods.setOrddtl_contract_enddate(rs.getTimestamp("orddtl_contract_enddate"));
				result.add(ods);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return result;
	}
	
	public OrderDetails getLastByCustomerId(int customerId)throws SQLException  {
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("SELECT od.orddtl_id, p.product_name, od.orddtl_contract_startdate, od.orddtl_contract_enddate from product p "
	        		+ "INNER JOIN order_details od ON p.product_id = od.product_id "
	        		+ "WHERE od.customer_id = ? "
	        		+ "ORDER BY od.orddtl_contract_enddate DESC");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());		
			pst.setObject(1, customerId);
			rs = pst.executeQuery();
			if(rs.next()){
				OrderDetails ods = new OrderDetails();
				ods.setOrddtl_id(rs.getLong("orddtl_id"));
				ods.setProduct_name(rs.getString("product_name"));
				ods.setOrddtl_contract_startdate(rs.getTimestamp("orddtl_contract_startdate"));
				ods.setOrddtl_contract_enddate(rs.getTimestamp("orddtl_contract_enddate"));
				return ods;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		
		return null;
	}

	public OrderDetails getById(String id) throws SQLException {
		OrderDetails result = new OrderDetails();
		Connection conn = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM order_details WHERE orddtl_id = ?");

			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);

			pst = conn.prepareStatement(sb.toString());
			pst.setInt(1, Integer.valueOf(id));

			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				result.setOrddtl_id(rs.getLong("orddtl_id"));
				result.setOrddtl_quantity(rs.getLong("orddtl_quantity"));
				result.setOrddtl_unitprice(rs.getLong("orddtl_unitprice"));
				result.setOrddtl_currency(rs.getString("orddtl_currency"));
				result.setOrddtl_discount(rs.getLong("orddtl_discount"));
				result.setOrddtl_contract_startdate(rs.getTimestamp("orddtl_contract_startdate"));
				result.setOrddtl_contract_enddate(rs.getTimestamp("orddtl_contract_enddate"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return result;
	}

}
