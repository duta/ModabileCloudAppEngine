package com.ptdmc.modabilefe.data;

import java.util.ArrayList;
import java.util.List;

import com.ptdmc.modabilefe.entity.Product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;

public class ProductRepo {
	DBProvider dbProvider;
	
	public ProductRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}

	public List<Product> GetAll() throws SQLException
	{
		List<Product> result = new ArrayList<Product>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from product order by pp_price asc");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());			
			rs = pst.executeQuery();
			while(rs.next()){
				Product p = new Product();
				p.setProduct_id(rs.getInt("product_id"));
				p.setProduct_name(rs.getString("product_name"));
				p.setProduct_description(rs.getString("product_description"));
				p.setPp_price(rs.getInt("pp_price"));
				p.setProduct_period(rs.getInt("product_period"));
				result.add(p);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}
	

	public Product GetOne(Product p) throws SQLException
	{
		Product result = null;
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from product where product_id = ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getProduct_id()));
			rs = pst.executeQuery();
			if(rs.next()){
				result = new Product();
				result.setProduct_id(rs.getInt("product_id"));
				result.setProduct_name(rs.getString("product_name"));
				result.setProduct_description(rs.getString("product_description"));
				result.setPp_price(rs.getInt("pp_price"));
				result.setProduct_period(rs.getInt("product_period"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}
	
	public List<Product> GetWhere(Product p) throws SQLException
	{
		List<Product> result = new ArrayList<Product>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("select * from product where product_id = ?");
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getProduct_id()));
			rs = pst.executeQuery();
			while(rs.next()){
				Product p2 = new Product();
				p2.setProduct_id(rs.getInt("product_id"));
				p2.setProduct_name(rs.getString("product_name"));
				p2.setProduct_description(rs.getString("product_description"));
				p2.setPp_price(rs.getInt("pp_price"));
				p2.setProduct_period(rs.getInt("product_period"));
				result.add(p2);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
        return result;
	}
	
	public void Insert(Product p)throws SQLException 
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("insert into product(product_name,product_description,product_createby,product_lastupdateby,pp_price) "
	        		+ "values(?,?,?,?,?)");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, p.getProduct_name().toString());
			pst.setObject(2, p.getProduct_description().toString());
			pst.setObject(3, p.getProduct_createby().toString());
			pst.setObject(4, p.getProduct_lastupdateby().toString());
			pst.setObject(5, Integer.valueOf(p.getPp_price()));
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	public void Update(Product p) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("update product set product_name = ?, product_description = ? where product_id = ?");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, p.getProduct_name().toString());
			pst.setObject(2, p.getProduct_description().toString());
			pst.setObject(3, Integer.valueOf(p.getProduct_id()));
			
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	public void Update_harga(Product p) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("update product set pp_price = ? where product_id = ?");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getPp_price()));
			pst.setObject(2, Integer.valueOf(p.getProduct_id()));
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	public void Delete(Product p) throws SQLException
	{
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			StringBuilder sb = new StringBuilder();
	        sb.append("delete from product where product_id = ? ");
			conn = dbProvider.OpenConnection();
			pst = conn.prepareStatement(sb.toString());
			pst.setObject(1, Integer.valueOf(p.getProduct_id()));
			pst.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
	}
}