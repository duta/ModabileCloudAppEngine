package com.ptdmc.modabilefe.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.ptdmc.modabilefe.data.ResultSetMapper.ResultSetMapperException;
import com.ptdmc.modabilefe.entity.ShipInfo;

public class ShipInfoRepo {
	DBProvider dbProvider;

	public ShipInfoRepo(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}

	public List<ShipInfo> getAllComplete() throws SQLException {
		List<ShipInfo> result = new ArrayList<ShipInfo>();

		StringBuilder sb = new StringBuilder();
		sb.append(" select * from ship_info ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();
			ResultSetMapper<ShipInfo> resultSetMapper = new ResultSetMapper<ShipInfo>();
			try {
				result = resultSetMapper.mapToList(rs, ShipInfo.class);
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		return result;
	}

	public List<Integer> getAll() throws SQLException {
		List<Integer> mmsis = new ArrayList<Integer>();

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT mmsi  FROM ship_info ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			// make sure autocommit is off
			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());

			ResultSet returnSql = st.executeQuery();
			while (returnSql.next()) {
				mmsis.add(returnSql.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return mmsis;
	}

	public List<ShipInfo> GetBySomeMMSI(List<Integer> mmsis)
			throws SQLException {
		List<ShipInfo> result = new ArrayList<ShipInfo>();

		StringBuilder sb = new StringBuilder();
		sb.append(" select * from ship_info where mmsi in ");
		sb.append(" ( ");
		for (Integer mmsi : mmsis) {
			sb.append("? ,");
		}
		sb.deleteCharAt(sb.length() - 1); // remove last comma
		sb.append(" ) ");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();

			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			int i = 1;
			for (Integer mmsi : mmsis) {
				st.setObject(i, mmsi);
				i += 1;
			}

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();
			ResultSetMapper<ShipInfo> resultSetMapper = new ResultSetMapper<ShipInfo>();
			try {
				result = resultSetMapper.mapToList(rs, ShipInfo.class);
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		return result;
	}

	public Integer insert(ShipInfo si, String creator) throws SQLException {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("INSERT INTO ship_info(");
		stringBuilder.append("vesselname, mmsi, dte, enginehp, enginediesel,  ");
		stringBuilder.append("dimensiontostern, dimensiontoport, dimensiontobow, dimensiontostarboard, ");
		stringBuilder.append("shiptype, shiptypedesc, ");
		stringBuilder.append("imonumber, callsign, grt, ");
		stringBuilder.append("si_creationdate, si_lastupdate, si_createby, si_lastupdateby, "); 
		stringBuilder.append("si_wide, si_length)");
		stringBuilder.append("VALUES (?, ?, ?, ?, ?, ");
		stringBuilder.append("?, ?, ?, ?, ?, ");
		stringBuilder.append("?, ?, ?, ?, now(), ");
		stringBuilder.append("now(), ?, ?, ?, ?) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, si.getVesselname());
			st.setLong(2, si.getMmsi());
			st.setLong(3, si.getDte());
			st.setLong(4, si.getEnginehp());
			st.setBoolean(5, "true".equals(si.getEnginediesel()));
			st.setLong(6, Long.parseLong(si.getDimensiontostern()));
			st.setLong(7, Long.parseLong(si.getDimensiontoport()));
			st.setLong(8, Long.parseLong(si.getDimensiontobow()));
			st.setLong(9, Long.parseLong(si.getDimensiontostarboard()));
			st.setLong(10, si.getShiptype());
			st.setString(11, si.getShiptypedesc());
			st.setLong(12, si.getImonumber());
			st.setString(13, si.getCallsign());
			st.setLong(14, si.getGrt());
			st.setString(15, creator);
			st.setString(16, creator);
			st.setLong(17, Long.parseLong(si.getDimensiontoport())+Long.parseLong(si.getDimensiontostarboard()));
			st.setLong(18, Long.parseLong(si.getDimensiontostern())+Long.parseLong(si.getDimensiontobow()));
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return res;
	}

	public int update(ShipInfo si, String email, int mmsi) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("UPDATE ship_info");
		stringBuilder.append("   SET vesselname=?, mmsi=?, dte=?, enginehp=?, enginediesel=?, ");
		stringBuilder.append("       dimensiontostern=?, dimensiontoport=?, dimensiontobow=?, dimensiontostarboard=?, ");
		stringBuilder.append("       shiptype=?, shiptypedesc=?, ");
		stringBuilder.append("       imonumber=?, callsign=?, ");
		stringBuilder.append("       grt=?, si_lastupdate=now(), si_lastupdateby=?, ");
		stringBuilder.append("       si_wide=?, si_length=?"); 
		stringBuilder.append(" WHERE mmsi=?");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setString(1, si.getVesselname());
			st.setLong(2, si.getMmsi());
			st.setLong(3, si.getDte());
			st.setLong(4, si.getEnginehp());
			st.setBoolean(5, "true".equals(si.getEnginediesel()));
			st.setLong(6, Long.parseLong(si.getDimensiontostern()));
			st.setLong(7, Long.parseLong(si.getDimensiontoport()));
			st.setLong(8, Long.parseLong(si.getDimensiontobow()));
			st.setLong(9, Long.parseLong(si.getDimensiontostarboard()));
			st.setLong(10, si.getShiptype());
			st.setString(11, si.getShiptypedesc());
			st.setLong(12, si.getImonumber());
			st.setString(13, si.getCallsign());
			st.setLong(14, si.getGrt());
			st.setString(15, email);
			st.setLong(16, Long.parseLong(si.getDimensiontoport())+Long.parseLong(si.getDimensiontostarboard()));
			st.setLong(17, Long.parseLong(si.getDimensiontostern())+Long.parseLong(si.getDimensiontobow()));
			st.setInt(18, mmsi);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
			if (st != null) {
					st.close();
			}
			if (conn != null) {
				conn.close();
			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}

	public int delete(int mmsi) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("DELETE FROM ship_info"); 
		stringBuilder.append(" WHERE mmsi=?");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		Integer res = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(stringBuilder.toString());
			st.setInt(1, mmsi);
			res = st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
			if (st != null) {
					st.close();
			}
			if (conn != null) {
				conn.close();
			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return res;
	}
    
    public class LabelValue {
        public String label;
        public String value;
    }

	public LabelValue[] GetLabelValueOfMMSIShipName(String filter) {

        StringBuilder sb = new StringBuilder();
        sb.append(" select mmsi|| ' ' ||vesselname lbl from ship_info  ");
        sb.append(" where mmsi::text like '%" + filter + "%' ");
        sb.append(" union ");
        sb.append(" select mmsi|| ' ' ||vesselname lbl from ship_info  ");
        sb.append(" where vesselname like '%" + filter.toUpperCase() + "%' ");

        LabelValue[] result;
        ArrayList<LabelValue> results = new ArrayList<LabelValue>();
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		conn = dbProvider.OpenConnection();
		try {
			st = conn.prepareStatement(sb.toString());
			rs = st.executeQuery();
			while (rs.next()) {
					LabelValue lblValue = new LabelValue();
	                lblValue.label = rs.getString("lbl");
	                lblValue.value = rs.getString("lbl");
	                results.add(lblValue);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return results.toArray(new LabelValue[0]);
	}
}
