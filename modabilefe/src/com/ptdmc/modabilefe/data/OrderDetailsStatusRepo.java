package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;

import com.ptdmc.modabilefe.entity.OrderDetailsStatus;

public class OrderDetailsStatusRepo {
	DBProvider dbProvider;
	
	public OrderDetailsStatusRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public void insert(OrderDetailsStatus orderDetailsStatus)throws SQLException  {
		// TODO: Insert Details Status
		StringBuilder sb = new StringBuilder();
        sb.append("insert into order_details_status(orddtl_id,orddtl_status,orddtl_status_date) ");
        sb.append(" values (?,?, now())");
        
        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
        try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setObject(1, orderDetailsStatus.getOrddtl_id());
			st.setString(2, orderDetailsStatus.getOrddtl_status());
			
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
    }

//	public List<OrderDetailsStatus> GetAll(){
//		List<OrderDetailsStatus> result;
//		 StringBuilder sb = new StringBuilder();
//	        sb.append("select * from order_ships");
//	        Connection conn = dbProvider.getSql2o().open();
//	        result = conn.createQuery(sb.toString()).executeAndFetch(OrderDetailsStatus.class);
//	        return result;
//	} 
}
