package com.ptdmc.modabilefe.data;

import java.util.Properties;

public class CallDBProvider {
	public static DBProvider dbProvider;
	
	private static CallDBProvider instance = null;
	
   private CallDBProvider() {
      // Exists only to defeat instantiation.
   }
   
   public static CallDBProvider getServer(Properties prop) {
      if(instance == null) {	    	  
         instance = new CallDBProvider();
         dbProvider = new DBProvider(prop.getProperty("server-pgconnstring"), 
        		 prop.getProperty("server-pguser"), prop.getProperty("server-pgpassword"));
      }
      return instance;
   }
   
   public static CallDBProvider getLocal(Properties prop){
	   if(instance == null) {	    	  
	         instance = new CallDBProvider();
	         dbProvider = new DBProvider(prop.getProperty("local-pgconnstring"), 
	        		 prop.getProperty("local-pguser"), prop.getProperty("local-pgpassword"));
	      }
	      return instance;
   }
	   
}
