package com.ptdmc.modabilefe.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;

import com.ptdmc.modabilefe.entity.Order;

public class OrdersRepo {
	DBProvider dbProvider;
	
	public OrdersRepo(DBProvider dbProvider){
		this.dbProvider = dbProvider;		
	}
	
	public int insert(Order order)throws SQLException {
		// TODO: Insert Order
		StringBuilder sb = new StringBuilder();
		int id = getNextVal();
        sb.append("insert into orders(order_date,order_po_number,order_id,order_createby,order_lastupdateby,customer_id, order_creationdate,order_lastupdate) ");
        sb.append(" values (now(), '"+order.getOrder_po_number()+"',"+id+",? ,? , ?, now(), now())");

        Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setString(1, order.getOrder_createby());
			st.setString(2, order.getOrder_lastupdateby());
			st.setLong(3, order.getCustomer_id());
			
			st.executeUpdate();
			return id;
    }
	
	private int getNextVal() throws SQLException {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select nextval('order_seq') nval ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement pst = null;
		try {
			conn = dbProvider.OpenConnection();
			conn.setAutoCommit(false);
			pst = conn.prepareStatement(stringBuilder.toString());
			rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getInt("nval");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (pst != null) {
				pst.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return 1;
	}

//	public List<Order> GetAll(){
//		List<Order> result;
//		 StringBuilder sb = new StringBuilder();
//	        sb.append("select * from order");
//	        Connection conn = dbProvider.getSql2o().open();
//	        result = conn.createQuery(sb.toString()).executeAndFetch(Order.class);
//	        return result;
//	} 
}
