package com.ptdmc.modabilefe.data;

import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Connection;

import com.ptdmc.modabilefe.data.ResultSetMapper.ResultSetMapperException;
import com.ptdmc.modabilefe.entity.ShipMislocation;

public class ShipMislocationRepo {
	DBProvider dbProvider;

	public ShipMislocationRepo(DBProvider dbProvider) {
		this.dbProvider = dbProvider;
	}

	public List<ShipMislocation> GetAll() {
		List<ShipMislocation> result = new ArrayList<ShipMislocation>();
		StringBuilder sb = new StringBuilder();
		sb.append(" select * from ship_mislocation ");
		sb.append(" order by mmsi, received_on desc ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();
			ResultSetMapper<ShipMislocation> resultSetMapper = new ResultSetMapper<ShipMislocation>();
			try {
				result = resultSetMapper.mapToList(rs, ShipMislocation.class);
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	public List<ShipMislocation> GetByMMSI(int mmsi) {
        List<ShipMislocation> result = new ArrayList<ShipMislocation>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select * from ship_mislocation ");
        sb.append(" where mmsi = ? order by received_on desc");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			// Turn use of the cursor on.
			st.setFetchSize(50);
			st.setInt(1, mmsi);
			rs = st.executeQuery();
			ResultSetMapper<ShipMislocation> resultSetMapper = new ResultSetMapper<ShipMislocation>();
			try {
				result = resultSetMapper.mapToList(rs, ShipMislocation.class);
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
    }
	public List<ShipMislocation> GetBySomeMMSI(List<Integer> mmsis)
			throws SQLException {
		List<ShipMislocation> result = new ArrayList<ShipMislocation>();

		StringBuilder sb = new StringBuilder();
        sb.append(" select * from ship_mislocation ");
        sb.append(" where mmsi in (");
		for (Integer mmsi : mmsis) {
			sb.append("? ,");
		}
		sb.deleteCharAt(sb.length() - 1); // remove last comma
        sb.append(") order by received_on desc");

		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();

			conn.setAutoCommit(false);
			st = conn.prepareStatement(sb.toString());
			int i = 1;
			for (Integer mmsi : mmsis) {
				st.setObject(i, mmsi);
				i += 1;
			}

			// Turn use of the cursor on.
			st.setFetchSize(50);
			rs = st.executeQuery();
			ResultSetMapper<ShipMislocation> resultSetMapper = new ResultSetMapper<ShipMislocation>();
			try {
				result.addAll(resultSetMapper.mapToList(rs, ShipMislocation.class));
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			if (st != null) {
				st.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

		return result;
	}

	public List<ShipMislocation> GetByMMSIReceivedOn(int mmsi, Timestamp received_on) {
        List<ShipMislocation> result = new ArrayList<ShipMislocation>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select * from ship_mislocation ");
        sb.append(" where mmsi = ? and received_on= ? order by received_on desc");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			// Turn use of the cursor on.
			st.setFetchSize(50);
			st.setInt(1, mmsi);
			st.setTimestamp(2, received_on);
			rs = st.executeQuery();
			ResultSetMapper<ShipMislocation> resultSetMapper = new ResultSetMapper<ShipMislocation>();
			try {
				result = resultSetMapper.mapToList(rs, ShipMislocation.class);
			} catch (ResultSetMapperException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
    }

	public void Insert(ShipMislocation shipMis) {
        StringBuilder sb = new StringBuilder();
        sb.append(" INSERT INTO ship_mislocation(mmsi, vesselname, received_on ");
        sb.append("  ) ");
        sb.append(" VALUES ( ?, ?, ?");
        sb.append("  ) ");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;
		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setLong(1, shipMis.getMmsi());
			st.setString(2, shipMis.getVesselname());
			st.setTimestamp(3, shipMis.getReceived_on());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }

	public void Delete(int mmsi, Timestamp received_on) {
        StringBuilder sb = new StringBuilder();
        sb.append(" delete from ship_mislocation ");
        sb.append(" where mmsi= ? and received_on= ?");
		Connection conn = null;
		ResultSet rs = null;
		java.sql.PreparedStatement st = null;

		try {
			conn = dbProvider.OpenConnection();
			st = conn.prepareStatement(sb.toString());
			st.setInt(1, mmsi);
			st.setTimestamp(2, received_on);
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
		} finally {
			try {
				if (st != null) {
					st.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
    }
}
