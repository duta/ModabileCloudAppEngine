package com.ptdmc.modabilefe;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ContohLanguage extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		Cookie[] cookies = req.getCookies();
		Language l = null;
		if (cookies != null){
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("lang")){
					l = new Language(cookies[i].getValue(), getServletContext());
					//prolong cookie
					Cookie c = new Cookie("lang",cookies[i].getValue());
					c.setMaxAge(3600*24*30);//30 days
					resp.addCookie(c);
				}
			}
		}
		if (l == null) l = new Language("id-ID", getServletContext());
	    req.setAttribute("language", l);
	    RequestDispatcher rd = req.getRequestDispatcher("/contoh.jsp");
	    rd.forward(req, resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		Cookie c = new Cookie("lang",req.getParameter("lang"));
		c.setMaxAge(3600*24*30);//30 days
		resp.addCookie(c);
	    req.setAttribute("language", new Language(req.getParameter("lang"), getServletContext()));
	    RequestDispatcher rd = req.getRequestDispatcher("/contoh.jsp");
	    rd.forward(req, resp);
	}

}
