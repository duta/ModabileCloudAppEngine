package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.ptdmc.modabilefe.entity.OrderDetails;
import com.ptdmc.modabilefe.entity.OrderShips;

public class AdminOrderServlet extends MasterServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1527736336970131205L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action != null) {
			if (action.equals("list")) {
				getListOrders(req, resp);
			} else if (action.equals("detail")) {
				getDetailOrder(req, resp);
			} else if (action.equals("ship")) {
				getOrderShips(req, resp);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String action = req.getParameter("action");
		if (action != null) {
			if (action.equals("ship")) {
				saveOrderShips(req, resp);
			}
		} else {
			saveOrderDetails(req, resp);
		}
	}

	private void saveOrderShips(HttpServletRequest req, HttpServletResponse resp) {
		try {
			String id = req.getParameter("id");
			JSONArray jsonDataKapal = new JSONArray(req.getParameter("dataKapal"));
			CallFactory.getInstance(req).getFactory().GetOrderShips().removeByOrder(Long.parseLong(id));
			for (int i = 0; i < jsonDataKapal.length(); i++) {
				JSONObject valData = jsonDataKapal.getJSONObject(i);
				OrderShips orderShips = new OrderShips();
				orderShips.setOrder_id(Long.parseLong(id));
				if (valData.getString("MMSI") != null && !valData.getString("MMSI").isEmpty()) {
					orderShips.setShip_mmsi(Long.valueOf(valData.getString("MMSI")));
				}
				orderShips.setShip_name(valData.getString("NamaKapal"));
				CallFactory.getInstance(req).getFactory().GetOrderShips().insert(orderShips);
			}
		} catch (JSONException e2) {
			e2.printStackTrace();
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	private void saveOrderDetails(HttpServletRequest req, HttpServletResponse resp) {
		String id = req.getParameter("orderDetailId");
		String quantity = req.getParameter("quantity");
		String price = req.getParameter("price");
		String currency = req.getParameter("currency");
		String discount = req.getParameter("discount");
		String startDate = req.getParameter("startDate");
		String endDate = req.getParameter("endDate");

		try {
			OrderDetails orderDetail = CallFactory.getInstance(req).getFactory().GetOrderDetails().getById(id);
			orderDetail.setOrddtl_quantity(Long.valueOf(quantity));
			orderDetail.setOrddtl_unitprice(Long.valueOf(price));
			orderDetail.setOrddtl_currency(currency);
			orderDetail.setOrddtl_discount(Long.valueOf(discount));
			DateFormat formatter= new SimpleDateFormat("dd MMMMMMMMMMMMMMMMMMMMM yyyy");
			Date date = (Date) formatter.parse(startDate);
			orderDetail.setOrddtl_contract_startdate(new Timestamp(date.getTime()));
			date = (Date) formatter.parse(endDate);
			orderDetail.setOrddtl_contract_enddate(new Timestamp(date.getTime()));

			CallFactory.getInstance(req).getFactory().GetOrderDetails().update(orderDetail);
			resp.sendRedirect("/adminOrder?action=list");
		} catch (SQLException e1) {
			e1.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getListOrders(HttpServletRequest req, HttpServletResponse resp) {
		resp.setContentType("text/html");

		try {
			List<OrderDetails> detailList = CallFactory.getInstance(req).getFactory().GetOrderDetails().getAll();
			req.setAttribute("orders", detailList);

			RequestDispatcher jsp = req.getRequestDispatcher("./views/admin/adminOrder.jsp");
			jsp.forward(req, resp);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getDetailOrder(HttpServletRequest req, HttpServletResponse resp) {
		resp.setContentType("text/html");

		try {
			String orderDetailId = req.getParameter("id");
			OrderDetails detail = CallFactory.getInstance(req).getFactory().GetOrderDetails().getById(orderDetailId);
			resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write('{');
	        resp.getWriter().write("\"orddtl_id\":"+detail.getOrddtl_id()+",");
	        resp.getWriter().write("\"orddtl_quantity\":"+detail.getOrddtl_quantity()+",");
	        resp.getWriter().write("\"orddtl_unitprice\":"+detail.getOrddtl_unitprice()+",");
	        resp.getWriter().write("\"orddtl_currency\":\""+detail.getOrddtl_currency()+"\",");
	        resp.getWriter().write("\"orddtl_discount\":"+detail.getOrddtl_discount()+",");
	        resp.getWriter().write("\"orddtl_contract_startdate\":\""+detail.getFormatted_startdate()+"\",");
	        resp.getWriter().write("\"orddtl_contract_enddate\":\""+detail.getFormatted_enddate()+"\"");
	        resp.getWriter().write('}');
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		 } catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getOrderShips(HttpServletRequest req, HttpServletResponse resp) {
		resp.setContentType("text/html");
		String orderDetailId = req.getParameter("id");
		try {
			OrderShips od = new OrderShips();
			od.setOrder_id(Long.parseLong(orderDetailId));
			List<OrderShips> detail = CallFactory.getInstance(req).getFactory().GetOrderShips().getWhere(od);
			resp.setContentType("application/json");
	        resp.setCharacterEncoding("UTF-8");
	        resp.getWriter().write('[');
	        for (int i = 0; i < detail.size(); i++) {
		        resp.getWriter().write('{');
		        resp.getWriter().write("\"Ship_mmsi\":"+detail.get(i).getShip_mmsi()+",");
		        resp.getWriter().write("\"Ship_name\":\""+detail.get(i).getShip_name()+"\"");
		        resp.getWriter().write('}');
		        if (i != detail.size() - 1) {
			        resp.getWriter().write(',');
		        }
			}
	        resp.getWriter().write(']');
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
