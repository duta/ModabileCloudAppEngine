package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.geojson.Feature;

import com.ptdmc.modabilefe.entity.ShipMislocation;
import com.ptdmc.modabilefe.geo.ShipFeatureCollection;

@SuppressWarnings("serial")
public class ShipMislocationServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){
		String path = req.getRequestURI();
		String[] next = path.split("/");
		if (next.length <= 2) {
			try {
				res.sendRedirect("/shipMislocation/set");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		if (next[2].equals("set")) {
			ShowSetPage(req, res);
		}
		if (next[2].equals("daftar")) {
			ShowDaftarPage(req, res);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res){
		String path = req.getRequestURI();
		String[] next = path.split("/");
		if (next[2].equals("set")) {
			if (req.getParameter("MMSISet") != ""){
				ShipMislocation shipMis = new ShipMislocation();
				shipMis.setMmsi(Long.parseLong(req.getParameter("MMSISet")));
				DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmss");
				try {
					shipMis.setReceived_on(new Timestamp(formatter2.parse(req.getParameter("TimeStampSet")).getTime()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				shipMis.setVesselname(req.getParameter("ShipNameSet"));
				CallFactory.getInstance(req).getFactory().GetShipMislocation().Insert(shipMis);
			}
			ShowSetPage(req, res);
		}
		if (next[2].equals("daftar")) {
			if (req.getParameter("MMSISet") != ""){
				DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
				try {
					CallFactory.getInstance(req).getFactory().GetShipMislocation().Delete(Integer.parseInt(req.getParameter("MMSISet")), new Timestamp(formatter2.parse(req.getParameter("TimeStampSet")).getTime()));
				} catch (NumberFormatException | ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			ShowDaftarPage(req, res);
		}
	}

	private void ShowSetPage(HttpServletRequest req, HttpServletResponse res) {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/shipmislocation/Set.jsp");
		try {
			if (req.getParameter("MMSIKapal") != null){				
				DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
				DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
				DateFormat formatter3 = new SimpleDateFormat("yyyyMMddHHmmss");
				DecimalFormat df2 = new DecimalFormat("#");
				Calendar cal = Calendar.getInstance();
				Date date = (Date) formatter.parse(req.getParameter("From"));
				cal.setTime(date);
				cal.add(Calendar.SECOND, -this.customer.getCustomer_gmtoffset());
				date = cal.getTime();
				
				Date date2 = (Date) formatter.parse(req.getParameter("To"));
				cal.setTime(date2);
				cal.add(Calendar.SECOND, -this.customer.getCustomer_gmtoffset());
				date2 = cal.getTime();
				
				ShipFeatureCollection sfc = CallFactory.getInstance(req).getFactory().GetIncomingAis().RecordedAllPositionByMMSIAndDate(Integer.parseInt(req.getParameter("MMSIKapal")), new Timestamp(date.getTime()), new Timestamp(date2.getTime()));
				List<String> filter = new ArrayList<String>();
				for (Feature feature : sfc.getFeatures()) {			        
					Date datez = formatter2.parse(df2.format(feature.getProperties().get("TimeStampReceiver")));					
					cal.setTime(datez);
					cal.add(Calendar.SECOND, this.customer.getCustomer_gmtoffset());
					
					if (CallFactory.getInstance(req).getFactory().GetShipMislocation().GetByMMSIReceivedOn((int) feature.getProperties().get("MMSI"), new Timestamp(formatter3.parse(df2.format(feature.getProperties().get("TimeStampReceiver")).substring(0, 14)).getTime())).size() == 0) {
						filter.add("<tr><td>"+formatter.format(cal.getTime())+"</td><td> <input type=\"button\"  value=\"Set Mislokasi\" onclick=\"SetMislokasi(" + feature.getProperties().get("MMSI") + ", " + df2.format(feature.getProperties().get("TimeStampReceiver")).substring(0, 14) + ", '" + feature.getProperties().get("ShipName") + "');\" /></td></tr>");
					}
				}
				req.setAttribute("filtered", filter);
				req.setAttribute("MMSIKapal", req.getParameter("MMSIKapal"));
				req.setAttribute("From", req.getParameter("From"));
				req.setAttribute("To", req.getParameter("To"));
			}
			List<Integer> mmsis = null;

			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
				if (role.role_name.equals("superadmin (ADMIN)"))
					mmsis = CallFactory.getInstance(req).getFactory().GetShipInfo()
							.getAll();
			}
			if (mmsis == null || mmsis.size() == 0)
				mmsis = CallFactory
						.getInstance(req)
						.getFactory()
						.GetCustomer()
						.getActiveMMSI(
								CallFactory.getInstance(req).getFactory()
										.GetCustomer().GetByEmail(this.user.getEmail()));
			req.setAttribute("mmsis", CallFactory.getInstance(req).getFactory().GetShipInfo().GetBySomeMMSI(mmsis));
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void ShowDaftarPage(HttpServletRequest req, HttpServletResponse res) {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/shipmislocation/Daftar.jsp");
		try {
			List<String> filter = new ArrayList<String>();
			List<Integer> mmsis = null;

			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
				if (role.role_name.equals("superadmin (ADMIN)"))
					mmsis = CallFactory.getInstance(req).getFactory().GetShipInfo()
							.getAll();
			}
			if (mmsis == null || mmsis.size() == 0)
				mmsis = CallFactory
						.getInstance(req)
						.getFactory()
						.GetCustomer()
						.getActiveMMSI(
								CallFactory.getInstance(req).getFactory()
										.GetCustomer().GetByEmail(this.user.getEmail()));
			List<ShipMislocation> lsm = CallFactory.getInstance(req).getFactory().GetShipMislocation().GetBySomeMMSI(mmsis);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			DateFormat formatter2 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");
			
			Calendar cal = Calendar.getInstance();
	        DecimalFormat df = new DecimalFormat("#");
			
			for (ShipMislocation feature : lsm) {
				cal.setTime(feature.getReceived_on());
				cal.add(Calendar.SECOND, this.customer.getCustomer_gmtoffset());
				
				filter.add("<tr><td>"+feature.getMmsi()+"</td><td>"+feature.getVesselname()+"</td><td>"+formatter.format(cal.getTime())+"</td><td><input type=\"button\"  value=\"Hapus\" onclick=\"DeleteMisLokasi("+feature.getMmsi()+", '"+formatter2.format(feature.getReceived_on())+"');\" /></td></tr>");
			}
			req.setAttribute("filtered", filter);
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
