package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.entity.Product;
import com.ptdmc.modabilefe.entity.ProductPrice;
import com.ptdmc.modabilefe.entity.leyfi.Roles;

@SuppressWarnings("serial")
public class ProductServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){
		List<Product> products = null;
		try {
			products = CallFactory.getInstance(req).getFactory().GetProduct().GetAll();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		req.setAttribute("products",products);

		RequestDispatcher jsp = req.getRequestDispatcher("/views/product/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException 
	{
		boolean isadmin = false;
		if (roles!=null){
			for (Roles role : roles) {
				if (role.role_name.equals("superadmin (ADMIN)")) isadmin = true;
			}
		}
		req.setAttribute("isADMIN", isadmin);		
		
		String action = req.getParameter("action");
		Product p = new Product();
		
		if(action.equals("insert") && isadmin){
			p.setProduct_name(req.getParameter("product_name"));
			p.setProduct_description(req.getParameter("product_description"));
			p.setPp_price(0);
			p.setProduct_createby("");
			p.setProduct_lastupdateby("");
			try {
				CallFactory.getInstance(req).getFactory().GetProduct().Insert(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("do_update") && isadmin){
			p.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			p.setProduct_name(req.getParameter("product_name"));
			p.setProduct_description(req.getParameter("product_description"));
			try {
				CallFactory.getInstance(req).getFactory().GetProduct().Update(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("update_harga") && isadmin){
			ProductPrice pp = new ProductPrice();
			p.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			p.setPp_price(Integer.valueOf(req.getParameter("pp_price")));
			pp.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			pp.setPp_price(Integer.valueOf(req.getParameter("pp_price")));
			pp.setPp_currency(req.getParameter("pp_currency"));
			pp.setPp_createby("");
			pp.setPp_lastupdateby("");
			try {
				CallFactory.getInstance(req).getFactory().GetProduct().Update_harga(p);
				CallFactory.getInstance(req).getFactory().GetProductPrice().Insert(pp);;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("delete") && isadmin){
			p.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			try {
				CallFactory.getInstance(req).getFactory().GetProduct().Delete(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		RequestDispatcher jsp = null;
		if(action.equals("update") && isadmin){
			p.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			List<Product> product = null;
			try {
				product = CallFactory.getInstance(req).getFactory().GetProduct().GetWhere(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			req.setAttribute("product_id",product.get(0).getProduct_id());
			req.setAttribute("product_name",product.get(0).getProduct_name());
			req.setAttribute("product_description",product.get(0).getProduct_description());
			jsp = req.getRequestDispatcher("/views/product/form.jsp");
		}else if(action.equals("harga") && isadmin){
			p.setProduct_id(Integer.valueOf(req.getParameter("product_id")));
			List<ProductPrice> prices = null;
			try {
				prices = CallFactory.getInstance(req).getFactory().GetProductPrice().GetWhere(p);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			req.setAttribute("product_id",p.getProduct_id());
			req.setAttribute("prices",prices);
			jsp = req.getRequestDispatcher("/views/product/harga.jsp");
		}else if(isadmin){
			List<Product> products = null;
			try {
				products = CallFactory.getInstance(req).getFactory().GetProduct().GetAll();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			req.setAttribute("products",products);
			jsp = req.getRequestDispatcher("/views/product/index.jsp");
		}
		try {
			if (req.getParameter("return")== null){
				jsp.forward(req, res);
			} else {
				res.sendRedirect("/adduser");
			}
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	
}
