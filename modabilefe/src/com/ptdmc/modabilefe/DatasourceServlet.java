package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.entity.AisDatasource;
import com.ptdmc.modabilefe.entity.leyfi.Roles;

@SuppressWarnings("serial")
public class DatasourceServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){			
		RequestDispatcher jsp = null;
		String pathString = req.getServletPath().toLowerCase();
		if (pathString.equals("/datasource") ) {
			List<AisDatasource> result = null;
			try {
				result = CallFactory.getInstance(req).getFactory().GetAisDataSource().GetAll();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			req.setAttribute("aisdatasourcelist", result);

			jsp = req.getRequestDispatcher("/views/datasource/index.jsp");
		} else if (pathString.equals("/datasource/ubah")) {
			int id = 0;
			try {
				id =Integer.parseInt(req.getParameter("id"));
				AisDatasource ads = new AisDatasource();
				try {
					ads = CallFactory.getInstance(req).getFactory().GetAisDataSource().GetById(id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				req.setAttribute("ads",ads);
				res.setContentType("text/html");
				jsp = req.getRequestDispatcher("../views/datasource/ubah.jsp");
			} catch (NumberFormatException e) {
				jsp = req.getRequestDispatcher("../views/datasource/index.jsp");
			}			
		} 
		
		
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException 
	{		
		boolean isadmin = false;
		if (roles!=null){
			for (Roles role : roles) {
				if (role.role_name.equals("superadmin (ADMIN)")) isadmin = true;
			}
		}
		req.setAttribute("isADMIN", isadmin);		
		
		String action = req.getParameter("action");
		AisDatasource p = new AisDatasource();
		
		if(action.equals("insert") && isadmin){
			p.setAd_vendorname(req.getParameter("vendor"));
			p.setAd_url(req.getParameter("url"));
			p.setAd_urlusername(req.getParameter("name"));
			p.setAd_urlpassword(req.getParameter("password"));
			try {
				CallFactory.getInstance(req).getFactory().GetAisDataSource().Insert(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("update") && isadmin){
			p.setAd_id(Integer.valueOf(req.getParameter("id")));
			p.setAd_vendorname(req.getParameter("vendor"));
			p.setAd_url(req.getParameter("url"));
			p.setAd_urlusername(req.getParameter("name"));
			p.setAd_urlpassword(req.getParameter("password"));
			try {
				CallFactory.getInstance(req).getFactory().GetAisDataSource().UpdateById(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("delete") && isadmin){
			p.setAd_id(Integer.valueOf(req.getParameter("id")));
			try {
				CallFactory.getInstance(req).getFactory().GetAisDataSource().Delete(p);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		try {
			res.sendRedirect("/datasource");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
