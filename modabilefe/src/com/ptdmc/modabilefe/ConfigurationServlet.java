package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.data.KonfigurasiRepo;
import com.ptdmc.modabilefe.data.leyfi.Roles;
import com.ptdmc.modabilefe.data.leyfi.Users;
import com.ptdmc.modabilefe.util.Mail;

@SuppressWarnings("serial")
public class ConfigurationServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/configuration/index.jsp");
		Factory factory = CallFactory.getInstance(req).getFactory();
		try {
			req.setAttribute("listKonfigurasi", factory.GetKonfigurasi().GetAll());
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException {
		KonfigurasiRepo factory = CallFactory.getInstance(req).getFactory().GetKonfigurasi();
		String path = ((HttpServletRequest) req).getRequestURI().toString();
		try {
			if (path.equals("/configuration/add")) {
				factory.Insert(req.getParameter("key"), req.getParameter("value"), "true".equalsIgnoreCase(req.getParameter("mandatory")));
			}
			if (path.startsWith("/configuration/edit")) {
				// save user
				factory.Update(req.getParameter("oldkey"), req.getParameter("key"), req.getParameter("value"), "true".equalsIgnoreCase(req.getParameter("mandatory")));
			}
			if (path.startsWith("/configuration/delete")) {
				factory.Delete(req.getParameter("key"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			try {
				res.sendError(500, errors.toString());
			} catch (IOException ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
	}
	
}
