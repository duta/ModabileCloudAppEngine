package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.entity.leyfi.*;

@SuppressWarnings("serial")
public class LeyfiServlet extends MasterServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		String path = req.getRequestURI();
		String[] next = path.split("/");
		if (next.length <= 2) {
			try {
				res.sendRedirect("/leyfi/users");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		if (next[2].equals("users")) {
			if (next.length > 3) {
				try {
					int id = Integer.parseInt(next[3]);
					Users user = CallFactory.getInstance(req).getFactory()
							.GetUsers().GetUserByUserID(id);
					req.setAttribute("userName", user.user_name);
					ArrayList<Roles> userRoles = CallFactory.getInstance(req)
							.getFactory().GetUsers().GetRolesByUserID(id);
					req.setAttribute("allRoles", userRoles);
					RequestDispatcher jsp = req
							.getRequestDispatcher("/views/leyfi/usersup.jsp");
					jsp.forward(req, res);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				doUser(req, res);
			}
		}
		if (next[2].equals("roles")) {
			if (next.length > 3) {
				try {
				if (next[3].startsWith("all")) {
						String x = req.getParameter("term");
						ArrayList<Roles> allEles = CallFactory.getInstance(req)
								.getFactory().GetRoles().GetAll();
						res.setContentType("application/json");
						// Get the printwriter object from response to write the
						// required json object to the output stream
						PrintWriter out = res.getWriter();
						// Assuming your json object is **jsonObject**, perform
						// the
						// following, it will return your json object
						out.print("[");
						String temp = "";
						for (Roles ele : allEles) {
							if (ele.role_name.indexOf(x) == -1)
								continue;
							temp += "{\"label\":\"" + ele.role_name
									+ "\",\"value\":" + ele.role_id + "},";
						}
						if (temp.length() > 0)
							temp = temp.substring(0, temp.length() - 1);
						out.print(temp);
						out.print("]");
						out.flush();
				} else {
						int id = Integer.parseInt(next[3]);
						Roles user = CallFactory.getInstance(req).getFactory()
								.GetRoles().GetRoleByID(id);
						req.setAttribute("userName", user.role_name);
						ArrayList<Elements> userRoles = CallFactory
								.getInstance(req).getFactory().GetRoles()
								.GetElementsPermittedByRoleID(id);
						req.setAttribute("allRoles", userRoles);
						RequestDispatcher jsp = req
								.getRequestDispatcher("/views/leyfi/rolesup.jsp");
						jsp.forward(req, res);
				}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				doRole(req, res);
			}
		}
		if (next[2].equals("elements")) {
			if (next.length > 3) {
				if (next[3].equals("all")) {
					try {
						String x = req.getParameter("term");
						ArrayList<Elements> allEles = CallFactory
								.getInstance(req).getFactory().GetElements()
								.GetAll();
						res.setContentType("application/json");
						// Get the printwriter object from response to write the
						// required json object to the output stream
						PrintWriter out = res.getWriter();
						// Assuming your json object is **jsonObject**, perform
						// the
						// following, it will return your json object
						out.print("[");
						String temp = "";
						for (Elements ele : allEles) {
							if (ele.element_type.indexOf(x) == -1)
								continue;
							temp += "{\"label\":\"" + ele.element_type
									+ "\",\"value\":" + ele.element_id + "},";
						}
						if (temp.length() > 0)
							temp = temp.substring(0, temp.length() - 1);
						out.print(temp);
						out.print("]");
						out.flush();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				doElement(req, res);
			}
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		String path = req.getRequestURI();
		String[] next = path.split("/");
		try {
			if (next[2].equals("users")) {
				if (next[3].equals("add")) {
					String name = req.getParameter("name");
					CallFactory.getInstance(req).getFactory().GetUsers()
							.Upsert(null, name, new byte[0]);
					int id = CallFactory.getInstance(req).getFactory()
							.GetUsers().GetUserIDByUserName(name);
					if (req.getParameter("tags").length() > 0) {
						String[] roles = req.getParameter("tags").split(",");
						for (String role : roles) {
							CallFactory
									.getInstance(req)
									.getFactory()
									.GetUsers()
									.InsertUsersInRoles(Integer.parseInt(role),
											id);
						}
					}
					res.sendRedirect("/leyfi/users");
				} else if (next[3].equals("delete")) {
					String id = req.getParameter("something");
					CallFactory.getInstance(req).getFactory().GetUsers()
							.Delete(Integer.parseInt(id));
				} else {
					int id = Integer.parseInt(next[3]);
					CallFactory.getInstance(req).getFactory().GetUsers()
							.Upsert(id, req.getParameter("name"), new byte[0]);
					ArrayList<Roles> userRoles = CallFactory.getInstance(req)
							.getFactory().GetUsers().GetRolesByUserID(id);
					for (Roles role : userRoles) {
						CallFactory.getInstance(req).getFactory().GetUsers()
								.DeleteUsersInRoles(role.role_id, id);
					}
					String[] roles = req.getParameter("tags").split(",");
					for (String role : roles) {
						CallFactory.getInstance(req).getFactory().GetUsers()
								.InsertUsersInRoles(Integer.parseInt(role), id);
					}
					res.sendRedirect("/leyfi/users");
				}
			}
			if (next[2].equals("roles")) {
				if (next[3].equals("add")) {
					String name = req.getParameter("name");
					CallFactory.getInstance(req).getFactory().GetRoles()
							.Upsert(null, name, "");
					int id = CallFactory.getInstance(req).getFactory()
							.GetRoles().GetIDByName(name);
					if (req.getParameter("tags").length() > 0) {
						String[] roles = req.getParameter("tags").split(",");
						for (String role : roles) {
							CallFactory
									.getInstance(req)
									.getFactory()
									.GetRoles()
									.InsertPermission(id,
											Integer.parseInt(role));
						}
					}
					res.sendRedirect("/leyfi/roles");
				} else if (next[3].equals("delete")) {
					String id = req.getParameter("something");
					CallFactory.getInstance(req).getFactory().GetRoles()
							.Delete(Integer.parseInt(id));
				} else {
					int id = Integer.parseInt(next[3]);
					CallFactory.getInstance(req).getFactory().GetRoles()
							.Upsert(id, req.getParameter("name"), "");
					ArrayList<Elements> userRoles = CallFactory
							.getInstance(req).getFactory().GetRoles()
							.GetElementsPermittedByRoleID(id);
					for (Elements role : userRoles) {
						CallFactory.getInstance(req).getFactory().GetRoles()
								.DeletePermission(id, role.element_id);
					}
					String[] roles = req.getParameter("tags").split(",");
					for (String role : roles) {
						CallFactory.getInstance(req).getFactory().GetRoles()
								.InsertPermission(id, Integer.parseInt(role));
					}
					res.sendRedirect("/leyfi/roles");
				}
			}
			if (next[2].equals("elements")) {
				if (next[3].equals("add")) {
					String name = req.getParameter("name");
					CallFactory.getInstance(req).getFactory().GetElements()
							.Upsert(null, name);
				} else if (next[3].equals("delete")) {
					String id = req.getParameter("something");
					CallFactory.getInstance(req).getFactory().GetElements()
							.Delete(Integer.parseInt(id));
				}
				doElement(req, res);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			req.setAttribute("ex", errors.toString());
			try {
				jsp.forward(req, res);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private void doElement(HttpServletRequest req, HttpServletResponse res) {
		try {
			ArrayList<Elements> allEles = CallFactory.getInstance(req)
					.getFactory().GetElements().GetAll();
			req.setAttribute("allEles", allEles);
			RequestDispatcher jsp = req
					.getRequestDispatcher("/views/leyfi/elements.jsp");
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
			req.setAttribute("ex", e);
			try {
				jsp.forward(req, res);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private void doRole(HttpServletRequest req, HttpServletResponse res) {
		try {
			ArrayList<Roles> allUsers = CallFactory.getInstance(req)
					.getFactory().GetRoles().GetAll();
			ArrayList<String[]> allValue = new ArrayList<String[]>();
			for (Roles user : allUsers) {
				String[] temp = new String[3];
				temp[0] = user.role_id.toString();
				temp[1] = user.role_name;
				temp[2] = "";
				ArrayList<Elements> userRoles = CallFactory.getInstance(req)
						.getFactory().GetRoles()
						.GetElementsPermittedByRoleID(user.role_id);
				for (Elements role : userRoles) {
					temp[2] += role.element_type + ", ";
				}
				if (temp[2].length() > 0)
					temp[2] = temp[2].substring(0, temp[2].length() - 2);
				allValue.add(temp);
			}
			req.setAttribute("allValue", allValue);
			RequestDispatcher jsp = req
					.getRequestDispatcher("/views/leyfi/roles.jsp");
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
			req.setAttribute("ex", e);
			try {
				jsp.forward(req, res);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	private void doUser(HttpServletRequest req, HttpServletResponse res) {
		try {
			ArrayList<Users> allUsers = CallFactory.getInstance(req)
					.getFactory().GetUsers().GetAll();
			ArrayList<String[]> allValue = new ArrayList<String[]>();
			for (Users user : allUsers) {
				String[] temp = new String[3];
				temp[0] = user.user_id.toString();
				temp[1] = user.user_name;
				temp[2] = "";
				ArrayList<Roles> userRoles = CallFactory.getInstance(req)
						.getFactory().GetUsers().GetRolesByUserID(user.user_id);
				for (Roles role : userRoles) {
					temp[2] += role.role_name + ", ";
				}
				if (temp[2].length() > 0)
					temp[2] = temp[2].substring(0, temp[2].length() - 2);
				allValue.add(temp);
			}
			req.setAttribute("allValue", allValue);
			RequestDispatcher jsp = req
					.getRequestDispatcher("/views/leyfi/users.jsp");
			jsp.forward(req, res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
			req.setAttribute("ex", e);
			try {
				jsp.forward(req, res);
			} catch (ServletException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
