package com.ptdmc.modabilefe;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

@SuppressWarnings("serial")
public class ReReservationServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		
		boolean local = "localhost".equals(req.getServerName());
		String loginUrl = userService.createLoginURL("/");
		String logoutUrl = userService.createLogoutURL("/");
		
		req.setAttribute("user", user);
		req.setAttribute("loginUrl", loginUrl);
		req.setAttribute("logoutUrl", logoutUrl);
		res.setContentType("text/html");
	
		
		if (user != null){
			req.setAttribute("useremail", user.getEmail());			
		}
		else {
			req.setAttribute("useremail", "not login yet");	
		}
		req.setAttribute("nameserver", req.getServerName());
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/rereservation/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
}
