package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.OrderDetails;
import com.ptdmc.modabilefe.entity.OrderShips;
import com.ptdmc.modabilefe.entity.ShipInfo;

@SuppressWarnings("serial")
public class ListServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){
		if (this.user != null){
			HashMap<OrderShips, ShipInfo> osList = null;
			OrderDetails o = null;
			try {
				Factory factory = CallFactory.getInstance(req).getFactory();
				o = factory.GetOrderDetails().getLastByCustomerId(this.customer.getCustomer_id());
				if (o!=null){
				long diff = (o.getOrddtl_contract_enddate().getTime() - o.getOrddtl_contract_startdate().getTime());
				req.setAttribute("diff",diff / 2592000000L);
				} else {
					req.setAttribute("diff",0);
				}
				osList = factory.GetOrderShips().getLastOrder(this.customer);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			req.setAttribute("useremail", this.user.getEmail());
			req.setAttribute("osList",osList);
			req.setAttribute("contractStartDate", new SimpleDateFormat("dd/MMM/yyyy").format(o.getOrddtl_contract_startdate()) );
			req.setAttribute("contractEndDate", new SimpleDateFormat("dd/MMM/yyyy").format(o.getOrddtl_contract_enddate()) );
		}
		else {
			req.setAttribute("useremail", "not login yet");	
		}
		req.setAttribute("nameserver", req.getServerName());
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/list/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse res){
//		if (this.user != null){
//			List<OrderShips> osList = null;
//			try {
//				Factory factory = CallFactory.getInstance(req).getFactory();
//				OrderDetails o = factory.GetOrderDetails().getLastByCustomerId(this.customer.getCustomer_id());
//				if (o!=null){
//				long diff = (o.getOrddtl_contract_enddate().getTime() - o.getOrddtl_contract_startdate().getTime());
//				req.setAttribute("diff",diff / 2592000000L);
//				} else {
//					req.setAttribute("diff",0);
//				}
//				//osList = factory.GetOrderShips().getLastOrder(this.customer);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//			req.setAttribute("useremail", this.user.getEmail());
//			req.setAttribute("osList",osList);
//		}
//		else {
//			req.setAttribute("useremail", "not login yet");	
//		}
//		req.setAttribute("nameserver", req.getServerName());
//		
//		RequestDispatcher jsp = req.getRequestDispatcher("./views/list/index.jsp");
//		try {
//			jsp.forward(req, res);
//		} catch (ServletException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
}
