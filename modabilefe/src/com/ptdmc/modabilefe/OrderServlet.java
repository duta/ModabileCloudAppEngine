package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.ptdmc.modabilefe.entity.OrderDetails;

@SuppressWarnings("serial")
public class OrderServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){	
		List<OrderDetails> od = null;
		try {
			od = CallFactory.getInstance(req).getFactory().GetOrderDetails().getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.setAttribute("orders",od);
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/order/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res){
		List<OrderDetails> od = null;
		try {
			od = CallFactory.getInstance(req).getFactory().GetOrderDetails().getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.setAttribute("orders",od);
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/order/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
