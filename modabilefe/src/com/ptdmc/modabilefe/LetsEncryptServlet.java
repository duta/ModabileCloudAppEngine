package com.ptdmc.modabilefe;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.*;

@SuppressWarnings("serial")
public class LetsEncryptServlet extends HttpServlet {
	public static final Map<String, String> challenges = new HashMap<String, String>();

    static {
        challenges.put("0u-1BTpe5zBp5MlaXzwve1XlxAeMSjkH_QFxTx7Vb6M",
                "0u-1BTpe5zBp5MlaXzwve1XlxAeMSjkH_QFxTx7Vb6M.Ha3F8QT0_XKgab2xFAOM8qSiyPYCQENs_yadcnlh5Mw");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (!req.getRequestURI().startsWith("/.well-known/acme-challenge/")) {
            resp.sendError(404);
            return;
        }
        String id = req.getRequestURI().substring("/.well-known/acme-challenge/".length());
        if (!challenges.containsKey(id)) {
            resp.sendError(404);
            return;
        }
        resp.setContentType("text/plain");
        resp.getOutputStream().print(challenges.get(id));
    }
}
