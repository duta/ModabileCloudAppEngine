package com.ptdmc.modabilefe;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.ptdmc.modabilefe.data.CustomerRepo;
import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.data.leyfi.Users;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.leyfi.Roles;
import com.ptdmc.modabilefe.util.Mail;

@SuppressWarnings("serial")
public class HomeServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){		
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		HttpSession dataSession = req.getSession();
		
		boolean local = "localhost".equals(req.getServerName());
		String loginUrl = userService.createLoginURL("/");
		String logoutUrl = userService.createLogoutURL("/");
		
		req.setAttribute("user", user);
		req.setAttribute("loginUrl", loginUrl);
		req.setAttribute("logoutUrl", logoutUrl);
		res.setContentType("text/html");
		
		if (user != null){

			req.setAttribute("useremail", user.getEmail());		
			
			String email = user.getEmail();
			
			//Set Session			
			dataSession.setAttribute("emailCustomer", email);
			
			//Check Register 
			try {
				Factory factory = CallFactory.getInstance(req).getFactory();
				Users userFactory = factory.GetUsers();
				CustomerRepo customerFactory = factory.GetCustomer();

				int checkRegister = customerFactory.checkRegisterUser(email);
				Customer cr = factory.GetCustomerMember().getCustomerByUserID(userFactory.GetUserIDByUserName(user.getEmail()));
				if (cr == null) cr = factory.GetCustomer().GetByEmail(user.getEmail());
				if(checkRegister == 0 && cr == null){
					//Not Yet
					Customer customer = new Customer();
					customer.setCustomer_email(email);
					
					try {
						customerFactory.insert(customer);
						res.sendRedirect("/profile");
						userFactory.Upsert(null, email, new byte[0]);
						userFactory.
							InsertUsersInRoles(factory.GetRoles().GetIDByName("customer admin (USER)"), 
									userFactory.GetUserIDByUserName(email));
						// send email
						String[] s = PropertiesReader.getInstance().getProperties().getProperty("admin-notified").split(",");
						ArrayList<String> recipients = new ArrayList<String>();
						for (int i = 0; i < s.length; i++) {
							recipients.add(s[i]);
						}
						HashMap<String, String> parameters = new HashMap<String, String>();
						parameters.put("creator", email);
						parameters.put("created", email);
						parameters.put("page", "Login");
						Mail.SendMail("noreply@modabile.appspotmail.com", recipients, "User baru Modabile App Engine", "resource/MailTemplate/NewUser.html", parameters);
					} catch (Exception e) {
						e.printStackTrace();
						RequestDispatcher jsp = req.getRequestDispatcher("/views/500.jsp");
						StringWriter errors = new StringWriter();
						e.printStackTrace(new PrintWriter(errors));
						req.setAttribute("ex", errors.toString());
						req.setAttribute("text", "Maaf sistem belum bisa melakukan login/pendaftaran.<br>Kembali ke <a href=\"/\">halaman utama</a><br>");
						try {
							jsp.forward(req, res);
							return;
						} catch (ServletException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}else{
					//Done
					try {
						boolean isAdmin = false;
						List<Roles> roles = factory.GetUsers().GetRolesByUserID(userFactory.GetUserIDByUserName(user.getEmail()));
						for (Roles r : roles) {
							if (r.role_name.equals("superadmin (ADMIN)")){
								isAdmin = true;
								res.sendRedirect("/customer");
							}
						}
						if (!isAdmin) res.sendRedirect("/list");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			req.setAttribute("useremail", "not login yet");	
		}
		req.setAttribute("nameserver", req.getServerName());
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/home/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
