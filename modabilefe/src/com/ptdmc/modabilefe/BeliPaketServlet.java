package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.Order;
import com.ptdmc.modabilefe.entity.OrderDetails;
import com.ptdmc.modabilefe.entity.OrderDetailsStatus;
import com.ptdmc.modabilefe.entity.OrderShips;
import com.ptdmc.modabilefe.entity.Product;
import com.ptdmc.modabilefe.util.CommonUtil;

@SuppressWarnings("serial")
public class BeliPaketServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){		
		List<Product> products = null;
		try {
			products = CallFactory.getInstance(req).getFactory().GetProduct().GetAll();
			req.setAttribute("cancel",(CallFactory.getInstance(req).getFactory().GetCustomer().hadAPacket(
					this.customer)?-1:CommonUtil.PRODUCT_TRIAL_ID));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		req.setAttribute("products",products);
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/belipaket/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		//long order_id = 1;
		//long orddtl_id = 1;
		long customer_id = -1;
		Product p = new Product();
		long product_id = Integer.valueOf(req.getParameter("product"));
		try {
			customer_id = this.customer.getCustomer_id();
			p.setProduct_id((int) product_id);
			p = CallFactory.getInstance(req).getFactory().GetProduct().GetOne(p);
		} catch (SQLException e3) {
			e3.printStackTrace();
			return;
		}
		long orddtl_discount= 0;
		long orddtl_quantity= 1;
		long orddtl_unitprice= orddtl_quantity * p.getPp_price();
		
		String orddtl_currency = "RP";
		String order_po_number = CommonUtil.generatePONumber();
		String orddtl_status = CommonUtil.ORDER_STATUS_PEMESANAN;
		
		String createby = user.getEmail();
		String tupdateby = user.getEmail();
		
		java.util.Date DateNow= new java.util.Date();
		String timeStamp = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(DateNow);
		String dateCurrent = new SimpleDateFormat("yyyy/MM/dd").format(DateNow);
		DateFormat formatter;
		formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date;
		java.sql.Timestamp timeStampDate = null;
		java.sql.Timestamp timeStampDateNext = null;
		try {
			date = (Date) formatter.parse(timeStamp);
			timeStampDate = new Timestamp(date.getTime());
			Calendar cal = Calendar.getInstance();
			cal.setTime(timeStampDate);
			cal.add(Calendar.DAY_OF_WEEK, 30 * p.getProduct_period());
			timeStampDateNext = new Timestamp(cal.getTime().getTime());			
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		
  
		Order order = new Order();
		order.setCustomer_id(customer_id);
		order.setOrder_createby(createby);
		//order.setOrder_id(order_id);
		order.setOrder_lastupdateby(tupdateby);
		order.setOrder_po_number(order_po_number);
		
		OrderDetails orderDetail = new OrderDetails();
		orderDetail.setCustomer_id(customer_id);
		orderDetail.setOrddtl_contract_enddate(timeStampDateNext);
		orderDetail.setOrddtl_contract_startdate(timeStampDate);
		orderDetail.setOrddtl_createby(createby);
		orderDetail.setOrddtl_currency(orddtl_currency);
		orderDetail.setOrddtl_discount(orddtl_discount);
		//orderDetail.setOrddtl_id(orddtl_id);
		orderDetail.setOrddtl_lastupdateby(tupdateby);
		orderDetail.setOrddtl_quantity(orddtl_quantity);
		orderDetail.setOrddtl_unitprice(orddtl_unitprice);
		//orderDetail.setOrder_id(order_id);
		orderDetail.setProduct_id(product_id);
		
		
		OrderDetailsStatus orderDetailStatus = new OrderDetailsStatus();
		//orderDetailStatus.setOrddtl_id(orddtl_id);
		orderDetailStatus.setOrddtl_status(orddtl_status);
		
		int orderid=-1;
		int orddtl=-1;
		try {
			orderid = CallFactory.getInstance(req).getFactory().GetOrders().insert(order);
			orderDetail.setOrder_id((long) orderid);
			orddtl=CallFactory.getInstance(req).getFactory().GetOrderDetails().insert(orderDetail);
			orderDetailStatus.setOrddtl_id((long) orddtl);
			CallFactory.getInstance(req).getFactory().GetOrderDetailsStatus().insert(orderDetailStatus);
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			JSONArray jsonDataKapal = new JSONArray(req.getParameter("dataKapal"));
			
			for (int i = 0; i < jsonDataKapal.length(); i++) {
				try {
					JSONObject valData = jsonDataKapal.getJSONObject(i);
					
					OrderShips orderShips = new OrderShips();
					orderShips.setOrder_id((long) orddtl);
					if (valData.getString("MMSI") != null && !valData.getString("MMSI").isEmpty()) {
						orderShips.setShip_mmsi(Long.valueOf(valData.getString("MMSI")));
					}
					orderShips.setShip_name(valData.getString("NamaKapal"));
					
					try {
						CallFactory.getInstance(req).getFactory().GetOrderShips().insert(orderShips);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
		      }
		} catch (JSONException e2) {
			e2.printStackTrace();
		}
	}
	
}
