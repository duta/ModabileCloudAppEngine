package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.entity.OrderShips;
import com.ptdmc.modabilefe.entity.ShipInfo;

@SuppressWarnings("serial")
public class ReservationServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){				
		RequestDispatcher jsp = req.getRequestDispatcher("./views/reservation/index.jsp");
		List<OrderShips> osList = new ArrayList<OrderShips>();
		try {			
			osList = CallFactory.getInstance(req).getFactory().GetOrderShips().getLastOrder2(this.customer);
			req.setAttribute("osList",osList);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
}
