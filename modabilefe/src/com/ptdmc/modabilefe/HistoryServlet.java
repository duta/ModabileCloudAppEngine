package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.OrderDetails;
import com.ptdmc.modabilefe.entity.OrderShips;

@SuppressWarnings("serial")
public class HistoryServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){	
		if (this.user != null){
			List<OrderDetails> products = null;
			try {
				String email = this.user.getEmail();
				products = CallFactory.getInstance(req).getFactory().GetOrderDetails().getByCustomerId(this.customer.getCustomer_id());
			} catch (SQLException e) {
				e.printStackTrace();
			}

			req.setAttribute("useremail", this.user.getEmail());
			req.setAttribute("products",products);
		}
		else {
			req.setAttribute("useremail", "not login yet");	
		}
		req.setAttribute("nameserver", req.getServerName());
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/history/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res){	
		RequestDispatcher jsp = null;
		if(req.getParameter("orddtl_id")!= null){
			List<OrderShips> osList = null;
			OrderShips os = new OrderShips();
			os.setOrder_id(Long.valueOf(req.getParameter("orddtl_id")));
			try {
				osList = CallFactory.getInstance(req).getFactory().GetOrderShips().getWhere(os);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			req.setAttribute("osList",osList);
			jsp = req.getRequestDispatcher("./views/history/list.jsp");
		}else{
			if (this.user != null){
				List<OrderDetails> products = null;
				try {
					String email = this.user.getEmail();
					products = CallFactory.getInstance(req).getFactory().GetOrderDetails().getByCustomerId(this.customer.getCustomer_id());
				} catch (SQLException e) {
					e.printStackTrace();
				}
				req.setAttribute("useremail", this.user.getEmail());
				req.setAttribute("products",products);
			}
			else {
				req.setAttribute("useremail", "not login yet");	
			}
			req.setAttribute("nameserver", req.getServerName());

			jsp = req.getRequestDispatcher("./views/history/index.jsp");
		}
		
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	
}
