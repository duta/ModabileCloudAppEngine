package com.ptdmc.modabilefe;

import java.io.IOException;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.SQLException;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.RequestDispatcher;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

//import modabilefe.data.DBProvider;

import com.ptdmc.modabilefe.entity.*;
import com.ptdmc.modabilefe.geo.ShipFeatureCollection;

@SuppressWarnings("serial")
public class TryShowUpsertServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		// try {
		//
		// Class.forName("org.postgresql.Driver"); //You forgot to load the
		// driver
		// Connection conn =
		// DriverManager.getConnection("jdbc:postgresql://104.196.114.92:5432/aisloggis_dlu",
		// "postgres", "d9HxNQfy");
		// if(conn == null){
		// req.setAttribute("resultdb","Connection null");
		// return;
		// }
		// ResultSet rs = conn.createStatement().executeQuery(
		// "select received_on, mmsi from ship_info");
		// String result = "";
		// while (rs.next()) {
		// result += rs.getString("mmsi") + "<br>";
		// }
		// req.setAttribute("resultdb", result);
		// } catch (SQLException e) {
		// e.printStackTrace();
		// } catch (ClassNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		// List<OrderShips> result =
		// CallFactory.getInstance(req).getFactory().GetOrderShips().GetAll();
		/*ShipFeatureCollection result = null;

		try {
			result = CallFactory
					.getInstance(req)
					.getFactory()
					.GetIncomingAis()
					.RecordedAllPositionByMMSIAndDate(525015977,
							Timestamp.valueOf("2015-03-30 00:36:07"),
							Timestamp.valueOf("2016-04-14 04:36:07"));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (result != null) {
			req.setAttribute("resultdb", result.getFeatures().get(0)
					.getProperties().get("MMSI").toString());
		}*/
		
		List<Integer> mmsis = new ArrayList<Integer>();
		mmsis.add(525005223);
		mmsis.add(525015378);
		mmsis.add(525015376);
		
		List<ShipInfo> result = null;
		try {
			result = CallFactory.getInstance(req).getFactory().GetShipInfo().GetBySomeMMSI(mmsis);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (result.size() > 0){
			req.setAttribute("resultdb", result.get(0).getVesselname());			
		}

		// req.setAttribute("resultdb",new DBProvider().TT());
		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		boolean local = "localhost".equals(req.getServerName());
		String loginUrl = userService.createLoginURL("/");
		String logoutUrl = userService.createLogoutURL("/");

		req.setAttribute("user", user);
		req.setAttribute("loginUrl", loginUrl);
		req.setAttribute("logoutUrl", logoutUrl);
		res.setContentType("text/html");
		RequestDispatcher jsp = req.getRequestDispatcher("./tryshowupsert.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
		String ss = req.getLocalName();
		String ss2 = req.getLocalAddr();
	}

}
