package com.ptdmc.modabilefe;

import javax.servlet.http.HttpServletRequest;

public class CheckIsLocalhost {
	private Boolean isLocal;
	
	public CheckIsLocalhost(HttpServletRequest req){
		isLocal =  "localhost".equals(req.getServerName());	
	}
	
	public Boolean getIsLocal(){
		return isLocal;
	}
}
