package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.ibm.icu.util.TimeZone;
import com.ptdmc.modabilefe.FunctionM;
import com.ptdmc.modabilefe.entity.Customer;

@SuppressWarnings("serial")
public class ProfileServlet extends MasterServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res){		
		//Get Session 
		String email = (String) req.getSession().getAttribute("emailCustomer");
		
		req.setAttribute("email",this.customer.getCustomer_email());
		req.setAttribute("name",this.customer.getCustomer_name());
		req.setAttribute("phone",this.customer.getCustomer_phone());
		req.setAttribute("country",this.customer.getCustomer_country());
		req.setAttribute("timezone",this.customer.getCustomer_timezone());
		req.setAttribute("company",this.customer.getCustomer_company());
		req.setAttribute("gmtoffset",this.customer.getCustomer_gmtoffset());
		req.setAttribute("timeabbreviation",this.customer.getCustomer_timeabbreviation());
		
		RequestDispatcher jsp = req.getRequestDispatcher("./views/profile/index.jsp");
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException 
	{
		
		String action = req.getParameter("action");
		
		if(action.equals("do_update")){
			try {
				Customer customer = this.customer;
				customer.setCustomer_company(req.getParameter("customer_company"));
				customer.setCustomer_name(req.getParameter("customer_name"));
				customer.setCustomer_phone(req.getParameter("customer_phone"));
				customer.setCustomer_country(req.getParameter("customer_country"));
				customer.setCustomer_email(req.getParameter("customer_email"));
				customer.setCustomer_timezone(req.getParameter("customer_timezone"));
				customer.setCustomer_timeabbreviation(req.getParameter("customer_abbreviation"));
			
				String gmtoffset = req.getParameter("customer_gmtoffset");
				int intGmt = FunctionM.getTimestamp(gmtoffset);
			
				customer.setCustomer_gmtoffset(intGmt);
				CallFactory.getInstance(req).getFactory().GetUsers().Upsert(CallFactory.getInstance(req).getFactory().GetUsers().GetUserIDByUserName(user.getEmail()), req.getParameter("customer_email"), new byte[0]);
				CallFactory.getInstance(req).getFactory().GetCustomer().Update(customer);
				try {
					res.sendRedirect("/profile");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}else if(action.equals("timezone")){
			
			String idCountry = req.getParameter("country");
			idCountry = idCountry.toUpperCase();
			Locale locale = Locale.ENGLISH;
			String[] locales = Locale.getISOCountries();
			
			ArrayList<String> dataLocation = new ArrayList<String>();
			ArrayList<String> dataGMT = new ArrayList<String>();
			ArrayList<String> dataGMTDetail = new ArrayList<String>();
			
			for (String dataNegara : locales) {

			    Locale obj = new Locale("", dataNegara);
//			    System.out.println("Country Code = " + obj.getCountry() 
//				+ ", Country Name = " + obj.getDisplayCountry(locale));
			    
			    String countryCode = obj.getCountry();
				
				if(obj.getCountry().equals(idCountry)){
					String[] timeZones = com.ibm.icu.util.TimeZone.getAvailableIDs(countryCode); // out : "Asia/Jakarta"
					
					
					
					List<TimeZone> timeZoneList = new ArrayList<TimeZone>();
					TimeZone tz;
					for (String timeZone : timeZones)
					{
						tz = TimeZone.getTimeZone(timeZone);
					  timeZoneList.add(tz);
					  
					  dataLocation.add(timeZone);
					 

					  //String tz = "Asia/Jakarta";
					  //SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm z");
					  //dateFormat.setTimeZone(TimeZone.getTimeZone(tz));
					  //String statusDate = dateFormat.format(new Date());
					  //System.out.println(statusDate);

					  /*SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy HH:mm Z");
					  dateFormat1.setTimeZone(TimeZone.getTimeZone(tz));
					  String statusDate1 = dateFormat1.format(new Date());
					  System.out.println(statusDate1);*/
						//dataGMTDetail.add(statusDate);
					}
					
					
					//System.out.println(timeZoneList);
				}
			 }
			
			 Object[] result = new Object[2];
	         result[0] = dataLocation;
	         
			 ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
	         String json;
			 try {
				 json = ow.writeValueAsString(result);
				 res.setContentType("application/json");
		         res.setCharacterEncoding("UTF-8");
		         try {
					res.getWriter().write(json);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 } catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			 }
	         
		}
		
	}
	
	
}
