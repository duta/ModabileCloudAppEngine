package com.ptdmc.modabilefe.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {

	public static void SendMail(String sender, List<String> recipients,
			String subject, String templateLocation,
			HashMap<String, String> parameters) throws MessagingException,
			IOException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(sender));
		for (String recipient : recipients) {
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					recipient));
		}
		msg.setSubject(subject);
		
		InputStream inputStream = new FileInputStream(new File(templateLocation));
		StringBuffer sb = new StringBuffer();
		writeInputStream(inputStream, sb);
		for (String key : parameters.keySet()) {
			Pattern p = Pattern.compile(key);
			Matcher m = p.matcher(sb);
			sb = new StringBuffer();
			while (m.find()) {
				m.appendReplacement(sb, parameters.get(key));
			}
			m.appendTail(sb);
		}

		Multipart mp = new MimeMultipart();
		MimeBodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(sb.toString(), "text/html");
		mp.addBodyPart(htmlPart);
		msg.setContent(mp);
		msg.saveChanges();

		Transport.send(msg);
	}

	public static void writeInputStream(InputStream ss, StringBuffer sb)
			throws IOException {
		byte[] buffer = new byte[2048];
		int length;
		while ((length = ss.read(buffer)) != -1) {
			sb.append(new String(buffer, 0, length));
		}
		ss.close();
	}
}
