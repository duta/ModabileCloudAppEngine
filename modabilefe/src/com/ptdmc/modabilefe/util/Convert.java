package com.ptdmc.modabilefe.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.geojson.LngLatAlt;
import org.geojson.Point;

import com.ptdmc.modabilefe.geo.ShipProperty;

public class Convert {
	public static Map<String, Object> BeanToMap(Object obj) throws Exception {
	    Map<String, Object> result = new HashMap<String, Object>();
	    BeanInfo info = Introspector.getBeanInfo(obj.getClass());
	    String keyString = "";
	    for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
	        Method reader = pd.getReadMethod();
	        if (reader != null)
	        	keyString = toTitleCase(pd.getName());
	            result.put(keyString, reader.invoke(obj));
	    }
	    return result;
	}
	
	public static String toTitleCase(String input) {
	    char c =  input.charAt(0);
	    String s = new String("" + c);
	    String f = s.toUpperCase();
	    return f + input.substring(1);
	}
	
	public static Map<String, Object> CreateProperties(ShipProperty property){
		Map<String, Object> maps = new HashMap<String, Object>();		
		try {
			maps = Convert.BeanToMap(property);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return maps;
	}
	
	public static Point CreatePoint(double lon, double lat){
		Point point = new Point();
		LngLatAlt lonlat = new LngLatAlt();
		lonlat.setLongitude(lon);
		lonlat.setLatitude(lat);
		point.setCoordinates(lonlat);
		return point;
	}
}
