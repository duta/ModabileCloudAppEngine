package com.ptdmc.modabilefe.util;

import java.util.Calendar;
import java.util.Random;

public class CommonUtil {

	private static Random RAND = new Random();

	public static final String ORDER_STATUS_PEMESANAN = "PEMESANAN";
	public static final String ORDER_STATUS_PERSIAPAN_SATELIT = "PERSIAPAN ENGINE AIS SATELIT";
	public static final String ORDER_STATUS_APLIKASI_SIAP = "APLIKASI SIAP";
	public static final String ORDER_STATUS_KONFIRM_APP_SIAP = "KONFIRMASI USER APP SIAP";
	public static final String ORDER_STATUS_TUNGGU_PAYMENT = "MENUNGGU PAYMENT";
	public static final String ORDER_STATUS_KONFIRM_PAYMENT = "USER KONFIRM PAYMENT";
	public static final String ORDER_STATUS_VALIDASI_PAYMENT = "VALIDASI PAYMENT";
	public static final String ORDER_STATUS_PAYMENT_OK = "PAYMENT OK";
	public static final String ORDER_STATUS_PAYMENT_NOT_OK = "PAYMENT NOT OK";
	public static final String ORDER_STATUS_OPEN_AKSES_USER = "OPEN AKSES USER";
	public static final String ORDER_STATUS_CLOSE_AKSES_USER = "CLOSE AKSES USER";
	public static final String ORDER_STATUS_BATAL = "BATAL PEMESANAN";
	public static final Integer PRODUCT_TRIAL_ID = 1;

	public static String generatePONumber() {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int date = cal.get(Calendar.DATE);

		StringBuilder random = new StringBuilder();
		for (int i = 0; i < 6; i++) {
			random.append(RAND.nextInt(9));
		}

		String monthStr = getMonth(month);
		String result = "TRIAL-" + random + "#" + year + "/" + monthStr + "/" + date;
		return result;
	}

	private static String getMonth(int month) {
		String result = null;
		switch (month) {
			case Calendar.JANUARY:
				result = "jan";
				break;
			case Calendar.FEBRUARY:
				result = "peb";
				break;
			case Calendar.MARCH:
				result = "mar";
				break;
			case Calendar.APRIL:
				result = "apr";
				break;
			case Calendar.MAY:
				result = "mei";
				break;
			case Calendar.JUNE:
				result = "jun";
				break;
			case Calendar.JULY:
				result = "jul";
				break;
			case Calendar.AUGUST:
				result = "ags";
				break;
			case Calendar.SEPTEMBER:
				result = "sep";
				break;
			case Calendar.OCTOBER:
				result = "okt";
				break;
			case Calendar.NOVEMBER:
				result = "nop";
				break;
			case Calendar.DECEMBER:
				result = "des";
				break;
			default:
				break;
		}
		return result;
	}

}
