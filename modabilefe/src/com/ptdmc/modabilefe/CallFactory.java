package com.ptdmc.modabilefe;

import javax.servlet.http.HttpServletRequest;

import com.ptdmc.modabilefe.data.Factory;

public class CallFactory {
	private Factory factory;
	private static CallFactory instance = null;
		
   private CallFactory(HttpServletRequest req) {
      this.factory = new Factory(new CheckIsLocalhost(req).getIsLocal(), 
			   PropertiesReader.getInstance().getProperties());
   }
	
	public static CallFactory getInstance(HttpServletRequest req) {
	      if(instance == null) {	    	  
	         instance = new CallFactory(req);	         
	      }
	      return instance;
	   }
	
	public Factory getFactory(){
		return this.factory;
	}
	
	
}
