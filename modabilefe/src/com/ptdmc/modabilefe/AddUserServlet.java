package com.ptdmc.modabilefe;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptdmc.modabilefe.data.CustomerMemberRepo;
import com.ptdmc.modabilefe.data.Factory;
import com.ptdmc.modabilefe.data.leyfi.Roles;
import com.ptdmc.modabilefe.data.leyfi.Users;
import com.ptdmc.modabilefe.entity.Customer;
import com.ptdmc.modabilefe.entity.ShipInfo;

public class AddUserServlet extends MasterServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3132590381505543446L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/adduser/index.jsp");
		Factory factory = CallFactory.getInstance(req).getFactory();
		for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
			if (role.role_name.equals("superadmin (ADMIN)")) {
				req.setAttribute("isADMIN", true);
				req.setAttribute("listPerusahaan", getJSONListPerusahaan(factory));
			}
		}
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		if (req.getParameter("action").equals("user")) insertUser(req, res);
		if (req.getParameter("action").equals("kapal")) insertKapal(req, res);
	}
	
	private void insertKapal(HttpServletRequest req, HttpServletResponse res) {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/adduser/index.jsp");
		Factory factory = CallFactory.getInstance(req).getFactory();
		try{
			ShipInfo si = new ShipInfo();
			si.setVesselname(req.getParameter("Name"));
			si.setMmsi(Long.parseLong(req.getParameter("MMSI")));
			si.setImonumber(Long.parseLong(req.getParameter("IMO")));
			si.setCallsign(req.getParameter("CallSign"));
			si.setDimensiontobow(req.getParameter("Bow"));
			si.setDimensiontostern(req.getParameter("Stern"));
			si.setDimensiontoport(req.getParameter("Port"));
			si.setDimensiontostarboard(req.getParameter("StarBoard"));
			si.setGrt(Long.parseLong(req.getParameter("GRT")));
			si.setEnginehp(Long.parseLong(req.getParameter("EHP")));
			si.setEnginediesel(req.getParameter("ED"));
			si.setDte("on".equals(req.getParameter("ED"))?1L:0L);
			String type = req.getParameter("ShipType");
			si.setShiptype(Long.parseLong(type.split(";")[0]));
			si.setShiptypedesc(type.split(";")[1]);
			factory.GetShipInfo().insert(si, user.getEmail());
			req.setAttribute("listPerusahaan", getJSONListPerusahaan(factory));
			req.setAttribute("message", "alert('Sukses');$('#tabs').tabs('option', 'active', 2);");
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void insertUser(HttpServletRequest req, HttpServletResponse res) throws ServletException {
		RequestDispatcher jsp = req.getRequestDispatcher("/views/adduser/index.jsp");
		Factory factory = CallFactory.getInstance(req).getFactory();
		boolean isadmin = false;
		for (com.ptdmc.modabilefe.entity.leyfi.Roles role : this.roles) {
			if (role.role_name.equals("superadmin (ADMIN)")){
				isadmin = true;
			}
		}
		try {
			// save users
			Roles roles = factory.GetRoles();
			Users users = factory.GetUsers();
			CustomerMemberRepo cmr = factory.GetCustomerMember();
			users.Upsert(null, req.getParameter("useremail"), new byte[0]);

			boolean docmr = true;
			Customer customer = this.customer;
			if (isadmin){
				String peru = req.getParameter("perusahaan");
				docmr = false;
				if (peru != null){
					List<Customer> oo = factory.GetCustomer().GetAll();
					for (int i = 0; i < oo.size(); i++) {
						if (peru.equals(oo.get(i).getCustomer_company())) {
							docmr = true;
							customer = oo.get(i);
						}
					}
				}
				if (!docmr) { //create customer
					customer = new Customer();
					customer.setCustomer_email(req.getParameter("useremail"));
					customer.setCustomer_company(peru);
					
					factory.GetCustomer().insert(customer);
					customer = factory.GetCustomer().GetByEmail(req.getParameter("useremail"));
				}
			}
			
			// save customermember
			boolean customeradmin = "cadmin".equals(req.getParameter("userrole"));
			boolean customermember = "cmember".equals(req.getParameter("userrole"));
			int userid = users.GetUserIDByUserName(req.getParameter("useremail"));
			if (customeradmin) {
				if (docmr){
					cmr.InsertCustomerRole(
							roles.GetIDByName("customer admin (USER)"), 
							customer.getCustomer_id(),
							userid);
				}
				users.InsertUsersInRoles(roles.GetIDByName("customer admin (USER)"), userid);
			}
			if (customermember) {
				if (docmr){
					cmr.InsertCustomerRole(
							roles.GetIDByName("customer member (USER)"), 
							customer.getCustomer_id(),
							userid);
				}
				users.InsertUsersInRoles(roles.GetIDByName("customer member (USER)"), userid);
			}
			req.setAttribute("message", "alert('Sukses')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			if (isadmin){
				req.setAttribute("isADMIN", true);
				req.setAttribute("listPerusahaan", getJSONListPerusahaan(factory));
			}
			jsp.forward(req, res);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String getJSONListPerusahaan(Factory f) {
		String ret = "";
		try {
			List<String> oo = f.GetCustomer().getListPerusahaan();
			StringBuilder sb = new StringBuilder();
			sb.append('[');
			for (int i = 0; i < oo.size(); i++) {
				sb.append("'"+oo.get(i)+"'");
				if (i != oo.size()-1) sb.append(',');
			}
			sb.append(']');
			ret = sb.toString();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}

}
