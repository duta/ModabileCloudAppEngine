package com.ptdmc.modabilefe;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletContext;

public class Language {

	public final String language;
	private final ServletContext servletContext;
	private final Locale l;
	private final ArrayList<String> loaded = new ArrayList<String>();
	private final HashMap<String, String> memory = new HashMap<String, String>();
	public Language(String string, ServletContext servletContext2) {
		// TODO Auto-generated constructor stub
		language = string;
		servletContext = servletContext2;
		l = Locale.forLanguageTag(string);
	}
	
	public String formatDate(int style, Date date){
		return DateFormat.getDateInstance(style, l).format(date);
	}
	
	public String formatTime(int style, Date date){
		return DateFormat.getTimeInstance(style, l).format(date);
	}
	
	public String formatDateTime(int datestyle, int timestyle, Date date){
		return DateFormat.getDateTimeInstance(datestyle, timestyle, l).format(date);
	}
	
	public String formatNumber(double number){
		return NumberFormat.getNumberInstance(l).format(number);
	}
	
	public String formatCurrency(double amount){
		return NumberFormat.getCurrencyInstance(l).format(amount);
	}
	
	public String formatPercent(double number){
		return NumberFormat.getPercentInstance(l).format(number);
	}
	
	public String translate(String source, String sentence){
		if (!loaded.contains(source)){
			loadSource(source);
		}
		if (memory.containsKey(sentence)) return memory.get(sentence);
		return sentence;
	}

	private void loadSource(String source) {
		InputStream is = servletContext.getResourceAsStream("/Translation/"+language+"/"+source);   
        String str = "";
		try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            if (is != null) {                            
                while ((str = reader.readLine()) != null) {
                	String[] split = str.split("\\|");
                	memory.put(split[0], split[1]);
                }                
            }
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            try { is.close(); } catch (Throwable ignore) {}
        }
		loaded.add(source);
	}

}
