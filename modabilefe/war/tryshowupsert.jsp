<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	    <title>Try Upsert</title>
	</head>
	<body>
	<p>
		${resultdb}
	</p>
	<c:choose>
		<c:when test="${user != null}">
			<p>
				Welcome, ${user.email}! You can <a href="${logoutUrl}">sign out</a>.
			</p>
		</c:when>
		<c:otherwise>
			<p>
				Welcome! <a href="${loginUrl}">Sign in or register</a> to customize.
			</p>
		</c:otherwise>
	</c:choose>
	<form action="tryshowupsert" method="post">
			<input type="hidden" name="action" value="add" />
			<input type="text" name="text" required/> <br>
			<input type="submit" value="tryshowupsert" id="submitButton">
		</form>
	</body>
</html>