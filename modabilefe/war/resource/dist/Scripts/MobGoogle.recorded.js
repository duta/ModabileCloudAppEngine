﻿var vector_layerGeoJson, ori_layerGeoJson, linbe_layerGeoJson;
var renderer = GetRenderer();
var featuresShipOnDisplay;
var SHADOW_Z_INDEX = 10;
var MARKER_Z_INDEX = 11;

$(document).ready(function () {
    $("#cmdGetKml").click(function () {
        params = BuildParamsDownloadKmlCsv();
        params += "&op=getkml";
        window.location = "/vessel?" + params;
    });

    $("#cmdGetCsv").click(function () {
        params = BuildParamsDownloadKmlCsv();
        params += "&op=getxlsx";
        window.location = "/vessel?" + params;
    });

    $("#reqDisplay").click(function () {
        if (!validateInputsDate()) return false;
        if (reqqing) return;
        reqqing = true;
        featuresShipOnDisplay = null;
        $("#reqDisplay").toggleClass('violet');
        vector_layerGeoJson.destroyFeatures();
        $('#reqDisplay').html('Loading ...');
        getListOfRequestDate();
        if (dateQueue.length() > 0) {
            animationQueue = [];
            var firstQueue = dateQueue.dequeue();
            RequestToServer(firstQueue.mmsi, firstQueue.dateFrom, firstQueue.dateTo);
        } else {
            reqqing = false;
        }
        return false;
    });
});
function RequestToServer(mmsiVal, dateFromVal, dateToVal) {
    try {
        $.ajax({
            type: "POST",
            url: '/vessel?op=getrecorded&mmsi='+mmsiVal+'&from='+dateFromVal+'&to='+dateToVal,
            data: JSON.stringify({
                mmsi: mmsiVal,
                "from": dateFromVal,
                "dateTo": dateToVal
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            processData: false,
            success: function (data) {
                readGeoJSON(data);
                drawFeature(mmsiVal);
                if (dateQueue.length() > 0) {
                    var firstQueue = dateQueue.dequeue();
                    $('#reqDisplay').html('Request ' + (mmsiVal == "-1" ? '' : firstQueue.mmsi + ' from ') + firstQueue.dateFrom);
                    setTimeout(RequestToServer(mmsiVal == "-1" ? "-1" : firstQueue.mmsi.split(" ")[0], firstQueue.dateFrom, firstQueue.dateTo), 800);
                } else {
                    //enable button add ships
                    map.moveTo(new OpenLayers.LonLat(vector_layerGeoJson.features[0].geometry.x, vector_layerGeoJson.features[0].geometry.y), 20);
                    $("#reqDisplay").toggleClass('violet');
                    $('#reqDisplay').html('Track');
                    $("#viewRecorded").hide();
                    $('.detailView').toggleClass('active');
                }
            }
        });
    } catch (e) {
        alert(e.Data);
    }
}

function readGeoJSON(data) {
    var geojson_format = new OpenLayers.Format.GeoJSON();
    var features = geojson_format.read(data);
    if (features.length > 0) {
        if (featuresShipOnDisplay == null) {
            featuresShipOnDisplay = features;
        }
        else {
            featuresShipOnDisplay = featuresShipOnDisplay.concat(features);
        }
    }
}
function drawFeature(mmsi) {
    if (featuresShipOnDisplay == null) return;
    var jmlFeature = featuresShipOnDisplay.length;

    if (jmlFeature == 0) { $('#reqDisplay').html('No Data Found'); isDrawing = false; return; }
    $('#reqDisplay').html('Drawing data ... [Receive approx. ' + jmlFeature + ' data]');
    time = 0;
    var size = new OpenLayers.Size(100, 100);
    var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
    var centeredYet = false;
    for (var i = 0; i < jmlFeature; i++) {
        vector_layerGeoJson.addFeatures(featuresShipOnDisplay[i]);
    }
    featuresShipOnDisplay = null;
    $('#reqDisplay').html('Done.');
}
reqqing = false;
var reqqing = false;
function BuildParamsDownloadKmlCsv() {
    var params = "";
    params += "dateStart=" + $('#calendarTime-datefrom').val().replace(" ", '-').replace(":", '-');
    params += "&dateEnd=" + $('#calendarTime-dateto').val().replace(" ", '-').replace(":", '-');
    params += "&Interval" + $('#unitSlider').html() + "=" + $('#valSlider').html();
    var mmsi = $('#namaKapal').tokenize().toArray();
    for (var i = 0; i < mmsi.length; i++) {
        params += "&mmsi[" + i + "]=" + mmsi[i].split(' ')[0];
    }
    return params;
}

function GetRenderer() {
    renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
    renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
    return renderer;
}

function ChangeProjection(map, layer) {
    layer.projection = map.displayProjection;
    layer.preFeatureInsert = function (feature) {
        feature.geometry.transform(epsg4326, epsg900913);
    };
}

function initMapLayers(mapGeoJson) {

    var contextShips = {
        getShipStyle: function (ft) {
            return GetShipImagePath(ft);
        },
        getShipWidth: function (ft) {
            return 30; //image width
        },
        getShipHeight: function (ft) {
            return 19; //image height
        },
        getShipLabel: function (ft) {
            var knot = (ft.attributes.SOG == undefined) ? 0 : (ft.attributes.SOG);
            return mapGeoJson.getScale() < 10000000 ? (ft.attributes.ShipName == null ? ft.attributes.MMSI : ft.attributes.ShipName) +
                "\n" + FormatTimestampReceiver(ft) + " / " + knot + " k" : "";
        }
    };

    var templateShip = {
        externalGraphic: "${getShipStyle}", // using context.getSize(feature)
        // Makes sure the background graphic is placed correctly relative
        // to the external graphic.
        backgroundXOffset: 0,
        backgroundYOffset: -7,
        //AAV:set size
        graphicWidth: "${getShipWidth}",
        graphicHeight: "${getShipHeight}",

        //show label and rotate image
        label: "${getShipLabel}",
        labelXOffset: 15,
        fontColor: "black",
        labelAlign: "l",
        labelOutlineColor: "beige",
        labelOutlineWidth: 3,
        //display: "${getShipVisibility}",

        // Set the z-indexes of both graphics to make sure the background
        // graphics stay in the background (shadows on top of markers looks
        // odd; let's not do that).
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX,
        pointRadius: 10
    };

    var styleShip = new OpenLayers.Style(templateShip, { context: contextShips });
    vector_layerGeoJson = new OpenLayers.Layer.Vector(
                    "ship_layer",
                    {
                        styleMap: new OpenLayers.StyleMap(styleShip),
                        isBaseLayer: false,
                        rendererOptions: { yOrdering: true },
                        renderers: renderer
                    });

    var templateOri = {
        styleMap: new OpenLayers.Style({
            externalGraphic: "/resource/dist/Styles/static_ori.png",
            graphicWidth: 15,
            graphicHeight: 15,
            pointRadius: 0
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    ori_layerGeoJson = new OpenLayers.Layer.Vector("ori_layer", templateOri);

    var templateInb = {
        styleMap: new OpenLayers.Style({
            externalGraphic: "/resource/dist/Styles/static_inb.png",
            graphicWidth: 15,
            graphicHeight: 15,
            pointRadius: 0
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    inb_layerGeoJson = new OpenLayers.Layer.Vector("inb_layer", templateInb);

    var templateLinbe = {
        styleMap: new OpenLayers.Style({
            strokeColor: "Green",
            strokeWidth: 2
        }, { context: {} }),
        isBaseLayer: false,
        rendererOptions: { yOrdering: true },
        renderers: renderer,
        graphicZIndex: MARKER_Z_INDEX,
        backgroundGraphicZIndex: SHADOW_Z_INDEX
    }
    linbe_layerGeoJson = new OpenLayers.Layer.Vector("linbe_layer", templateLinbe);
}

function GetShipImagePath(ft) {
    if (ft.attributes.ShipTypeDescription != null) {
        if (ft.attributes.ShipTypeDescription.indexOf("Cargo") != -1) {
            return "/resource/dist/Styles/kargo_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Passenger") != -1) {
            return "/resource/dist/Styles/penumpang_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Tanker") != -1) {
            return "/resource/dist/Styles/tanker_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Tug") != -1) {
            return "/resource/dist/Styles/tunda_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Search") != -1) {
            return "/resource/dist/Styles/SAR_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Container") != -1) {
            return "/resource/dist/Styles/kargo_static.png";
        }
        else if (ft.attributes.ShipTypeDescription.indexOf("Pleasure") != -1) {
            return "/resource/dist/Styles/pesiar_static.png";
        }
    }
    return "/resource/dist/Styles/tidak-terdaftar_static.png";
}

function FormatTimestampReceiver(feature) {
    return feature.attributes.TimeStampReceiver.toString().substring(6, 8) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(4, 6) +
    '-' + feature.attributes.TimeStampReceiver.toString().substring(0, 4) +
    ' ' + feature.attributes.TimeStampReceiver.toString().substring(8, 10) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(10, 12) +
    ':' + feature.attributes.TimeStampReceiver.toString().substring(12, 14);
}

function FindFeatureAndSelect(mmsi) {
    feat = vector_layerGeoJson.getFeaturesByAttribute("MMSI", mmsi);
    onFeatureGJSelect(feat[0]);
}

function FeatureIndexed(mmsi, index) {
    onFeatureGJSelect(featuresShipOnDisplay[mmsi][index]);
}

function Queue() {
    this.stac = new Array();
    this.dequeue = function () {
        return this.stac.pop();
    }
    this.enqueue = function (item) {
        this.stac.unshift(item);
    }
    this.length = function () {
        return this.stac.length;
    }
}

var intervalInSecond = 0;
function validateInputsDate() {
    var From = $('#calendarTime-datefrom').val();
    var To = $('#calendarTime-dateto').val();
    var intervalAmount = parseInt($("#valSlider").html());

    if (From == '' & To == '') {
        alert("Tanggal tidak boleh kosong.");
        $('#calendarTime-datefrom').focus();
        return false;
    }

    if (compareDate(From, To) <= 0) {
        alert("Tanggal akhir harus lebih dari tanggal mulai.");
        $('#calendarTime-dateto').focus();
        return false;
    }
    else {
        diffSecond = compareDate(From, To);
        intervalInSecond = 0;
        var selected = $('#dropdownSlider option:selected').text();
        if (selected == "Second") {
            if (diffSecond < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + $("#valSlider").val() + " second(s)");
                return false;
            } else {
                intervalInSecond = intervalAmount;
            }
        }
        else if (selected == "Minute") {
            diffMinute = diffSecond / 60;
            if (diffMinute < 1) {
                alert("Date range is not applicable to minute interval.");
                return false;
            } else if (diffMinute < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + parseInt($("#valSlider").html()) + " minute(s)");
                return false;
            } else {
                intervalInSecond = parseInt($("#valSlider").html()) * 60;
            }
        }
        else if (selected == "Hour") {
            diffMinute = diffSecond / 3600;
            if (diffMinute < 1) {
                alert("Date range is not applicable to hour interval.");
                return false;
            } else if (diffMinute < intervalAmount) {
                alert("Jarak waktu yang dipilih kurang dari " + parseInt($("#valSlider").html()) + " hour(s)");
                return false;
            } else {
                intervalInSecond = parseInt($("#valSlider").html()) * 3600;
            }
        }
    }
    return true;
}

function compareDate(dateFromStr, dateEndStr) {
    dateFrom = getDateObject(dateFromStr)
    dateEnd = getDateObject(dateEndStr);

    return (dateEnd.getTime() - dateFrom.getTime()) / 1000; // returned in millisecond base so we need to convert it to second, just for convenience
}

function getDateObject(dateStr) {
    dateExpr = getDateExpression(dateStr);
    return new Date(dateExpr[1], dateExpr[2] - 1, dateExpr[3], dateExpr[4], dateExpr[5]);
}

function getDateExpression(dateString) {
    format = /^(\d{1,4})\-(\d{1,2})\-(\d{1,2}) (\d{1,2}):(\d{1,2})$/;
    return dateString.match(format);
}
var dateQueue;
function getListOfRequestDate() {
    var iUnit = $('#dropdownSlider option:selected').val();
    currentDate = getDateObject($('#calendarTime-datefrom').val());
    endDate = getDateObject($('#calendarTime-dateto').val());
    //var jsonObj = []; //declare array
    dateQueue = new Queue();
    while (currentDate < endDate) {
        var fromStr =
            +currentDate.getFullYear() + "-"
            + (currentDate.getMonth() + 1) + "-"
            + currentDate.getDate() + " "
            + currentDate.getHours() + ":"
            + (iUnit != 'hour' ? currentDate.getMinutes() : '00') + ":"
            + (iUnit == 'second' ? currentDate.getSeconds() : '00');

        currentDate.setTime(currentDate.getTime() + intervalInSecond * 1000);
        if (currentDate > endDate) currentDate = endDate;

        var toStr =
        +currentDate.getFullYear() + "-"
        + (currentDate.getMonth() + 1) + "-"
        + currentDate.getDate() + " "
        + currentDate.getHours() + ":"
        + (iUnit != 'hour' ? currentDate.getMinutes() : '00') + ":"
        + (iUnit == 'second' ? currentDate.getSeconds() : '00') + '.000000';

        //loop each mmsi selected
        var mmsi = $('#namaKapal').tokenize().toArray();
        if (mmsi.length == 0) {
            dateQueue.enqueue({  mmsi: "-1", dateFrom: fromStr, dateTo: toStr });
        } else {
            for (var i = 0; i < mmsi.length; i++) {
                dateQueue.enqueue({ mmsi: mmsi[i].split(" ")[0], dateFrom: fromStr, dateTo: toStr });
            }
        }
    }
    $('#reqDisplay').html('Finish collecting');
}