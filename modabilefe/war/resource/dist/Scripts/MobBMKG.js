﻿var map, layer, hs, dates = [], day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
function pad(pad, str, padLeft) {
    if (typeof str === 'undefined')
        return pad;
    if (padLeft) {
        return (pad + str).slice(-pad.length);
    } else {
        return (str + pad).substring(0, pad.length);
    }
};
var epsg4326 = new OpenLayers.Projection('EPSG:4326');
var epsg900913 = new OpenLayers.Projection('EPSG:900913');
$(document).ready(function () {
    initMap();
    initMapLayers(map);

    ChangeProjection(map, vector_layerGeoJson);
    map.addLayer(vector_layerGeoJson);

    ChangeProjection(map, ori_layerGeoJson);
    map.addLayer(ori_layerGeoJson);

    ChangeProjection(map, inb_layerGeoJson);
    map.addLayer(inb_layerGeoJson);

    ChangeProjection(map, linbe_layerGeoJson);
    map.addLayer(linbe_layerGeoJson);
});

function initMap() {
    map = new OpenLayers.Map('mapGeoJson', {
        numZoomLevels: 20,
        projection: epsg900913,
        displayProjection: epsg4326,
        theme:"theme/default/style-v2.css",
        allOverlays: true
    });
    initBmkgLayers();
}

function initBmkgLayers() {
    hs = new OpenLayers.Layer.TMS("Custom TMS",
             "",
             { layername: "20160404000000", type: "png", serviceVersion: '1.5.0', sphericalMercator: false, isBaseLayer: false });

    map.addLayer(hs);

    var layer1 = new OpenLayers.Layer.TMS(
        "MapBox Layer",
        ["http://peta-maritim.bmkg.go.id/2.0//peta/island.php"],
        { 'type': 'png', 'getURL': get_my_url, sphericalMercator: false, isBaseLayer: false }
    );
    map.addLayer(layer1);

    graticuleCtl = new OpenLayers.Control.Graticule({
        numPoints: 1,
        labelled: true,
        targetSize: 600,
        lineSymbolizer: { strokeColor: "#888888", strokeWidth: 1 },
        labelSymbolizer: { fontColor: "#000000", fontSize: "10px" }
    });
    map.addControl(graticuleCtl);
    graticuleCtl.activate();
    extent = new OpenLayers.Bounds(88, -17, 152, 17).transform(map.displayProjection, map.getProjectionObject());
    map.setCenter(new OpenLayers.LonLat(115.817, -0.842).transform(map.displayProjection, map.getProjectionObject()), 5);
    map.setOptions({ restrictedExtent: extent });
    //map.setCenter(new OpenLayers.LonLat(lon, lat).transform(epsg4326, epsg900913), zoom);
    $('#mapselector').change(redrawHS);
    $('#timestampselector').change(redrawHS);
    $('#runselector').change(redrawHS);
    fillBMKGFilters();
}
function redrawHS() {
    if (dates.indexOf($('#runselector').val()) != 0 && dates.indexOf($('#timestampselector').val()) < 4) {
        $('#timestampselector').val(dates[4]);
    }
    hs.layername = $('#runselector').val();
    hs.url = "http://peta-maritim.bmkg.go.id/2.0//peta/tile/" + $('#mapselector').val() + ".php/" + $('#timestampselector').val() + "/";
    hs.redraw(true);
}

function fillBMKGFilters() {
    // regex to be used : ">.*(\d{2,4}).*(\d{2,4}).*(\d{4}).*?(\d{2}).* UTC Model Run
    var datt = new Date();
    datt.setDate(datt.getDate()-1);
    datt.setHours(0);
    datt.setMinutes(0);
    datt.setSeconds(0);
    datt.setMilliseconds(0);
    for (var i = 0; i < 41; i++) {
        dates.push(datt.getFullYear() + '' + pad('00', (datt.getMonth() + 1), true) + '' + pad('00', datt.getDate(), true) + '' + pad('00', datt.getHours(), true) + '0000');
        $('#timestampselector').append('<option value="' + datt.getFullYear() + '' + pad('00', (datt.getMonth() + 1), true) + '' + pad('00', datt.getDate(), true) + '' + pad('00', datt.getHours(), true) + '0000">' + day[datt.getDay()] + ' ' + pad('00', datt.getDate(), true) + '-' + pad('00', (datt.getMonth() + 1), true) + '-' + datt.getFullYear() + ' ' + pad('00', datt.getHours(), true) + ':00 UTC</option>');
        if (i==0 || i==4)
            $('#runselector').append('<option value="' + datt.getFullYear() + '' + pad('00', (datt.getMonth() + 1), true) + '' + pad('00', datt.getDate(), true) + '' + pad('00', datt.getHours(), true) + '0000">' + day[datt.getDay()] + ' ' + pad('00', datt.getDate(), true) + '-' + pad('00', (datt.getMonth() + 1), true) + '-' + datt.getFullYear() + ' ' + pad('00', datt.getHours(), true) + ':00 UTC Model Run</option>')
        datt = new Date(datt.getFullYear(), datt.getMonth(), datt.getDate(), datt.getHours() + 3, 0, 0, 0);
    }
    redrawHS();
}

function get_my_url(bounds) {
    var res = map.getResolution();
    var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
    var y = Math.round((90 - this.maxExtent.bottom + bounds.bottom) / (res * this.tileSize.h));
    var z = map.getZoom();

    var path = "?z=" + z + "&x=" + x + "&y=" + y;
    var url = this.url;
    if (url instanceof Array) {
        url = this.selectUrl(path, url);
    }
    return url + path;

}

function getBounds() {
    return map.calculateBounds().transform(epsg900913, epsg4326);
}

function getCursorLonLat(mouseEvent) {
    return map.getLonLatFromViewPortPx(mouseEvent.xy).transform(epsg900913, epsg4326);
}