<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Tambah User</jsp:attribute>
 	<jsp:attribute name="header">
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script></jsp:attribute>
 	<jsp:body>
  <style>
    @media (max-width: 640px){
        .hideyhole {display:none !important;}
    }
  </style>
			<c:if test="${isADMIN}"> 
		<div id="tabs">
  			<ul>
    			<li><a href="#tabs-1">User</a></li>
    			<li><a href="#tabs-2">Produk</a></li>
    			<li><a href="#tabs-3">Kapal</a></li>
  			</ul>
  			<div id="tabs-1">
			</c:if>
 		<form class="ui form row" action="" method="POST" onsubmit="return validateForm();">
			<input type="hidden" name="action" value="user">
		<div class="ui vertical stripe segment">
		    <div class="ui middle aligned stackable grid container">
		      <div class="row">
		        <div class="two wide column hideyhole">
		        </div>
		        <div class="six wide column">
		        	<h3 class="ui header">Buat Akun</h3>
		        </div>
		      </div>
		      
		      <div class="row">
		        <div class="two wide column hideyhole">
		        </div>
		        <div class="twelve wide column">
		        	<div class="ui form">
			      	  <div class="required field">
					    <label>Email Akun</label>
					    <input type="text" name="useremail" placeholder="Email Akun" value="">
					  </div>
					</div>
		        </div>
		      </div>
		      
		      <div class="row">
		        <div class="two wide column hideyhole">
		        </div>
		        <div class="twelve wide column">
		        	<div class="ui form">
			      	  <div class="required field">
					    <label>Role Akun</label>
					    <select name="userrole">
					    	<option value="cadmin">Customer Admin</option>
					    	<option value="cmember">Customer Member</option>
					    </select>
					  </div>
					</div>
		        </div>
		      </div>
		      
			<c:if test="${isADMIN}">
		      <div class="row">
		        <div class="two wide column hideyhole">
		        </div>
		        <div class="twelve wide column">
		        	<div class="ui form">
			      	  <div class="required field">
					    <label>Perusahaan</label>
					    <input type="text" name="perusahaan" placeholder="Perusahaan" value="">
					  </div>
					</div>
		        </div>
		      </div>
			</c:if>
		      
		      <div class="row">
		        <div class="eight wide column "></div>
		        <div class="six wide right aligned column ">
					<button type="submit" class="ui green button">Save</button>
		        </div>
		        <div class="two wide column "></div>
		      </div>
		    </div>
		</div>
		</form>
		<c:if test="${isADMIN}"> 
			</div>
  			<div id="tabs-2">
	  	<form class="ui form content" action="product" method="POST">
			<input type="hidden" name="action" value="insert">
			<input type="hidden" name="return" value="user">
				<div class="fields">
					<div class="sixteen wide field">
						<label>Nama Produk</label>
			        	<input type="text" name="product_name" placeholder="Nama Produk">
			    	</div>
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>Deskripsi Produk</label>
			        	<textarea name="product_description" placeholder="Deskripsi Produk"></textarea>
			    	</div>		      
				</div>
		  	<div class="fields">
		  		<button type="submit" class="ui positive labeled icon approve button"> 
		      		Simpan
		      		<i class="save icon"></i>
		    	</button>
			</div>
		</form>
  			</div>
  			<div id="tabs-3">
	  			<form class="ui form content" action="" method="POST">    			
					<input type="hidden" name="action" value="kapal">
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Name</label>
			        				<input type="text" name="Name" placeholder="Vessel Name">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>MMSI</label>
			        				<input type="text" name="MMSI" placeholder="MMSI">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>IMO Number</label>
			        				<input type="text" name="IMO" placeholder="IMO Number">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Call Sign</label>
			        				<input type="text" name="CallSign" placeholder="Call Sign">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Bow</label>
			        				<input type="text" name="Bow" placeholder="Bow">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Stern</label>
			        				<input type="text" name="Stern" placeholder="Stern">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Port</label>
			        				<input type="text" name="Port" placeholder="Port">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Star Board</label>
			        				<input type="text" name="StarBoard" placeholder="Star Board">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>GRT</label>
			        				<input type="text" name="GRT" placeholder="GRT">
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Engine HP</label>
			        				<input type="text" name="EHP" placeholder="Engine HP">
			        				<input type="checkbox" name="ED"> Diesel Engine
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
					<div class="row">
		        		<div class="twelve wide column">
		        			<div class="ui form">
			      	  			<div class="field">
					    			<label>Ship Type</label>
					    			<select name="ShipType">
					    				<option value="0;Not available (default)">Not available (default)</option>
					    				<option value="20;Wing in ground (WIG), all ships of this type">Wing in ground (WIG), all ships of this type</option>
										<option value="21;Wing in ground (WIG), Hazardous category A">Wing in ground (WIG), Hazardous category A</option>
										<option value="22;Wing in ground (WIG), Hazardous category B">Wing in ground (WIG), Hazardous category A</option>
										<option value="23;Wing in ground (WIG), Hazardous category C">Wing in ground (WIG), Hazardous category A</option>
										<option value="24;Wing in ground (WIG), Hazardous category D">Wing in ground (WIG), Hazardous category A</option>
					    				<option value="30;Fishing">Fishing</option>
										<option value="31;Towing">Towing</option>
										<option value="32;Towing: length exceeds 200m or breadth exceeds 25m">Towing: length exceeds 200m or breadth exceeds 25m</option>
										<option value="33;Dredging or underwater ops">Dredging or underwater ops</option>
										<option value="34;Diving ops">Diving ops</option>
										<option value="35;Military ops">Military ops</option>
										<option value="36;Sailing">Sailing</option>
										<option value="37;Pleasure Craft">Pleasure Craft</option>
					    				<option value="40;High speed craft (HSC), all ships of this type">High speed craft (HSC), all ships of this type</option>
										<option value="41;High speed craft (HSC), Hazardous category A">High speed craft (HSC), Hazardous category A</option>
										<option value="42;High speed craft (HSC), Hazardous category B">High speed craft (HSC), Hazardous category B</option>
										<option value="43;High speed craft (HSC), Hazardous category C">High speed craft (HSC), Hazardous category C</option>
										<option value="44;High speed craft (HSC), Hazardous category D">High speed craft (HSC), Hazardous category D</option>
					    				<option value="49;High speed craft (HSC), No additional information">High speed craft (HSC), No additional information</option>
					    				<option value="50;Pilot Vessel">Pilot Vessel</option>
					    				<option value="51;Search and Rescue vessel">Search and Rescue vessel</option>
					    				<option value="52;Tug">Tug</option>
					    				<option value="53;Port Tender">Port Tender</option>
					    				<option value="54;Anti-pollution equipment">Anti-pollution equipment</option>
					    				<option value="55;Law Enforcement">Law Enforcement</option>
					    				<option value="56;Spare - Local Vessel">Spare - Local Vessel</option>
					    				<option value="57;Spare - Local Vessel">Spare - Local Vessel</option>
					    				<option value="58;Medical Transport">Medical Transport</option>
					    				<option value="59;Ship according to RR Resolution No. 18">Ship according to RR Resolution No. 18</option>
					    				<option value="60;Passenger, all ships of this type">Passenger, all ships of this type</option>
					    				<option value="61;Passenger, Hazardous category A">Passenger, Hazardous category A</option>
					    				<option value="62;Passenger, Hazardous category B">Passenger, Hazardous category B</option>
					    				<option value="63;Passenger, Hazardous category C">Passenger, Hazardous category C</option>
					    				<option value="64;Passenger, Hazardous category D">Passenger, Hazardous category D</option>
					    				<option value="69;Passenger, No additional information">Passenger, No additional information</option>
					    				<option value="70;Cargo, all ships of this type">Cargo, all ships of this type</option>
					    				<option value="71;Cargo, Hazardous category A">Cargo, Hazardous category A</option>
					    				<option value="72;Cargo, Hazardous category B">Cargo, Hazardous category B</option>
					    				<option value="73;Cargo, Hazardous category C">Cargo, Hazardous category C</option>
					    				<option value="74;Cargo, Hazardous category D">Cargo, Hazardous category D</option>
					    				<option value="79;Cargo, No additional information">Cargo, No additional information</option>
					    				<option value="80;Tanker, all ships of this type">Tanker, all ships of this type</option>
					    				<option value="81;Tanker, Hazardous category A">Tanker, Hazardous category A</option>
					    				<option value="82;Tanker, Hazardous category B">Tanker, Hazardous category B</option>
					    				<option value="83;Tanker, Hazardous category C">Tanker, Hazardous category C</option>
					    				<option value="84;Tanker, Hazardous category D">Tanker, Hazardous category D</option>
					    				<option value="89;Tanker, No additional information">Tanker, No additional information</option>
					    				<option value="90;Other Type, all ships of this type">Other Type, all ships of this type</option>
					    				<option value="91;Other Type, Hazardous category A">Other Type, Hazardous category A</option>
					    				<option value="92;Other Type, Hazardous category B">Other Type, Hazardous category B</option>
					    				<option value="93;Other Type, Hazardous category C">Other Type, Hazardous category C</option>
					    				<option value="94;Other Type, Hazardous category D">Other Type, Hazardous category D</option>
					    				<option value="99;Other Type, no additional information">Other Type, no additional information</option>
					    				
					    			</select>
					  			</div>
							</div>
		        		</div>
		      		</div>
		      		<br>
		      		<br>
		      <div class="row">
		        <div class="eight wide column "></div>
		        <div class="six wide right aligned column ">
					<button type="submit" class="ui green button">Save</button>
		        </div>
		        <div class="two wide column "></div>
		      </div>
				</form>	
  			</div>
		</div>
		</c:if>
		
	<script>
	function validateForm(){
		var failed = false;
		failed = failed || $('[name="useremail"]').val() == undefined;
		failed = failed || $('[name="useremail"]').val() == "";
		failed = failed || $('[name="userrole"]').val() == undefined;
		failed = failed || $('[name="userrole"]').val() == "";
		<c:if test="${isADMIN}">
			failed = failed || $('[name="perusahaan"]').val() == undefined;
			failed = failed || $('[name="perusahaan"]').val() == "";
		</c:if>
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		return !failed;
	}
	 	$(document).ready(function(){
			<c:if test="${isADMIN}">
	 		$('[name=perusahaan]').autocomplete({
	 		      source: ${listPerusahaan},
	 	    });
	 	    $( "#tabs" ).tabs();
			</c:if>
			${message}
	 	});
	</script>
 	</jsp:body>
 </t:app>
