<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Daftar Konfigurasi</jsp:attribute>
 	
 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script>
 		<style>
 		ul.ui-autocomplete {
    		z-index: 1100;
		}
 		</style>
 	</jsp:attribute>
 	
 	<jsp:body>
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	      <div class="row">
  			<h2 class="ui dividing header">Daftar Konfigurasi</h2><br/>
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="plus icon"></i> Tambah Konfigurasi </button> 
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>Key</th>
	                <th>Value</th>
	                <th>Mandatory</th>
	                <th class="center aligned column">Action</th>
	            </tr>
	        </thead>
            <tbody>
            </tbody>
		  </table>
	     </div>
	    </div>
	  </div><br/><br/>
	  
	  <div class="ui small modal add">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    <span id="action"></span> Konfigurasi
	  </div>
	  <div class="ui form content">
		 <div class="fields">
			<div class="eight wide required field">
				<label>Key</label>
		        <input type="text" id="key" placeholder="Key">
				<label>Value</label>
		        <input type="text" id="value" placeholder="Value">
				<label>Mandatory</label>
		        <input type="checkbox" id="mandatory" placeholder="Mandatory">
		    </div>
	  	</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>
	
	<script>
	function validateForm(){
		var failed = false;
		failed = failed || $('#key').val() == undefined;
		failed = failed || $('#key').val() == "";
		failed = failed || $('#value').val() == undefined;
		failed = failed || $('#value').val() == "";
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		return !failed;
	}
		var obj = [
					<c:forEach items="${listKonfigurasi}" var="os" varStatus="nmr">
						{
							"key":'${os.konfig_key}',
							"value":'${os.konfig_value}',
							"mandatory":'${os.konfig_keyasmandatory}',
						},
					</c:forEach>
	               ];
	 	$(document).ready(function(){
	 		var t = $("#example").DataTable( {
	 			"ordering": false,
	 		    "bPaginate": false,
	 		    "bFilter": false,
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 0, 'asc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();

		    for (var i = 0; i < obj.length; i++) {
	 	        t.row.add( [
	 	            obj[i].key,
	 	            obj[i].value,
	 	            (obj[i].mandatory=='t'?'Ya':'Tidak'),
	 	           	'<div class="center aligned column"><div class="ui buttons"><button uid="'+obj[i].key+'" class="ui green button edit"><i class="Edit icon"></i>Edit</button></div> <button uid="'+obj[i].key+'" class="ui red button delete"><i class="trash icon"></i>Delete</button></div></div>',
	 	        ] ).draw( false );
		    }

	 	    $("#addrow").on("click", function () {
	 	    	if(validateForm()){
	 	    	if ($("#action").html()=='Tambah'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/configuration/add',
		            	data: {
		            		'key' : $("#key").val(),
		            		'value' : $("#value").val(),
		            		'mandatory' : $("#mandatory").prop('checked')
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	},
		            	error: function (data) {
		            		alert("Error "+data.status+" "+data.statusText);
		            	}
		        	});
	 	    	}
	 	    	if ($("#action").html()=='Edit'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/configuration/edit',
		            	data: {
		            		'oldkey' : $("#action").attr('uid'),
		            		'key' : $("#key").val(),
		            		'value' : $("#value").val(),
		            		'mandatory' : $("#mandatory").prop('checked')
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	},
		            	error: function (data) {
		            		alert("Error "+data.status+" "+data.statusText);
		            	}
		            	
		        	});
	 	    	}
	 	    	}
	 	    } );

	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$("#action").html('Tambah');
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".edit").on("click",function(){
	 			$("#action").html('Edit');
	 			$("#action").attr('uid', $(this).attr('uid'));
	 			$("#key").val($($(this).closest('tr').find('td')[0]).html());
	 			$("#value").val($($(this).closest('tr').find('td')[1]).html());
	 			$("#mandatory").prop('checked', 'Ya'==$($(this).closest('tr').find('td')[2]).html());
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".delete").on("click",function(){
	 	    	if (confirm("Yakin hapus konfigurasi ini?")){
	 				$.ajax({
		            	type: "POST",
		            	url: '/configuration/delete',
		            	data: {
		            		'key' : $(this).attr('uid')
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	}
		        	});
	 	    	}
	 		});
	 	   
	 	});
	 	//hapus row
	 	$('#example tbody').on( 'click', 'tr > td > div > div > button.delete', function () {
	 	    //alert($(this).parent().parent().parent().parent().index());
	 	   	$("#example").DataTable().row($(this).parent().parent().parent().parent()).remove().draw();
	 	} );
	</script>
 	</jsp:body>
 	
 </t:app>
 
 