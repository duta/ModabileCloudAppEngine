<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Daftar Customer Member</jsp:attribute>
 	
 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script>
 		<style>
 		ul.ui-autocomplete {
    		z-index: 1100;
		}
 		</style>
 	</jsp:attribute>
 	
 	<jsp:body>
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	      <div class="row">
  			<h2 class="ui dividing header">Daftar Customer Member</h2><br/>
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="plus icon"></i> Tambah Customer Member </button>
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>Role</th>
	                <th>Email</th>
	                <th class="center aligned column">Action</th>
	            </tr>
	        </thead>
            <tbody>
            </tbody>
		  </table>
	     </div>
	    </div>
	  </div><br/><br/>
	  
	  <div class="ui small modal add">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    <span id="action"></span> Customer
	  </div>
	  <div class="ui form content">
		 <div class="fields">
			<div class="eight wide required field">
				<label>Email</label>
		        <input type="text" id="email" placeholder="Email">
		    </div>
			<div class="eight wide required field">
			   	<label>Role Customer</label>
			    <select id="role">
			    	<option value="customer member (USER)">customer member (USER)</option>
			    	<option value="customer admin (USER)">customer admin (USER)</option>
			    </select>
			</div>	      
	  	</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>
	
	<script>
	function validateForm(){
		var failed = false;
		failed = failed || $('#email').val() == undefined;
		failed = failed || $('#email').val() == "";
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		return !failed;
	}
		var obj = [
					<c:forEach items="${osList}" var="os" varStatus="nmr">
						{
							"email":'${os.key.user_name}',
							"roles":'${os.value.role_name}',
							"uid":'${os.key.user_id}',
							"cid":'${os.value.role_id}',
						},
					</c:forEach>
	               ];
	 	$(document).ready(function(){
	 		var t = $("#example").DataTable( {
	 			"ordering": false,
	 		    "bPaginate": false,
	 		    "bFilter": false,
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 0, 'asc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();

		    for (var i = 0; i < obj.length; i++) {
	 	        t.row.add( [
	 		 	    obj[i].roles,
	 	            obj[i].email,
	 	           	'<div class="center aligned column"><div class="ui buttons"><button uid='+obj[i].uid+' cid='+obj[i].cid+' class="ui green button edit"><i class="Edit icon"></i>Edit</button></div> <button uid='+obj[i].uid+' cid='+obj[i].cid+' class="ui red button delete"><i class="trash icon"></i>Delete</button></div></div>',
	 	        ] ).draw( false );
		    }

	 	    $("#addrow").on("click", function () {
	 	    	if(validateForm()){
	 	    	if ($("#action").html()=='Tambah'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/customermember/add',
		            	data: {
		            		'email' : $("#email").val(),
		            		'role' : $("#role").val()
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	}
		        	});
	 	    	}
	 	    	if ($("#action").html()=='Edit'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/customermember/edit/'+$("#action").attr('uid'),
		            	data: {
		            		'email' : $("#email").val(),
		            		'role' : $("#role").val()
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	}
		        	});
	 	    	}
	 	    	}
	 	    } );

	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$("#action").html('Tambah');
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".edit").on("click",function(){
	 			$("#action").html('Edit');
	 			$("#action").attr('uid', $(this).attr('uid'));
	 			$("#action").attr('cid', $(this).attr('cid'));
	 			$("#email").val($($(this).closest('tr').find('td')[1]).html());
				$("#role").val($($(this).closest('tr').find('td')[0]).html());
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".delete").on("click",function(){
	 	    	if (confirm("Yakin hapus user ini?")){
	 				$.ajax({
		            	type: "POST",
		            	url: '/customermember/delete/'+$(this).attr('uid'),
		            	data: {
		            		'email' : $("#email").val(),
		            		'role' : $("#role").val()
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	}
		        	});
	 	    	}
	 		});
	 	   
	 	});
	</script>
 	</jsp:body>
 	
 </t:app>
 
 