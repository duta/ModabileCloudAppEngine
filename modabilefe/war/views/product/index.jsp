<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Produk</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Produk</h2><br/>
        		<div class="ui form">
			    	<div class="fields">
						<div class="inline twelve wide field"></div>
						<c:if test="${isADMIN}">
						<div class="six wide field">
							<a class="ui green right floated button" id="add"><i class="plus icon"></i> Tambah</a>
						</div>
						</c:if>
			  		</div>
	   	  			<table class="ui celled padded table">
  			  			<thead>
  			  				<tr>
				  			    <th><h3>Nama Produk</h3></th>
				  			    <th><h3>Deskripsi Produk</h3></th>
				  			    <th><h3>Harga</h3></th>
								<c:if test="${isADMIN}">
				  			    <th></th>
								</c:if>
  			  				</tr>
  			  			</thead>
  			  			<tbody>
  			  				<c:forEach items="${products}" var="product">
	  			    			<tr>
	  			      				<td>
	  			        				<h3 class="ui center aligned header">${product.product_name}</h3>
	  			      				</td>
	  			      				<td>
	  			        				${product.product_description}
	  			      				</td>
	  			      				<td>
	  			        				${product.formatted_pp_price}
	  			      				</td>
									<c:if test="${isADMIN}">
	  			      				<td class="row">
		  			      				<div class="fields">
		  			      					<div class="two wide field"></div>
		  			      					<div class="ui center aligned four wide field">
		  			      						<form class="ui form" action="product" method="POST">
													<input type="hidden" name="action" value="harga">
													<input type="hidden" name="product_id" value="${product.product_id}">
				  			      					<button type="submit" class="ui teal button">
														<i class="money icon"></i>
														Harga
													</button>
												</form>
											</div>
		  			      					<div class="ui center aligned four wide field">
		  			      						<form class="ui form" action="product" method="POST">
													<input type="hidden" name="action" value="update">
													<input type="hidden" name="product_id" value="${product.product_id}">
								  			        <button type="submit" class="ui blue button">
														<i class="edit icon"></i>
														Edit
													</button>
												</form>
											</div>
		  			      					<div class="ui center aligned four wide field">
												<form class="ui form" action="product" method="POST">
													<input type="hidden" name="action" value="delete">
													<input type="hidden" name="product_id" value="${product.product_id}">
													<button type="submit" class="ui red button">
														<i class="trash icon"></i>
														Delete
													</button>
												</form>
											</div>
											<div class="two wide field"></div>
										</div>
	  			      				</td>
	  			      				</c:if>
	  			    			</tr>
	  			    		</c:forEach>
  			  			</tbody>
  					</table>
        			<br>
		  		</div>
	  		</div>
		</div>
	</div>
	
	
	<c:if test="${isADMIN}">
	<div class="ui small modal add">
		<i class="close icon"></i>
		<div class="header">
			<i class="ship icon"></i>
		   	Form Tambah Produk
	  	</div>
	  	<form class="ui form content" action="product" method="POST">
			<input type="hidden" name="action" value="insert">
				<div class="fields">
					<div class="sixteen wide field">
						<label>Nama Produk</label>
			        	<input type="text" name="product_name" placeholder="Nama Produk">
			    	</div>
			    	<!--div class="eight wide field">
						<label>Harga Produk</label>
			        	<input type="number" name="product_price" placeholder="Harga Produk">
			    	</div-->		      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>Deskripsi Produk</label>
			        	<textarea name="product_description" placeholder="Deskripsi Produk"></textarea>
			    	</div>		      
				</div>
		  	<div class="fields">
		  		<button type="submit" class="ui positive labeled icon approve button"> 
		      		Simpan
		      		<i class="save icon"></i>
		    	</button>
		    	<button class="ui red deny right labeled icon button">
		      		Batal
		      		<i class="close icon"></i>
		    	</button>
			</div>
		</form>
	</div>
	
	
	<script>
	 	$(document).ready(function(){
	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 	});
	</script>
	</c:if>
 	</jsp:body>
 
</t:app>
 
 