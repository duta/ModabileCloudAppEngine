<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Edit Produk</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Edit Produk</h2><br/>
        		<form class="ui form row" action="product" method="POST">
        			<input type="hidden" name="action" value="do_update">
					<input type="hidden" name="product_id" value="${product_id}">
        			<div class="two wide column"></div>
        			<div class="twelve wide column">
        				<div class="fields">
							<div class="sixteen wide field">
								<label>Nama Produk</label>
					        	<input type="text" name="product_name" placeholder="Nama Produk" value="${product_name}">
					    	</div>		      
						</div>
						<div class="fields">
							<div class="sixteen wide field">
								<label>Deskripsi Produk</label>
					        	<textarea name="product_description" placeholder="Deskripsi Produk">${product_description}</textarea>
					    	</div>		      
						</div>
						<div class="fields">
							<div class="right floated sixteen wide field">
								<button type="submit" class="ui blue button">
									<i class="check icon"></i>
									Save
								</button>
					    	</div>		      
						</div>
        			</div>
        			<div class="two wide column"></div>
		  		</form>
	  		</div>
		</div>
	</div>
	
 	</jsp:body>
</t:app>
 
 