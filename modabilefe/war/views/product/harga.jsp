<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">History Harga</jsp:attribute>
 	
 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
 		
 	</jsp:attribute>
 	<jsp:body>
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	      <div class="row">
  			<h2 class="ui dividing header">History Harga</h2><br/>
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      	<!-- <label>Nama Perusahaan : </label>
			        <input type="text" class="six wide field" id="NamaPerusahaan" placeholder="Nama Perusahaan"> -->
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="edit icon"></i> Edit Harga </button> 
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>No.</th>
	                <th>Harga Paket</th>
	                <th>Mata Uang</th>
	                <th>Tanggal</th>
	                <th>Dibuat Oleh</th>
	            </tr>
	        </thead>
	        	<c:forEach items="${prices}" var="price" varStatus="nmr">
	        		<tr>
	        			<td>${nmr.index}</td>
	        			<td>${price.formatted_pp_price}</td>
	        			<td>${price.pp_currency}</td>
	        			<td>${price.pp_creationdate}</td>
	        			<td>${price.pp_createby}</td>
	        		</tr>
	        	</c:forEach>
	        <tbody>
	        </tbody>
		  </table>
	     </div>
	    </div>
	  </div><br/><br/>
<br/><br/>
	  
<div class="ui small modal add">
  <i class="close icon"></i>
	<div class="header">
	  <i class="edit icon"></i>
	  Edit Harga Paket
	</div>
	<form class="ui form content" action="product" method="POST">
	  <input type="hidden" name="action" value="update_harga">
	  <input type="hidden" name="product_id" value="${product_id}">
	    <div class="fields">
		  <div class="sixteen wide field">
		    <label>Harga Paket</label>
            <input type="number" name="pp_price" placeholder="Harga Paket">
    	  </div>		      
	    </div>
	    <div class="fields">
		  <div class="sixteen wide field">
		    <label>Mata Uang</label>
            <input type="text" name="pp_currency" placeholder="Mata Uang">
    	  </div>		      
	    </div>
	  <div class="fields">
	  	<button class="ui positive  labeled icon approve  button" type="submit"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	  </div>
	</form>
</div>
	  
	
	<script>
	 	$(document).ready(function(){
	 		// Declare Datatable
	 		var t = $("#example").DataTable( {
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 3, 'desc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();

	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});

	 	});
	</script>
 	</jsp:body>
 	
 </t:app>
 
 