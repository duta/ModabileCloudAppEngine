<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Beli Paket</jsp:attribute>

 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

 		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
 	</jsp:attribute>

 	<jsp:body>
 	<c:set var="colors">orange,blue,red,violet,brown</c:set>
	<c:set var="arrColors" value="${fn:split(colors, ',')}" />
	
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	    	<div class="row"><br/>
	   		<h2 class="ui dividing header">Beli Paket</h2><br/>
	   		<div class="ui three  top link steps ">
				<div class="active link step" data-tab="first"  id="first" >
					<i class="file text outline icon"></i>
					<div class="content">
		    			<div class="title">Paket</div>
		    			<div class="description">Pilih paket yang di inginkan</div>
		  			</div>
				</div>
				<div class="link disabled  step" data-tab="second" id="second" >
			    	<i class="cart icon"></i>
			    	<div class="content">
			      		<div class="title">Pembelian</div>
			      		<div class="description">Informasi pembelian paket</div>
			    	</div>
			  	</div>
				<div class="link disabled step" data-tab="third" id="third" >
			  		<i class="payment icon"></i>
			  		<div class="content">
			    		<div class="title">Konfirmasi Pembelian</div>
			    		<div class="description">Verifikasi pembelian</div>
			  		</div>
				</div>
			</div>
			<div class="ui active tab segment" data-tab="first"  id="firsts" >
	   	 		<h2 class="ui  header">Paket</h2><br/>
				<div class="ui special link stackable four cards" >
			  		<c:forEach items="${products}" var="product" varStatus="nmr">
				 		<div class="${arrColors[nmr.index]} four card">
							<div class="column">
					  			<div class="ui ${arrColors[nmr.index]} segment">
					    			<c:choose>
		  				 				<c:when test="${product.pp_price == 0}">
					      					<a class="ui ${arrColors[nmr.index]} ribbon label">FREE</a><b>${product.product_name}</b>
		  				 				</c:when>
		  				 				<c:otherwise>
		  				  					<a class="ui ${arrColors[nmr.index]} ribbon label">PAKET ${nmr.index}</a><b>${product.product_name}</b>
		  								</c:otherwise>
		  							</c:choose>
					  			</div>
							</div>
							<div class="blurring dimmable image">
								<div class="ui dimmer">
					    			<div class="content">
					      				<div class="center">
					        				<div class="ui inverted button free">Coba Sekarang</div>
					      				</div>
					    			</div>
					  			</div>
							</div>
							<div class="content center aligned column">
					  			<c:choose>
	  				  				<c:when test="${product.pp_price == 0}">
				      	  				<a class="header">FREE TRIAL</a>
	  				  				</c:when>
	  				  				<c:otherwise>
	  				  	  				<a class="header">${product.formatted_pp_price}</a>
	  				  				</c:otherwise>
	  							</c:choose>
					    		<div class="meta">
			      					<span class="description">${product.product_description}</span>
			      				</div>
				    		</div>
							<div class="extra content">
					  			<div class="center aligned column">
					    			<c:choose>
	  				  	  				<c:when test="${product.pp_price == 0}">
				      	    				<div product="${product.product_id}" period="${product.product_period}" warna="${arrColors[nmr.index]}" class="ui ${arrColors[nmr.index]} button 
					    				<c:choose>
	  				  	  					<c:when test="${product.product_id == cancel}">order</c:when>
	  				  	  					<c:otherwise>disabled</c:otherwise>
	  				  	  				</c:choose>">Coba Sekarang</div>
	  				  	  				</c:when>
	  				  	  				<c:otherwise>
	  				  	    				<div product="${product.product_id}" period="${product.product_period}" warna="${arrColors[nmr.index]}" class="ui ${arrColors[nmr.index]} button 
					    				<c:choose>
	  				  	  					<c:when test="${cancel == -1}">order</c:when>
	  				  	  					<c:otherwise>disabled</c:otherwise>
	  				  	  				</c:choose>">Beli Sekarang</div>
	  				  	  				</c:otherwise>
	  				    			</c:choose>
								</div>
							</div>
				 		</div>
					</c:forEach>
				</div>
			</div>
			<div class="ui tab segment" data-tab="second" id="seconds" >
				<h2 class="ui  header">Data Kapal </h2>
				<div style="display:inline-block; width:100%;"><button class="ui blue right floated button" id="addKapal"><i class="plus icon"></i> Tambah Kapal </button></div>
				<br />
				<table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
					<thead>
						<tr>
							<th>No.</th>
							<th>MMSI</th>
							<th>Nama Kapal</th>
	                		<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
	   	 		<h2 class="ui  header">Informasi Pembelian</h2><br/>
				<table class="ui red definition table" id="infobeli">
					<thead>
						<tr>
							<th></th>
			  				<th colspan="2" class="center aligned">Pembelian Paket</th>
			  			</tr>
					</thead>
			  		<tbody>
			  			<tr>
			      			<td class="ui two wide">Tipe Paket</td>
			      			<td><span id="tipepaket">PAKET 1 BASIC</span></td>
			    		</tr>
			    		<tr>
			      			<td>Harga Paket</td>
			      			<td><span id="hargapaket">IDR. 1000,000</span></td>
			    		</tr>
			    		<tr>
			      			<td>Waktu Paket</td>
			      			<td><span id="waktupaket"></span> Bulan</td>
			    		</tr>
			    		<tr>
			      			<td>Deskripsi Paket</td>
			      			<td><span id="des">Kami menerima pembayaran melalui transfer Bank, segera kami menerima pembayaran melalui kartu kredit.</span></td>
			    		</tr>
			    		<tr class="paymentInfos">
			      			<td colspan="2" class="center aligned" >Info Bank</td>
			    		</tr>
			     		<tr class="paymentInfos">
			      			<td>Bank</td>
			      			<td><span id="bank">BRI</span></td>
			    		</tr>
					   	<tr class="paymentInfos">
					    	<td>No. Rek</td>
					    	<td><span id=norek>83294732947</span></td>
					    </tr>
					    <tr class="paymentInfos">
					    	<td>Nama</td>
					    	<td><span id="nama">DMC</span></td>
					    </tr>
					    <tr class="paymentInfos">
					    	<td colspan="2" class="center aligned" >Konfirmasi</td>
					    </tr>
					    <tr class="paymentInfos">
					    	<td>Informasi</td>
					    	<td><span id="info">Segera lakukan konfirmasi setelah melakukan transfer dengan mengirim bukti transfer ke email dutamediacipta@gmail.com</span></td>
					    </tr>
					</tbody>
			 		<tfoot class="full-width">
					    <tr>
					    	<th></th>
					    	<th colspan="4">
					        	<div class="ui right floated small primary labeled icon button" id="konfirm">
					          		<i class="cart icon" ></i> Proses
					        	</div>
					        	<div class="ui small button" id="back1">
					          		<i class="Arrow Left icon"></i> Kembali
					        	</div>
					      	</th>
						</tr>
		  			</tfoot>
				</table>
			</div>
			<div class="ui  tab segment" data-tab="third" id="thrids" >
	   	   		<h2 class="ui dividing header">Konfirmasi Pembelian</h2>
		   		<div class="ui floating icon message">
			  		<i class="info icon"></i>
			  		<div class="content">
			    		<div class="header">
			      			Informasi
			    		</div>
			    		<p>Terima Kasih atas pembelian Anda. Data Kapal anda akan segera kami proses.</p>
			  		</div>
				</div><br/>
			<a href="list" class="ui button green"><i class="ship icon"></i> Lihat Daftar Kapal</a>
	   	  	<!-- <form action="#" enctype="multipart/form-data" class="ui form" method="post">
				<div class=" fields">
					<div class="inline twelve wide field">
						<label class="three wide field">No. Rekening Anda</label>
						<input type="text" class="six wide field" id="norek" placeholder="No. Rekening Anda">
					</div>
				</div>
				<div class="fields">
					<div class="inline twelve wide field">
						<label class="three wide field">Nama</label>
						<input type="text" class="six wide field" id="Nama" placeholder="Nama">
					</div>
				</div>
				<div class="fields">
					<div class="inline twelve wide field">
						<label class="three wide field">Bank</label>
						<input type="text" class="six wide field" id="bank" placeholder="Bank">
					</div>
				</div>
				<div class="fields">
					<div class="ui inline twelve wide field">
						<label class="three wide field" >Foto struk </label>
						<input type="file"  class="ui icon button six wide field"  name="files[]" multiple="multiple" />
						<span class="ui red tag label">Jika ada *</span>
					</div>
				</div>
				<br/>

				<div class="ui inline twelve wide field">
				<button type="reset" class="ui orange button" id="back2">Back &nbsp;&nbsp;<i class="refresh icon"></i> </button>
				<button type="button" class="ui green button" id="finis"><i class="save icon"></i> Konfirmasi Transefer </button>
				</div>
		  	</form> -->
			</div>
	   	 	<br/><br/>
		  </div>
	  </div>
	</div>

<input type="hidden" id="id">

	<div class="ui small modal add">
	  <i class="close icon"></i>
	  <div class="" id="warna">
	  </div>
	  <div class="image content">
	  	<div class="ui small image ">
	      <i class="cart plus icon"></i>
	    </div>
	    <div class="description">
	      <div class="ui header"><span id="header"></span></div>
	      <b><span id="des"></span></b><br/><br/>
	      <span>Apakah anda yakin memilih paket ini ?</span>
	    </div>
	  </div>
	  <div class="actions">
	    <div class="ui red deny left labeled icon button">
	      <i class="close icon"></i>
	      Batal
	    </div>
	    <div id="buy" class="ui positive right labeled icon button">
	      Beli Paket
	      <i class="cart icon"></i>
	    </div>
	  </div>
	</div>

	  <div class="ui small modal addKapal">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    Form Tambah Kapal
	  </div>
	  <div class="ui form content">
		 <div class="fields">
			<div class="eight wide field">
				<label>MMSI</label>
		        <input type="text" id="MMSI" placeholder="MMSI">
		    </div>
			<div class="eight wide field">
			   	<label>Nama Kapal</label>
			    <input type="text" id="NamaKapal" placeholder="Nama Kapal">
			</div>	      
	  	</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	     <button class="ui yellow reset button">
	      Reset
	      <i class="refresh icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>

		<script>
	 	$(document).ready(function(){
		    var t = $("#example").DataTable( {
	 		    "bPaginate": false,
	 		    "bFilter": false,
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 1, 'asc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();
	 		
	 		var retrievedObject = localStorage.getItem('dataKapal');
		    var obj = JSON.parse(retrievedObject);

		    if (!$.isEmptyObject(obj)){
		    	for (var i = 0; i < obj.length; i++) {
		 	        t.row.add( [
		 	            '',
		 	            obj[i].data_kapal.MMSI,
		 	            obj[i].data_kapal.NamaKapal,
		 	           	'<div class="center aligned column"><div class="ui buttons"><button class="ui red button delete"><i class="trash icon"></i>Hapus</button></div></div>',
		 	        ] ).draw( false );
			    }
		    }

	 	    $("#addrow").on("click", function () {
	 	        t.row.add( [
	 	            '',
	 	            $("#MMSI").val(),
	 	            $("#NamaKapal").val(),
	 	            '<div class="center aligned column"><div class="ui buttons"><button class="ui red button delete"><i class="trash icon"></i>Hapus</button></div></div>',
	 	        ] ).draw( false );
	 	    	var data = t.rows().data();
	 	    	var dataKapal = [];
				for (var i = 0; i < data.length; i++) {
			 	    dataKapal.push({
				        "data_kapal": {
				            "MMSI": data[i][1],
				            "NamaKapal": data[i][2]
				        }
				    });
			 	}
		 	    
		 	 	localStorage.removeItem('dataKapal');
		 	 	localStorage.setItem('dataKapal', JSON.stringify(dataKapal));
	 	    } );

	 	    

	 	    // reset Modal
	 	   $(".reset").on("click",function(){
	 		  $("#MMSI").val('');
	          $("#NamaKapal").val('');
	 	   });

	 		// Show Modal
	 		$("#addKapal").on("click",function(){
	 			$('.ui.small.modal.addKapal')
		 		  .modal('show')
		 		;
	 		});
	 		
		 	$('#example tbody').on( 'click', 'tr > td > div > div > button.delete', function () {
		 	    //alert($(this).parent().parent().parent().parent().index());
		 	   	$("#example").DataTable().row($(this).parent().parent().parent().parent()).remove().draw();
	 	    	var data = t.rows().data();
	 	    	var dataKapal = [];
				for (var i = 0; i < data.length; i++) {
			 	    dataKapal.push({
				        "data_kapal": {
				            "MMSI": data[i][1],
				            "NamaKapal": data[i][2]
				        }
				    });
			 	}
		 	    
		 	 	localStorage.removeItem('dataKapal');
		 	 	localStorage.setItem('dataKapal', JSON.stringify(dataKapal));
		 	} );
	 		 
	 		$('.steps .step').tab();

	 		$('.special.cards .image').dimmer({
	 			  on: 'hover'
	 		});
	 		
	 		var aku = -1;
	 		var pilih = -1;
	 		var product_id = -1;
	 		var product_period = -1;
	 		var product_name = "";
	 		var harga = "";
	 		var desc = "";
	 		
	 		$(".order").click(function(){
	 			aku = 1;
	 			product_id = $(this).attr("product");
	 			product_period = $(this).attr("period");
	 			$("#id").val(product_id);
	 			$("#waktupaket").html(product_period);
	 			$("#warna").attr("class","ui label header "+$(this).attr("warna"));
	 			product_name = $(this).parent().parent().prev().prev().prev().find("a").html()+ " " +$(this).parent().parent().prev().prev().prev().find("b").html();
	 			$('#warna').html("<i class='alarm icon'> "+product_name);
	 			harga = $(this).parent().parent().prev().find("a.header").html();
	 			$("#header").text(harga);
	 			if (harga == "FREE TRIAL") {
	 				$('.paymentInfos').css('display','none');
	 			} else {
	 				$('.paymentInfos').css('display','table-row');
	 			}
	 			desc = $(this).parent().parent().prev().find("span.description").html();
	 			$('#des').text(desc);
	 			$('.small.modal.add')
	 			  .modal('show')
	 			;
	 		});

	 		$('#buy').on('click',function(){
	 			if(aku == 1){
	 				$("#tipepaket").html(product_name);
	 				$("#hargapaket").html(harga);
	 				document.getElementById("first").className = 'completed disabled step';
		 			document.getElementById("second").className = 'active link step';
		 			$('.ui.steps').find('.step').tab('change tab', 'second');
	 			}else if(aku == 2){
	 				document.getElementById("second").className = 'completed disabled step';
		 			document.getElementById("third").className = 'active link step';
		 			$('.ui.steps').find('.step').tab('change tab', 'third');

		 			var arr = [];
		 	    	var data = t.rows().data();
		 			for (var i = 0; i < data.length; i++) {

			 	     	arr.push({
					            "MMSI": data[i][1],
					            "NamaKapal": data[i][2],
					    });

				    }

		 			// Start Insert Database
		 			$.ajax({
			            type: "POST",
			            url: '/belipaket',
			            data: {
			            	'product' : product_id,
			            	'dataKapal' : JSON.stringify(arr)
			            },
			            success: function (data) {
			                localStorage.removeItem('dataKapal');
			            }
			        });
		 			// End Insert Database

		 		}else if(aku == 3){

		 			window.location="/";
	 			}

	 		});
	 		$('#back1').on('click',function(){
	 			document.getElementById("first").className = 'active link step';
	 			document.getElementById("second").className = ' disabled link step';
	 			$('.ui.steps').find('.step').tab('change tab', 'first');
	 		});
	 		$('#konfirm').on('click',function(){
	 			document.getElementById("warna").className = "ui label header blue";
	 			$('#warna').html('<i class="alarm icon"></i>KONFIRMASI');
	 			$('#header').text($('#free').text());
	 			$('#des').text($('#desfree').text());
	 			$('.small.modal.add')
	 			  .modal('show')
	 			;
	 			//pilih=1;

	 			aku=2;
	 		});
	 		$('#back2').on('click',function(){
	 			document.getElementById("second").className = 'active link step';
	 			document.getElementById("third").className = ' disabled link step';
	 			$('.ui.steps').find('.step').tab('change tab', 'second');
	 		});
	 		$('#finis').on('click',function(){
	 			document.getElementById("warna").className = "ui label header green";
	 			$('#warna').html('<i class="alarm icon"></i>FINISH');
	 			$('#header').text('THANK YOU VERY MUCH');
	 			$('#des').text('Terimaksih telah menyelesaikan pembelian paket. Salam DMC  ');
	 			$('.small.modal.add')
	 			  .modal('show')
	 			;
	 			//pilih=1;

	 			aku=3;
	 		});
	 	});
		</script>
 	</jsp:body>

 </t:app>
