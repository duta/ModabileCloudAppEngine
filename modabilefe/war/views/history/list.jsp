<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	
	<c:forEach items="${osList}" var="os" varStatus="nmr">
		<tr>
			<td class="center aligned">${nmr.index+1}.</td>
			<td>${os.ship_mmsi}</td>
			<td>${os.ship_name}</td>
			<!-- <td>${os.ship_imo}</td>
			<td>${os.ship_callsign}</td> -->
		</tr>
	</c:forEach>
						