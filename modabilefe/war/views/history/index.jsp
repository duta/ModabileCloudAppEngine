<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Riwayat Pemesanan</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>

	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<br>
			<div class="row">
				<h2 class="ui dividing header">Riwayat Pemesanan</h2><br/>
				<table class="ui celled padded table">
					<thead>
						<tr>
							<th>Nama Produk</th>
							<th>Jangka Waktu</th>
							<th>Lihat List Kapal</th>
	    			  	</tr>
					</thead>
					<tbody>
						<c:forEach items="${products}" var="product">
							<tr>
								<td>
									<h2 class="ui center aligned header">${product.product_name}</h2>
								</td>
								<td class="single line">
									${product.formatted_startdate} - ${product.formatted_enddate}
								</td>
								<td class="center aligned single line">
									<div order="${product.orddtl_id}" class="ui vertical animated button orange" tabindex="0">
										<div class="hidden content">Kapal</div>
										<div class="visible content">
											<i class="ship icon"></i>
										</div>
									</div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br>
			</div>
		</div>
  	</div>
  	
  	<div class="ui large modal">
		<i class="close icon"></i>
		<div class="ui label header blue"><i class="ship icon"></i> List Kapal</div>
		<div class="content">
			<div class="description" style="width:100%;">
				<table class="ui celled padded table">
					<thead>
						<tr>
							<th class="center aligned" style="width:60px;">No.</th>
							<th>MMSI</th>
							<th>Nama Kapal</th>
							<!-- <th>IMO</th>
							<th>Call Sign</th>-->
						</tr>
					</thead>
					<tbody id="list">
					</tbody>
				</table>
			</div>
		</div>
	</div>
  	
  	<script>
  	$(document).ready(function(){
  		$(".button").click(function(){
  			//alert($(this).attr("order"));
  			$.ajax({
	            type: "POST",
	            url: '/history',
	            data: {
	            	'orddtl_id' : $(this).attr("order")
	            },
	            success: function (data) {
	            	$("#list").html(data);
	            	$('.large.modal')
		 			  .modal('show')
		 			;
	            }
	        });
  		});
  	});
  	</script>
 	</jsp:body>
 </t:app>
