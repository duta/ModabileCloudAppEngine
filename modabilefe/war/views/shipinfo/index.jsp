<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Master Kapal</jsp:attribute>
 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script>
 	</jsp:attribute>
 	<jsp:body>
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	      <div class="row">
  			<h2 class="ui dividing header">Daftar Kapal</h2><br/>
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="plus icon"></i> Tambah Kapal </button> 
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>Vessel Name</th>
	                <th>MMSI</th>
	                <th>IMO Number</th>
	                <th>Call Sign</th>
	                <th>Ship Type</th>
	                <th class="center aligned column">Gambar</th>
	                <th class="center aligned column">Action</th>
	            </tr>
	        </thead>
            <tbody>
            </tbody>
		  </table>
	     </div>
	    </div>
	  </div><br/><br/>
	  
	  <div class="ui small modal add">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    <span id="action"></span> Kapal
	  </div>
	  <div class="ui form content"> 			
					<input type="hidden" name="action" value="kapal">
		 <div class="fields">   
			      	  			<div class="field required">
					    			<label>Vessel Name</label>
			        				<input type="text" name="Name" id="Name" placeholder="Vessel Name">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field required">
					    			<label>MMSI</label>
			        				<input type="number" name="MMSI" id="MMSI" placeholder="MMSI">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field required">
					    			<label>IMO Number</label>
			        				<input type="number" name="IMO" id="IMO" placeholder="IMO Number">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Call Sign</label>
			        				<input type="text" name="CallSign" id="CallSign" placeholder="Call Sign">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Bow</label>
			        				<input type="number" name="Bow" id="Bow" placeholder="Bow">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Stern</label>
			        				<input type="number" name="Stern" id="Stern" placeholder="Stern">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Port</label>
			        				<input type="number" name="Port" id="Port" placeholder="Port">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Star Board</label>
			        				<input type="number" name="StarBoard" id="StarBoard" placeholder="Star Board">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>GRT</label>
			        				<input type="number" name="GRT" id="GRT" placeholder="GRT">
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Engine HP</label>
			        				<input type="number" name="EHP" id="EHP" placeholder="Engine HP">
			        				<input type="checkbox" name="ED" id="ED"> Diesel Engine
					  			</div>
					  			</div>
		      <div class="fields">
			      	  			<div class="field">
					    			<label>Ship Type</label>
					    			<select name="ShipType" id="ShipType">
					    				<option value="0">Not available (default)</option>
					    				<option value="20">Wing in ground (WIG), all ships of this type</option>
										<option value="21">Wing in ground (WIG), Hazardous category A</option>
										<option value="22">Wing in ground (WIG), Hazardous category A</option>
										<option value="23">Wing in ground (WIG), Hazardous category A</option>
										<option value="24">Wing in ground (WIG), Hazardous category A</option>
					    				<option value="30">Fishing</option>
										<option value="31">Towing</option>
										<option value="32">Towing: length exceeds 200m or breadth exceeds 25m</option>
										<option value="33">Dredging or underwater ops</option>
										<option value="34">Diving ops</option>
										<option value="35">Military ops</option>
										<option value="36">Sailing</option>
										<option value="37">Pleasure Craft</option>
					    				<option value="40">High speed craft (HSC), all ships of this type</option>
										<option value="41">High speed craft (HSC), Hazardous category A</option>
										<option value="42">High speed craft (HSC), Hazardous category B</option>
										<option value="43">High speed craft (HSC), Hazardous category C</option>
										<option value="44">High speed craft (HSC), Hazardous category D</option>
					    				<option value="49">High speed craft (HSC), No additional information</option>
					    				<option value="50">Pilot Vessel</option>
					    				<option value="51">Search and Rescue vessel</option>
					    				<option value="52">Tug</option>
					    				<option value="53">Port Tender</option>
					    				<option value="54">Anti-pollution equipment</option>
					    				<option value="55">Law Enforcement</option>
					    				<option value="56">Spare - Local Vessel</option>
					    				<option value="57">Spare - Local Vessel</option>
					    				<option value="58">Medical Transport</option>
					    				<option value="59">Ship according to RR Resolution No. 18</option>
					    				<option value="60">Passenger, all ships of this type</option>
					    				<option value="61">Passenger, Hazardous category A</option>
					    				<option value="62">Passenger, Hazardous category B</option>
					    				<option value="63">Passenger, Hazardous category C</option>
					    				<option value="64">Passenger, Hazardous category D</option>
					    				<option value="69">Passenger, No additional information</option>
					    				<option value="70">Cargo, all ships of this type</option>
					    				<option value="71">Cargo, Hazardous category A</option>
					    				<option value="72">Cargo, Hazardous category B</option>
					    				<option value="73">Cargo, Hazardous category C</option>
					    				<option value="74">Cargo, Hazardous category D</option>
					    				<option value="79">Cargo, No additional information</option>
					    				<option value="80">Tanker, all ships of this type</option>
					    				<option value="81">Tanker, Hazardous category A</option>
					    				<option value="82">Tanker, Hazardous category B</option>
					    				<option value="83">Tanker, Hazardous category C</option>
					    				<option value="84">Tanker, Hazardous category D</option>
					    				<option value="89">Tanker, No additional information</option>
					    				<option value="90">Other Type, all ships of this type</option>
					    				<option value="91">Other Type, Hazardous category A</option>
					    				<option value="92">Other Type, Hazardous category B</option>
					    				<option value="93">Other Type, Hazardous category C</option>
					    				<option value="94">Other Type, Hazardous category D</option>
					    				<option value="99">Other Type, no additional information</option>
					    				
					    			</select>
					  			</div>
					  			</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>
	  <div class="ui small modal picdiv">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    Upload Gambar Kapal
	  </div>
	  <form id="uploadForm" action="/shipInfo" method="post" enctype="multipart/form-data">
	  <div class="ui form content">
		 <div class="fields">   
			      	  			<div class="field required">
					    			<label>Upload File</label>
			        				<input type="file" name="pic" accept="image/*">
					  			</div>
					  			</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="pushPic"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	  </form>
	</div>
	
	<script>
	function validateForm(){
		var failed = false;
		failed = failed || $('#Name').val() == undefined;
		failed = failed || $('#Name').val() == "";
		failed = failed || $('#MMSI').val() == undefined;
		failed = failed || $('#MMSI').val() == "";
		failed = failed || $('#IMO').val() == undefined;
		failed = failed || $('#IMO').val() == "";
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		return !failed;
	}
		var obj = [
					<c:forEach items="${osList}" var="os" varStatus="nmr">
						{
							"vesselname":'${os.vesselname}',
							"mmsi":'${os.mmsi}',
							"imonumber":'${os.imonumber}',
							"callsign":'${os.callsign}',
							"dimensiontobow":'${os.dimensiontobow}',
							"dimensiontostern":'${os.dimensiontostern}',
							"dimensiontoport":'${os.dimensiontoport}',
							"dimensiontostarboard":'${os.dimensiontostarboard}',
							"grt":'${os.grt}',
							"enginehp":'${os.enginehp}',
							"enginediesel":'${os.dte}',
							"shiptype":'${os.shiptype}'
						},
					</c:forEach>
	               ];
	 	$(document).ready(function(){
	 		var t = $("#example").DataTable( {
	 			"ordering": false,
	 		    "bPaginate": false,
	 		    "bFilter": false,
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 0, 'asc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();

		    for (var i = 0; i < obj.length; i++) {
	 	        t.row.add( [
	 	            obj[i].vesselname,
	 	            obj[i].mmsi,
	 	            obj[i].imonumber,
	 	            obj[i].callsign,
	 	            (($("#ShipType option[value='"+obj[i].shiptype+"']").length==0) ? "Other" : $("#ShipType option[value='"+obj[i].shiptype+"']").text()) + " (<span>" + obj[i].shiptype + "</span>)",
	 	            '<div style="width:190px;"><img src="/vessel?op=getimage&mmsi='+obj[i].mmsi+'" onerror="this.style.display=\'none\'"  style="max-width:190px; max-height=130px;"><br><div class="ui buttons"><button mmsi='+obj[i].mmsi+' class="ui blue button upload"><i class="upload icon"></i>Upload</button></div></div>',
	 	           	'<div style="width:140px;" class="center aligned column"><div class="ui buttons"><button mmsi='+obj[i].mmsi+' dimensiontobow='+obj[i].dimensiontobow+' dimensiontostern='+obj[i].dimensiontostern+' dimensiontoport='+obj[i].dimensiontoport+' dimensiontostarboard='+obj[i].dimensiontostarboard+' grt='+obj[i].grt+' enginehp='+obj[i].enginehp+' enginediesel="'+obj[i].enginediesel+'" class="ui green button edit"><i class="Edit icon"></i>Edit Details</button></div><br><br><div class="ui buttons"><button mmsi='+obj[i].mmsi+' class="ui red button delete"><i class="trash icon"></i>Delete</button></div></div>',
	 	        ] ).draw( false );
		    }

	 	    $("#addrow").on("click", function () {
	 	    	if(validateForm()){
	 	    	if ($("#action").html()=='Tambah'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/shipInfo/add',
		            	data: {
		            		'Name' : $("#Name").val(),
		            		'MMSI' : $("#MMSI").val(),
		            		'IMO' : $("#IMO").val(),
		            		'CallSign' : $("#CallSign").val(),
		            		'Bow' : $("#Bow").val(),
		            		'Stern' : $("#Stern").val(),
		            		'StarBoard' : $("#StarBoard").val(),
		            		'Port' : $("#Port").val(),
		            		'GRT' : $("#GRT").val(),
		            		'EHP' : $("#EHP").val(),
		            		'ED' : $("#ED").prop('checked'),
		            		'ShipType' : $("#ShipType").val(),
		            		'ShipTypeDesc' : $("#ShipType option[value='"+$("#ShipType").val()+"']").text()
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	},
		            	error: function (data) {
		            		alert("Error "+data.status+" "+data.statusText);
		            	}
		        	});
	 	    	}
	 	    	if ($("#action").html()=='Edit'){
	 				$.ajax({
		            	type: "POST",
		            	url: '/shipInfo/edit/'+$("#action").attr('uid'),
		            	data: {
		            		'Name' : $("#Name").val(),
		            		'MMSI' : $("#MMSI").val(),
		            		'IMO' : $("#IMO").val(),
		            		'CallSign' : $("#CallSign").val(),
		            		'Bow' : $("#Bow").val(),
		            		'Stern' : $("#Stern").val(),
		            		'StarBoard' : $("#StarBoard").val(),
		            		'Port' : $("#Port").val(),
		            		'GRT' : $("#GRT").val(),
		            		'EHP' : $("#EHP").val(),
		            		'ED' : $("#ED").prop('checked'),
		            		'ShipType' : $("#ShipType").val(),
		            		'ShipTypeDesc' : $("#ShipType option[value='"+$("#ShipType").val()+"']").text()
		            	},
		            	success: function (data) {
		            		window.location.reload();
		            	},
		            	error: function (data) {
		            		alert("Error "+data.status+" "+data.statusText);
		            	}
		            	
		        	});
	 	    	}
	 	    	}
	 	    } );

	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$("#action").html('Tambah');
	 			$("#action").attr('uid', '');
	 			$("#Name").val('');
	 			$("#MMSI").val('');
	 			$("#IMO").val('');
	 			$("#CallSign").val('');
	 			$("#Bow").val('');
	 			$("#Stern").val('');
	 			$("#StarBoard").val('');
	 			$("#Port").val('');
	 			$("#GRT").val('');
	 			$("#EHP").val('');
	 			$("#ED").prop('checked', false);
	 			$("#ShipType").val(0);
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".edit").on("click",function(){
	 			$("#action").html('Edit');
	 			$("#action").attr('uid', $(this).attr('mmsi'));
	 			$("#Name").val($($(this).closest('tr').find('td')[0]).html());
	 			$("#MMSI").val($($(this).closest('tr').find('td')[1]).html());
	 			$("#IMO").val($($(this).closest('tr').find('td')[2]).html());
	 			$("#CallSign").val($($(this).closest('tr').find('td')[3]).html());
	 			$("#Bow").val($(this).attr('dimensiontobow'));
	 			$("#Stern").val($(this).attr('dimensiontostern'));
	 			$("#StarBoard").val($(this).attr('dimensiontostarboard'));
	 			$("#Port").val($(this).attr('dimensiontoport'));
	 			$("#GRT").val($(this).attr('grt'));
	 			$("#EHP").val($(this).attr('enginehp'));
	 			$("#ED").prop('checked', $(this).attr('enginediesel')=="1");
	 			$("#ShipType").val($($(this).closest('tr').find('td')[4]).find('span').html());
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 		$(".upload").on("click",function(){
	 			$("#uploadForm").attr("action", "/shipInfo/image/"+$(this).attr('mmsi'));
	 			$('.ui.small.modal.picdiv').modal('show');
	 		});
	 	    $("#addrow").on("click", function () {
	 	    	$("#uploadForm").submit();
	 	    });
	 		$(".delete").on("click",function(){
	 	    	if (confirm("Yakin hapus kapal ini?")){
	 				$.ajax({
		            	type: "POST",
		            	url: '/shipInfo/delete/'+$(this).attr('mmsi'),
		            	success: function (data) {
		            		window.location.reload();
		            	}
		        	});
	 	    	}
	 		});
	 	   
	 	});
	 	//hapus row
	 	$('#example tbody').on( 'click', 'tr > td > div > div > button.delete', function () {
	 	    //alert($(this).parent().parent().parent().parent().index());
	 	   	$("#example").DataTable().row($(this).parent().parent().parent().parent()).remove().draw();
	 	} );
	</script>
 	</jsp:body>
 </t:app>