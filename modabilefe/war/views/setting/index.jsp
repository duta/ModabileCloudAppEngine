<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Konfigurasi Peta</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>

    <div class="ui vertical segment">
  	    <div class="ui middle aligned stackable  container">
  	      <div class="row">
  	   	  <h2 class="ui dividing header">Konfigurasi Peta</h2><br/>
          <div class="center aligned column">
            <table class="ui very basic collapsing celled table" style="margin:0 auto;">
              <thead>
              </thead>
              <tbody>
                <tr>
                  <td>
                    Heading
                  </td>
                  <td>
                    <div class="ui toggle checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label></label>
                  </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    Image
                  </td>
                  <td>
                    <div class="ui toggle checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label></label>
                  </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    Label
                  </td>
                  <td>
                    <div class="ui toggle checkbox">
                    <input type="checkbox" tabindex="0" class="hidden">
                    <label></label>
                  </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          <br>
          <br>
          <br>
  		  </div>
  	  </div>
  	</div>
 	</jsp:body>

 </t:app>
