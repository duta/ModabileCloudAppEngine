<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Paket Harga</jsp:attribute>
 	
 	<jsp:attribute name="header">
 	</jsp:attribute>
 	
 	<jsp:body>
 	
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable   container">
	      <div class="row"><br/>
	   	   <h2 class="ui dividing header">Paket Harga</h2><br/>
	   	   <div class="ui floating icon message">
			  <i class="info icon"></i>
			  <div class="content">
			    <div class="header">
			      Informasi
			    </div>
			    <p>* Jika client sudah melakukan masa trial maka tidak bisa trial lagi.</p>
			  </div>
			</div><br/>
	   	   <div class="ui special link stackable four cards">
			  <div class="orange four card">
			  <div class="column">
			    <div class="ui orange segment">
			      <a class="ui orange ribbon label">FREE</a><b>TRIAL</b>
			    </div>
			  </div>
			    <div class="blurring dimmable image">
			      <div class="ui dimmer">
			        <div class="content">
			          <div class="center">
			            <div class="ui inverted button free">Coba Sekarang</div>
			          </div>
			        </div>
			      </div>
			      <img src="http://www.halosfun.com/wordpress/wp-content/themes/halos/public/images/free-game/free-sticker.png">
			    </div>
			    <div class="content center aligned column">
			      <a class="header" id="free">FREE TRIAL</a>
			      <div class="meta">
			        <span class="description" id="desfree">GRATIS 2 MINGGU</span>
			      </div>
			    </div>
			    <div class="extra content">
				    <div class="center aligned column">
				       <div class="ui orange button free">Coba Sekarang</div>
				    </div>
			    </div>
			  </div>
			  <div class="red  four column card">
			   <div class="column">
			    <div class="ui red segment">
			      <a class="ui red ribbon label">PAKET I</a> <b>BASIC</b>
			    </div>
			  </div>
			    <div class="blurring dimmable image">
			      <div class="ui inverted dimmer">
			        <div class="content">
			          <div class="center">
			            <div class="ui inverted button basik">Beli Sekarang</div>
			          </div>
			        </div>
			      </div>
			      <img src="https://www.drupal.org/files/project-images/basic_logo.jpg">
			    </div>
			    <div class="content center aligned column">
			      <a class="header" id="harga1">IDR : 100,000,00</a>
			      <div class="meta">
			        <span class="description" id="des1">PER BULAN</span>
			      </div>
			    </div>
			    <div class="extra content">
			       <div class="center aligned column">
				       <div class="ui red button basik">Beli Sekarang</div>
				    </div>
			    </div>
			  </div>
			  <div class="blue four column card">
			  <div class="column">
			    <div class="ui blue segment">
			      <a class="ui blue ribbon label">PAKET II</a> <b>START UP</b>
			    </div>
			  </div>
			    <div class="blurring dimmable image">
			      <div class="ui dimmer">
			        <div class="content">
			          <div class="center">
			            <div class="ui inverted button startup">Beli Sekarang</div>
			          </div>
			        </div>
			      </div>
			      <img src="http://semantic-ui.com/images/avatar/large/jenny.jpg">
			    </div>
			    <div class="content center aligned column">
			      <a class="header" id="harga2">IDR : 580,000,000</a>
			      <div class="meta">
			        <span class="description" id="des2">PER 6 BULAN</span>
			      </div>
			    </div>
			    <div class="extra content">
			   		<div class="center aligned column">
				       <div class="ui blue button startup">Beli Sekarang</div>
				    </div>
			    </div>
			  </div>
			  <div class="violet card">
			   <div class="column">
			    <div class="ui violet segment">
			      <a class="ui violet ribbon label">PAKET III</a> <b>ENTERPRISE</b>
			    </div>
			  </div>
			    <div class="blurring dimmable image">
			      <div class="ui dimmer">
			        <div class="content">
			          <div class="center">
			            <div class="ui inverted button enterprise" >Beli Sekarang</div>
			          </div>
			        </div>
			      </div>
			      <img src="http://semantic-ui.com/images/avatar/large/elliot.jpg">
			    </div>
			    <div class="content center aligned column">
			      <a class="header" id="harga3">IDR : 950,000,000</a>
			      <div class="meta">
			        <span class="description" id="des3">PER 1 TAHUN</span>
			      </div>
			    </div>
			    <div class="extra content">
			       <div class="center aligned column">
				       <div class="ui violet button enterprise">Beli Sekarang</div>
				    </div>
			    </div>
			  </div>
			</div>
	   	 		 <br/><br/>
		  </div>
	  </div>
	</div>
		<div class="ui small modal">
		  <i class="close icon"></i>
		  <div class="" id="warna">
		  </div>
		  <div class="image content">
		  	<div class="ui small image ">
		      <i class="cart plus icon"></i>
		    </div>
		    <div class="description">
		      <div class="ui header"><span id="header"></span></div>
		      <b><span id="des"></span></b><br/><br/>
		      <span>Apakah anda yakin memilih paket ini ?</span>
		    </div>
		  </div>
		  <div class="actions">
		    <div class="ui red deny left labeled icon button">
		      <i class="close icon"></i>
		      Batal
		    </div>
		    <div id="buy" class="ui positive right labeled icon button">
		      Beli Paket
		      <i class="cart icon"></i>
		    </div>
		  </div>
		</div>
		<script>
	 	$(document).ready(function(){
	 		$('.special.cards .image').dimmer({
	 			  on: 'hover'
	 		});
	 		$(".free").on("click",function(){
	 			document.getElementById("warna").className = "ui label header orange";
	 			$('#warna').html('<i class="alarm icon"></i>FREE TRIAL');
	 			$('#header').text($('#free').text());
	 			$('#des').text($('#desfree').text());
	 			$('.small.modal')
	 			  .modal('show')
	 			;
	 			//alert('Free');
	 		});
	 		$(".basik").on("click",function(){
	 			document.getElementById("warna").className = 'ui label header red';
	 			$('#warna').html('<i class="alarm icon"></i>PAKET I BASIC');
	 			$('#header').text($('#harga1').text());
	 			$('#des').text($('#des1').text());
	 			//$('#isi').text('Apakah anda yakin memilih paket ini');
	 		
	 			$('.small.modal')
	 			  .modal('show')
	 			;
	 			
	 			//alert('Basic');
	 		});
	 		$(".startup").on("click",function(){
	 			document.getElementById("warna").className = 'ui label header blue';
	 			$('#warna').html('<i class="alarm icon"></i>PAKET II STARTUP');
	 			$('#header').text($('#harga2').text());
	 			$('#des').text($('#des2').text());
	 		//	$('#isi').text('Apakah anda yakin memilih paket ini');
	 			$('.small.modal')
	 			  .modal('show')
	 			;
	 			//alert('Start up');
	 		});
	 		$(".enterprise").on("click",function(){
	 			document.getElementById("warna").className = 'ui label header violet';
	 			$('#warna').html('<i class="alarm icon"></i>PAKET III ENTERPRICE');
	 			$('#header').text($('#harga3').text());
	 			$('#des').text($('#des3').text());
	 		//	$('#isi').text('Apakah anda yakin memilih paket ini');
	 			$('.small.modal')
	 			  .modal('show')
	 			;
	 			//alert('Enterprise');
	 		});
	 		
	 		$('#buy').on('click',function(){
				 window.location="/belipaket";
	 		});
	 		
	 	});
		</script>
 	</jsp:body>
 	
 </t:app>
 
 