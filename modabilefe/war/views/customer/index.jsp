<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Pelanggan</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<br>
			<div class="row">
				<h2 class="ui dividing header">Pelanggan</h2><br/>
				<div class="ui form">
					<div class="fields">
					<div class="inline twelve wide field">
					</div>
						<c:if test="!${isADMIN}">
					<div class="six wide field">
					<a href="reservation" class="ui brown  right floated button"><i class="add square icon"></i> Tambah</a>
					</div>
						</c:if>
				</div>
				<table class="ui celled padded table">
					<thead>
						<tr>
							<th>Nama Pelanggan</th>
							<th>Email</th>
							<th>Produk</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					    <c:forEach items="${customers}" var="cust" varStatus="nmr">
							<tr>
							    <td class="single line">
                                    ${cust.customer_name}
                                </td>
                                <td class="single line">
                                    ${cust.customer_email}
                                </td>
                                <td>
                                    <h2 class="ui center aligned header">${cust.productName}</h2>
                                </td>
                                <td class="center aligned single line">
                                    <div order="${cust.orderDetailId}" class="ui vertical animated button orange" tabindex="0">
                                        <div class="hidden content">Kapal</div>
                                        <div class="visible content">
                                        <i class="ship icon"></i>
                                        </div>
                                    </div>
                                    <a href="/map" target="_blank" order="${cust.customer_email}" class="ui vertical animated button green petabutton" tabindex="0">
                                        <div class="hidden content">Peta</div>
                                        <div class="visible content">
                                        <i class="marker icon"></i>
                                        </div>
                                    </a>
                                    <a href="/pemesananproduk/${cust.customer_id}" target="_blank" class="ui button purple" tabindex="0">
                                        Histori Pemesanan Produk
                                    </a>
                                    <a href="/customermember/${cust.customer_id}" target="_blank" class="ui button blue" tabindex="0">
                                        Add new member 
                                    </a>
                                </td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<br>
			</div>
		</div>
	</div>
	
	<div class="ui large modal">
        <i class="close icon"></i>
        <div class="ui label header blue"><i class="ship icon"></i> List Kapal</div>
        <div class="content">
            <div class="description" style="width:100%;">
                <table class="ui celled padded table">
                    <thead>
                        <tr>
                            <th class="center aligned" style="width:60px;">No.</th>
                            <th>MMSI</th>
                            <th>Nama Kapal</th>
                            <th>IMO</th>
                            <th>Call Sign</th>
                        </tr>
                    </thead>
                    <tbody id="list">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
	<script>
    $(document).ready(function(){
        $(".button.orange").click(function(){
            $.ajax({
                type: "POST",
                url: '/history',
                data: {
                    'orddtl_id' : $(this).attr("order")
                },
                success: function (data) {
                    $("#list").html(data);
                    $('.large.modal')
                      .modal('show')
                    ;
                }
            });
        });
    });
    </script>
 	</jsp:body>
 
 </t:app>
 
 