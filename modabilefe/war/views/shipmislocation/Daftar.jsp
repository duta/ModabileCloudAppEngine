<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">Daftar Kapal Salah Lokasi</jsp:attribute>
	<jsp:attribute name="header">
    <link rel="stylesheet" href="/resource/dist/asset/kendo.common-material.min.css" />
    <link rel="stylesheet" href="/resource/dist/asset/kendo.material.min.css" />
    <script src="/resource/dist/asset/kendo.all.min.js"></script>
	</jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Daftar Kapal Mislokasi</h2>
					<a href="/shipMislocation/set"> Set Kapal Mislokasi </a><br /><br />
            		<div id="resultDaftarMislokasi">
            			<table id="resultTable" class="ui padded  striped  blue celled table">
            				<thead>
								<tr>
									<td>MMSI</td>
									<td>Nama Kapal</td>
									<td>Timestamp</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${filtered}" var="shipdata">
									${shipdata}
								</c:forEach>
							</tbody>
            			</table>
            			<form id=mainForm action="/shipMislocation/daftar" method="POST">
                        <input type="hidden" name="MMSISet" id="MMSISet"/>
                        <input type="hidden" name="TimeStampSet" id="TimeStampSet"/>
            			</form>
					</div>
            	</div>
            </div>
    	</div>
     	<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
    	<script>
    	$(document).ready(function(){
    	    $("#resultTable").DataTable( {
     		    "bFilter": false
     	    });
    	});

        function DeleteMisLokasi(mmsi, timestamp) {
    		$('#MMSISet').val(mmsi)
    		$('#TimeStampSet').val(timestamp)
    		$('#mainForm').submit();
        }
    	</script>
 	</jsp:body>
</t:app>