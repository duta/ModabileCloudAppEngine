<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">Set Kapal Salah Lokasi</jsp:attribute>
	<jsp:attribute name="header">
    <link rel="stylesheet" href="/resource/dist/asset/kendo.common-material.min.css" />
    <link rel="stylesheet" href="/resource/dist/asset/kendo.material.min.css" />
    <script src="/resource/dist/asset/kendo.all.min.js"></script>
	</jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Set Kapal Mislokasi</h2>
					<a href="/shipMislocation/daftar"> Daftar Kapal Mislokasi </a><br /><br />
        		<form id=mainForm class="ui form row" action="/shipMislocation/set" method="POST">
        			<div class="twelve wide column">
        				<div class="fields">
							<div class="sixteen wide field">
                        <label for="MMSIKapals" style="font-size: medium">
                            MMSI</label>
                        <select ID="MMSIKapals">
							<c:forEach items="${mmsis}" var="mmsi">
	          					<option value="${mmsi.mmsi}">${mmsi.mmsi} - ${mmsi.vesselname}</option>						
							</c:forEach>
                        </select>
                        <input type="hidden" name="MMSIKapal" id="MMSIKapal"/>
					    	</div>		      
						</div>
						<div class="fields">
							<div class="sixteen wide field">
                        <label for="From" style="font-size: medium">
                            From</label>
                        <input type="text" name="From" id="From"/>
					    	</div>		      
						</div>
						<div class="fields">
							<div class="sixteen wide field">
                        <label for="To" style="font-size: medium">
                            To</label>
                        <input type="text" name="To" id="To" />
					    	</div>		      
						</div>
						<div class="fields">
							<div class="sixteen wide field">
                        		<input type="submit" name="reqDisplay" id="reqDisplay" value="Tampilkan" />
					    	</div>		      
						</div>
						</div>
                        <input type="hidden" name="MMSISet" id="MMSISet"/>
                        <input type="hidden" name="TimeStampSet" id="TimeStampSet"/>
                        <input type="hidden" name="ShipNameSet" id="ShipNameSet"/>
						</form>
            		<div id="resultDisplay">
            			<table id="resultTable" class="ui padded  striped  blue celled table">
            				<thead>
							<tr>
								<td>Timestamp</td>
								<td></td>
							</tr>
							</thead>
							<tbody>
							<c:forEach items="${filtered}" var="shipdata">
								${shipdata}
							</c:forEach>
							</tbody>
            			</table>
					</div>
            	</div>
            </div>
    	</div>

     	<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>
    	<script>
    	$(document).ready(function(){

    	    $("#resultTable").DataTable( {
     		    "bFilter": false
     	    });
    	    
            $("#From").kendoDateTimePicker({
                value: ('${From}'==''?new Date():'${From}'),
                format: "yyyy-MM-dd HH:mm"
            });

            $("#To").kendoDateTimePicker({
                value: ('${To}'==''?new Date():'${To}'),
                format: "yyyy-MM-dd HH:mm"
            });
            $('#MMSIKapals').change(function(){
            	$('#MMSIKapal').val($('#MMSIKapals').val())
            })
            $('#MMSIKapals').val(${MMSIKapal})
        	$('#MMSIKapal').val($('#MMSIKapals').val())
    	});
    	
    	function SetMislokasi(mmsi, timestamp, vesselname) {
    		$('#MMSISet').val(mmsi)
    		$('#TimeStampSet').val(timestamp)
    		$('#ShipNameSet').val(vesselname)
    		$('#mainForm').submit();
        }
    	</script>
 	</jsp:body>
</t:app>
