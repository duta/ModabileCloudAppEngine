<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Modabile</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	
 	<div class="ui vertical stripe segment">
	    <div class="ui middle aligned stackable grid container">
	      <div class="row">
	          <h3 class="ui header">Deliver your vessel track and history</h3>
	      </div>
	      <div class="row">
	        	<div class="center aligned column">
	      			<h3>Getting Started</h3>
	        	</div>
	      </div>
	      	<div class="row">
	        	<div class="center aligned column">
	        		<c:choose>
						<c:when test="${user != null}">
	          				<span class="ui huge basic label" href="/login">1. Login</span>
						</c:when>
						<c:otherwise>
	          				<a class="ui huge green button" href="/login">1. Login</a>
						</c:otherwise>
	        		</c:choose>
	        	</div>
	      	</div>
	      	<div class="row">
	        	<div class="center aligned column">
	        		<c:choose>
						<c:when test="${user == null}">
	          				<span class="ui huge basic label">2. Pesan kapal</span>
						</c:when>
						<c:otherwise>
	          				<a class="ui huge blue button" href="/reservation">2. Pesan kapal</a>
						</c:otherwise>
	        		</c:choose>
	        	</div>
	      	</div>
	      	<div class="row">
	        	<div class="center aligned column">
	          		<span class="ui huge basic label">3. Tunggu email konfirmasi kapal yang MMSI-nya tersedia</span>
	        	</div>
	      	</div>
	      	<div class="row">
	        	<div class="center aligned column">
	        		<c:choose>
						<c:when test="${user == null}">
	          				<span class="ui huge basic label">4. Buka peta kapal yang available</span>
						</c:when>
						<c:otherwise>
	          				<a class="ui huge red button" href="/map">4. Buka peta kapal yang available</a>
						</c:otherwise>
	        		</c:choose>
	        	</div>
	      	</div>
	    </div>
	  </div>
	
	
	  <!-- div class="ui vertical stripe quote segment">
	    <div class="ui equal width stackable internally celled grid">
	      <div class="center aligned row">
	        <div class="column">
	          <p>name server is <b>"${nameserver}"</b></p>
	        </div>
	        <div class="column">
	          <p>
	            <img src="asset/img/nan.jpg" class="ui avatar image"> <b>${useremail}</b> Chief Fun Officer Acme Toys
	          </p>
	        </div>
	      </div>
	    </div>
	  </div -->
	 
	    
 	</jsp:body>
 
 </t:app>
 
 