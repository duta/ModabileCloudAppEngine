<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
    <jsp:attribute name="title">Daftar Pemesanan Pelanggan</jsp:attribute>
    <jsp:attribute name="header"></jsp:attribute>
    <jsp:body>
    
    <div class="ui vertical segment">
        <div class="ui middle aligned stackable  container">
            <br>
            <div class="row">
                <h2 class="ui dividing header">Daftar Pemesanan Pelanggan</h2><br/>
                <table class="ui celled padded table">
                    <thead>
                        <tr>
                            <th>Pelanggan</th>
                            <th>Produk</th>
                            <th>Jangka Waktu</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${orders}" var="order">
                            <tr>
                                <td class="single line">${order.customer_company}</td>
                                <td><h2 class="ui center aligned header">${order.product_name}</h2></td>
                                <td class="single line">${order.formatted_startdate} - ${order.formatted_enddate}</td>
                                <td class="center aligned single line">
                                    <div order="${order.orddtl_id}" class="ui vertical animated button orange" tabindex="0">
                                        <div class="hidden content">Edit</div>
                                        <div class="visible content">
                                            <i class="ship icon"></i>
                                        </div>
                                    </div>
                                    <div order="${order.orddtl_id}" class="ui vertical animated button green show" tabindex="0">
                                        <div class="hidden content">Kapal</div>
                                        <div class="visible content">
                                            <i class="ship icon"></i>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>

    <div class="ui large modal data">
        <i class="close icon"></i>
        <div class="ui label header blue"><i class="ship icon"></i> Edit Pemesanan</div>
        <div class="content">
            <div class="description" style="width:100%;">
                <form class="ui form row" action="adminOrder" method="POST" onsubmit="return validateFormDetail();">
                    <div class="ui vertical stripe segment">
                        <div class="ui middle aligned stackable grid container">
	                        <div class="row">
				                <div class="two wide column ">
				                </div>
				                <div class="twelve wide column">
				                    <div class="ui form">
				                        <input type="hidden" id="orderDetailId" name="orderDetailId">
				                        <div class="required field">
				                            <label>Jumlah</label>
				                            <input type="text" id="quantity" name="quantity" placeholder="Jumlah">
				                        </div>
				                        <div class="required field">
                                            <label>Harga</label>
                                            <input type="text" id="price" name="price" placeholder="Harga">
                                        </div>
                                        <div class="required field">
                                            <label>Mata Uang</label>
                                            <input type="text" id="currency" name="currency" placeholder="Mata Uang">
                                        </div>
                                        <div class="field">
                                            <label>Diskon</label>
                                            <input type="text" id="discount" name="discount" placeholder="Diskon">
                                        </div>
                                        <div class="required field">
                                            <label>Tanggal Mulai</label>
                                            <input type="text" id="startDate" name="startDate" class="date" placeholder="Tanggal Mulai">
                                        </div>
                                        <div class="required field">
                                            <label>Tanggal Selesai</label>
                                            <input type="text" id="endDate" name="endDate" class="date" placeholder="Tanggal Selesai">
                                        </div>
				                    </div>
				                </div>
				            </div>
				            <div class="row">
			                    <div class="eight wide column "></div>
			                    <div class="six wide right aligned column ">
			                        <button type="submit" class="ui green button">Simpan</button>
			                    </div>
			                    <div class="two wide column "></div>
			                </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="ui large modal ship">
        <i class="close icon"></i>
        <div class="ui label header blue"><i class="ship icon"></i> Edit Daftar Kapal</div>
        <div class="content">
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      	<!-- <label>Nama Perusahaan : </label>
			        <input type="text" class="six wide field" id="NamaPerusahaan" placeholder="Nama Perusahaan"> -->
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="plus icon"></i> Tambah Kapal </button> 
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>No.</th>
	                <th>MMSI</th>
	                <th>Nama Kapal</th>
	                <th class="center aligned column">Action</th>
	            </tr>
	        </thead>
            <tbody>
            </tbody>
		  </table>
        </div><br/><br/>
	  <div class="ui vertical segment">
	    <div class="ui middle aligned stackable grid  container">
	      <div class="row">
	        <div class="center aligned column">
	          <button class="ui green huge button" id="PesanKapal"><i class="cart icon"></i>Update Data Kapal</button>
	        </div>
	      </div>
	    </div><br/><br/>
	  </div>
    </div>

	  <div class="ui small modal addKapal">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    Form Tambah Kapal
	  </div>
	  <div class="ui form content">
		 <div class="fields">
			<div class="eight wide required field">
				<label>MMSI</label>
		        <input type="text" id="MMSI" placeholder="MMSI">
		    </div>
			<div class="eight wide required field">
			   	<label>Nama Kapal</label>
			    <input type="text" id="NamaKapal" placeholder="Nama Kapal">
			</div>	      
	  	</div>
	  </div> 
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow"> 
	      Simpan
	      <i class="save icon"></i>
	    </button>
	     <button class="ui yellow reset button">
	      Reset
	      <i class="refresh icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>
	
  		<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
		<script src="/resource/dist/asset/jquery-ui.min.js"></script>
     	<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/dataTables.semanticui.min.js"></script>

  <link rel="stylesheet" href="/resource/dist/asset/kendo.common-material.min.css" />
  <link rel="stylesheet" href="/resource/dist/asset/kendo.material.min.css" />

  <!--Start Date Time Mobile-->
  <script src="/resource/dist/asset/kendo.all.min.js"></script>
  <!--End Date Time Mobile-->
    <script>
	function validateFormDetail(){
		var failed = false;
		failed = failed || $('#quantity').val() == undefined;
		failed = failed || $('#quantity').val() == "";
		failed = failed || $('#quantity').val() == "0";
		failed = failed || $('#price').val() == undefined;
		failed = failed || $('#price').val() == "";
		failed = failed || $('#price').val() == "0";
		failed = failed || $('#currency').val() == undefined;
		failed = failed || $('#currency').val() == "";
		failed = failed || $('#startDate').val() == undefined;
		failed = failed || $('#startDate').val() == "";
		failed = failed || $('#endDate').val() == undefined;
		failed = failed || $('#endDate').val() == "";
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		if (new Date($('#startDate').val()).getTime()>new Date($('#endDate').val()).getTime()){
			alert("Tanggal Selesai tidak boleh mendahului Tanggal Mulai");
			failed = true;
		}
		return !failed;
	}
	function validateForm(){
		var failed = false;
		failed = failed || $('#MMSI').val() == undefined;
		failed = failed || $('#MMSI').val() == "";
		failed = failed || $('#NamaKapal').val() == undefined;
		failed = failed || $('#NamaKapal').val() == "";
		if (failed){
			alert("Field dengan tanda bintang (*) harus diisi.");
		}
		return !failed;
	}
    $(document).ready(function(){
        $( ".date" ).kendoDatePicker({
        	format: 'dd MMMM yyyy' ,
        });
	    var t = $("#example").DataTable( {
 	        "columnDefs": [ {
 	            "searchable": false,
 	            "orderable": false,
 	            "targets": 0
 	        } ],
 	        "order": [[ 1, 'asc' ]]
 	    });
 		 t.on( 'order.dt search.dt', function () {
 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
 	            cell.innerHTML = i+1;
 	        } );
 	    } ).draw();
        $(".button.orange").click(function(){
        	var orderDetailId = $(this).attr("order");
            $.ajax({
                type: "GET",
                url: '/adminOrder?action=detail&id=' + orderDetailId,
                data: {
                },
                success: function (data) {
                	var obj = JSON.parse(JSON.stringify(data));

                	$('#orderDetailId').val(obj['orddtl_id']);
                	$('#quantity').val(obj['orddtl_quantity']);
                	$('#price').val(obj['orddtl_unitprice']);
                	$('#currency').val(obj['orddtl_currency']);
                	$('#discount').val(obj['orddtl_discount']);
                	$('#startDate').val(obj['orddtl_contract_startdate']);
                	$('#endDate').val(obj['orddtl_contract_enddate']);
                	
                    $('.large.modal.data').modal('show');
                }
            });
        });

        $(".button.show").click(function(){
            var orderDetailId = $(this).attr("order");
            $.ajax({
                type: "GET",
                url: '/adminOrder?action=ship&id=' + orderDetailId,
                data: {
                },
                success: function (data) {
                    var obj = JSON.parse(JSON.stringify(data));
                    t.clear();
        		    for (var i = 0; i < obj.length; i++) {
    	 	        t.row.add( [
    	 		 	            '',
    	 		 	          obj[i].Ship_mmsi,
    	 		 	        obj[i].Ship_name,
    	 		 	           	'<div class="center aligned column"><div class="ui buttons"><button class="ui red button delete"><i class="trash icon"></i>Hapus</button></div></div>',
    	 		 	        ] ).draw( false );
        		    }
                    $('.large.modal.ship').modal('show');
                }
            });
        });
	 	$(".reset").on("click",function(){
		 		  $("#MMSI").val('');
		          $("#NamaKapal").val('');
		});
 		$("#add").on("click",function(){
 			$('.ui.small.modal.addKapal')
	 		  .modal('show')
	 		;
 		});

 	    $("#addrow").on("click", function () {
 	    	if (!validateForm()){
            $('.large.modal.ship').modal('show');
 	        t.row.add( [
 	            '',
 	            $("#MMSI").val(),
 	            $("#NamaKapal").val(),
 	            '<div class="center aligned column"><div class="ui buttons"><button class="ui red button delete"><i class="trash icon"></i>Hapus</button></div></div>',
 	        ]).draw( false );
 	    	}
 	    } );
	 	$('#example tbody').on( 'click', 'tr > td > div > div > button.delete', function () {
	 	    //alert($(this).parent().parent().parent().parent().index());
	 	   	$("#example").DataTable().row($(this).parent().parent().parent().parent().index()).remove().draw();
	 	} );
 	    $("#PesanKapal").on("click",function(){
 	    	//Add Localstorage
 	    	var data = t.rows().data();
 	    	var dataKapal = [];
			for (var i = 0; i < data.length; i++) {
		 	    dataKapal.push({
			            "MMSI": data[i][1],
			            "NamaKapal": data[i][2]
			    });
		 	}
			if (dataKapal.length == 0){
				alert("Daftar kapal tidak boleh kosong");
				return;
			}
            var orderDetailId = $(".button.show").attr("order");
            $.ajax({
                type: "POST",
                url: '/adminOrder',
                data: {
                	"action":"ship",
                	"id":orderDetailId,
                	"dataKapal":JSON.stringify(dataKapal)
                },
                success: function (data) {
                    $('.large.modal.ship').modal('hide');
                }
            });
	 	});
    });
    </script>
    </jsp:body>
 </t:app>