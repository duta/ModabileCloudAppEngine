﻿<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Modabile ${company}</title>
    <link href="/resource/dist/Scripts/jquery-tab/vanilla-tab-style.css" rel="stylesheet" type="text/css" />
    <link href="/resource/dist/theme/fullscreen/map-fullscreen.css" rel="stylesheet" type="text/css" />

  <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="/asset/img/fav-icon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/asset/img/fav-icon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/asset/img/fav-icon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/asset/img/fav-icon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/asset/img/fav-icon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/asset/img/fav-icon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/asset/img/fav-icon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/asset/img/fav-icon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/asset/img/fav-icon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/asset/img/fav-icon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/asset/img/fav-icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/asset/img/fav-icon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/asset/img/fav-icon/favicon-16x16.png">
        
    <link rel="stylesheet" href="/resource/dist/Styles/table-detail.css" />
    <%--<link rel="stylesheet" type="text/css" href="css/ui-darkness/jquery-ui-1.8.21.custom.css" />--%>
    <link rel="stylesheet" type="text/css" href="/resource/dist/asset/jquery-ui-1.10.2.custom.min.css" />
    <link href="/resource/dist/Styles/hover-menu.css" rel="stylesheet" type="text/css" />
    <link href="/resource/dist/Styles/gaya.css" rel="stylesheet" type="text/css" />
    <link href="/resource/dist/Styles/google.css" rel="stylesheet" type="text/css" />
    <link href="/resource/dist/Scripts/theme/default/style-v2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/resource/dist/font-awesome-4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="/resource/dist/asset/datetimepicker/DateTimePicker.css" />
    <link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">

    <link rel="stylesheet" href="/resource/dist/asset/kendo.common-material.min.css" />
    <link rel="stylesheet" href="/resource/dist/asset/kendo.material.min.css" />

    <%-- <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>--%>
    <%--   <script src="Scripts/jquery-1.7.2.min.js" type="text/javascript"></script>--%>

    <script src="/resource/dist/asset/jquery-1.11.1.min.js"></script>

    <!--Start Date Time Mobile-->
    <script src="/resource/dist/asset/kendo.all.min.js"></script>
    <!--End Date Time Mobile-->

    <script src="/resource/dist/asset/jquery-ui.min.js" type="text/javascript"></script>
    <!--<script src="Scripts/jquery-ui-1.8.21.custom.min.js" type="text/javascript"></script>-->
    <script type="text/javascript" src="/resource/dist/asset/jquery.ui.touch-punch.min.js"></script>
    <script src="/resource/dist/asset/jquery-migrate-1.1.1.min.js"></script>
    <!--<script src="Scripts/jquery-tab/jquery-ui-personalized-1.5.2.packed.js" type="text/javascript"></script>-->
    <script src="/resource/dist/Scripts/jquery-tab/sprinkle.js" type="text/javascript"></script>
    <script src="/resource/dist/Scripts/jquery.timer.min.js" type="text/javascript"></script>
    <script src="/resource/dist/asset/jquery.cookie.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.css" />
    <script src="/resource/dist/asset/semantic/semantic.js"></script>
    <script src="/resource/dist/asset/semantic/semantic.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/resource/dist/asset/table-autocomplete/tautocomplete.css" />
    <script src="/resource/dist/asset/table-autocomplete/tautocomplete.js"></script>

    <script type="text/javascript" src="/resource/dist/asset/jquery.slimscroll.js"></script>

    <script type="text/javascript" src="/resource/dist/asset/OpenLayers.js"></script>

    <link rel="stylesheet" type="text/css" href="/resource/dist/asset/Tokenize-master/jquery.tokenize.css" />
    <script type="text/javascript" src="asset/Tokenize-master/jquery.tokenize.js"></script>

    <style>
        .detailView {
            background: #FFF;
            padding-right: .25em;
            position: absolute;
            left: -40em;
            top: 0;
            width: 350px;
            box-sizing: border-box;
            z-index: 20;
            height: 100%;
            padding-left: 3px;
            -webkit-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
            box-shadow: 1px 1px 6px rgba(0, 0, 0, 0.3);
            filter: alpha(opacity=40); /* For IE8 and earlier */
        }

            .detailView.active {
                left: 0;
            }

        .detailTools {
            background: #FFF;
            padding-right: .25em;
            position: absolute;
            display: none;
            left: 10px;
            bottom: 52px;
            width: 310px;
            box-sizing: border-box;
            z-index: 10;
            padding-left: 3px;
            box-shadow: 1px 1px 6px rgba(0, 0, 0, 0.3);
            border-radius: 4px;
        }

            .detailTools.active {
                display: block;
            }

        .viewSignout {
            background: #FFF;
            padding-right: .25em;
            position: absolute;
            display: none;
            right: 10px;
            top: 63px;
            width: 248px;
            box-sizing: border-box;
            z-index: 10;
            height: 93px;
            padding-left: 3px;
            box-shadow: 1px 1px 6px rgba(0, 0, 0, 0.3);
            border-radius: 2px;
        }

            .viewSignout.active {
                display: block;
            }

        hr {
            border: 0;
            height: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
        }

        #sliderInterval .ui-slider-range {
            background: #729fcf;
        }

        #sliderInterval .ui-slider-handle {
            border-color: #729fcf;
        }

        #sliderInterval .ui-widget-content {
            background: #FFF;
        }

        #sliderInterval {
            float: left;
            clear: left;
            width: 90%;
            margin: 15px;
        }

        #labelSlider {
            text-align: center;
            font-weight: bold;
            margin-bottom: -10px;
        }

        .header {
            font-size: 15px;
        }

        .headerSub {
            color: #424242;
            font-size: 12px;
        }

        .item-detail {
            margin-bottom: 8px;
        }

        .k-select {
            background-color: #FAFAFA;
            border: 1px solid #EBEBEB;
        }

        .triangle {
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            margin-top: -7px;
            border-bottom: 15px solid white;
            margin-left: 88%;
        }
    </style>
</head>
<body>
    <div class="detailView">
        <div id="viewKapal">
            <div class="ui form btnMenu" style="margin-top: 0px;">
                <div style="width: 100%; display: block; position: initial;">
                    <div style="width: 90%; float: left;">
                        <a class="ui big label" style="background: #FFF;" href="/mapRecorded">Peta Recorded
                        </a>
                    </div>
                    <div style="width: 90%; float: left;">
                        <a class="ui big label" style="background: #FFF;"><span class="span-ship-count" id="ssc"></span>Kapal Terdeteksi
                        </a>
                    </div>
                    <div style="margin-top: 3px; text-align: right; color: #896666; width: 10%; float: left;">
                        <i class="fa fa-caret-left  fa-2x"></i>
                    </div>
                </div>
            </div>
            <hr style="width: 100%; float: left;">
            <div class="ui form" style="float: left; margin-top: 5px; display: none;">
                <div class="two fields">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>Kargo (<span class="span-ship-count" id="Span1"></span>)</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>Penumpang (<span class="span-ship-count" id="Span2"></span>)</label>
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>SAR (<span class="span-ship-count" id="Span3"></span>)</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>Pesiar (<span class="span-ship-count" id="Span4"></span>)</label>
                        </div>
                    </div>
                </div>
                <div class="two fields">
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>Sedang Dipandu (<span class="span-ship-count" id="Span5"></span>)</label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="ui checkbox">
                            <input type="checkbox">
                            <label>Sedang Ditunda (<span class="span-ship-count" id="Span6"></span>)</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui compact menu" style="width: 100%; border: 0px; min-height: 0px;">
            </div>
            <div class="ui" id="contenty" style="bottom: 0;width: 100%;">
                <div id="testDiv4">
                    <div class="ui list" id="ship-table">
                    </div>
                </div>
            </div>
        </div>
        <div id="viewDetailKapal">
            <div class="ui form btnMenu" style="margin-top: 0px;">
                <div style="width: 100%; display: block; position: initial;">
                    <div style="width: 90%; float: left;">
                        <a class="ui big label" style="background: #FFF;" id="detail-shipname"></a>
                    </div>
                    <div style="margin-top: 3px; text-align: right; color: #896666; width: 10%; float: left;">
                        <i class="fa fa-caret-left  fa-2x"></i>
                    </div>
                </div>
            </div>
            <hr style="width: 100%; float: left;">
            <div class="imageShip ui large" style="margin-top: 5px; color: #896666; display:inline-block; background-size: contain; background-image: url(Styles/kapal.jpg)">
            </div>
            <div class="ui compact menu" style="width: 100%;">
                <a class="item" id="detail-next" style="width: 50%;">
                    <i class="list icon"></i>
                    Next
                </a>
                <a class="item" id="detail-prev" style="width: 50%;">
                    <i class="marker icon"></i>
                    Prev
                </a>
            </div>
            <div class="content" id="contentz" style="bottom: 0;">
                <div id="testDiv3">
                    <div class="ui list">
                        <div class="ui form">
                            <div class="two fields" style="width: 100% !important;">
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">MMSI</div>
                                        <div class="header" style="font-weight: 700;" id="detail-mmsi"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Callsign</div>
                                        <div class="header" style="font-weight: 700;" id="detail-callsign"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">LOA</div>
                                        <div class="header" style="font-weight: 700;" id="detail-loa"></div>
                                    </div>
                                </div>
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">Wide</div>
                                        <div class="header" style="font-weight: 700;" id="detail-wide"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">GRT (SIUK)</div>
                                        <div class="header" style="font-weight: 700;" id="detail-grt"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Destination</div>
                                        <div class="header" style="font-weight: 700;" id="detail-destination"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item item-detail">
                            <div class="headerSub">Type</div>
                            <div class="header" id="detail-type"></div>
                        </div>
                        <div class="ui form">
                            <div class="two fields" style="width: 100% !important;">
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">SOG</div>
                                        <div class="header" style="font-weight: 700;" id="detail-sog"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">COG</div>
                                        <div class="header" style="font-weight: 700;" id="detail-cog"></div>
                                    </div>
                                </div>
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">Heading</div>
                                        <div class="header" style="font-weight: 700;" id="detail-heading"></div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Draught</div>
                                        <div class="header" style="font-weight: 700;" id="detail-draught"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item item-detail">
                            <div class="headerSub"><i class="info icon"></i>Nav Status</div>
                            <div class="header" style="font-weight: 700;" id="detail-nav-status"></div>
                        </div>
                        <div class="item item-detail">
                            <div class="headerSub"><i class="marker icon"></i>Posisi</div>
                            <div class="header" style="font-weight: 700;" id="detail-posisi"></div>
                        </div>
                        <div class="item item-detail">
                            <div class="headerSub"><i class="wait icon"></i>TimeStamp</div>
                            <div class="header" style="font-weight: 700;" id="detail-timestamp"></div>
                        </div>
                        <div class="ui form">
                            <div class="two fields" style="width: 100% !important;">
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">Temperature</div>
                                        <div class="header" style="font-weight: 700;" id="detail-temp-air">29.2 °C</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Wind</div>
                                        <div class="header" style="font-weight: 700;" id="detail-wind">8.2 m/s  WSW (256 °)</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Waves</div>
                                        <div class="header" style="font-weight: 700;" id="detail-waves">0.8 m  W (279 °)</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Relative humidity</div>
                                        <div class="header" style="font-weight: 700;" id="detail-rel-humidity">71 %</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Current</div>
                                        <div class="header" style="font-weight: 700;" id="detail-current">0.3 m/s  WNW (291 °)</div>
                                    </div>
                                </div>
                                <div class="field" style="width: 50% !important;">
                                    <div class="item item-detail">
                                        <div class="headerSub">Sea Water Temperature</div>
                                        <div class="header" style="font-weight: 700;" id="detail-temp-sea-water">29.5 °C</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Surface Wind Gust</div>
                                        <div class="header" style="font-weight: 700;" id="detail-surface-wind-gust">9.4 m/s</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Wave Period</div>
                                        <div class="header" style="font-weight: 700;" id="detail-wave-period">4.3 s</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Mean sea level pressure</div>
                                        <div class="header" style="font-weight: 700;" id="detail-mean-sea-lvl-pressure">1007.75 hPa</div>
                                    </div>
                                    <div class="item item-detail">
                                        <div class="headerSub">Total cloud cover</div>
                                        <div class="header" style="font-weight: 700;" id="detail-total-cloud-cover">0 %</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="viewMenu">
            <div class="ui form btnMenu" style="margin-top: 0px;">
                <div style="width: 100%; display: block; position: initial;">
                    <div style="width: 90%; float: left;">
                        <a class="ui big label" style="background: #FFF;">Menu
                        </a>
                    </div>
                    <div style="margin-top: 3px; text-align: right; color: #896666; width: 10%; float: left;">
                        <i class="fa fa-caret-left  fa-2x"></i>
                    </div>
                </div>
            </div>
            <hr style="width: 100%; float: left;">
            <div class="ui list" style="font-size: 14px; margin-left: 17px; margin-top: 3px; float: left;">
                <div class="item">
                    <i class="film  icon"></i>
                    <div class="content">
                        Recorded
                    </div>
                </div>
                <div class="item">
                    <i class="film  icon"></i>
                    <div class="content">
                        Recorded CCTV
                    </div>
                </div>
                <div class="item">
                    <i class="newspaper  icon"></i>
                    <div class="content">
                        Daftar Kapal Diawasi
                    </div>
                </div>
                <div class="item">
                    <i class="road icon"></i>
                    <div class="content">
                        Penghitungan Jarak KT
                    </div>
                </div>
            </div>
        </div>
        <div id="viewRecorded">
            <div class="ui form btnMenu" style="margin-top: 0px;">
                <div style="width: 100%; display: block; position: initial;">
                    <div style="width: 90%; float: left;">
                        <a class="ui big label" style="background: #FFF;">Filter
                        </a>
                    </div>
                    <div style="margin-top: 3px; text-align: right; color: #896666; width: 10%; float: left;">
                        <i class="fa fa-caret-left  fa-2x"></i>
                    </div>
                </div>
            </div>
            <hr style="width: 100%; float: left;">
			
        </div>
    </div>
    <div class="detailTools">
        <div class="ui form">
            <div class="grouped fields">
                <div class="field toolsButton" id="tools_hand">
                    <div class="ui">
                        <a class="circular mini ui icon button purple" id="btn_tools_hand"><i class="fa fa-hand-paper-o"></i></a>
                        <label>Hand</label>
                    </div>
                </div>
                <hr />
                <div class="field toolsButton" id="tools_circle">
                    <div class="ui">
                        <a class="circular mini ui icon button" id="btn_tools_circle"><i class="cloud icon"></i></a>
                        <label>Measure Radius</label>
                    </div>
                </div>
                <div class="field toolsButton" id="tools_line">
                    <div class="ui">
                        <a class="circular mini ui icon button" id="btn_tools_line"><i class="exchange icon"></i></a>
                        <label>Measure Line</label>
                    </div>
                </div>
                <hr />
                <div class="field" id="tools_filter">
                    <select id="tmsselector">
                    </select>
                </div>
                <div class="field" id="tools_osm" onclick="toggleosm()">
                    <div class="ui">
                        <a class="circular mini ui icon button" id="btn_tools_osm"><i class="repeat icon"></i></a>
                        <label>Map Quest Background</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="Form1" runat="server">
        <div class="viewSignout">
            <div class="ui form">
                <div class="triangle"></div>
                <div class="grouped fields">
                    <div class="field toolsButton" id="Div1">
                        <div class="ui">
                            <i class="circular inverted violet user tiny icon"></i>
                            <label>
                                <asp:Label ID="lblUser" runat="server" Font-Size="Large">${user.email}</asp:Label></label>
                        </div>
                    </div>
                    <hr />
                    <div class="field toolsButton" id="Div2" style="text-align: right;">
                        <div class="ui">
                            <a class="ui icon button" id="A2" href="${logoutUrl}">
                                Sign out
                                <i class="sign out icon"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="mapGeoJson" style="width: 100%; height: 100%;">
    </div>
    <div class="headerCustom ">
        <a class="ui label user-label" style="background: #FFF">
            ${company}
        </a>
        <div class="ui inverted icon button btnSignout" style="color: #767676; background: #FFF;">
            <i class="circular inverted violet user tiny icon" style="margin-top: -5px;"></i>
        </div>
    </div>
    <div class="searchKapal">

        <%--<div class="ui inverted icon button btnMenu" style="color:#767676;margin-left: 2px;">
          <i class="fa fa-bars fa-lg"></i>
        </div>--%>
        <div class="ui inverted icon button btnKapal" style="color: #767676; margin-left: 2px;">
            <i class="fa fa-bars fa-lg"></i>
        </div>
        <div class="ui inverted right icon input">
            <input placeholder="MMSI/Nama Kapal/CallSign" type="text" size="27" id="namaKapal" />
            <i class="search icon"></i>
        </div>
    </div>
    <div class="showHide">
        <div class="ui inverted icon button" style="color: #767676; margin-left: 2px;">

            <div class="divShow">
                <i class="fa fa-angle-left fa-2x"></i>
            </div>
            <div class="divHide">
                <i class="fa fa-angle-right fa-2x"></i>
            </div>
        </div>
    </div>
    <div class="tools">
        <div class="ui inverted icon button" style="color: #767676; margin-left: 2px;">
            <i class="fa fa-cogs fa-lg"></i>Tools
        </div>
    </div>

    <script>
    	var token = "<%= (String) request.getAttribute("token") %>";
        $(document).ready(function () {

            //$('#calendarTime-date1').mobiscroll().datetime({
            //    theme: 'wp-light',     // Specify theme like: theme: 'ios' or omit setting to use default
            //    mode: 'scroller',       // Specify scroller mode like: mode: 'mixed' or omit setting to use default
            //    display: 'modal', // Specify display mode like: display: 'bottom' or omit setting to use default
            //    lang: 'en',       // Specify language like: lang: 'pl' or omit setting to use default
            //    closeOnSelect: true,
            //    dayText:'Day',
            //    dateFormat: 'dd/mm/yy',
            //    monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
            //    dayNames :['Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu', 'Minggu'],
            //    timeFormat: 'HH:ii:ss',
            //    timeWheels: 'HHii',
            //    stepMinute: 5  // More info about stepMinute: http://docs.mobiscroll.com/2-14-0/datetime#!opt-stepMinute
            //});

            $("#calendarTime-date2").kendoDateTimePicker({
                value: new Date()
            });

            $("#calendarTime-date1").kendoDateTimePicker({
                value: new Date()
            });

            $('#tokenize').tokenize();

            $(function () {
                $("#sliderInterval").slider({
                    orientation: "horizontal",
                    range: "min",
                    min: 1,
                    max: 30,
                    value: 20,
                    slide: function (event, ui) {
                        $("#valSlider").html(ui.value);
                    }
                });
                $("#sliderInterval").slider("value", 60);
                $("#valSlider").html($("#sliderInterval").slider("value"));
            });
        });

        $(".showHide").click(function () {
            $('.divHide').toggleClass('active');
            $('.divShow').toggleClass('nonActive');
            $('.headerCustom').toggleClass('nonActive');
            $('.searchKapal').toggleClass('nonActive');
            $('.tools').toggleClass('nonActive');
            $('.recorded').toggleClass('nonActive');
        });

        $('.btnMenu').click(function () {
            $("#viewMenu").show();
            $("#viewKapal").hide();
            $("#viewDetailKapal").hide();
            $("#viewRecorded").hide();
            $('.detailView').toggleClass('active');
        });

        $('.btnKapal').click(function () {
            $("#viewKapal").show();
            $("#viewDetailKapal").hide();
            $("#viewMenu").hide();
            $("#viewRecorded").hide();
            $('.detailView').toggleClass('active');

            $('#testDiv4').slimScroll({
                height: ($(window).height() - $('#contenty').position().top) + 'px',
                alwaysVisible: true
            });
        });

        $('.btnViewKapal').click(function () {
            $("#viewKapal").hide();
            $("#viewDetailKapal").show();;
            $("#viewMenu").hide();
            $("#viewRecorded").hide();
            $('.detailView').toggleClass('active');

            $('#testDiv3').slimScroll({
                height: ($(window).height() - $('#contentz').position().top) + 'px',
                alwaysVisible: true
            });
        });

        $('.recorded').click(function () {

            $("#viewRecorded").show();
            $("#viewKapal").hide();
            $("#viewDetailKapal").hide();
            $("#viewMenu").hide();
            $('.detailView').toggleClass('active');
        });

        function showDetail() {
            $('.ui.modal')
                .modal('setting', 'transition', 'vertical flip')
                .modal({
                    inverted: true
                })
                .modal('show');
        }
        $(document).bind("mouseup touchend", function (e) {
            var container = $(".viewSignout");
            var containerbtn = $(".btnSignout");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {

                if ($(window).width() >= 597) {
                    $(containerbtn).click(function () {
                        $('.viewSignout').toggleClass('active');
                        return false;
                    });
                }

                container.removeClass('active');
            }
        });

        $(document).ready(function () {
            text2 = $("#namaKapal").tautocomplete({
                placeholder: "MMSI/Nama Kapal",
                columns: ['MMSI', 'Nama Kapal'],
                data: FindFeaturesShip,
                width: $(".searchKapal").width(),
                onchange: function () {
                    $("#namaKapal").val(text2.all()["MMSI"]);
                    text2.settext('');
                    FindFeatureAndSelect(parseInt(text2.all()["MMSI"]));
                }
            });

            $('.menu .item').tab();

            $('#testDiv3').slimScroll({
                height: ($(window).height() - $('#contentz').position().top) + 'px',
                alwaysVisible: true
            });

            $('#testDiv4').slimScroll({
                height: ($(window).height() - $('#contenty').position().top) + 'px',
                alwaysVisible: true
            });

            $('.tools').click(function () {
                $('.detailTools').toggleClass('active');
            });

            $('.btnSignout').click(function () {
                $('.viewSignout').toggleClass('active');
            });

            $('#scrollListFormFilter').slimScroll({
                height: ($(window).height() - 80) + 'px',
                alwaysVisible: true
            });

            if ($(window).width() <= 767) {
                $('#slimtest1').slimScroll({
                    height: ($(window).height() - 350) + 'px',
                    alwaysVisible: true
                });
            } else {

            }

            $(window).resize(function () {

                if ($(window).width() <= 767) {
                    $('#slimtest1').slimScroll({
                        height: ($(window).height() - 350) + 'px',
                        alwaysVisible: true
                    });
                }

                $('#testDiv3').slimScroll({
                    height: ($(window).height() - $('#contentz').position().top) + 'px',
                    alwaysVisible: true
                });

                $('#testDiv4').slimScroll({
                    height: ($(window).height() - $('#contenty').position().top) + 'px',
                    alwaysVisible: true
                });

                if ($(window).width() <= 596) {
                    $(".searchKapal").css("top", "63px");
                    $(".user-label").css("width", "80%");
                    //$(".olControlLayerSwitcher").css("top","120px");

                    $(".headerCustom").css("left", "10px");
                    $(".headerCustom").css("right", "");
                    $(".headerCustom").removeProp('right');

                    $(".viewSignout").css("left", "10px");
                    $(".viewSignout").css("right", "");
                    $(".viewSignout").removeProp('right');

                    $(".viewSignout").css("width", "305px");
                    $(".triangle").css("margin-left", "84%");
                } else {
                    $(".searchKapal").css("top", "10px");
                    $(".user-label").css("width", "");
                    //$(".olControlLayerSwitcher").css("top","60px");

                    $(".headerCustom").css("left", "");
                    $(".headerCustom").css("right", "10px");
                    $(".headerCustom").removeProp('left');

                    $(".viewSignout").css("right", "10px");
                    $(".viewSignout").css("left", "");
                    $(".viewSignout").removeProp('left');

                    $(".viewSignout").css("width", "248px");
                    $(".triangle").css("margin-left", "84%");
                }

                if ($(window).width() <= 597) {
                    $(".headerCustom").css("width", "305px");
                }

            });
            if ($(window).width() <= 597) {
                $(".headerCustom").css("width", "305px");
            }

            if ($(window).width() <= 596) {
                $(".searchKapal").css("top", "63px");
                $(".user-label").css("width", "80%");
                //$(".olControlLayerSwitcher").css("top","120px");

                $(".headerCustom").css("left", "10px");
                $(".headerCustom").css("right", "");
                $(".headerCustom").removeProp('right');

                $(".viewSignout").css("left", "10px");
                $(".viewSignout").css("right", "");
                $(".viewSignout").removeProp('right');

                $(".viewSignout").css("width", "305px");
                $(".triangle").css("margin-left", "84%");
            } else {
                $(".searchKapal").css("top", "10px");
                $(".user-label").css("width", "");

                $(".headerCustom").css("left", "");
                $(".headerCustom").css("right", "10px");
                $(".headerCustom").removeProp('left');

                $(".viewSignout").css("right", "10px");
                $(".viewSignout").css("left", "");
                $(".viewSignout").removeProp('left');

                $(".viewSignout").css("width", "248px");
                $(".triangle").css("margin-left", "84%");
            }

        });
    </script>
    <script src="/resource/dist/Scripts/MobTMS.js" type="text/javascript"></script>
    <script src="/resource/dist/Scripts/MobGoogle.data.js" type="text/javascript"></script>
    <script src="/resource/dist/Scripts/MobGoogle.tools.js" type="text/javascript"></script>
</body>
</html>