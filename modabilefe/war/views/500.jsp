<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">Internal Error</jsp:attribute>
	<jsp:attribute name="header"></jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
 		${text}
 		 	
 		<h1>Internal Error</h1>
 		${ex}
		</div>
 	</jsp:body>
</t:app>
