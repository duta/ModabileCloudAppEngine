<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">AIS Datasource</jsp:attribute>
	<jsp:attribute name="header"></jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
  	    <div class="ui middle aligned stackable  container">
          <br>
  	      <div class="row">
  	   	  <h2 class="ui dividing header">AIS Datasource</h2>
        		<div class="ui form">
			    	<div class="fields">
						<div class="inline twelve wide field"></div>
						<c:if test="${isADMIN}">
						<div class="six wide field">
							<a class="ui green right floated button" id="add"><i
										class="plus icon"></i> Tambah</a>
						</div>
						</c:if>
			  		</div>
  	   	  <table class="ui celled padded table">
   			  <thead>
   			    <th>#</th>
   			    <th>URL</th>
   			    <th>Vendor Name</th>
   			    <th>URL Username</th>
   			    <th colspan="2"></th>
   			  </thead>
   			  <tbody>
   			  	<c:forEach items="${aisdatasourcelist}" var="ads">
    			    <tr>
  			    			<td class="single line">${nmr.index+1}</td>
  			      			<td>${ads.ad_url}</td>
  			      			<td>${ads.ad_vendorname}</td>
  			      			<td class="center aligned single line">
								${ads.ad_urlusername}
  			      			</td>
  			      			<td><a class="ui blue button" href="datasource/ubah?id=${ads.ad_id}">Ubah</a></td>
  			      			<td><a class="ui red button" href="javascript:doDelete(${ads.ad_id})" >Hapus</a></td>
  			    		</tr>
   			    </c:forEach>
   			  </tbody>
   			</table>
			  		</div>
			  		
			  		
	<c:if test="${isADMIN}">
	<div class="ui small modal add">
		<i class="close icon"></i>
		<div class="header">
			<i class="ship icon"></i>
		   	Form Tambah Datasource
	  	</div>
	  	<form class="ui form content" action="" method="POST">
			<input type="hidden" name="action" value="insert">
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL</label>
			        	<input type="text" name="url" placeholder="URL">
			    	</div>		      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>Vendor Name</label>
			        	<input type="text" name="vendor" placeholder="Nama Vendor">
			    	</div>	      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL User Name</label>
			        	<input type="text" name="name" placeholder="Nama User">
			    	</div>	      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL Password</label>
			        	<input type="password" name="password" placeholder="Password User">
			    	</div>	      
				</div>
		  	<div class="fields">
		  		<button type="submit" class="ui positive labeled icon approve button"> 
		      		Simpan
		      		<i class="save icon"></i>
		    	</button>
		    	<button class="ui red deny right labeled icon button">
		      		Batal
		      		<i class="close icon"></i>
		    	</button>
			</div>
		</form>
	</div>
	
	
	<script>
	 	$(document).ready(function(){
	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});
	 	});

	 	function doDelete(ud){
	 		if (confirm('Yakin ingin menghapus?')){
	 	        $.ajax({
	 	            type: "POST",
	 	            url: '/datasource',
	 	            data: { action: "delete", id: ud },
	 	            success: function (data) {
	 	                window.location = "/datasource";
	 	            }
	 	        });
	 		}
	 	}
	</script>
	</c:if>
          <br>
  		  </div>
  	  </div>
  	</div>
 	</jsp:body>

</t:app>

