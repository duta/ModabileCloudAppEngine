<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">AIS Datasource</jsp:attribute>
	<jsp:attribute name="header"></jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
  	    <div class="ui middle aligned stackable  container">
	<div class="ui small add">
		<div class="header">
			<i class="ship icon"></i>
		   	Form Ubah Datasource
	  	</div>
	  	<form class="ui form content" action="/datasource" method="POST">
			<input type="hidden" name="action" value="update">
			<input type="hidden" name="id" value="${ads.ad_id}">
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL</label>
			        	<input type="text" name="url" placeholder="URL" value="${ads.ad_url}">
			    	</div>		      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>Vendor Name</label>
			        	<input type="text" name="vendor" placeholder="Nama Vendor" value="${ads.ad_vendorname}">
			    	</div>	      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL User Name</label>
			        	<input type="text" name="name" placeholder="Nama User" value="${ads.ad_urlusername}">
			    	</div>	      
				</div>
				<div class="fields">
					<div class="sixteen wide field">
						<label>URL Password</label>
			        	<input type="password" name="password" placeholder="Password User" value="${ads.ad_urlpassword}">
			    	</div>	      
				</div>
		  		<button type="submit" class="ui positive labeled icon approve button"> 
		      		Simpan
		      		<i class="save icon"></i>
		    	</button>
		    	<button class="ui red deny right labeled icon button">
		      		Batal
		      		<i class="close icon"></i>
		    	</button>
          	</form>
        </div>
	  	</div>
    </div>
    </jsp:body>
</t:app>    