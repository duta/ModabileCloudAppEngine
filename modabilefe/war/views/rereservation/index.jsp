<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Re Reservation | DMC</jsp:attribute>

 	<jsp:attribute name="header">
		<link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/dataTables.semanticui.min.css">

 		<script src="/resource/dist/asset/semantic/jquery.dataTables.min.js"></script>
 		<script src="/resource/dist/asset/semantic/js/dataTables.semanticui.min.js"></script>
 	</jsp:attribute>

 	<jsp:body>
 	<div class="ui vertical segment">
	    <div class="ui middle aligned stackable  container">
	      <div class="row"><br/>
  			<h2 class="ui dividing header">Re Reservasi</h2><br/><br/>
		  	<div class="ui form">
			    <div class="fields">
			      <div class="inline twelve wide field">
			      	<label>Nama Perusahaan : </label>
			        <input type="text" class="six wide field" id="NamaPerusahaan" placeholder="Nama Perusahaan">
			      </div>
			      <div class="six wide field">
		     		 <button class="ui blue right floated button" id="add"><i class="plus icon"></i>Tambah Kapal&nbsp;&nbsp;&nbsp;&nbsp;<i class="ship icon"></i> </button>
			      </div>
			  </div>
		  	</div> <br/><br/>
		  <table id="example" class="ui selectable striped  blue celled table"  style="cellspacing:0;width:100%;">
	        <thead>
	            <tr>
	                <th>No.</th>
	                <th>MMSI</th>
	                <th>Nama Kapal</th>
	                <th class="center aligned column">Action</th>
	            </tr>
	        </thead>
		  </table>
	     </div>
	    </div>
	  </div><br/><br/>
	  <div class="ui vertical segment">
	    <div class="ui middle aligned stackable grid  container">
	      <div class="row">
	        <div class="center aligned column">
	          <button class="ui green huge button" id="PesanKapal"><i class="cart icon"></i>Pesan Sekarang</button>
	        </div>
	      </div>
	    </div><br/><br/>
	  </div><br/><br/>

	  <div class="ui small modal add">
	 	 <i class="close icon"></i>
	  <div class="header">
		  <i class="ship icon"></i>
		    Form Tambah Kapal
	  </div>
	  <div class="ui form content">
		 <div class="fields">
			<div class="eight wide field">
				<label>MMSI</label>
		        <input type="text" id="MMSI" placeholder="MMSI">
		    </div>
			<div class="eight wide field">
			   	<label>Nama Kapal</label>
			    <input type="text" id="NamaKapal" placeholder="Nama Kapal">
			</div>
	  	</div>
	  </div>
	  <div class="actions">
	  	<button class="ui positive  labeled icon approve  button" id="addrow">
	      Simpan
	      <i class="save icon"></i>
	    </button>
	     <button class="ui yellow reset button">
	      Reset
	      <i class="refresh icon"></i>
	    </button>
	    <button class="ui red deny right labeled icon  button">
	      Batal
	      <i class="close icon"></i>
	    </button>
	  </div>
	</div>

	<script>
	 	$(document).ready(function(){
	 		// Declare Datatable
	 		var t = $("#example").DataTable( {
	 	        "columnDefs": [ {
	 	            "searchable": false,
	 	            "orderable": false,
	 	            "targets": 0
	 	        } ],
	 	        "order": [[ 1, 'asc' ]]
	 	    });
	 		 t.on( 'order.dt search.dt', function () {
	 	        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	 	            cell.innerHTML = i+1;
	 	        } );
	 	    } ).draw();

	 		//Add Data Kapal Dari modal
	 		var counter = 0;
	 	    $("#addrow").on("click", function () {
	 	        t.row.add( [
	 	            '',
	 	            $("#MMSI").val(),
	 	            $("#NamaKapal").val(),
	 	            '<div class="center aligned column"><div class="ui mini buttons"><a href="" class="ui yellow button"><i class="edit icon"></i>Ubah</a><div class="or"></div><button onclick="hapusFunction(id='+counter+')" class="ui red button"><i class="trash icon"></i>Hapus</button></div></div>',
	 	        ] ).draw( false );
	 	        counter ++;
	 	    } );

	 	    // reset Modal
	 	   $(".reset").on("click",function(){
	 		  $("#MMSI").val('');
	          $("#NamaKapal").val('');
	 	   });
	 		// Show Modal
	 		$("#add").on("click",function(){
	 			$('.ui.small.modal.add')
		 		  .modal('show')
		 		;
	 		});

	 	    // Pesan Kapal
	 	    $("#PesanKapal").on("click",function(){
	 	    	if($("#NamaPerusahaan").val() == ''){
		 			   alert('Mohon isikan nama Perusahaan sebelum memesan paket!');
		 	    	}else{

			 	    		alert('Memesan Paket...');
			 	    		window.location = "/belipaket";
		 	    	}
		 	});

	 	});
	 	//hapus row
	 	function hapusFunction(id) {
	 	 alert(id);
		 $("#example").DataTable().row(id).remove().draw();
		}
	</script>
 	</jsp:body>

 </t:app>
