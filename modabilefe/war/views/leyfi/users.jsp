<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Daftar User</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Daftar User</h2><br/>
				<div class="ui form">
					<form action="/leyfi/users/add" method="post">
					<table class="ui celled padded table">
						<thead>
							<tr>
								<th><input type="text" name="name" placeholder="Nama user"></th>
								<th><input type="text" name="tags" placeholder="Daftar role" id="tags"></th>
								<th><input type="submit" value="Tambah User Baru"></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${allValue}" var="os" varStatus="nmr">
								<tr>
									<td>
										${nmr.index+1}. ${os[1]}
									</td>
									<td>
										${os[2]}
									</td>
									<td>
										<a href="/leyfi/users/${os[0]}">Update User</a>
									</td>
									<td>
										<a onclick="javascript:dele(${os[0]})" style="cursor: pointer;">Hapus User</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					</form>
					<br>
				</div>
			</div>
		</div>
  	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
  	<link rel="stylesheet" href="/resource/dist/asset/jquery.tagit.css">
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script>
	<script src="/resource/dist/asset/tag-it.min.js"></script>
 	<script type="text/javascript">
 	$(document).ready(function(){
 	    $('.table').DataTable({
 	        "ordering": false
 	    });
 	   $("#tags").tagit({
 		  allowSpaces :true,
 		 placeholderText : "Daftar role",
 		  autocomplete:{source:"/leyfi/roles/all"}
 		});
 	});
 	function dele(ud){
 		if (confirm('Hapus User?')){
 	        $.ajax({
 	            type: "POST",
 	            url: '/leyfi/users/delete',
 	            data: { something: ud },
 	            success: function (data) {
 	                window.location = "/leyfi/users";
 	            }
 	        });
 		}
 	}
 	</script>
 	</jsp:body>
 </t:app>
