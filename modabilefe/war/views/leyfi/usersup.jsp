<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">Update User | DMC</jsp:attribute>
	<jsp:attribute name="header"></jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Update ${userName}</h2>
					<br />
					<form action="" method="post">
					<table class="ui celled padded table">
						<thead>
							<tr>
								<th><input type="text" name="name" value="${userName}"></th>
								<th><input type="text" name="tags" id="tags"></th>
								<th><input type="submit" value="Simpan User"></th>
								<th></th>
							</tr>
						</thead>
					</table>
					</form>
					<br>
				</div>
			</div>
		</div>
  	<link rel="stylesheet" href="/resource/dist/asset/jquery.tagit.css">
  	<link rel="stylesheet" href="/resource/dist/asset/jquery-ui.css">
	<script src="/resource/dist/asset/jquery-ui.min.js"></script>
	<script src="/resource/dist/asset/tag-it.min.js"></script>
 	<script type="text/javascript">
			$(document).ready(function() {
				$("#tags").tagit({
					allowSpaces : true,
					autocomplete : {
						source : "/leyfi/roles/all"
					}
				});
				<c:forEach items="${allRoles}" var="os" varStatus="nmr">
				$("#tags").tagit('createTag', {label:"${os.role_name}",value:"${os.role_id}"});
				</c:forEach>
			});
		</script>
 	</jsp:body>
</t:app>
