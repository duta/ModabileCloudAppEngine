<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/"%>
<t:app>
	<jsp:attribute name="title">Daftar Element</jsp:attribute>
	<jsp:attribute name="header"></jsp:attribute>
	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Daftar Element</h2>
					<br />
				<div class="ui form">
					<form action="/leyfi/elements/add" method="post">
					<table class="ui celled padded table">
						<thead>
							<tr>
								<th><input type="text" name="name" placeholder="Regex untuk element"></th>
								<th><input type="submit" value="Tambah Element"></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${allEles}" var="os" varStatus="nmr">
								<tr>
									<td>
										${nmr.index+1}. ${os.element_type}
									</td>
									<td>
										<a onclick="javascript:dele(${os.element_id})" style="cursor: pointer;">Hapus Element</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					</form>
					<br>
				</div>
			</div>
		</div>
  	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
	<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
 	<script type="text/javascript">
 	$(document).ready(function(){
 	    $('.table').DataTable({
 	        "ordering": false
 	    });
 	});
 	function dele(ud){
 		if (confirm('Hapus Element?')){
 	        $.ajax({
 	            type: "POST",
 	            url: '/leyfi/elements/delete',
 	            data: { something: ud },
 	            success: function (data) {
 	                window.location = "/leyfi/elements";
 	            }
 	        });
 		}
 	}
 	</script>
	
	
	
	</jsp:body>
</t:app>
