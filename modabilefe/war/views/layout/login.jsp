<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>Login</title>

  <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="/asset/img/fav-icon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/asset/img/fav-icon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/asset/img/fav-icon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/asset/img/fav-icon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/asset/img/fav-icon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/asset/img/fav-icon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/asset/img/fav-icon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/asset/img/fav-icon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/asset/img/fav-icon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/asset/img/fav-icon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/asset/img/fav-icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/asset/img/fav-icon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/asset/img/fav-icon/favicon-16x16.png">
        
  <!-- Site Properties -->
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.css">
  <link rel="stylesheet" type="text/css" href="/resource/dist/stylesheets/main.css">
  <link rel="stylesheet" href="/resource/dist/font-awesome-4.5.0/css/font-awesome.min.css">

  <script src="/resource/dist/asset/jquery-1.12.0.min.js"></script>
  <script src="/resource/dist/asset/semantic/semantic.min.js"></script>
  <script src="/resource/dist/stylesheets/main.js"></script>
</head>
<body >
<br>
<br>
<div class="ui vertical segment">
  <div class="ui middle aligned stackable grid container">
    <div class="row">
      <div class="center aligned column">
        <img src="/resource/dist/image/logo.png" >
      </div>
    </div>
  </div>
  <br />
  <br />
</div>
<br />
<br />
<div class="ui vertical">
  	<div class="ui middle aligned stackable grid container">
		<div class="row">
			<div class="five wide column"></div>
			<div class="six wide column">
			  	<label>Dengan menekan Login, saya mengkonfirmasi telah menyetujui <a href="/terms/index" target='_blank'>Syarat dan Ketentuan</a>, serta <a href="/terms/privacy" target='_blank'>Kebijakan Privasi</a>.</label>
			</div>
			<div class="five wide column"></div>
		</div>
		<div class="row">
			<div class="five wide column"></div>
			<div class="six wide column center aligned">
				<a href="${loginUrl}">
		        	<button class="ui google plus button">
				    	<i class="google plus icon"></i>
				    	Login with Google
					</button>
				</a>
			</div>
			<div class="five wide column"></div>
		</div>
  	</div>
</div>

</body>
</html>
