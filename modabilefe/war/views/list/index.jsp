<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Daftar Kapal</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
 	<div class="ui vertical segment">
		<div class="ui middle aligned stackable  container">
			<div class="row">
				<h2 class="ui dividing header">Daftar Kapal</h2><br/>
				<div class="ui form">
					<div class="fields">
						<div class="inline twelve wide field"></div>
						<div class="six wide field">
							<c:if test="${isUSERa}">
								<a href="reservation" class="ui green  right floated button"><i class="cart icon"></i> Pesan Sekarang</a>
							</c:if>
						</div>
					</div>
					<table class="ui celled padded table">
						<thead>
							<tr>
								<th class="center aligned" style="width:60px;">No.</th>
								<th>MMSI</th>
								<th>Nama Kapal</th>
								<th>IMO</th>
								<th>Callsign</th>
								<th>Timestamp</th>
								<th>Lokasi</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${osList}" var="os" varStatus="nmr">
								<tr>
									<td class="center aligned">
										${nmr.index+1}.
									</td>
									<td>
										<h2 class="ui center aligned header">${os.key.ship_mmsi}</h2>
									</td>
									<td class="single line">
										${os.key.ship_name}
									</td>
									<td class="single line">
										${os.key.ship_imo}
									</td>
									<td class="single line">
										${os.key.ship_callsign}
									</td>
									<td class="single line">
										${os.value.si_position_date}
									</td>
									<td class="single line">
										Lat ${os.value.si_latitude} Lon ${os.value.si_longitude}
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class="ui icon message">
						<i class="info icon"></i>
						<div class="content">
 							<div class="header">
								Periode ${diff} Bulan
							</div>
							<p>${contractStartDate}  - ${contractEndDate}</p>
						</div>
					</div>
					<br>
				</div>
			</div>
		</div>
 	</jsp:body>
 </t:app>
