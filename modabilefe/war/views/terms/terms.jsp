<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Syarat</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
	<h2 class="ui dividing header">Syarat dan Ketentuan :</h2>	
	<ul class="ui">
	  <li>Perkenalan
	  	<ul>
	  		<li>Modabile menyediakan informasi tentang posisi geografis kapal terkini beserta informasi lainnya jika tersedia.</li>
	  	</ul>
	  </li>
	  <li>Pendaftaran
	  	<ul>
	  		<li>Pengguna website terdaftar dapat menggunakan beberapa servis extra dan servis data, sesuai produk yang dipilih.</li>
	  		<li>Pendaftaran di Modabile menggunakan akun Google. Jika belum mempunyai akun Google, anda dapat mendaftar lebih dulu, dengan langkah-langkah yang ada di halaman Google Support <a target="_blank" href="https://support.google.com/accounts/?hl=en#topic=3382296">ini</a>.</li>
	  		<li>Syarat dan ketentuan menggunakan layanan akun Google ada di halaman <a target="_blank" href="https://www.google.com/policies/terms/">ini</a>.</li>
	  		<li>Segala informasi yang anda sediakan di pendaftaran adalah milik eksklusif anda.</li>
	  	</ul>
	  </li>
	  <li>Perlindungan Informasi Rahasia
	    <ul>
	      <li>Setiap pengguna bertanggung jawab atas semua tindakan dan transaksi yang dilakukan melalui akunnya. Anda bertanggung jawab untuk menjaga kerahasiaan password Anda dan Anda harus memastikan bahwa Anda keluar dengan baik dari akun di akhir setiap sesi.</li>
	      <li>Dalam kasus Anda melihat penggunaan yang tidak sah dari akun Anda atau semua bentuk pelanggaran keamanan (nyata atau potensial), Anda harus segera memberitahu administrator Website ini. Kami tidak bertanggung jawab atas kerugian atau kerusakan yang timbul dari kegagalan Anda untuk mematuhi aturan ini. Kami berhak untuk menghapus akun, jika terbukti melanggar salah satu Ketentuan ini.</li>
	    </ul>
	  </li>
	  <li>Data Pribadi
	  	<ul>
	  		<li>Kami menyimpan file dan data pribadi pengguna terdaftar sebagaimana dijelaskan dalam Kebijakan Privasi. Tujuan dari penyimpanan ini juga dijelaskan dalam Kebijakan Privasi. Dengan menyatakan persetujuan terhadap Syarat dan Ketentuan dan Kebijakan Privasi ini, anda mengijinkan Modabile memproses data pribadi anda.</li>
	  	</ul>
	  </li>
	  <li>Batasan
	  	<ul>
	  		<li>Modabile tidak menjamin kebenaran, keabsahan atau ketepatan ilmiah dari informasi yang terkandung di Website. Baik itu dalam bentuk gambar, video, teks, maupun grafis.</li>
	  		<li>Informasi posisi dan identitas kapal berasal langsung dari AIS (Automatic Identification System) kapal, yang mengirimkan informasi ini melalui frekuensi radio publik. </li>
	  		<li>Data yang sudah dikoleksi dan dipublish ini mungkin mengandung error karena keterbatasan komunikasi radio (coverage yang terbatas, interferensi, kondisi cuaca, dan lain-lain), kesalahan konfigurasi AIS oleh kru kapal, kesalahan posisi yang diterima GPS kapal dank arena factor-faktor lain diluar kendali provider.</li>
	  		<li>Data posisi kapal mungkin sampai satu jam lebih lama atau tidak lengkap terkirim.</li>
	  		<li>Data yang tersedia hanya bersifat informasi pendukung saja dan tidak digunakan untuk keselamatan navigasi.</li>
	  		<li>Konsekuensi dari hal tersebut dalam poin diatas, kami sebagai administrator Modabile tidak dapat memberikan jaminan maupun tanggung jawab atas kebenaran, validitas, dan akurasi informasi. Tujuan penggunaan servis Modabile hanya untuk tujuan menampilkan informasi saja.</li>
	  		<li>Layanan peta BMKG disediakan oleh BMKG Indonesia.</li>
	  		<li>Layanan peta nautical chart disediakan oleh Transas WMS. Syarat dan ketentuan ada di halaman <a href="http://wms.transas.com/terms-use">ini</a>.</li>
	  		<li>Karena interaksi ini melalui internet, dalam keadaan apapun, termasuk dalam keadaan lalai, kami tidak bertanggung jawab atas setiap jenis kerusakan yang mungkin anda derita dari penggunaan atau kunjungan dan isi dari website. Anda mengunjungi konten dan layanan atas inisiatif dan resiko anda sendiri.</li>
	  	</ul>
	  </li>
	  <li>Ketiadaan Garansi
	  	<ul>
	  		<li>Konten, layanan, peta Google, peta BMKG, peta Transas WMS, dan software Modabile disediakan “sebagaimana adanya”.</li>
	  		<li>Modabile tidak memberikan jaminan bahwa halaman, layanan, fungsi dan isi website akan diberikan tanpa gangguan atau tanpa kesalahan atau bahwa kesalahan akan diperbaiki.</li>
	  		<li>Modabile tidak menjamin bahwa website atau server penyedia tidak mengandung virus atau komponen berbahaya lainnya, meskipun kami melakukan segala upaya untuk memastikan ketiadaan virus dan komponen berbahaya.</li>
	  		<li>Kami sebagai provider, atau orang-orang yang terkait, atau administrator Modabile tidak akan bertanggung jawab kepada anda atau orang lain atau badan hukum atas kerugian atau kehilangan pendapatan langsung maupun tidak langsung yang disebabkan oleh penggunaan website.</li>
	  	</ul>
	  </li>
	  <li>Tautan dan Layanan Pihak Ketiga
	  	<ul>
	  		<li>Modabile menggunakan atau mempunyai link dan layanan yang dikelola oleh pihak ketiga seperti peta. </li>
	  		<li>Modabile tidak bertanggung jawab atas konten, maupun segala tipe kerusakan dan kerugian finansial yang lain untuk pengguna yang mengikuti link tersebut.</li>
	  		<li>Modabile tidak memberi jaminan link dan layanan pihak ketiga tersebut akan selalu konstan aktif atau hanya sementara.</li>
	  		<li>Modabile berhak untuk menghapus atau menambahkan link tanpa peringatan apapun kepada pihak manapun untuk mengubah isi dari situs web ini setiap saat.</li>
	  		<li>Dengan menggunakan Modabile, anda memahami dan menerima fakta bahwa kami tidak bertanggung jawab untuk konten link dan layanan pihak ketiga. Dan untuk produk atau jasa yang ditawarkan disana, dan untuk iklan yang terkandung didalamnya.</li>
	  	</ul>
	  </li>
	  <li>Hukum dan Pengadilan yang Berlaku
	  	<ul>
	  		<li>Syarat dan ketentuan ini, serta modifikasi atau perubahannya tunduk pada hukum Indonesia. Semua persyaratan tersebut diatas adalah sangat penting.</li>
	  		<li>Jika salah satu syarat tersebut ada dugaan melawan hukum, maka akan dikoreksi dan dihapus tanpa mempengaruhi keabsahan persyaratan lainnya.</li>
	  		<li>Jika ada sengketa atau perbedaan timbul sehubungan dengan jasa Modabile, harus diselesaikan di pengadilan Indonesia yang memiliki yurisdiksi eksklusif.</li>
	  		<li>Jika anda tidak setuju dengan persyaratan dan ketentuan tersebut, anda tidak perlu menggunakan jasa website ini. </li>
	  	</ul>
	  </li>
	  <li>Perubahan dalam Syarat dan Ketentuan
	  	<ul>
	  		<li>Kami berhak mengubah atau memodifikasi Syarat dan Ketentuan ini tanpa pemberitahuan sebelumnya. Silahkan kembali ke halaman ini secara berkala untuk melihat versi terbaru dari Syarat dan Ketentuan ini. Dengan melanjutkan akses dan penggunaan Situs, Isi dan Layanan, Anda setuju untuk terikat dengan Persyaratan diperbarui.</li>
	  		<li>Jika ada fitur baru untuk menambah atau meningkatkan kualitas layanan, termasuk rilis perlengkapan baru dan sumber daya lainnya, maka harus tunduk pada Syarat dan Ketentuan penggunaan layanan ini. </li>
	  	</ul>
	  </li>
</ul>
	</jsp:body>
 
 </t:app>