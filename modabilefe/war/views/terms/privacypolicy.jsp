<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
 <t:app>
 	<jsp:attribute name="title">Kebijakan Privasi</jsp:attribute>
 	<jsp:attribute name="header"></jsp:attribute>
 	<jsp:body>
	<h2 class="ui dividing header">Kebijakan Privasi :</h2>	
	<ul class="ui">
	  <li>Pengumpulan Informasi
	    <ul>
	      <li>Misi dari Modabile adalah menyediakan layanan ke professional di dunia maritim, khususnya pemilik kapal atau pihak yang berhubungan dengan transportasi laut.</li>
	      <li>Untuk mendukung misi ini lebih baik, dan demi terselenggaranya software yang lebih stabil, kami tidak membuka semua data kapal ke publik. Data hanya tersedia untuk anda yang mendaftar.</li>
	      <li>Dengan mendaftar ke Modabile, anda secara sukarela menyediakan Modabile beberapa informasi, termasuk data personal anda.</li>
	      <li>Jika anda memiliki keraguan untuk memberikan informasi kepada kami, anda tidak harus mendaftar ke layanan Modabile; dan jika anda sudah mendaftar, anda harus menutup akun anda.</li>
	    </ul>
	  </li>
	  <li>Informasi Login
	    <ul>
	      <li>Untuk mendaftar di Modabile cukup menggunakan akun Google. Kami tidak menyimpan password anda. Jika lupa password, anda bisa melakukan langkah-langkah di halaman Google Support <a href="https://support.google.com/accounts/answer/41078?hl=en">ini</a>.</li>
	      <li>Informasi lain sebagai syarat untuk menjadi anggota adalah :
	    <ul>
	      <li>Nama</li>
	      <li>Alamat email</li>
	      <li>Negara</li>
	      <li>Nama organisasi atau perusahaan</li>
		</ul>
	      </li>
		</ul>
	  </li>
	  <li>Customer Service dan Sales
	    <ul>
	      <li>Kami mengumpulkan informasi ketika anda berinteraksi dengan tim Penjualan dan tim Customer Service kami dengan tujuan kemudahan mengkategorikan dan membalas permintaan anda.</li>
	    </ul>
	  </li>
	  <li>Cookies
	    <ul>
	      <li>Akun Google menggunakan cookies untuk memudahkan akses login ke Modabile. Segera logout jika sudah selesai menggunakan Modabile. </li>
	      <li>Beberapa setting dalam Modabile memerlukan penyimpanan cookies untuk kenyamanan penggunaan software.</li>
	      <li>Menghapus cookies dapat dilakukan di setting browser.</li>
	    </ul>
	  </li>
	  <li>Komunikasi Email
	    <ul>
	      <li>Ketika anda baru mendaftar Modabile dan mendaftarkan kapal pilihan anda, kami akan menginfokan ketersediaan data kapal kepada anda.</li>
	      <li>Jika masa kontrak akan berakhir kami akan melakukan notifikasi dan mengkomunikasikan pilihan yang akan anda ambil setelah kontrak berakhir.</li>
	      <li>Alamat email anda tidak akan kami bagi ke pihak lain.</li>
	    </ul>
	  </li>
	  <li>Keamanan
	    <ul>
	      <li>Kami berkomitmen untuk melindungi data anda, namun transmisi data internet tidak bisa dijamin 100% aman walaupun kami sudah menerapkan SSL.</li>
	    </ul></li>
	  <li>Perubahan terhadap Kebijakan Privasi
	    <ul>
	      <li>Perubahan akan dilakukan jika diperlukan dan kami akan selalu berkomitmen melindungi data privasi anda.</li>
	    </ul>
	  </li>
	</ul>
 	</jsp:body>
 
 </t:app>