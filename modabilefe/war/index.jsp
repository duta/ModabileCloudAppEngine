<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Hello App Engine</title>
  </head>

  <body>
    <h1>Hello App Engine!</h1>
	<p>
		name server is ${nameserver}
	</p>
	<p>
		user email is ${useremail}
	</p>
    <table>
      <tr>
        <td colspan="2" style="font-weight:bold;">Available Servlets:</td>        
      </tr>
      <tr>
        <td><a href="modabilefe">Modabilefe</a></td>
        <td><a href="other">Other</a></td>
 		<td><a href="tryshowupsert">TryShowUpsert</a></td>       
      </tr>
    </table>
  </body>
</html>
