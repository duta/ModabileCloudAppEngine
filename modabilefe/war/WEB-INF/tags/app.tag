
<%@tag import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="title" fragment="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<% 
com.google.appengine.api.users.User user = (com.google.appengine.api.users.User) request.getAttribute("user");
java.util.ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles> userRoles = new java.util.ArrayList<com.ptdmc.modabilefe.entity.leyfi.Roles>();
if (user != null) {
int userid =com.ptdmc.modabilefe.CallFactory.getInstance(request).getFactory().GetUsers().GetUserIDByUserName(user.getEmail());
userRoles = com.ptdmc.modabilefe.CallFactory.getInstance(request).getFactory().GetUsers().GetRolesByUserID(userid);
String useremail = user.getEmail();
try{request.setAttribute("useremail", useremail.substring(0, useremail.indexOf('@'))+ "&#8203;@" + useremail.substring(useremail.indexOf('@')+1));
} finally {}
}
%>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title><jsp:invoke fragment="title"/></title>


  <link rel="shortcut icon" href="/asset/img/logo.png" />
  <!-- Site Properties -->
  <style>
    @media (max-width: 640px){
        .item {padding-left:0px !important;padding-right:0px !important;}
    }
    @media (max-width: 480px){
        .avatar {display:none !important;}
    }
    @media (min-width: 640px){
        .logo {width:200px !important;}
    }
  </style>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.1.8/semantic.min.css">
  <!--link rel="stylesheet" type="text/css" href="/resource/dist/asset/semantic/semantic.min.css"-->
  <link rel="stylesheet" type="text/css" href="/resource/dist/stylesheets/main.css">
  <link rel="stylesheet" href="/resource/dist/font-awesome-4.5.0/css/font-awesome.min.css">
  
  <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="/asset/img/fav-icon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/asset/img/fav-icon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/asset/img/fav-icon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/asset/img/fav-icon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/asset/img/fav-icon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/asset/img/fav-icon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/asset/img/fav-icon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/asset/img/fav-icon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/asset/img/fav-icon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/asset/img/fav-icon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/asset/img/fav-icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/asset/img/fav-icon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/asset/img/fav-icon/favicon-16x16.png">

  <script src="/resource/dist/asset/jquery-1.12.0.min.js"></script>
  <script src="/resource/dist/asset/semantic/semantic.min.js"></script>
  <script src="/resource/dist/stylesheets/main.js"></script>
  
  <script src="/resource/dist/node_modules/moment/moment.js"></script>
  <script src="/resource/dist/node_modules/moment-timezone/builds/moment-timezone-with-data.js"></script>
  <script src="/resource/dist/node_modules/moment-timezone/builds/moment-timezone-with-data-2010-2020.js"></script>
</head>
  <body >

  	<div id="pageheader">
     	<jsp:invoke fragment="header"/>
    </div>
    <div id="body" >
		<!-- Sidebar Menu -->
		<div class="ui vertical left inverted sidebar  menu  " id="toc" >
			<div class="item" style="font-size:20px;height:80px;">
			  <a class="ui logo icon image" href="/">
				    <img  src="/asset/img/logo.png">
			  </a>
			</div>
			<%
			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : userRoles) {
				if (role.role_name.equals("superadmin (ADMIN)")) request.setAttribute("isADMIN", true);
			}
			%>
			<c:if test="${isADMIN}">
			<a href="/" class="active item"><i class="home icon"></i> Dashboard</a>
			<a href="/map" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta</a>
			<a href="/mapBmkg" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta BMKG</a>
			<a href="/mapTMS" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta Nautical Transas</a>
			<a href="/mapRecorded" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta Recorded</a>
<!-- 			<a href="/configuration" class="item"><i class="fa fa-cog icon"></i> Konfigurasi</a> -->
			<a href="/leyfi/users" class="item"><i class="home icon"></i> Leyfi User</a>
			<a href="/leyfi/roles" class="item"><i class="home icon"></i> Leyfi Role</a>
			<a href="/leyfi/elements" class="item"><i class="home icon"></i> Leyfi Element</a>
			<a href="/datasource" class="item"><i class="fa fa-database icon"></i> AIS Datasource</a>
			<a href="/adduser" class="item"><i class="fa fa-database icon"></i> Tambah User</a>
			<a href="/superadmin" class="item"><i class="fa fa-database icon"></i> Daftar Superadmin</a>
			<a href="/shipMislocation" class="item"><i class="fa fa-list-alt icon"></i> Kapal Salah Lokasi</a>
			<a href="/shipInfo" class="item"><i class="fa fa-list-alt icon"></i> Master Kapal</a>
			<a href="/adminOrder?action=list" class="item"><i class="fa fa-database icon"></i> Daftar Pemesanan</a>
			<a href="/customer" class="item"><i class="fa fa-users icon"></i> Pelanggan</a>
			<a href="/product" class="item"><i class="file text outline icon"></i> Produk</a>
			<a href="/configuration" class="item"><i class="fa fa-database icon"></i> Konfigurasi</a>
			</c:if>
			<%
			for (com.ptdmc.modabilefe.entity.leyfi.Roles role : userRoles) {
				if (role.role_name.equals("customer admin (USER)")) {
					request.setAttribute("isUSER", true);
					request.setAttribute("isUSERa", true);
				}
				if (role.role_name.equals("customer member (USER)")) request.setAttribute("isUSER", true);
			}
			%>
			<c:if test="${isUSERa}">
			<a href="/customermember" class="item"><i class="fa fa-database icon"></i> Daftar Customer Member</a>
			<a href="/product" class="item"><i class="file text outline icon"></i> Produk</a>
			<a href="/profile" class="item"><i class="fa fa-user icon"></i> Profil</a>
			<a href="/shipMislocation" class="item"><i class="fa fa-list-alt icon"></i> Kapal Salah Lokasi</a>
			<a href="/history" class="item"><i class="fa fa-list-alt icon"></i> Riwayat Pemesanan</a>
			<a href="/pemesananproduk" class="item"><i class="fa fa-database icon"></i> Daftar Pemesanan Produk</a>
			</c:if>
			<c:if test="${isUSER}">
			<a href="/map" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta</a>
			<a href="/mapBmkg" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta BMKG</a>
			<a href="/mapTMS" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta Nautical Transas</a>
			<a href="/mapRecorded" target="_blank" class="item"><i class="fa fa-map-marker icon"></i> Peta Recorded</a>
<!-- 			<a href="/setting" class="item"><i class="fa fa-cogs icon"></i> Konfigurasi Peta</a> -->
			<a href="/list" class="item"><i class="fa fa-ship icon"></i> Daftar Kapal</a>
			<a href="/terms/index" class="item"><i class="file text outline icon"></i> Syarat & Ketentuan</a>
			</c:if>
		</div>


    	<div class="ui inverted vertical masthead center aligned segment" style="height:90px;margin-top:-10px;">
			<div class="ui container">
				<div class="ui large secondary inverted  menu">
				<div class="item">
					    <div class="ui logo shape">
						    <div class="sides">
					    		<div class="active ui side">
					    		    <a href="/home"><img class="ui logo icon image" src="/asset/img/logo.png" ></a>
					    	 	</div>
					   		</div>
					     </div>
					 <a class="toc item" data-transition="scale down"><i class="sidebar icon"></i> Menu</a>
				</div>
				<c:choose>
					<c:when test="${user != null}">
						<div class="right menu inverted">
							<div class="ui icon top right pointing dropdown item" style="width:100%; text-align: right;">
						  		<div class="text">
									${useremail}! &nbsp;
									      <img class="ui avatar image" src="/asset/img/jenny.jpg">
								 </div>
								 <div class="menu">
 							     	<div class="divider"></div>
 									<a  href="${logoutUrl}" class="item">
 								     <i class="privacy icon"></i>Logout
 									</a>
							     </div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="right menu inverted">
						      <div class="ui pointing dropdown  item">
					  				<a href="login" class="text">
								      Log in &nbsp;
								      <i class="lock icon"></i>
								    </a>
						      </div>
							  <!-- div class="ui language floating dropdown link item" id="languages">
							       <i class="world icon"></i>
							       <div class="text">Indonesia </div>
							       <div class="menu transition hidden">
							         <div class="scrolling menu">
									  <div class="item " data-value="id" data-text="Indonesian" data-english="Indonesia"><i class="id flag"></i> Indonesia </div>
									  <div class="item " data-value="en" data-text="English" data-english="English"><i class="us flag"></i> English </div>
									</div>
						      		</div>
							  </div -->
				        </div>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	    <!-- Page Contents -->
	    <div class="pusher">
	    <div class="full height">

		 <!-- Following Menu -->
	    <div class="ui  top fixed hidden menu" style="height:80px;">
			 <div class="ui container">
			      <div class="ui large secondary menu ">
				    <div class="item">
			        	<div class="ui logo shape">
			          		<div class="sides">
			           		 <div class="active ui side">
			             		 <a href="/home"><img class="ui logo icon image" src="/asset/img/logo.png" ></a>
			            	</div>
			          </div>
		        </div>
	      </div>
		<div class="item">
		 <a class="toc item" data-transition="scale down"><i class="sidebar icon"></i> Menu</a>
		</div>
		</div>
		  <c:choose>
			<c:when test="${user != null}">
			<div class="right  menu ">
			     <div class="ui icon top right pointing dropdown item">
		 				<div class="text">
				      ${useremail}! &nbsp;
				      <img class="ui avatar image" src="/asset/img/jenny.jpg">
				    </div>				    
 				    <div class="menu">
 					    <div class="divider"></div>
 					    <a  href="${logoutUrl}" class="item">
 					      <i class="privacy icon"></i>Logout
 					    </a>
					  </div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="right menu">
		      <div class="ui pointing dropdown  item">
  				<a href="login" class="text">
			      Log in &nbsp;
			      <i class="lock icon"></i>
			    </a>
		      </div>
		    </div>
		</c:otherwise>
	</c:choose>
  </div>
</div>
	<jsp:doBody/>

			<!-- div id="pagefooter">
			   <div class="ui inverted vertical footer segment">
			    <div class="ui container">
			      <div class="ui stackable inverted divided equal height stackable grid">
			        <div class="three wide column">
			          <h4 class="ui inverted header">About</h4>
			          <div class="ui inverted link list">
			            <a href="#" class="item">Sitemap</a>
			            <a href="#" class="item">Contact Us</a>
			            <a href="#" class="item">Religious Ceremonies</a>
			            <a href="#" class="item">Gazebo Plans</a>
			          </div>
			        </div>
			        <div class="three wide column">
			          <h4 class="ui inverted header">Services</h4>
			          <div class="ui inverted link list">
			            <a href="#" class="item">Banana Pre-Order</a>
			            <a href="#" class="item">DNA FAQ</a>
			            <a href="#" class="item">How To Access</a>
			            <a href="#" class="item">Favorite X-Men</a>
			          </div>
			        </div>
			      </div>
			    </div>
			  </div -->
			     <jsp:invoke fragment="footer"/>
			 </div>
			</div>
		</div>
	</div>
	</body>
</html>
